String.prototype.trim = function() { 
        return this.replace(/^\s+|\s+$/g,""); 
}

/* Hide navigation menu in reponsiveness after clicking */

$(function(){ 
	
	var $navMain = $(".navbar-collapse");

	$(window).resize(function(){
		if($(window).width() <= 765){
			$navMain.on("click", "a", null, function () {
				if(!$(this).hasClass("dd-menu"))
					$navMain.collapse('hide');
			});
		}
	});
});

/* login validation  */
function inValidation()
    {

        var email = $("#lemail").val();
        var pwd = $("#lpwd").val();
            
            if(email!='' && pwd!='') {
                var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
                if(!filter.test(email)){
                    $('.errMsg p').html('Enter Valid Email Address');
                     }
                 else {

                            $('#lsign').html('Please Wait');
                            var url  = "/pyscripts/logincourse.py";
                            var params = {};
                            params['lemail'] = email;
                            params['lpwd'] = pwd;
                            $.post(url,params, function(data) {
                                if(data.trim() == '0')
                                {
                                    $('.errMsg p').html('Invalid Email or Password');
                                    $('#lsign').html('Sign In');
                                }
                                else
                                {
                                    window.location = data;
                                }
                            });
                                            
                }
            }
            else
            {
                $('.errMsg p').html('Fields cannot be empty');  
            }
    }

/* signup validation */
function signupnext(ref)
    {

        var email = $("#uemail").val();
        var pwd = $("#upassword").val();
        var mob = $('#uphone').val();


        if(email!='' && pwd!='' && mob!='') {
            var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
            if(!filter.test(email)){
                $('.uerrMsg p').html('Enter Valid Email Address');
            }
            else {

                var mfilter = /^\d{10}$/; 
                if (!mfilter.test(mob))
                {
                    $('.uerrMsg p').html('Enter Valid phone number');
                }
                else
                {
                    $('#lsignup').html('Please Wait');
                    var url  = "/pyscripts/signup.py";
                    var params = {};
                    params['e'] = email;
                    params['p'] = pwd;
                    params['m'] = mob;
                    params['r'] = ref;
                    $.post(url,params, function(data) {
                        if(data.trim() == '0')
                        {
                            $('.uerrMsg p').html('email already registered');
                            $('#lsignup').html('Sign Up');
                        }
                        else
                        {
                            var next = gq('next');
                            if (next == "0")
                                window.location = data;
                            else
                                window.location = next;
                        }
                    });

                }

            }
        }
        else
        {
            $('.uerrMsg p').html('Fields cannot be empty');
        }
    }

/* update password */
	function updatepassword(email,e)
	{
    	var newpass =$('#updatepass').val();
    	if(newpass == '')
    	{
    	    $('#updMsg p').html('Please Enter a Password');
	    }
    	else
    	{

            var params = {};
            params['newpass']  = newpass;
            params['email']  = email;
            params['auth']  = e;
            var url = '/pyscripts/updatepassword.py';
            $('#updatesubmit').html('Please Wait..');
            $.post(url,params, function(data) {
                if(data.trim() == '0')
                {
    	            $('#updMsg p').html('Invalid Passowrd/auth code.');
                    $('#updatesubmit').html('Submit');
                }
                else
                {
                    $('#upassform').hide();
    	            $('#updMsg p').html('Password Reset Successfully.');
                }
            });
       }
	}
/*forgetpass toggle */
function toggleForgotPass()
    {
        $('#signin').hide();
        $('.forgotpass-wrap').show();
        $('.forgotpass-wrap .resetemail').show();
        $('.forgotpass-wrap form').show();
        $('.forgotpass-wrap .resetpass').hide();
        location.href="#forgotpass";
    }  
/* reset password */
	function resetPassword()
    {

        var email = $("#remail").val();
        if(email!='')
        {
            var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
            if(!filter.test(email))
            {
                $('.rerrMsg p').html('Enter Valid Email Address');
            }
            else
            {


                var url  = "/pyscripts/forgotpass.py";
                var params = {};
                params['email'] = email;
                $('#lreset').html('Please Wait...');
                $.post(url,params, function(data) {
                    if(data.trim() == '0')
                    {
                        $('.rerrMsg p').html('Email ID not registered.');
                        $('#lreset').html('Submit');
                    }
                    else
                    { 
                        $('.resetemail').hide();
                        $('.resetform').hide();
                        $('.forgotpass-wrap .resetpass').show();
                    }
                });




            }
        }
        else
        {
            $('.rerrMsg p').html('Please Enter your email');
        }

        

    }
/* facebook login */
window.fbAsyncInit = function() {
        FB.init({
            appId      : 432629830121838,
            status     : true, // check login status
            cookie     : true, // enable cookies to allow the server to access the session
            xfbml      : true  // parse XFBML
        });
     };

     function submitData(pid){
                var params = {};
                params['pid']  = pid;
                var url = '/myaccount/share/addprelog.cgi';
                $.post(url,params,function(data) {
                    $('#sharefb').hide();
                    $('#dlshare').html(data);

                });
            }
            function resourcesshare() {
                var shareTitle = "Free Document : Tips and Tricks | Oliveboard"
                var obj = {
                    method: 'feed',
                    name: shareTitle,
                    link: 'http://www.oliveboard.in/resources.php',
                    picture: 'http://www.oliveboard.in/myaccount/images/JustLogo.png',
                    description: "I just downloaded Oliveboard's FREE ebook for my preparation"
                };

                function callback(response) {
                    if(response){
                        submitData(response['post_id'])
                    }
                }
                FB.ui(obj, callback);

            }

            function bsubmitData(pid){
                var params = {};
                params['pid']  = pid;
                params['b']  = 1;
                var url = 'myaccount/share/addprelog.html';
                $.post(url,params,function(data) {
                    $('#sharefb').hide();
                    $('#dlshare').html(data);
                });
            }
            function bresourcesshare() {
                var shareTitle = "Free Document : Banking Terms and Definitions | Oliveboard"
                var obj = {
                    method: 'feed',
                    name: shareTitle,
                    link: 'http://www.oliveboard.in/bresources.php',
                    picture: 'http://www.oliveboard.in/myaccount/images/JustLogo.png',
                    description: "I just downloaded Oliveboard's FREE ebook for my preparation"
                };

                function callback(response) {
                    if(response){
                        bsubmitData(response['post_id'])
                    }
                }
                FB.ui(obj, callback);

            }

     function obfacebook(ref)
     {

        FB.getLoginStatus(function (loginresponse) {
            FB.login(function (response) {
                if (response.status == "connected") {
                    FB.api("/me?fields=name,email", function (udata) {
                        var data = udata;
                        data['fbaccesstoken']= response.authResponse.accessToken;
                        data['ref'] = ref;
                            var url  = '/pyscripts/fblogin_nf.py';
                            $.ajax({
                                url: url,
                                type: "POST",
                                data: data,
                                success: function(data) {
                                    if(data){
                                        redirectOnLeave = false;
                                        window.location = data;
                                    }else{
                                         $(errid).html('Please try again');
                                    }

                                }
                            });
                        })
                    } 
                }, {
                    scope: "public_profile,email"
                })
            })
    }

     function obfacebookn(ref)
     {

        FB.getLoginStatus(function (loginresponse) {
            FB.login(function (response) {
                if (response.status == "connected") {
                    FB.api("/me?fields=name,email", function (udata) {
                        var data = udata;
                        data['fbaccesstoken']= response.authResponse.accessToken;
                        data['ref'] = ref;
                            var url  = '/pyscripts/fblogin_nf.py';
                            $.ajax({
                                url: url,
                                type: "POST",
                                data: data,
                                success: function(data) {
                                    if(data){
                                        redirectOnLeave = false;
                                    var next = gq('next');
                                    if (next == "0")
                                        window.location = data;
                                    else
                                        window.location = next;
                                    }else{
                                         $(errid).html('Please try again');
                                    }

                                }
                            });
                        })
                    } 
                }, {
                    scope: "public_profile,email"
                })
            })
    }
    // Load the SDK Asynchronously
    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "../connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));


function goto(url){ window.location = url; }