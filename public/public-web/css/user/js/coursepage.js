var cww=$(window).width();
$(document).ready(function(){
    if(cww < 768){
        var wrs=window.innerHeight - 62;
        $(".wrap-rightSide").height(wrs+"px");
    }else{
        var wrs=window.innerHeight - 129;
        $(".wrap-rightSide").height(wrs+"px");
    }

    /* arrow change username*/
    $(".pstl-username").on("click",function(e) {
        $(this).next(".profile-menu").slideToggle("slow");
        if($(this).next(".profile-menu").hasClass("usin")){
            $(this).find(".pstl-caret").css("backgroundPosition", "0px -275px");
            $(this).next(".profile-menu").removeClass("usin");
        }else{
            $(this).find(".pstl-caret").css("backgroundPosition", "-14px -275px");
            $(this).next(".profile-menu").addClass("usin");
        }
        e.stopPropagation();
    });

    $("#wrapper").on("click",function() {
        if(!$(".profile-menu").hasClass("usin")){
            $(".profile-menu").slideToggle("slow");
            $(".pstl-username").find(".pstl-caret").css("backgroundPosition", "-14px -275px");
            $(".profile-menu").addClass("usin");
        }
    });

    /* arrow change username end*/

    /* alert */
    $(".pt-alert-close").on("click", function(){
        $("#pt-alert").slideUp("slow");
    });

});/* document .ready end */
$(window).resize(function(){
    cww=$(window).width();
    if(cww < 768){
        var wrs=window.innerHeight - 62;
        $(".wrap-rightSide").height(wrs+"px");
    }else{
        var wrs=window.innerHeight - 129;
        $(".wrap-rightSide").height(wrs+"px");
    }

});

/*menu bar close */
$('#btn-mbarClose').on("click", function(){
    $("#navbar-toggle").trigger("click");
});
$("#layout-list").on("click", function(){
    $("#sec-mockGrid").css("display", "none");
    $("#sec-mockList").css("display", "block");
    $("#sec-gridlable").css("display", "none");
    $("#sec-listlable").css("display", "block");
    $("#layout-list").removeClass("layout-list");
    $("#layout-list").addClass("layout-list-active");
    $("#layout-grid").addClass("layout-grid");
    $("#layout-grid").removeClass("layout-grid-active");
});
$("#layout-grid").on("click", function(){
    $("#sec-mockList").css("display", "none");
    $("#sec-mockGrid").css("display", "block");
	$("#sec-listlable").css("display", "none");
	$("#sec-gridlable").css("display", "block");
    $("#layout-grid").removeClass("layout-grid");
    $("#layout-grid").addClass("layout-grid-active");
    $("#layout-list").addClass("layout-list");
    $("#layout-list").removeClass("layout-list-active");
});

$('.modalClose').on("click",function(){
    $('#examsModal').modal('hide');
});
/* check device */
if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
    /* custom scrollbar */
    $('#examshd-collapsemock').enscroll({
        showOnHover: true,
        verticalTrackClass: 'track3',
        verticalHandleClass: 'handle3',
        addPaddingToPane:false,
        propagateWheelEvent: false,
        scrollIncrement: 30
    });

}
/* tooltip */
$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        html:true
    });
});
$(function(){ 

    var $navMain = $(".navbar-collapse");

    if($(window).width() <= 767){
        $('.navbar-toggle').on("click",function(){
            if($navMain.hasClass("in")){
                $('.sidebar-open-bg').hide(); 
            }else{
                $('.sidebar-open-bg').show();
            }
        });
        $('.sidebar-open-bg').on('click', function(){
            $navMain.collapse('hide');
            $('.sidebar-open-bg').hide();
        });
    }
});
$("#backtomenu").on("click", function() {
    $(".navbar-collapse").collapse('show');
    $('.sidebar-open-bg').show();
});

//added on 13 feb 2019
/* mocktest colexp first */
/* examnames colexp */
$('.examItems-divfirst').on('click',function() {
    $(this).next(".examsnames-collapsefirst").slideToggle("slow");
    if($(this).next(".examsnames-collapsefirst").hasClass('sin')){
        $(this).find(".items-expColapse").html("+");
        $(this).next(".examsnames-collapsefirst").removeClass('sin');
    }else{
        $(this).find(".items-expColapse").html("-");
        $(this).next(".examsnames-collapsefirst").addClass('sin');
    }
    
});
$('.testsItem-divall').on('click',function() {
    $(this).next(".examshd-collapseall").slideToggle("slow");
    if($(this).next(".examshd-collapseall").hasClass('sin')){
        $(this).find(".tabscaretall").css('backgroundPosition', '0px -1235px');
        $(this).next(".examshd-collapseall").removeClass('sin');
    }else{
        $(this).find(".tabscaretall").css('backgroundPosition', '-14px -1235px');
        $(this).next(".examshd-collapseall").addClass('sin');
    }
});
/* examnames colexp */
$('.examItems-divall').on('click',function() {
    $(this).next(".examsnames-collapseall").slideToggle("slow");
    if($(this).next(".examsnames-collapseall").hasClass('sin')){
        $(this).find(".items-expColapse").html("-");
        $(this).next(".examsnames-collapseall").removeClass('sin');
    }else{
        $(this).find(".items-expColapse").html("+");
        $(this).next(".examsnames-collapseall").addClass('sin');
    }
});
