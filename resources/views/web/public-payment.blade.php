<!DOCTYPE html>
<html>
	
<!-- Mirrored from www.oliveboard.in/ibps-po/?ref=chan by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 22 May 2019 19:06:22 GMT -->
<head>
		  <meta charset="utf-8">
        <title>EntryTest</title>
        <meta name="description" content="EntryTest.">
        <meta name="keywords" content="Entrytest, Ecat, MCAT">
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="{{ url('/')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{asset('public-web/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('public-web/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('public-web/css/package.css')}}" rel="stylesheet">

        <link href="../bootstrap3.0/css/stylenp1ded1.css?version=8" rel="stylesheet">
        
                
	</head>
	
	<body id="home">
		<div class="sidebar-open-bg"></div>
		<!-- Wrapper -->
		<div class="wrapper">
		
			<!-- Header Area -->
			<div class="header">
				<!-- Navigation Menu -->
				<nav class="navbar navbar-fixed-top" role="navigation">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="{{url('/')}}">
                                <img src="{{asset('images/logo.png')}}" height="50" width="50" style="margin:0px;"/>
                            </a>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
                                <!-- <li class="active"><a href="#home">IBPS</a></li>
								<li><a href="#pricing">Pricing</a></li>
								<li><a href="#exam-pattern">Pattern</a></li>
								<li><a href="#syllabus">Syllabus</a></li>
                                <li class="dropdown">
                                    <a  class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown">COURSES <b class="caret"></b></a>
                                    <ul class="dropdown-menu courses-dropdown">
                                        <li><a href="#"><b>MBA</b></a></li>
                                        <li><a class="obcolor" href="../tests/cat/index.html" onclick="goto('../tests/cat/index.html');">CAT</a></li>
                                        <li><a class="obcolor" href="../tests/cmat/index.html" onclick="goto('../tests/cmat/index.html');">CMAT</a></li>
                                        <li><a class="obcolor" href="../tests/xat/index.html" onclick="goto('../tests/xat/index.html');">XAT</a></li>
                                        <li><a class="obcolor" href="../tests/mhcet/index.html" onclick="goto('../tests/mhcet/index.html');">MHCET</a></li>
                                        <li><a class="obcolor" href="../tests/nmat/index.html" onclick="goto('../tests/nmat/index.html');">NMAT</a></li>
                                        <li><a class="obcolor" href="../tests/snap/index.html" onclick="goto('../tests/snap/index.html');">SNAP</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><b>Banking &amp; Govt. Exams</b></a></li>
                                        <li><a class="obcolor" href="index.html" onclick="goto('index.html');">IBPS PO</a></li>
                                        <li><a class="obcolor" href="../sbi-po/index.html" onclick="goto('../sbi-po/index.html');">SBI PO</a></li>
                                        <li><a class="obcolor" href="../sbi-clerk/index.html" onclick="goto('../sbi-clerk/index.html');">SBI Clerk</a></li>
                                        <li><a class="obcolor" href="../rbi-grade-b/index.html" onclick="goto('../rbi-grade-b/index.html');">RBI Grade B</a></li>
                                        <li><a class="obcolor" href="../rbi-assistant/index.html" onclick="goto('../rbi-assistant/index.html');">RBI Assistant</a></li>
                                        <li><a class="obcolor" href="../ibps-clerk/index.html" onclick="goto('../ibps-clerk/index.html');">IBPS Clerk</a></li>
                                        <li><a class="obcolor" href="../ibps-so/index.html" onclick="goto('../ibps-so/index.html');">IBPS SO</a></li>
                                        <li><a class="obcolor" href="../lic-aao/index.html" onclick="goto('../lic-aao/index.html');">LIC AAO</a></li>
                                        <li><a class="obcolor" href="../railways-rrb/index.html" onclick="goto('../railways-rrb/index.html');">Railways RRB Group D</a></li>
                                        <li><a class="obcolor" href="../railways-rrb-alp/index.html" onclick="goto('../railways-rrb-alp/index.html');">Railways RRB ALP</a></li>
                                        <li><a class="obcolor" href="../railway-rrb-je/index.html" onclick="goto('../railway-rrb-je/index.html');">Railways RRB JE</a></li>
                                        <li><a class="obcolor" href="../cwc/index.html" onclick="goto('../cwc/index.html');">Central Warehousing Corporation</a></li>
                                        <li><a class="obcolor" href="../nhb-assistant-manager/index.html" onclick="goto('../nhb-assistant-manager/index.html');">NHB</a></li>
                                        <li><a class="obcolor" href="../esic-udc/index.html" onclick="goto('../esic-udc/index.html');">ESIC UDC</a></li>
                                        <li><a class="obcolor" href="../fci/index.html" onclick="goto('../fci/index.html');">Food Corporation of India</a></li>
                                        <li><a class="obcolor" href="../lic-ado/index.html" onclick="goto('../lic-ado/index.html');">LIC ADO</a></li>
                                        <li><a class="obcolor" href="../epfo-assistant/index.html" onclick="goto('../epfo-assistant/index.html');">EPFO Asst.</a></li>
                                        <li><a class="obcolor" href="../jk-bank-po-associate/index.html" onclick="goto('../jk-bank-po-associate/index.html');">JK Bank</a></li>
                                        <li><a class="obcolor" href="../railway-rrb-ntpc/index.html" onclick="goto('../railway-rrb-ntpc/index.html');">RRB NTPC</a></li>
                                        <li><a class="obcolor" href="../pnb-it-officer/index.html" onclick="goto('../pnb-it-officer/index.html');">PNB IT Officer</a></li>
                                        <li><a class="obcolor" href="../nabard-grade-a-b/index.html" onclick="goto('../nabard-grade-a-b/index.html');">NABARD</a></li>
                                        <li><a class="obcolor" href="../nicl-ao/index.html" onclick="goto('../nicl-ao/index.html');">NICL AO</a></li>
                                        <li><a class="obcolor" href="../niacl-ao/index.html" onclick="goto('../niacl-ao/index.html');">NIACL AO</a></li>
                                        <li><a class="obcolor" href="../ippb-officer/index.html" onclick="goto('../ippb-officer/index.html');">IPPB Officer</a></li>
                                        <li><a class="obcolor" href="../ibps-rrb-officer/index.html" onclick="goto('../ibps-rrb-officer/index.html');">IBPS RRB Officer</a></li>
                                        <li><a class="obcolor" href="../ibps-rrb-assistant/index.html" onclick="goto('../ibps-rrb-assistant/index.html');">IBPS RRB Assistant</a></li>
                                        <li><a class="obcolor" href="../bob-manipal/index.html" onclick="goto('../bob-manipal/index.html');">BOB Manipal</a></li>
                                        <li><a class="obcolor" href="../lakshmi-vilas-bank-po/index.html" onclick="goto('../lakshmi-vilas-bank-po/index.html');">Lakshmi Vilas Bank</a></li>
                                        <li><a class="obcolor" href="../dena-bank-po/index.html" onclick="goto('../dena-bank-po/index.html');">Dena Bank PO</a></li>
                                        <li><a class="obcolor" href="../bom-manipal/index.html" onclick="goto('../bom-manipal/index.html');">BOM Manipal</a></li>
                                        <li><a class="obcolor" href="../syndicate-bank-po/index.html" onclick="goto('../syndicate-bank-po/index.html');">Syndicate Bank PO</a></li>
                                        <li><a class="obcolor" href="../ecgc-po/index.html" onclick="goto('../ecgc-po/index.html');">ECGC PO</a></li>
                                        <li><a class="obcolor" href="../canara-bank-po/index.html" onclick="goto('../canara-bank-po/index.html');">Canara Bank PO</a></li>
                                        <li><a class="obcolor" href="../idbi-bank-executive/index.html" onclick="goto('../idbi-bank-executive/index.html');">IDBI Executive</a></li>
                                        <li><a class="obcolor" href="../indian-bank-po/index.html" onclick="goto('../indian-bank-po/index.html');">Indian Bank PO</a></li>
                                        <li><a class="obcolor" href="../idbi-bank-po/index.html" onclick="goto('../idbi-bank-po/index.html');">IDBI Bank PO</a></li>
                                        <li><a class="obcolor" href="../ssc-cpo/index.html" onclick="goto('../ssc-cpo/index.html');">SSC CPO</a></li>
                                        <li><a class="obcolor" href="../ssc-cgl/index.html" onclick="goto('../ssc-cgl/index.html');">SSC CGL</a></li>
                                        <li><a class="obcolor" href="../ssc-chsl/index.html" onclick="goto('../ssc-chsl/index.html');">SSC CHSL</a></li>
                                        <li><a class="obcolor" href="../ssc-mts/index.html" onclick="goto('../ssc-mts/index.html');">SSC MTS</a></li>
                                        <li><a class="obcolor" href="../ias/index.html" onclick="goto('../ias/index.html');">UPSC (IAS Exam)</a></li>
                                        <li><a class="obcolor" href="../niacl-assistant/index.html" onclick="goto('../niacl-assistant/index.html');">NIACL Assistant</a></li>
                                        <li><a class="obcolor" href="../nicl-assistant/index.html" onclick="goto('../nicl-assistant/index.html');">NICL Assistant</a></li>
                                        <li><a class="obcolor" href="../uiic-assistant/index.html" onclick="goto('../uiic-assistant/index.html');">UIIC Assistant</a></li>
                                        <li><a class="obcolor" href="../oicl-ao/index.html" onclick="goto('../oicl-ao/index.html');">OICL AO</a></li>
                                        <li><a class="obcolor" href="../irdai-assistant-manager/index.html" onclick="goto('../irdai-assistant-manager/index.html');">IRDAI Asst.</a></li>
                                        <li><a class="obcolor" href="../ib-acio/index.html" onclick="goto('../ib-acio/index.html');">IB ACIO</a></li>
                                        <li><a class="obcolor" href="../ib-security-assistant/index.html" onclick="goto('../ib-security-assistant/index.html');">IB Security Asst</a></li>
                                        <li><a class="obcolor" href="../lic-hfl/index.html" onclick="goto('../lic-hfl/index.html');">LIC HFL</a></li>
                                        <li><a class="obcolor" href="../nabard-assistant/index.html" onclick="goto('../nabard-assistant/index.html');">NABARD Asst.</a></li>
                                        <li><a class="obcolor" href="../esic-sso/index.html" onclick="goto('../esic-sso/index.html');">ESIC SSO</a></li>
                                        <li><a class="obcolor" href="../sebi/index.html" onclick="goto('../sebi/index.html');">SEBI</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="#" onclick="javascript:signToggler(2);" class="sign-up" id="sign-up-tog">Register</a></li> -->
                                <li><a href="{{url('/')}}" onclick="javascript:signToggler(1);" class="sign-up" id="sign-in-tog">Home</a></li>
{{--                                 <li><a href="https://play.google.com/store/apps/details?id=com.project.prepon" /><img src="{{asset('public-web/img/play.png')}}"></a></li>
 --}}                            </ul>
						</div>
					</div>
				</nav>
				<!-- Navigation End -->
			</div>
			<!-- Header End -->

 
    <div id="sdstrip" style="display: none;"></div>			<!-- For navigation -->
			<section id="home1">
				<div class="features-ele cpadd flex">
						<div class="container">
                        <div class="row  flex-two cpadd">
                        	<h3 class="text-center">Points To Ponder</h3>
                        </div>
                        	
                            <div class="col-xs-12 col-sm-7 pull-left">
									
									<div class="col-md-12 col-sm-12 float-left" >
									<!-- services Item -->
									<div class="features-ele-item animated fadeInUp cremaxwidth">
										<!-- Image -->
                                        <span class="lpi-icons lpi-learn"></span>
										<!-- Heading -->
										<h4 class="cph-text" style="margin-left: 30px"> Time Management  </h4>
										<!-- Paragraph -->
										<!-- Paragraph -->
										<p class="cpp-text"> Devise a Timeline and study schedule to avoid procrastination</p>
									</div>
                                    <div class="features-ele-item animated fadeInUp cremaxwidth">
										<!-- Image -->
                                        <span class="lpi-icons lpi-practice"></span>
										<!-- Heading -->
										<h4 class="cph-text">Aim</h4>
										<!-- Paragraph -->
										<!-- Paragraph -->
										<p class="cpp-text">Your aim should be to perform the best</p>
									</div>
                                    <div class="features-ele-item animated fadeInUp cremaxwidth">
										<!-- Image -->
                                        <span class="lpi-icons lpi-improve"></span>
										<!-- Heading -->
										<h4 class="cph-text">Improvise your performance</h4>
										<!-- Paragraph -->
										<!-- Paragraph -->
										<p class="cpp-text">To be able to overcome your weak areas, you need to identify them first</p>
									</div>
                                    <div class="features-ele-item animated fadeInUp cremaxwidth">
                                        <!-- Image -->
                                        <span class="lpi-icons lpi-improve"></span>
                                        <!-- Heading -->
                                        <h4 class="cph-text">Self-Evaluation</h4>
                                        <!-- Paragraph -->
                                        <!-- Paragraph -->
                                        <p class="cpp-text">Do not forget to evaluate your work</p>
                                    </div>
								</div>
									
                            </div>
                                                   
                        </div>
				</div>
			</section>
			
    			<!-- Main Content -->
			<div class="main-content">



        <section id="pricing">
		


					<!-- Pricing Start -->
					<div class="pricing nppadd-top">
						<div class="container">
							<!-- Default Heading Area -->
							<div class="heading text-center">
								<!-- Heading -->
								<h2>Subscription Packages</h2>
								<div class="bor"></div>
							</div>
							{{-- <div class="test-instruction nppadd-bot">
								<div class="row">
									<div class="col-md-3 col-sm-6 col-xs-12">
                                        <h4 class="test-inst1">Mock tests available in urdu &amp; English</h4>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<h4 class="test-inst2">Tests can be taken on Mobile App or PC</h4>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
                                        <h4 class="test-inst3">Tests available in new pattern</h4>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<h4 class="test-inst4">Tests can be attempted anytime. Validity: Jan 31<sup>st</sup> 2020</h4>
									</div>
								</div>
							</div> --}}
						</div>
						<div class="coffer-pricing nppadd-topbot"><!-- combo offer pricing block -->
							<div class="container">
								<div class="np-row"><!-- pricing row -->




                                    <div class="np-card card-col-3"><!-- coffer card dark blue-->
                                        <div class="np-card-inner">
                                            <div class="npcard-header npcard-lblue">
                                                
                                                <div class="card-img-outer"><span class="card-img img-lblue">&nbsp;Weekly</span></div>
                                                <h1><span class="lined-amt"></span>Rs.299 /Weekly</h1>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="npcard-content">
                                                <div class="nprice-lists">
                                                    <ul>
														<li><span style="font-weight:bold;font-size:18px">All Practice Tests for MDCAT & ECAT Exams</span></li>
                                                <li style="margin-bottom:2px"><span style="font-weight:bold;font-size:18px"> Best MCQ's Database</span></li>
                                               {{--  <li style="margin-bottom:2px">All Mock Tests for ECAT exams</li>
                                                <li style="margin-bottom:2px"><b>- Does NOT include ECAT Tests</b></li> --}}
                                                <li style="margin-bottom:2px;font-style:italic;font-size:13px;">*Enjoy the Full Access of the App within 24 hours after the payment.</li>


                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="npcard-footer">
                                                <div class="npcard-footer-inner">
                                                    {{-- <a href="https://www.easypaisa.com.pk/easypay/easypaisaaccount">
											<button id="p2019lt108" type="button" class="btn btn-np-buy">Buy Now</button></a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.coffer card dark blue -->


                                    <div class="np-card card-col-3"><!-- coffer card dark blue-->
                                        <div class="np-card-inner">
                                            <div class="npcard-header npcard-dblue">
                                                <div class="card-img-outer"><span class="card-img img-dblue">&nbsp;One Time Payment</span></div>
                                                <h1><span >Rs.3,999 /one time payment</h1>
                                                <p>&nbsp;</p>
                                            </div>
                                            <div class="npcard-content">
                                                <div class="nprice-lists">
                                                    <ul style="">
														<li> <span style="font-weight:bold;font-size:18px">All Practice Tests for MDCAT & ECAT Exams</span></li>
                                                <li style="margin-bottom:2px"><span style="font-weight:bold;font-size:18px"> Best MCQ's Database</span></li>
                                               {{--  <li style="margin-bottom:2px">All Mock Tests for ECAT exams</li>
                                                <li style="margin-bottom:2px"><b>- Does NOT include ECAT Tests</b></li> --}}
                                                <li style="margin-bottom:2px;font-style:italic;font-size:13px;">*Enjoy the Full Access of the App within 24 hours after the payment.</li>


                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="npcard-footer">
                                                <div class="npcard-footer-inner">
                                                     {{-- <a href="https://www.easypaisa.com.pk/easypay/easypaisaaccount">
											<button id="p2019lt4" type="button" class="btn btn-np-buy">Buy Now</button></a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.coffer card dark blue -->




								</div><!-- /.pricing row -->
							</div>
						</div><!-- /.combo offer pricing block -->
					</div>
							<!-- Pricing End -->


		</section>

    				<section id="free-download">
				<div class="mini-cta bbottom">
					<div class="container" >
						<h3 style="color: black;">Study on the go with PrepOn 's Android Mobile app </h3> <a class="btn btn-theme" href="https://play.google.com/store/apps/details?id=com.project.prepon">Download App</a>					
						
					</div>
				</div>
				</section>	
			
				<!-- Exam Pattern -->
				{{-- <section id="exam-pattern">	
                    <div class="exam-patterns padd">
                       <div class="container big-padd">
                                <div class="heading text-center">
                                    <h2>Exam Pattern</h2>
                                    <div class="bor"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        
                                            
                                    </div>
                                    <p>&nbsp;</p>
                                     <h4>MCAT Prelims Pattern</h4>
                                                <table class="table exam-table">
                                                    
                                                    <thead>
                                                        <tr>
                                                            <th>Section</th>
                                                            <th class="text-center">Number of Questions</th>
                                                            <th class="text-center">Maximum Marks</th>
                                                            <th class="text-center">Time</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Logical Reasoning</td>
                                                            <td class="text-center">35</td>
                                                            <td class="text-center">35</td>
                                                            <td rowspan="3" class="cvmiddle text-center">60</td>
                                                        </tr>
                                                         <tr>
                                                            <td>Quantitative Aptitude</td>
                                                            <td class="text-center">35</td>
                                                            <td class="text-center">35</td>
                                                        </tr>
                                                        <tr>
                                                            <td>English Language</td>
                                                            <td class="text-center">30</td>
                                                            <td class="text-center">30</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total </td>
                                                            <td class="text-center">100</td>
                                                            <td class="text-center">100</td>
                                                            <td class="text-center">60</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                        <h5>Duration of exam : 60 minutes | 1 marks allotted for every correct answer </h5>
										<h5>*For each wrong answer 0.25 mark is deducted</h5>
                                    
                                </div>
                            </div>
                    </div>				
				</section>
				 --}}
				<!-- Syllabus -->
				{{-- <section id="syllabus">
				
					<!-- Syllabus Start -->
					<div class="features-ele padd">
                    	 <div class="container big-padd">
                                <div class="heading text-center">
                                    <h2>Syllabus</h2>
                                    <div class="bor"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 syl-head">
                                    	<h4><u>Logical Reasoning</u></h4>
                                    </div> 
                                    <div class="col-sm-3 syl-topic">
                                    	<p>Cubes &amp; Dice</p>
                                        <p>Blood Relations</p>
                                        <p> Critical Reasoning</p>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>Number &amp; Letter Series</p>
                                        <p>Seating Arrangement</p>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>Venn Diagrams</p>
                                        <p>Clocks &amp; Calendars</p>
                                    </div> 
                                     <div class="col-sm-3 syl-topic">
                                    	<p>Analytical Reasoning</p>
                                        <p>Syllogism</p>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-sm-12 syl-head">
                                    	<h4><u>English Language </u></h4>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                    	<p>Reading Comprehension</p>
                                        <p>Parajumbles</p>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>Vocabulary</p>
                                        <p>English Grammar</p>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>Sentence Correction</p>
                                    	<p>Analogies</p>
                                    </div>
                                    <div class="col-sm-3 syl-topic">
                                        <p>Facts / Inference /Judgement</p>                                       
                                        <p>Fill in the Blanks</p>
                                    </div>
                                </div>
                                <div class="row">
                                	<div class="col-sm-12 syl-head">
                                    	<h4><u>Quantitative Aptitude</u></h4>
                                    </div>
                                    <div class="col-sm-3 syl-topic">
                                    	<p>Number series</p>
                                        <p>Table</p>
                                        <p>Line graph</p>
                                        <p>Pie chart</p>
                                    </div> 
                                    <div class="col-sm-3 syl-topic">
                                        <p>DI(statements)</p>
                                        <p>Percentage, Profit &amp; Loss</p>
                                        <p>Simple &amp; Compound Interest</p>
                                        <p>Speed, Time &amp; Distance</p>
                                    </div> 
                                     <div class="col-sm-3 syl-topic"> 
                                        <p>Time &amp; Work</p>
                                        <p>Mensuration</p>
                                        <p>Geometry</p>
                                    </div>
                                    <div class="col-sm-3 syl-topic"> 
                                        <p>DS(3 statements)</p>
                                        <p>Line Graphs</p>
                                        <p>Equations</p>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-sm-12 syl-head">
                                    	<h4><u>General Awareness</u></h4>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>Current Affairs</p>
                                        <p>Politics</p>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>Sports</p>
                                        <p>Art and Culture</p>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                    	<p>Business and Economy</p>
                                    </div>
                                    <div class="col-sm-3 syl-topic">
                                        <p>Banking Awareness</p>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-sm-12 syl-head">
                                    	<h4><u>Computer Awareness</u></h4>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>History of Computers</p>
                                        <p>Basics of Database</p>
                                        <p>Applications</p>
                                    </div>
                                     <div class="col-sm-3 syl-topic">
                                        <p>Microsoft Windows Usage</p>
                                        <p>MS Office</p>
                                        <p>Security Tools</p>
                                    </div>
                                    <div class="col-sm-3 syl-topic">
                                    	<p>Microsoft Windows Commands
                                    	<p>Basic Hardware</p>
                                        <p>Software of Computers</p>
                                    </div>
                                    <div class="col-sm-3 syl-topic">
                                        <p>Networking Basics</p>
                                        <p> Internet - History</p>
                                    </div>
                                </div>
                            </div>
						
					</div>
					<!-- Syllabus End -->
				</section> --}}
        				<!-- Free Downloads -->
				{{-- <section id="free-download">	
                    <div class="free-downloads padd">
                       <div class="container big-padd">
                                <div class="heading text-center">
                                    <h2>Free Download</h2>
                                    <div class="bor"></div>
                                </div>
                                <div class="row">
                                	<div class="col-md-6 col-sm-6  col-centered">
                                    	<div class="col-md-9 col-sm-10">
                                        	<h3>Shortcuts, Formulas &amp; Tips</h3>
                                        </div>
                                        <div class="col-md-3 col-sm-2 mtopp">
                                        	<a href="#" class="btn btn-primary btn-lg">Download</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>				
				</section>	 --}}
				<!-- FAQ -->
				<section id="faq">
				
					<!-- FAQ Start -->
					<div class="features-ele padd">
                    	 <div class="container big-padd">
                                <div class="heading text-center">
                                    <h2>FAQs</h2>
                                    <div class="bor"></div>
                                </div>
                                <div class="row"> 
                                	<div class="col-sm-6 faq-content"> 
                                            	<h4>1) What is PrepOn?</h4>
                                                <p class="text-justify">PrepOn is an online MDCAT & ECAT test preparation app that enables you to practice your skills and overcome your weak areas for the best performance. </p>
                                            	<h4>2) Why PrepOn?</h4>
                                            	<p class="text-justify">PrepOn app must be your foremost priority because it is time saving, and it gives you access to the best MCQ’S database. Here, you can get the evaluation and success tips by the best teachers from across the country. You can easily access it anywhere at any time.</p>
                                        
                                            	<h4>3) How can I pay for the course?</h4>
                                            	<p class="text-justify">Go to your nearest easypaisa shop or pay through easypaisa app, pay the subscription amount, Rs. 3,999/- one time payment or Rs. 299/weekly (quarterly). Send your email id, whatsapp number and transaction id on our whatsapp number, 0333-424269.</p>
                                       
                                            	<h4>4) Does the course on your app cover all syllabus?</h4>
                                            	<p class="text-justify">Yes, it does. It gives you the largest and the best MCQ’S database. No study-aid option has been left behind.</p>
                                        
                                    </div>
                                    <div class="col-sm-6 faq-content"> 
                                        
                                            	<h4>5)  How can I access the app?</h4>
                                            	<p class="text-justify">Once you have sent the payment details on our whatsapp number, you will be given the full access to the app within 24 hours.</p>
                                        
                                            	<h4>6) What are the other benefits of this app?</h4>
                                            	<p class="text-justify">It gives you success tips, health tips, preparation tips, and time management tips. </p>
                                        
                                            	<h4>7) Can I create my own timeline and study schedule for practice?</h4>
                                            	<p class="text-justify">Of course, you can create your own personalized timeline and study schedule for smarter study in shorter period of time. </p>   
                                        </div>
                                </div>
                            </div>
					</div>
					<!-- FAQ End -->
				</section>
                <!-- Forget password -->
				<section id="forgotpass">	
                    <div class="forgotpass-wrap">
                       <div class="container">
                                <div class="forgotpass less-padd">
                                    <div class="heading text-center">
                                        <h2>Forgot Password</h2>
                                        <div class="bor"></div>
                                    </div>
                                </div>
                               <div class="row">
                                    <div class=" col-sm-8 col-md-8 col-centered">
                                    <h4 class="text-center resetemail less-padd">Enter email ID and we will send you a link to reset your password.</h4>
                                    <h4 class="text-center resetpass less-padd">Password reset details have been mailed to you.</h4>
                                       
                                        <!-- Reset Form -->
                                        <form role="form" id="resetForm" class="resetform" method="post">
                                            <div class="row">
                                               
                                                <div class="col-md-10 col-sm-10">
                                                    <div class="form-group">
                                                        <input type="email" required="" value="" placeholder="Enter Email..." name="email" id="remail" class="form-control">
                                                    </div>
                                                </div>
                                               
                                                <div class="col-md-2 col-sm-2">
                                                   
                                                    <a href="javascript:resetPassword();" id="lreset" name="lreset" class="btn btn-theme">Submit</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8">
                                                <div class="rerrMsg less-padd">
                                                    <p>&nbsp;</p>
                                                </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>				
				</section>
<!-- CCAvenue -->
<form name='ccavenue' id="ccavenue1" action='https://www.ccavenue.com/shopzone/cc_details.jsp' method='post'>
<input type=hidden name="Merchant_Id" value="M_abh24288_24288" />

<input type=hidden name="Amount" value="" />
<input type=hidden name="Checksum" value="" />

<input type=hidden name="Order_Id" value="" />
<input type=hidden name="Redirect_Url" value="../pricingregister.html" />
<input type="hidden" name="billing_cust_name" value="" />
<input type="hidden" name="billing_cust_address" value="" />
<input type="hidden" name="billing_cust_country" value="" />
<input type="hidden" name="billing_cust_state" value="" />
<input type="hidden" name="billing_zip" value="" />
<input type="hidden" name="billing_cust_tel" value="" />
<input type="hidden" name="billing_cust_email" value="" />
<input type="hidden" name="delivery_cust_name" value="" />
<input type="hidden" name="delivery_cust_address" value="" />
<input type="hidden" name="delivery_cust_country" value="" />
<input type="hidden" name="delivery_cust_state" value="" />
<input type="hidden" name="delivery_cust_tel" value="" />
<input type="hidden" name="delivery_cust_notes" value="" />
<input type="hidden" name="Merchant_Param" value="74692c397d32f95a6d99747a56aa1234" />
<input type="hidden" name="billing_cust_city" value="" />
<input type="hidden" name="billing_zip_code" value="" />
<input type="hidden" name="delivery_cust_city" value="" />
<input type="hidden" name="delivery_zip_code" value="" />
</form>
<!--CCAvenue -->
			</div>
			<!-- Main Content End -->
			
			<!-- Footer start -->
			 <div class="footer text-center">
                <div class="container link-footer">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <p><img src="{{asset('images/logo.png')}}" height="80" width="80" style="margin:0px;"></p>
                            <p> M. Shaukat Ali Road, Lahore, Pakistan</p>
                            <p>prepon.entrytests@gmail.com</p>
                            <p>03334242069</p>
                            <div class="footer-social social">
                                <!--<a href="#" onclick="window.open('', '_blank'); return false;" class="facebook"><i class="fa fa-facebook"></i></a>-->
                                <a href="#" onclick="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                {{-- <a href="#" onclick="#" class="google-plus"><i class="fa fa-google-plus"></i></a> --}}
                                {{-- <a href="#" onclick="#" class="linkedin"><i class="fa fa-linkedin"></i></a> --}}
                                {{-- <a href="#" onclick="#" class="youtube"><i class="fa fa-youtube"></i></a>      --}}
                            </div>    
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            {{-- <div class="footer-social social"> --}}
                                {{-- <a href="#" onclick="#" class="facebook"><i class="fa fa-facebook"></i></a> --}}
                                {{-- <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a> --}}
                            {{-- </div> --}}
                            <h3>Social</h3>
                            <ul>
                                <li><a href="https://www.facebook.com/PrepOn-636371010108789/?modal=admin_todo_tour" disabled ><i class="right isDisabled"></i><span>Facebook</span></a></li>
                                {{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Twitter</span></a></li> --}}
                                {{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Careers</span></a></li> --}}
                                {{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Testimonials</span></a></li> --}}
                            </ul>

                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            {{-- <h3>Resources</h3> --}}
                            {{-- <ul>
                                <li><a href="#"><i class="right"></i><span>Live Practice</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Video Course</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Free eBooks</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Blog</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Discuss Forum</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Current Affairs</span></a></li>
                            </ul> --}}
                            
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            {{-- <h3>Popular Exams</h3>
                            <ul>
                                <li><a href="#"><i class="right"></i><span>ECAT</span></a></li>
                                <li><a href="#"><i class="right"></i><span>MCAT</span></a></li>
                                <li><a href="#"><i class="right"></i><span>PPSC</span></a></li>
                                <li><a href="#"><i class="right"></i><span>CSS</span></a></li>
                                <li><a href="#"><i class="right"></i><span>NTS</span></a></li>
                                <li><a href="#"><i class="right"></i><span>PTS</span></a></li>
                               
                            </ul> --}}
                            
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <h3>Download the App</h3>
                            <ul>

                                <li><a href="https://play.google.com/store/apps/details?id=com.project.prepon" /><img src="{{asset('public-web/img/play.png')}}"></a></li>
                            </ul>

                        </div>
                    </div> 
                </div>

                <div class="container">

                    <!-- Footer Copyright -->
                    <div class="copyright">
                        {{-- <p><a class="footerlinks" href="#">Careers</a> | --}} <a class="footerlinks" href="{{url('/privacy/policy')}}">Privacy Policy</a> {{-- | <a class="footerlinks" href="#">Terms</a> --}}</p>
                        <p>&copy; Copyright <a href="#">entrytest4u.com</a> - All Rights Reserved.</p>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
            <!-- Footer End -->
		
		</div>
		<!-- Wrapper End -->

<div id="open_app" class="openinapp"><a href="#" target="_blank">Open in app</a></div>     
		
          <script src="{{asset('public-web/js/jquery.js')}}"></script>
        <script src="{{asset('public-web/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public-web/js/jquery.nav.js')}}"></script>
        <script src="bootstrap3.0/js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 988819829;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="../../www.googleadservices.com/pagead/f.txt">
    </script>

    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/988819829/?guid=ON&amp;script=0"/>
    </div>
    </noscript>
        
	<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-33831595-1']);
    _gaq.push(['_setDomainName', 'oliveboard.in']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();


    function ccapaynow(cslistids,totalamount,divid)
    {
        $('#'+divid).html('Please wait..');
        $("input[name=Amount]").val(totalamount);

        var url = '../myaccount/co/getchecksumbpre.html';
        var params = {};
        params['items'] = cslistids;
        params['amount'] = totalamount;
        params['uauth'] = '74692c397d32f95a6d99747a56aa1234';

        $.post(url,params, function(data) {
            eval(data);
            $("input[name=Checksum]").val(resp[0]);
            $("input[name=Order_Id]").val(resp[1]);
            document.getElementById("ccavenue1").submit();
        });
    }
    function mobileAdClose(){
        $("#mobile-appAd").remove();
    }
    $(window).load(function(){
    if($('#sdstrip').length){
        $("#sdstrip").slideDown("slow");
    }
    });

    if( (/Android|webOS|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
    if($('#open_app').length){
        $(".openinapp").css("display", "flex");
        }    
    }   

    </script>

	</body>	

</html>

