<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<title>EntryTest</title>
	<meta name="description" content="EntryTest.">
	<meta name="keywords" content="Entrytest, Ecat, MCAT">
	<meta name="google-site-verification" content="w5V6AC46YbTlolvDWbldcKsluUm4vO4hBRq9yextC_g" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="{{ url('/')}}">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link href="{{asset('public-web/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('public-web/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('public-web/css/home5.css')}}" rel="stylesheet">

	<script type="text/javascript">
		var BASE_URL = "{{ url('/')}}";
	</script>

</head>

<body id="home">
	<div class="sidebar-open-bg"></div>
	<div id="open_app" class="openinapp"><a href="#" target="_blank">Open in app</a></div>
	<div class="wrapper">
		<!-- Header Area -->
		<div class="header">
			<!-- Navigation Menu -->
			<nav class="navbar navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="{{url('/')}}">
							<img src="{{asset('images/logo.png')}}" height="50" width="50" style="margin:0px;"/>
						</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
								{{-- <li class="active"><a id="remove-home" href="{{url('/')}}">Home</a></li>
								<li><a href="#">Features</a></li>
								<li><a href="#">Exams</a></li>
								<li><a href="#" >Live Course</a></li>
								<li><a href="#">App</a></li>
								<li><a href="#" >Blog</a></li>
								<li><a href="#">Contact</a></li> --}}
								<li><a href="{{url('/package')}}"  >Packages</a></li>
								<li><a href="{{route('user_web.login')}}"  >Login</a></li>
								<li><a href="{{route('user_web.register')}}"  class="sign-up">Register</a></li>

								{{-- <li><a href="https://play.google.com/store/apps/details?id=com.project.prepon" /><img src="{{asset('public-web/img/play.png')}}"></a></li> --}}
							</ul>
						</div>
					</div>
				</nav>
				<!-- Navigation End -->		
			</div>
			<!-- Header End -->	
			<!-- For navigation -->
			<section id="">
				<div class=" flex">
					{{-- <div class="js-slider slider">
						<ul class="slider-list">
							<li class="slider-item"><img src="http://placekitten.com/800/300" class="img-responsive"></li>
							<li class="slider-item"><img src="http://placehold.it/800x300" class="img-responsive"></li>
							<li class="slider-item"><img src="http://placekitten.com/800/300" class="img-responsive"></li>
						</ul>
					</div> --}}
					{{-- <div class="flexslider">
						<ul class="slides">
							<li class="olive-flex">
							<div class="flex-two" style="padding-top: 470px; padding-left: 50px">          
							 <h3>Ab Tyari Pakro!</h3>             
							<p>Join the most comprehensive online preparation portal for ECAT, MCAT and Government exams. Explore a range of mock tests and study material.</p>
							<p>Prepare on PC or Mobile with study synchronized across devices.</p> 
							 <a class="btn btn-theme btn-lg btn-startnow" href="#" >Start Free Trial</a> 
							<a class="btn btn-theme btn-lg btn-downloadapp" href="#" >Download App </a>
							</div>
							</li>                           

						</ul>
					</div> --}}
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div class="item active">
								<img src="{{asset('public-web/img/1.jpg')}}" alt="Los Angeles" style="width:100%;">
							</div>

							<div class="item">
								<img src="{{asset('public-web/img/2.jpg')}}" alt="Chicago" style="width:100%;">
							</div>

							<div class="item">
								<img src="{{asset('public-web/img/3.jpg')}}" alt="New york" style="width:100%;">
							</div>
						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</section>
			   @component('_components.alerts-default')
        @endcomponent
			<!-- Main Content -->
			<div class="main-content">
				<section id="app"><!-- app download start -->
				<div class="mini-cta bbottom">
					<div class="container">
						
						<div class="row">
							<div class="col-xs-12 martop">
								<a href="#" onclick="window.open('https://play.google.com/store/apps/details?id=com.project.prepon', '_blank'); return false;" class="btn-google-ply"></a>
							</div>
						</div>
						 					
						
					</div>
				</div>
				</section>
				
				<section id="features">
					<!-- features-ele Start -->
					<div class="features-ele padd">
						<div class="container">
							<div class="heading text-center">
								<h2>Features</h2>
								<div class="bor"></div>
							</div>
							<div class="ol-mar">
								<div class="featureCol">
									<div class="features-ele-item">
										<span class="img-responsive feature-bg feature-sp-c"> </span>
										<h4>Present Day Exam Content</h4>
										<p>Content generated by highly professional teachers, exam-top scorers, and exceedingly experienced faculty from across the country.</p>
									</div>
								</div>
								<div class="featureCol">
									<div class="features-ele-item">
										<span class="img-responsive feature-bg feature-sp-up"> </span>
										<h4>Absolute Course Material</h4>
										<p> Visual lessons, audio lessons, Course related e-books, Variable Tests, and all that is needed to recuperate your weak areas.</p>
									</div>
								</div>
								<div class="featureCol">
									<div class="features-ele-item">
										<span class="img-responsive feature-bg feature-sp-r"> </span>
										<h4>Considerable Online Test Series</h4>
										<p>The best MCQ’S Database (Questions and Answers according to the contemporary exam-pattern) and All Practice Tests needed for success in the exams. </p>
									</div>
								</div>
								<div class="featureCol">
									<div class="features-ele-item">
										<span class="img-responsive feature-bg feature-sp-sp"> </span>
										<h4>Instant Feedback</h4>
										<p>Corrective practice sessions to help you overcome your weak areas. Study smart with shorter study times leaving no preparation aid behind.</p>
									</div>
								</div>
								{{-- <div class="featureCol">
									<div class="features-ele-item">
										<span class="img-responsive feature-bg feature-sp-a"> </span>  
										<h4>Smarter Study Planning</h4>
										<p>“Time once gone is gone forever.” Only an intelligent mind can create an intelligent planning. Create your own study schedule based on important topics and your availability of time.</p>
									</div>
								</div>  --}}
								{{-- <div class="featureCol">
									<div class="features-ele-item">
										<span class="img-responsive feature-bg feature-sp-gs"> </span>
										<h4>Motivational Group Practice Sessions</h4>
										<p>Amusing and engaging group practice sessions increase confidence and make you learn more. Compete with your friends online in a game-like captivating environment.</p>
									</div>
								</div>  --}}
                               {{--  <div class="col-md-12 col-sm-12 col-xs-12 text-center martop"><!--btn-join-->
                                    <button onclick="goto('register81f6.html?ref=home');return false;" type="button" class="btn-join btn-theme btn-join-new">Join Now</button> 
                                </div><!-- --}}
                            </div>
                        </div>
                    </div>

                </section><!-- feature end -->
                <section id="exams">
                	<!-- exams-set Start -->
                	<div class="exams-set padd">
                		<div class="container" >
                			<div class="heading">
                				<h2 class="text-center">Exams</h2>
                				<div class="bor"></div>
                			</div>

                			<!-- exams-set Block Content -->
                			<div class="ol-mar" >
                				<!-- Banking-->
                				<div class="examCol">
                					<div class="examSet text-center examset-bank">
                						<div class="ol-ex-md">
                							<h3 class="examHeading">Engineering</h3>
                						</div>
                						<span class="ol-ex-md exams-sprite exam-img1 img-responsive"></span>
                						<div class="examName">
                							<span>
                								{{-- <a href="#" class="btn btnExam">ECAT</a> --}}
                								<span class=" btnExam">ECAT</span>
                							</span>
                                           {{--  |
                                            <span>
                                                <a href="#" class="btn btnExam">UET TEST</a>
                                            </span>
                                            |
                                            <span>
                                                <a href="#" class="btn btnExam">LUMS TEST</a>
                                            </span>
                                            |
                                            <span>
                                                <a href="#" class="btn btnExam">NUST TEST</a>
                                            </span> --}}
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- mangement -->
                                <div class="examCol">
                                	<div class="examSet text-center examset-mba">
                                		<div class="ol-ex-md">
                                			<h3 class="examHeading">MEDICAL</h3>
                                		</div>
                                		<span class="ol-ex-md exams-sprite exam-img2 img-responsive"></span>
                                		<div class="examName">
                                			<span>
                                				{{-- <a href="#" class="btn btnExam">MCAT</a> --}}
                                				<span class=" btnExam">MCAT</span>
                                			</span>
                                           {{--  |
                                            <span>
                                                <a href="#" class="btn btnExam">MCAT</a>
                                            </span> --}}
                                            
                                        </div>
                                    </div>

                                </div>

                                <!-- Government Recruitment -->
                                {{-- <div class="examCol">
                                	<div class="examSet text-center examset-govtins">
                                		<div class="ol-ex-md">
                                			<h3 class="examHeading">Government and Insurance Exams</h3>
                                		</div>
                                		<span class="ol-ex-md exams-sprite exam-img3 img-responsive"></span>
                                		<div class="examName">
                                			<span>
                                				<a href="#" class="btn btnExam">NTS </a>
                                			</span> --}}
                                            {{-- |
                                            <span>
                                                <a href="#" class="btn btnExam">PPSC</a>
                                            </span>
                                            |
                                            <span>
                                                <a href="#" class="btn btnExam">CSS</a>
                                            </span>
                                            |
                                            <span>
                                                <a href="#" class="btn btnExam">PTS</a>
                                            </span> --}}
                                            

                                      {{--   </div>
                                    </div>
                                </div>

 --}}
                                <!-- CRT -->
                               {{--  <div class="examCol">
                                    <div class="examSet text-center examset-crt">
                                        <div class="ol-ex-md">
                                            <h3 class="examHeading">Campus Placements</h3>
                                        </div>
                                        <span class="ol-ex-md exams-sprite exam-img4 img-responsive"></span>
                                        <div class="examName">
                                            <span>
                                                <a href="campus-placements/index.html" class="btn btnExam">TCS | Infosys | Accenture | IBM | Wipro | L&amp;T | Deloitte | CTS | HCL</a>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- \.CRT --> --}}


                            </div>

                        </div>
                    </div>
                </section>
                <!-- end of exmas -->
                <section id="app"><!-- app download start -->
                	<div class="mini-cta bbottom">
                		<div class="container">
                			<div class="row">
                				<div class="col-md-6 col-sm-6 col-xs-12">
                					<h3 class="text-capitalize">  Why <span class="olive-highlight">PrepOn</span> must be your ‘FIRST PRIORITY?’</h3>
                					<ul class="list-unstyled seo-bulletlist">
                						<li>Time Saving</li>
                						<li>Innovative</li>
                						<li>Easily accessible anywhere anytime</li>
                						<li>Auspicious opportunity</li>
                						<li>Tips, Notes, Evaluations, Practice Tests</li>
                					</ul>
                					<h3>In fact, everything that you need to prepare for MDCAT & ECAT is available here on this app, PrepOn</h3>
                				</div>

                				<div class="col-md-6 col-sm-6 col-xs-12 ">
                					<img class="img-responsive img-oliveapp" src="{{asset('public-web/img/mobileapp2.png')}}" alt=" Mobile App"/>
                				</div>
                			</div>
                			<div class="row">
                				<div class="col-xs-12 martop">
                					<a href="#" ></a>
                				</div>
                			</div>


                		</div>
                	</div>
                </section><!-- app download end -->
                <!-- end of exmas -->
                <section id="" style="background-color: white"><!-- app download start -->
                	<div class="mini-cta bbottom">
                		<div class="container">
                			<div class="row">
                				<div class="col-md-6 col-sm-6 col-xs-12">
                					<h3 class="text-uppercase"> Registration / Payment  Process</h3>
                					<ul class="list-unstyled seo-bulletlist">
                						<li>Step 1: Go to Google Playstore, search and download ‘PrepOn’ and register your account. Enjoy the free trial! </li>
                						<li>Step 2: To get full access, go to your local easypaisa retailer or use easypaisa app, and send the subscription amount (4,999/month or only 3,333/month if paid quarterly) to account 0333-4242069</li>
                						<li>Step 3: Whatsapp your email id, phone number and transaction id on our whatsapp number</li>
                						<li>Final Step: You will be subscribed within 24 hours and get your PrepOn app updated for full access.</li>
                					</ul>
                				</div>

                				<div class="col-md-6 col-sm-6 col-xs-12 ">
                					<img class="img-responsive img-oliveapp" src="{{asset('public-web/img/mobileapp2.png')}}" alt=" Mobile App"/>
                				</div>
                			</div>
                			<div class="row">
                				<div class="col-xs-12 martop">
                					<a href="#" ></a>
                				</div>
                			</div>


                		</div>
                	</div>
                </section><!-- app download end -->
                <!-- For navigation -->
                <section id="contact">				
                	<!-- Contact Start -->
                	<div class="contact padd">
                		<div class="container">
                			<div class="heading text-center">
                				<i class="fa fa-envelope-o color"></i>
                				<h2>Get In Touch</h2>
                				<div class="bor"></div>
                				<p>Get in touch with us for anything and everything</p>
                			</div>
                		</div>

                		<div class="container">
                			<div class="contact-info">
                				<div class="row">
                					{{-- <div class="col-md-3 col-sm-3 col-xs-12">
                						<div class="info-item">
                							<i class="fa fa-map-marker"></i>
                							{{-- <h4>Address</h4>
                							<p>PrepOn <br/> Township<br/>Lahore </p> 
                						</div>
                					</div> --}}
                					<div class="col-md-4 col-sm-4 col-xs-12">
                						<div class="info-item">
                							<i class="fa fa-envelope-o"></i>
                							<h4>Email</h4>
                							<p>prepon.entrytests@gmail.com</p>
                						</div>
                					</div>
                					<div class="col-md-4 col-sm-4 col-xs-12">
                						<div class="info-item">
                							<i class="fa fa-phone"></i>
                							<h4>Call</h4>
                							<p><strong>P</strong> : 03334242069<br /></p>
                							<p>Available Monday<br/>to Friday between<br/>9am to 6pm</p>
                						</div>
                					</div>
                					<div class="col-md-4 col-sm-4 col-xs-12">
                						<div class="info-item">
                							<i class="fa fa-question-circle"></i>
                							<h4>FAQs</h4>
                							<p>Answers to the most frequently asked questions about PrepOn</p>
                						</div>
                					</div>
                				</div>
                			</div>
                		</div>


                		<div class="container">
                			<h5 class="contactmsg">&nbsp;</h5>
                			<!-- Contact Contents -->
                			<div class="contact-content" id="contactcontainer">
                				<h5 class="text-center">Send Your Message</h5>
                				<div class="bor"></div>
                				<br />
                				<p class="text-center">Note: For account activation queries, please drop a mail to prepon.entrytests@gmail.com</p>
                				<br />
                				<form  action="javascript:;" id="contactForm" method="post">
                					@csrf
                					<div class="row">
                						<div class="col-md-4 col-sm-4">
                							<div class="form-group">
                								<input type="text" class="form-control" name="name" id="contact-name" placeholder="Enter Name..." value="">
                								<label for="name" class="nameMsg commonMsg"></label>
                							</div>

                						</div>
                						<div class="col-md-4 col-sm-4">
                							<div class="form-group">
                								<input type="email" class="form-control" id="contact-email" name="email" placeholder="Enter Email..." value="">
                								<label for="email" class="emailMsg commonMsg"></label>
                							</div>
                						</div>
                						<div class="col-md-4 col-sm-4">
                							<div class="form-group">
                								<input type="text" class="form-control" id="contact-subject" name="subject" placeholder="Enter subject..." value="">
                								<label for="subject" class="phoneMsg commonMsg"></label>
                							</div>
                						</div>
                						<div class="col-md-12 col-sm-12">
                							<div class="form-group">
                								<textarea class="form-control" name="user_message" id="contact-message" rows="6" placeholder="Enter Your Message..."></textarea>
                								<label for="subject" class="subjectMsg commonMsg"></label>
                							</div> 
                							<button onclick="contactUs(this)"  id="sMsg" type="submit" name="submit" class="btn btn-theme">Send Message</button>
                						</div>
                					</div>
                				</form>									
                			</div>
                		</div>
                	</div>
                	<!-- Contact End -->
                </section>

            </div>
            <!-- Main Content End -->		
            <!-- Footer start -->
            <div class="footer text-center">
            	<div class="container link-footer">
            		<div class="row">
            			<div class="col-md-3 col-sm-6 col-xs-12">
            				<p><img src="{{asset('images/logo.png')}}" height="80" width="80" style="margin:0px;"></p>
            				<p>M. Shaukat Ali Road, Lahore, Pakistan</p>
            				<p>prepon.entrytests@gmail.com</p>
            				<p>03334242069</p>
            				<div class="footer-social social">
            					<!--<a href="#" onclick="window.open('', '_blank'); return false;" class="facebook"><i class="fa fa-facebook"></i></a>-->
            					<a href="https://www.facebook.com/PrepOn-636371010108789/?modal=admin_todo_tour"  class="facebook"><i class="fa fa-facebook"></i></a>
            					{{-- <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="google-plus"><i class="fa fa-google-plus"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="linkedin"><i class="fa fa-linkedin"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="youtube"><i class="fa fa-youtube"></i></a>      --}}
            				</div>    
            			</div>
            			<div class="col-md-2 col-sm-6 col-xs-12">
            				{{-- <div class="footer-social social"> --}}
            					{{-- <a href="#" onclick="#" class="facebook"><i class="fa fa-facebook"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a> --}}
            				{{-- </div> --}}
            				<h3>Social</h3>
            				<ul>
            					<li><a href="https://www.facebook.com/PrepOn-636371010108789/?modal=admin_todo_tour" disabled ><i class="right isDisabled"></i><span>Facebook</span></a></li>
            					{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Twitter</span></a></li> --}}
            					{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Careers</span></a></li> --}}
            					{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Testimonials</span></a></li> --}}
            				</ul>

            			</div>
            			<div class="col-md-2 col-sm-6 col-xs-12">
            				{{-- <h3>Resources</h3> --}}
                            {{-- <ul>
                                <li><a href="#"><i class="right"></i><span>Live Practice</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Video Course</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Free eBooks</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Blog</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Discuss Forum</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Current Affairs</span></a></li>
                            </ul> --}}
                            
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            {{-- <h3>Popular Exams</h3>
                            <ul>
                                <li><a href="#"><i class="right"></i><span>ECAT</span></a></li>
                                <li><a href="#"><i class="right"></i><span>MCAT</span></a></li>
                                <li><a href="#"><i class="right"></i><span>PPSC</span></a></li>
                                <li><a href="#"><i class="right"></i><span>CSS</span></a></li>
                                <li><a href="#"><i class="right"></i><span>NTS</span></a></li>
                                <li><a href="#"><i class="right"></i><span>PTS</span></a></li>
                               
                            </ul> --}}
                            
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        	<h3>Download the App</h3>
                        	<ul>

                        		<li><a href="https://play.google.com/store/apps/details?id=com.project.prepon" /><img src="{{asset('public-web/img/play.png')}}"></a></li>
                        	</ul>

                        </div>
                    </div> 
                </div>

                <div class="container">

                	<!-- Footer Copyright -->
                	<div class="copyright">
                		{{-- <p><a class="footerlinks" href="#">Careers</a> | --}} <a class="footerlinks" href="{{url('/privacy/policy')}}">Privacy Policy</a> {{-- | <a class="footerlinks" href="#">Terms</a> --}}</p>
                		<p>&copy; Copyright <a href="#">entrytest4u.com</a> - All Rights Reserved.</p>
                	</div>
                </div>
            </div>
            <!-- Footer End -->


            <!-- Scroll to top -->
            <span class="totop"><a href="#"> <i class="fa fa-chevron-up"></i> </a></span> 

        </div>
        <!-- Wrapper End -->

        <!-- Javascript files -->
        <script src="{{asset('public-web/js/jquery.js')}}"></script>
        <script src="{{asset('public-web/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public-web/js/jquery.nav.js')}}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        {{-- <script src="bootstrap3.0/js/home2.min.js"></script> --}}
        {{-- <script type="text/javascript" src="https://www.google.com/jsapi"></script> --}}
        <script type="text/javascript">
        	function contactUs(id){
			    $.ajax({
			      	method:"POST",
			      	url:"{{route('ContactFom')}}",
			      	data : {user_message:$('#contact-message').val(),email:$('#contact-email').val(),name:$("#contact-name").val(),subject:$('#contact-subject').val()},
			      	dataType:"json",
			      	success:function(data){
			        	console.log(data);
			        	 $('#contactForm')[0].reset();
			           if(data.status){
			           	
			           	swal("Your Message Has Been Send Successfully", "Thanks", "success");
			           }else{
			           		swal("Your Message Has Been Send Successfully", "Thanks", "success");
			           }
			        }
			    });
		    }
        	
        </script>
   {{--  <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/988819829/?guid=ON&amp;script=0"/>
    </div>
</noscript> --}}

<script type="text/javascript">
   {{-- var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-33831595-1']);
    _gaq.push(['_setDomainName', 'oliveboard.in']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    --}}
    function mobileAdClose(){
    	$("#mobile-appAd").remove();
    }
    /* check device */
    if( (/Android|webOS|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
    	$(".openinapp").css("display", "flex");
    }    
   
    $('#myCarousel').carousel({
    interval: 3000
});
    
</script>

</body>	

</html>
