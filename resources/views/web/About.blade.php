﻿<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	 <link rel="stylesheet" href="{{asset('web/css/style.css')}}">
				 <link rel="stylesheet" href="{{asset('web/css/bootstrap.mins.css')}}"/> 
				<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        

		<title>About Us</title>
		<style>
			html {
				scroll-behavior: smooth;
			}
			span{
				position: absolute;
				color: rgb(253, 252, 252);
				left: 30px;
				bottom: 100px;
				right: 0px;
				margin: 2px;
				font-family: sans-serif;
				padding: 3px 0px;
				font-size: 10px;
				transform: translateY(50px);
				opacity: 1;
				transition: all 1s ease-in;
				font-weight: 300px;
			}
		</style>
  </head>
  <body>
	    <!--navBar-->
			<div class="container-fluid bg-black">

        <nav class="container navbar navbar-expand-lg navbar-dark bg-black">
          
   <a class="navbar-brand" href="">About Us</a>
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
     <span class="navbar-toggler-icon"></span>
   </button>
 
   <div class="collapse navbar-collapse" id="navbarSupportedContent">
     <ul class="navbar-nav mr-auto">
       <li class="nav-item active">
         <a class="nav-link" href="{{route('dashboard')}}">Home</a>
       </li>
       <li class="nav-item active">
           <a class="nav-link" href="#" >Services </a>
				 </li>
				 <li class="nav-item active">
					<a class="nav-link" href="#ser" >Terms & Condition </a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="{{route('login')}}" >Sign in </a>
				</li>
				 
         
      
     </ul>
     <form class="form-inline my-2 my-lg-0">
       <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
       <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
     </form>
   </div>
 </nav>
 </div>
 <!--main-->
<div class="container about mt-5 pb-5">
	<div class="row">
		<div class="col-md-12 text-center">
			<h1>About Us</h1>
			<div class="border"></div>
		</div>
	</div>
	

	
	<div class="row">
		<div class="col-md-12">
			<p class="hero-text text-center mt-3">
					Since 2010, Education Premium Home Appliances Company 
					has been turning houses into homes with thoughtfully innovative 
					household appliances. The legacy of ASL – that now stretches 
					over more than 1 decades - started off with the production of 
					Refrigerators. Today, ASL – with reliability as its core 
					value - provides cutting edge Washing Machines, Chest Freezers,
					Vertical Freezers, Split AC & Microwave Ovens.
					Over the last 35 years, ASL has not only developed the largest 
					dealer network but has also established the largest after-sales service
					across Pakistan.
			</p>
		</div>
	</div>
	
</div> 
<div class="row">
		<div class="col-md-12 ">
			<style>
				body{
						margin: 0;
						padding: 0;
				}    
				
				#box2{
						height: 50vh;
						width: 100%;
						background-image: url({{asset('web/slider/img2.jpg')}});
						background-size: cover;
						display: table;
						background-attachment: fixed;
				}
		
		</style>
		</head>
		<body>
				
				<div id="box2">
						<h1></h1>
				</div>
		</div>
	</div>
         
            
               
                  
 
 </div>   
 
 <!--Our Team-->
<div class="container-fluid">

	<div class="container pt-5">
		 
			<div class="row " id="team">
					<div class="col-md-12 text-center">
							<h1>Our Team</h1>
						</div>
						<div class="row">
								
			</div>
				 
			</div>
	</div>
	<div class="container mt-5">
					<div class="row">
							<div class="col-md-4">
									<div class="card wow animated fadeInUp">
											<img src="{{asset('web/images/img12.jpg')}}" alt="card-1" class="card-img-top">
											<span> <h1>Umar Wazir</h1>
														 /Founder and CEO
														 <br>
														 <a href="#"><i class="fab fa-facebook-f"></i></a>
											 <a href="#"><i class="fab fa-twitter"></i></a>
											 <a href="#"><i class="fab fa-youtube"></i></a>
											 <a href="#"><i class="fab fa-instagram"></i></a>
											</span>
									</div>
							</div>
						 
							<div class="col-md-4">
									<div class="card wow animated fadeInRight">
											<img src="{{asset('web/images/img14.jpg')}}" alt="card-1" class="card-img-top">
											<span><h1> Hina khan/</h1>
												/Founder and COO
												<br>
												<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>
									<a href="#"><i class="fab fa-youtube"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
								 </span>
							</div>
							
					</div>
					<div class="col-md-4">
						<div class="card wow animated fadeInRight">
								<img src="{{asset('web/images/img15.jpg')}}" alt="card-1" class="card-img-top">
								<span><h1> Ahsan Ali</h1>
									/General Manager
									<br>
									<a href="#"><i class="fab fa-facebook-f"></i></a>
						<a href="#"><i class="fab fa-twitter"></i></a>
						<a href="#"><i class="fab fa-youtube"></i></a>
						<a href="#"><i class="fab fa-instagram"></i></a>
					 </span>
				</div>
				
			</div> 
</div>

</div>
</div>
</div>
</div>
<br>
</div>

<!--footer-->
<section>
	<footer>
		<div class="container-fluid pt-5 pb-5 bg-dark text-light">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
					<div class="row">
						<h5>My Mart</h5>
					</div>
					<div class="row mb-4">
					<div class="underline bg-light" style="width: 50px;"></div>
					</div>
						
							<p><i class="fa fa-chevron-right" aria-hidden="true"></i> Register</p>
							<p><i class="fa fa-chevron-right" aria-hidden="true"></i> Carrer </p>
							<p><i class="fa fa-chevron-right" aria-hidden="true"></i> JoBs</p>
							<p><i class="fa fa-chevron-right" aria-hidden="true"></i> Terms</p>
							<p><i class="fa fa-chevron-right" aria-hidden="true"></i> Policy</p>
						
					</div>
					
					
					
				
					
					<div class="col-md-3 pl-4">
						<div class="row">
						<h5>About</h5>
					</div>
					<div class="row mb-4">
					<div class="underline bg-light" style="width: 50px;"></div>
					</div>
					<div class="row">
						<p>We are providing all ectronics in easy Installments at your doorstep.</p>
					</div>
					</div>
					
					<div class="col-md-3">
						<div class="row">
						<h5>APPs</h5>
					</div>
					<div class="row mb-4">
					<div class="underline bg-light" style="width: 50px;"></div>
					</div>
				 
					</div>
					<form action="">
						<form>
								<div class="form-group">
										<label>Name</label>
										<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your Name">
										
									</div>
								<div class="form-group">
									<label>Email address</label>
									<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
									
								</div>
								
							 
								<div class="form-group">
										<label >Message</label>
										<textarea placeholder="Type your Message" class="form-control" id="exampleFormControlTextarea1" rows="1"></textarea>
									</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
				</form>
			</div>
	
				</div>
			</div>
		</div>
	</footer>
	</section>
</body>
</html>                       
                              