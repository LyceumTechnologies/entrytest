<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
   
            
            
     
	 <style>
	 @media only screen and (max-width: 768px) {
	
	   #tests-image {width:600px;}
	  }
	  
	  @media only screen and (max-width: 480px) {
	
	   #tests-image {width:340px;}
	  }
	  
	  @media only screen and (min-width:769px) {
	     #tests-image {width:800px;}
	  }
	 </style>
	 

    <section class="content">
        <article class="main">
           <h1>How to Prepare?</h1>
            <img src="images/how-to-prepare.png" title="How To Prepare Entry Test for medical and engineering colleges" alt="how to prepare" id="tests-image" />	   
           
		    
		   


         </article>
            
   <article class="col34" style="margin:0 2% 20px 0; width:95%;">
		   
		   <div class="columns cat-archive" id="sample">
	        <h2><span>Free Practise and Mock Tests</span></h2>			
			<section class="col3">
				<h3>Sample Physics MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructionse043.html?demo=1" title="Sample Physics MCQs"><span class="img-border"><img alt="Physics Mcqs" src="{{asset('new/content/wc/images/physics-demo.png')}}" title="Physics MCQS on entrytest4u"></span></a></p>
				
				
			</section><section class="col3">
				<h3>Sample Chemistry MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructions20c4.html?demo=2" title="Sample Chemistry MCQs"><span class="img-border"><img alt="Chemistry mcqs" src="{{asset('new/content/wc/images/chemistry-demo.png')}}" title="Chemistry MCQS on entrytest4u"></span></a></p>
				
			</section><section class="col3">
				<h3>Sample Biology MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructions9f12.html?demo=3" title="Sample Biology MCQs"><span class="img-border"><img alt="Biology mcqs for entry test" title="Biology MCQS on entrytest4u" src="{{asset('new/content/wc/images/biology-demo.png')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>Sample Maths MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructions6e3b.html?demo=4" title="Sample Maths MCQs"><span class="img-border"><img alt="Maths mcqs" src="{{asset('new/content/wc/images/english-demo.png')}}" title="Mathmatics MCQS on entrytest4u"></span></a></p>
				
			</section><section class="col3">
				<h3>Timed NUST Mock Test</h3>
				<p class="img"><a href="demo/demo-test-instructionsc1c7.html?demo=5" title="Timed NUST Mock Test"><span class="img-border"><img alt="mock tests" src="{{asset('new/content/wc/images/engg-demo.png')}}" title="Timed NUST Mock Test"></span></a></p>
				
			</section><section class="col3">
				<h3>Timed Punjab MCAT Mock</h3>
				<p class="img"><a href="demo/demo-test-instructions13d4.html?demo=6" title="Timed Punjab MCAT Mock"><span class="img-border"><img alt="mock tests" src="{{asset('new/content/wc/images/med-demo.png')}}" title="Timed Punjab MCAT Mock"></span></a></p>
				
				
			</section>
			
		     </div>
			 <h2><span>Entry Test Format of Leading Engineering Universities of Pakistan:</span></h2>
			 <article class="tab-content">
  
  <ul class="tick" style="float:left; margin:0px;">
		<li><a href="blogs/2014/04/02/nust-entry-test-net-format-a-snapshot/index.html" title="NUST" target="_blank">NUST</a></li>
		<li><a href="blogs/2014/05/12/uet-ecat-entry-test-format-a-snapshot/index.html" title="Punjab UET ECAT" target="_blank">Punjab UET ECAT</a></li>
		<li><a href="blogs/2014/04/29/giki-entry-test-format-a-snapshot/index.html" title="GIKI" target="_blank">GIKI</a></li>
		<li><a href="blogs/2014/05/20/kpk-engineering-colleges-entry-test-ecat-a-snapshot/index.html" title="ECAT" target="_blank">KPK ECAT</a></li>
		<li><a href="blogs/2014/04/11/fast-entry-test-format-a-snapshot/index.html" title="FAST" target="_blank">FAST</a></li>
		<li><a href="blogs/2014/08/23/international-islamic-university-iiu-engineering-entry-test-format-a-snapshot/index.html"  title="International Islamic University (IIU"target="_blank">International Islamic University (IIU)</a></li>
		<li><a href="blogs/2014/09/26/muhammad-ali-jinnah-university-maju-entry-test-format-a-snapshot/index.html"  title="MAJU"target="_blank">MAJU</a></li>
   </ul>
</article>
 <div style="clear:both;"></div>
 <h2><span>Entry Test Format of Leading Medical Colleges of Pakistan:</span></h2>
 <article class="tab-content">
  
  <ul class="tick" style="float:left; margin:0px;">
		<li><a href="blogs/2014/06/23/punjab-medical-colleges-admission-test-mcat-format-a-snapshot/index.html" title="Punjab MCAT" target="_blank">Punjab MCAT</a></li>
<li><a href="blogs/2014/06/18/sindh-medical-colleges-admission-test-mcat-format-a-snapshot/index.html" title="MCAT" target="_blank">Sindh MCAT</a></li> 
<li><a href="blogs/2014/05/20/kpk-medical-colleges-entry-test-format-mcat-a-snapshot/index.html" title="KPK MCAT" target="_blank">KPK MCAT</a></li>
<li><a href="blogs/2014/04/27/agha-khan-university-entry-test-a-snap-shot/index.html" title="gha Khan Medical University" target="_blank">Agha Khan Medical University</a></li>
<li><a href="blogs/2014/04/02/nust-entry-test-net-format-a-snapshot/index.html" title="NUST" target="_blank">NUST</a></li>
<li><a href="blogs/2014/10/26/fmdc-entry-test-format-a-snapshot/index.html" title="Federal Medical & Dental College" target="_blank">Federal Medical & Dental College</a></li>
<li><a href="blogs/2014/08/23/international-islamic-medical-college-iimc-entry-test-a-snapshot/index.html" title="International Islamic Medical College" target="_blank">International Islamic Medical College</a></li>

   </ul>
</article>
 <div style="clear:both;"></div>
 <h2><span>Useful Entry Test Preparation Articles:</span></h2>
 <article class="tab-content">
  
  <ul class="tick" style="float:left; margin:0px;">
		<li><a href="blogs/2014/06/03/time-management-in-entry-test/index.html" title="Entry Tests" target="_blank">Time Management in Entry Tests</a></li>

<li><a href="blogs/2014/03/30/how-to-get-most-out-of-your-entrance-test-preparation/index.html" title="aptitude test" target="_blank">How to get most out of your Entrance Test Preparation</a></li>

<li><a href="blogs/2014/03/30/how-to-handle-entry-test-pressure/index.html" title="Entry test preparation" target="_blank">How to handle Entry Test Pressure?</a></li>

<li><a href="blogs/2014/03/30/how-i-prepared-for-nust-entry-test/index.html" title=" NUST Entry Test" target="_blank">How to prepare for NUST Entry Test NET</a></li>

<li><a href="blogs/2014/06/23/important-physics-topics-for-punjab-mcat/index.html" title="Punjab MCAT" target="_blank">Important Physics Topics for Punjab MCAT</a></li>

<li><a href="blogs/2014/07/03/important-chemistry-topics-for-punjab-mcat/index.html" title="Chemistry mcqs" target="_blank">Important Chemistry Topics for Punjab MCAT</a></li>

<li><a href="blogs/2014/04/24/how-to-prepare-for-uet-entry-test/index.html" title="UET Entry Test" target="_blank">How to Prepare for UET Entry Test</a></li>

<li><a href="blogs/2014/07/01/what-to-do-the-night-before-nust-entrance-test/index.html" title="Entry Test" target="_blank">What to do Night Before Your Entry Test</a></li>

   </ul>
</article>
 <div style="clear:both;"></div>
 <h2><span>How to use entrytest4u.com</span></h2>
			 <p>Please Do Not share your account with any one. Doing so would have the following negative effect on your test preparation:</p>
					<ul>
				<li>Your analytics would not remain unique and you would not be able to strategize your test preparation</li>
				<li>Only one person can log in from one account at a time and you would not be able to benefit from our services at the time of your choice</li>
				<li>You shall not be able to take all the mock tests – Mock tests are limited and can be taken only once</li>
				<li>Our system does not repeat questions in a subject until you take all the question, thus sharing may result in missing key questions</li>
                </ul>
        
			<p>Our MCQs touch all the concepts in the FSc coursework. They have been carefully designed to test your conceptual understanding as required by the entry tests. All MCQs are arranged subject, topic and difficulty wise to make your test preparation organized. All MCQs are explained with figures, graphs and diagrams, wherever required. If you are using <a href="index.html" title="An online Entry Test Prepration in Pakistan">entrytest4u.com</a>, you do not need any other preparatory book or guide.</p>


		<p><strong>Good Luck with your Test Preparation! </strong></p>
		<p><strong>Team entrytest4u.com</strong></p>
	
	
	</article> 
 
		
    
	<a class="go-top" href="#top" style="display: none;" title="entrytest4u">Go to top of page</a>
    
            
    </section>



  @endsection