
<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
   
            
            
     
	 	 

    <section class="content">
        <article class="main">
           <h1>Free Entry Test and Mock Test Demo</h1>
            
			<p>Please try our free demo tests without registering an account!</p>
              
		   


         </article>
            
   <article class="col34" style="margin:0 2% 20px 0; width:95%;">
		   
		   <div class="columns cat-archive" id="sample">
	        <h2><span>Demo Tests</span></h2>			
			<section class="col3">
				<h3>Sample Physics MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructionse043.html?demo=1"><span class="img-border"><img alt="" src="{{asset('new/content/wc/images/physics-demo.png')}}"></span></a></p>
				
				
			</section><section class="col3">
				<h3>Sample Chemistry MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructions20c4.html?demo=2"><span class="img-border"><img alt="" src="{{asset('new/content/wc/images/chemistry-demo.png')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>Sample Biology MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructions9f12.html?demo=3"><span class="img-border"><img alt="" src="{{asset('new/content/wc/images/biology-demo.png')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>Sample Maths MCQs</h3>
				<p class="img"><a href="demo/demo-test-instructions6e3b.html?demo=4"><span class="img-border"><img alt="" src="{{asset('new/content/wc/images/english-demo.png')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>Timed NUST Mock Test</h3>
				<p class="img"><a href="demo/demo-test-instructionsc1c7.html?demo=5"><span class="img-border"><img alt="" src="{{asset('new/content/wc/images/engg-demo.png')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>Timed Punjab MCAT Mock</h3>
				<p class="img"><a href="demo/demo-test-instructions13d4.html?demo=6"><span class="img-border"><img alt="" src="{{asset('new/content/wc/images/med-demo.png')}}"></span></a></p>
				
				
			</section>
			
		     </div>
          <div style="clear:both;"></div>
    <div class="columns cat-archive" id="sample">
	    	<section class="col3">
				<h3>UHS MCAT Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions7c7b.html?demo=7" title="Sample Physics MCQs"><span class="img-border"><img alt="Sample Physics MCQs" src="{{asset('new/content/wc/images/uhs-mcat-papers.jpg')}}"></span></a></p>
				
				
			</section><section class="col3">
				<h3>ECAT Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructionsc120.html?demo=8" title="Sample Chemistry MCQs"><span class="img-border"><img alt="Sample Chemistry MCQs" src="{{asset('new/content/wc/images/uhs-ecat-papers.jpg')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>NUST Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions789d.html?demo=9" title="Sample Biology MCQs"><span class="img-border"><img alt="Sample Biology MCQs" src="{{asset('new/content/wc/images/nust-papers.jpg')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>GIKI Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions7497.html?demo=10" title="Sample Maths MCQs"><span class="img-border"><img alt="Sample Maths MCQs" src="{{asset('new/content/wc/images/giki-papers.jpg')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>ETEA Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions572b.html?demo=11" title="mock tests"><span class="img-border"><img alt="Timed NUST Mock Test" src="{{asset('new/content/wc/images/etea-past-papers.jpg')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>NTS Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions80d1.html?demo=12" title="MCAT"><span class="img-border"><img alt="Timed Punjab MCAT Mock" src="{{asset('new/content/wc/images/nts-papers.jpg')}}"></span></a></p>
				
				
			</section>
        </div>
        <div style="clear:both;"></div>
    <div class="columns cat-archive" id="sample">
	       
			<section class="col3">
				<h3>PAF Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions617f.html?demo=13" title="Sample Physics MCQs"><span class="img-border"><img alt="Sample Physics MCQs" src="{{asset('new/content/wc/images/pakistan-airforce.jpg')}}"></span></a></p>
				
				
			</section><section class="col3">
				<h3>TCC Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions9727.html?demo=14" title="Sample Chemistry MCQs"><span class="img-border"><img alt="Sample Chemistry MCQs" src="{{asset('new/content/wc/images/tcc-past-papers.jpg')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>PIEAS Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions3394.html?demo=15" title="Sample Biology MCQs"><span class="img-border"><img alt="Sample Biology MCQs" src="{{asset('new/content/wc/images/pieas-past-papers.jpg')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>AMC Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructionscacd.html?demo=16" title="Sample Maths MCQs"><span class="img-border"><img alt="Sample Maths MCQs" src="{{asset('new/content/wc/images/amc-past-papers.jpg')}}"></span></a></p>
				
			</section><section class="col3">
				<h3>FMDC Past Papers</h3>
				<p class="img"><a href="demo/demo-test-instructions80a7.html?demo=17" title="mock tests"><span class="img-border"><img alt="Timed NUST Mock Test" src="{{asset('new/content/wc/images/fmdc-past-papers.jpg')}}"></span></a></p>
				
			</section>
        </div>
			 
  </article>  
	<a class="go-top" href="#top" style="display: none;">Go to top of page</a>
    
            
    </section>


@endsection
