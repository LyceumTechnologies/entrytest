<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
            
            
<section class="content">
        <article class="main">
       

    <h1>Testimonials</h1>
          <article class="full-width" style="margin:0 2% 20px 0; width:95%;">
				<section class="product-list-full">
				<h2><span>Latest testimonials from successful candidates</span></h2>

         <p>Mohammad Sharjeel, who got 2nd position in GIKI enterance exam and 5th in NUST enternace exam, has sent us this video message about his experience of using entrytest4u.com </p>   
		<iframe width="100%" height="315" src="https://www.youtube.com/embed/cdbUzFtxUdU" frameborder="0" allowfullscreen></iframe>
<p>Azam Nawaz, who got 17th position in the KP Medical entrance exam has sent us this video message about his experience of using entrytest4u.com </p>
<iframe width="100%" height="315" src="https://www.youtube.com/embed/hgW5EVj0Y0U" frameborder="0" allowfullscreen></iframe>
      
   <p>Mohammad Ahmad from interior Sindh, who got admission in FAST-NU Karachi, IBA Karachi, IBA Sukkur and Mohammad Ali Jinnah University, has sent us this video message about his experience of using entrytest4u.com </p>   
		<iframe width="100%" height="315" src="https://www.youtube.com/embed/i9EcAIO_eqU" frameborder="0" allowfullscreen></iframe>				 
				<p>Mr Hassan Ali Anwar from Chichawatni,Sahiwal has sent us this video message about his experience of using entrytest4u.com</p>
				<iframe width="520" height="315" src="https://www.youtube.com/embed/NmuDU-FgJzE" frameborder="0" allowfullscreen></iframe>

<p>ETEA Medical Topper of 2016 Mr. Sualeh Hassan has sent us this video message about his experience of using entrytest4u.com </p>  
				<iframe width="520" height="315" src="https://www.youtube.com/embed/TRa4-zcUJKk" frameborder="0" allowfullscreen></iframe>
				<p>Our student, Abdul Basit Chaaran from Hyderabad has sent us this video message about his experience of using entrytest4u.com</p>
				<iframe width="520" height="315" src="https://www.youtube.com/embed/mGmforfX4JA" frameborder="0" allowfullscreen></iframe>
				
				 <ul>
                            <li>
                		                <h3><span>Adeen Jawad Qazi , NUST Software Engineering (2018)</span></h3>
						                <p>I got admission in Software Engineering at NUST. NUST was always my first priority and entrytest4u.com helped me a lot during my journey. The MCQs with various difficulty levels and the mock tests are really helpful. My concepts were cleared as well as my time management got better with the help from entrytest4u.com. My advice to students would be to do self-study before the entrance exams and to opt for entrytest4u.com for practice, as practice makes a man perfect.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Osama Masood , NUST Chemical Engineering (2018)</span></h3>
						                <p>entrytest4u was very helpful as it focused on the main points required for preparing for any entrance test. I am from an A level background and entrytest4u saved me a lot of time as I did not have to go through the entire FSc books. With the help of the MCQs provided, I learned most of the FSc course and successfully secured admission in Chemical engineering in my desired university NUST. 10/10 I would recommend entrytest4u.com to all students.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Abdullah Sheikh , NUST (2018)</span></h3>
						                <p>entrytest4u helped me a lot. I did 80 percent of my preparation through entrytest4u.com. As per my experience, one should read the whole chapter thoroughly and then take MCQS from entrytest4u.com. This will help you to analyze one’s concepts. MOCK tests helped me a lot before your test. Thanks entrytest4u.com</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Rimsha Maryam , University of Central Punjab (2018)</span></h3>
						                <p>My name is Rimsha Maryam and I successfully got admission in the university of Central Punjab in the department of veterinary and animal sciences. This website is quite beneficial in seeking admission in different universities of your choice. Solved and explained MCQs are very helpful. This website has proper scheme of teaching and testing the students especially separate MCQs with different difficulty levels of each chapter that helped me to check and improve my concepts. Thanks entrytest4u for your help.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Tarique Rashid , Heavy Industry Taxila (2018)</span></h3>
						                <p>I got selected in Heavy Industry Taxila Education city in the program of BS Computer Engineering Program. Thanks to entrytest4u.com that was my major source of entry test preparation.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Hasnain Abbas , UET Peshawar Mechanical Engineering  (2018)</span></h3>
						                <p>My experience of using entrytest4u.com was great. And I would advise all students preparing for entry tests to try entrytest4u.com. My advice to the students would be to start using entrytest4u just after giving their F.Sc examinations in order to get best experience.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Suleman Hasan Khan , NUST Chemical engineering  (2018)</span></h3>
						                <p>I had a great experience of using entrytest4u.com. It prepared me for all sorts of MCQs.  I got 153 in NET3 and I am very grateful to entrytest4u for their friendly interface, variety of MCQs and Mock tests. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Waleed Bin Tahir , NUST Electrical Engineering  (2018)</span></h3>
						                <p>My experience of using entrytest4u.com has been good. I gave NET 2 without any preparation and got 134. I used entrytest4u.com for NET 3 but got 129. However, entrytest4u.com isn’t to be blamed for that. I had very less time to prepare therefore didn’t perform well in test. I wasn’t able to read text books and paid the price for it. The MCQs available on entrytest4u.com are very similar to those in NET. If you read your text books thoroughly and then use entrytest4u.com, In Sha Allah you will get good results. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sameen Zaka , National Textile University Faisalabad (2018)</span></h3>
						                <p>My experience of using entrytest4u was a very good. This website contains a lot of good material and practice MCQs. Now I got admitted in National Textile University doing Software Engineering. I also got admission in FAST in Electrical Engineering, but I want to do Software Engineering so that’s why I did not go to FAST. It is my advice to every junior to read the 
FSc books and then do practice from this website. You will surely get admission in good university.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Haseeb Maka , NED Karachi (2018)</span></h3>
						                <p>I had cleared FAST-NU test, NUST test and some other universities as well. But I went for civil engineering in NED Alhamdulilah. This website helped me a lot. The quality of MCQs is outstanding. Thank you entrytest4u.com to help me get admission in my dream university.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Ali , NUST (2018)</span></h3>
						                <p>I am Muhammad Ali and currently I am in Risalpur. A year ago, I heard about entrytest4u.com for its entry test materials. I immediately registered on it and its registration was worth it. It helped a lot in achieving my dream. It is a very good effort by entrytest4u.com and I strongly recommend it to the students who are preparing for the entry tests. Now Alhamdullilah I am studying in Military College of Engineering, NUST. Thanks to Allah Almighty and then entrytest4u.com.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Noorani , MUET Mechanical Engineering (2018)</span></h3>
						                <p>My name is Din Muhammad Noorani. I use entrytest4u.com from February 2018 and I got selected at NED UET where my merit number was 27 in Sukkur. In MUET Jamshoro my merit number was 26 and I got Mechanical engineering department. I joined MUET finally.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Naveed , International Islamic University Islamabad (2018)</span></h3>
						                <p>entrytest4u.com is good website for sharpening concepts on every chapter of FSc syllabus. I am now studying Mechanical Engineering at entrytest4u.com.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Malik Umair , Hitec University Electronical Engineering (2018)</span></h3>
						                <p>Firstly Allah, my parents & then using this website eased the way to achieve my future goal. I advise all the students to use entrytest4u.com. Practice MCQs 1 year before you appear in entry test sessions. Thanks entrytest4u.com !!!!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Adeel Ahmed , Air University Mechanical Engineering (2018)</span></h3>
						                <p>My passion was to become a mechanical engineer and entrytest4u.com played an important role in achieving my goal. Its interface is very easy and is like you are actually giving the entry test. No matter how good you are in college, you must be familiar with MCQs based test in order to pass them. entrytest4u.com helps you gain familiarity with this type of test and after several MCQ tests you will be able to pass any test easily. Students don’t make the mistake that I did which was of not doing MCQs and just relying on knowledge. A whole year was the penalty for this mistake. Please don't do this. I wish you all best of luck and use entrytest4u.com</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Aasim , UET (2017)</span></h3>
						                <p>
It is a nice self preparatory software and covers whole syllabus for entry test preparation. Also using this gives a good experience of e-learning which is very important for further studies in the university.  </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Amar Fatima Rashid , NUST (2017)</span></h3>
						                <p>Allahamdulliah got admission in GIKI and NUST. It was a wonderful experience. entrytest4u MCQ's helped me a lot to clear my concepts. Thank you entrytest4u.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Arbab Nizamani , FAST (2017)</span></h3>
						                <p>I was the first one to invest money on entrytest4u.com from my hometown and friends. Besides some concerns that I heard back then. Now, I must  say it was the best decision I made for my successful future. I used this website frequently and daily for testing my preparation.
I did qualify for Software Engineering at NUST (SEECS), Physics (SNS) and BS Computer Science at FAST (Islamabad campus). Alhamdulillah, I have achieved my goal of studying in finest university of Computer Science. Thank You entrytest4u. 
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Qaiser Iqbal , UET Taxila (2017)</span></h3>
						                <p>I want to thank entrytest4u website team. You guys are doing a great job. It is an amazing sit and I used it during my entry test days.It was very useful for me and by using it I turned my dream of getting admission into UET Taxila into reality. All credit goes to entrytest4u.com. Thumbs up!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Atiqa Zubairi , NUST (2017)</span></h3>
						                <p>I am pleased to inform you that I have got admission in NUST (college of E&ME in CPTR Engineering) after making the preparation from entrytest4u. Thanks a lot.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Farhan , UET Peshawar (2017)</span></h3>
						                <p>ALLHAMDULLILAH I have been selected for Civil Engineering UET PESHAWAR. Pak prep helped me a lot. The MCQ’s here are totally conceptual and related to the test of engineering and medical colleges, I found it very helpful and I will advise all my juniors to register yourself as soon as possible for the incoming test. Thanks entrytest4u.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Yasir Khan , UET Peshawar (2017)</span></h3>
						                <p>I got admission BS Computer Science in UET Peshawar. I prepare from entrytest4u and ALLAMDULILAH I am selected. Pakrep provided me a platform where I studied and clear my entry test. I am very thankful to entrytest4u.com
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Shirza Qaiser , Lahore Medical and Dental college (2017)</span></h3>
						                <p>Thank you entrytest4u for providing me the platform where I studied and passed my test. Thank you so much.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Ali Sher , Rahbar Medical and Dental College (2017)</span></h3>
						                <p>The usage of this website really proved a great helping hand. I was recommended entrytest4u.com by a friend of mine only a few days before my MDCAT 2017 exam. The best thing about this website is that it runs and helps according to one’s own needs and requirements. I managed to secure a good secure and got admission in Punjab Ranges Medical and Dental College (Rahbar). Currently, I am studying MBBS at this college. The best and the foremost feature of this website is that it provides up to date and solved PAST PAPPER of all the entrance examinations being held all over Pakistan. Really helps understanding the nature and type of questions being asked by the examiner. These past paper feature surely makes this website second to none. I my self-used this website hardly for a month but it really paid me off. I have recommended it to my friends and juniors even during their intermediate days so that they can really have a clear notion about the topics and get prepared well before time and secure even better than me.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Shaista Arshad , Allama Iqbal Medical College  (2017)</span></h3>
						                <p>My name is Shaista Arshad. I am from Pak Pattan. Alhamdulillah I have gotten admission in Allama Iqbal Medical College (AIMC). entrytest4u.com helped me in achieving this success. The mcqs presented by entrytest4u.com are very helpful especially Physics' Mcqs are very helpful and conceptual. After Allah, parents and my very respectable teachers Sir M.Amin, Sir Rai Munir and Sir Razzaq I am very thankful to entrytest4u.com and its team as it remained really very cooperative throughout this period. I suggest all the students to use entrytest4u.com and secure their admission especially the students who cannot afford academies must use this source. I assure those students that they will get Insha’Allah what they want.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Rehma Khan , Nishtar Institute of Dentistry  (2017)</span></h3>
						                <p>I used entrytest4u for my MDCAT preparation and found it very useful indeed. It covers all the necessary topics and has a wide range of MCQs. There are no irrelevant questions and no annoying advertisements. They show you the progress that you make while taking all the tests. It is, overall, a better website than most. Good luck to you all.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Waqar khan , Khyber Medical College (2017)</span></h3>
						                <p>I found it very useful especially your mock tests. It helped me a lot in entry tests.
I got admission in Khyber medical college Peshawar. Thank you so much</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Amir , UET Peshawar (2017)</span></h3>
						                <p>I am from Abbottabad. Got admission in Civil engineering department. Thank you entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Haseeb Malik , NUST (2017)</span></h3>
						                <p>I’m from Sargodha. Thanks entrytest4u for saving me from ACADEMY'S Trash. Now I’m happy that I preferred entrytest4u over academies and I got my admission in Pakistan's No1 University NUST.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Umer , GIKI (2017)</span></h3>
						                <p>From Sargodha. entrytest4u helped me to prepare the GIKI entry test and ALHAMDULLILAH I got admission in Electronics Engineering. Thank you so much entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Hassan Ali Anwaar , Nawaz Sharif Medical College (2017)</span></h3>
						                <p>All thanks to Almighty, I have got admission in Nawaz Sharif Medical College, Gujrat. The true key to success in MCAT is actually your grip on your text books. All you have to do is just to study your book lines deeply keeping yourself far away from the academy books, notes or so. I regard this as soul of MCAT and I found entrytest4u the mirror reflection of this soul, providing you with the MCQs which demand you to pay attention to your book lines, explaining to you the logic based reasons for all MCQs. I strongly recommend it if you want to succeed as bad as you want to breath. Grand salute to entrytest4u team.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Syed Saadullah Shah , NUST  (2017)</span></h3>
						                <p>I belong from Peshawar. I wanted to get admission in NUST. The first NUST NET I gave I got 98 marks and was hopeless. Then I saw this website entrytest4u.com and solved a few MCQ’s from it and straight away I knew this was good material. I got my subscription and started preparing for net 3. I studied till 3 months and ALHAMDULILLAH got 131 marks in net	3. I recommend text books and entrytest4u for better marks in any university entry test.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Farahh Sayyar , UET Peshawar (2017)</span></h3>
						                <p> I am admitted in UET Peshawar. I am very happy from entrytest4u network.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sidra Sheikh , CMH  Lahore (2017)</span></h3>
						                <p>I am from Faisalabad. It’s really helpful and successful experience. I got admission in CMH Lahore just because of entrytest4u. I’m very Thankful to the entrytest4u Team.   </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Noor Mobeen , IBA University (2017)</span></h3>
						                <p>I got admission in IBA University. With my entry tests entrytest4u helped me a lot while preparing. This proved as a worth consuming website for me, where I could practice my weak points. And I got full scholarship in software engineering.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Faiqa Amir , UET (2017)</span></h3>
						                <p>Overwhelming experience as one gets an idea about the challenging entrance tests. I did not use it the last time before my net-3 that is why I lost my marks to score admission in NUST. But got admission in UET, Air Univeristy, and FAST. All thanks goes to Team entrytest4u.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Hadiya Rafique , NUST (2017)</span></h3>
						                <p>I got selected in NUST. I barely had a month to prepare for the NET test. Then I came across this amazing site and without wasting my money on any academy based prep tests, I prepared for the NET from this site, which proved to be helpful. The difficulty level of NUST NET MCQ's was not an issue as they were similar to the ones I had practiced using entrytest4u. Especially the Mock Tests helped a lot. The simulated environment that entrytest4u provided me through its mock tests made me feel at ease during the actual test. Thumbs up for entrytest4u.

</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sadaf Nawab , ETEA (2017)</span></h3>
						                <p>A perfect way to boost up your mind at the end of revising chapters. Mock teats are highly recommended before 1 week of ETEA especially I found Physics MCQs are extremely helpful . Love entrytest4u. Thank You so much. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Fahad , Islamic University (2017)</span></h3>
						                <p>I am admitted in Islamic university and currently doing mechanical engineering. Thanks entrytest4u that was extremely useful in my entrance test preparation.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Atiqa Zubairi , NUST (2017)</span></h3>
						                <p>I am pleased to inform you that I have got admission in NUST (college of E&ME in CPTR Engineering) after making the preparation from entrytest4u. Thanks a lot.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Amir Taimur , UET Peshawar (2017)</span></h3>
						                <p>I got admission in UET PESHAWAR. I did not attended any academy. MCQs of the entrytest4u helped me a lot to clear the ETEA engineering test.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Farahh Syed , UET Peshawar (2017)</span></h3>
						                <p>Salam. I got selected in mechanical engineering in UET Peshawar. entrytest4u.com was instrumental in my preparation and I am thankful to the team for preparing excellent selection of MCQs and mock tests that are very relevant to the entry test preparation. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Aftab Lodhi , UET Peshawar (2017)</span></h3>
						                <p>I did use entrytest4u.com regularly and it was quiet fruitful. I solved each and every MCQ and the explanation of MCQs in the end was quiet help full. Mock test and chapter wise quizzes guided me all along. With the hard work I passed ETEA and got selected in University of Engineering and Technology Peshawar in Electrical Power Engineering which was my first preference. In a nutshell it could not have been possible without entrytest4u.com because it helped me a lot. I would definitely recommend it to other students.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Samar Fatima Rashid , NUST (2017)</span></h3>
						                <p>My experience of using entrytest4u was Wonderful... My BROTHER told me to use this site. I got threshold marks in whatever university i applied. I got admission in GIKI and NUST. In GIKI my merit position was 502 and in NUST I got 130. I'm just mentioning Main universities here for which I focused to get admission. This site helped me a lot to clear my concepts. I suggest juniors not to go any academy and waste money and time. Instead stay in home and do self- study. Practice as much MCQ'S as you can from entrytest4u. Clear all your concepts.
Thank you entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Abdul Basit , GIK (2017)</span></h3>
						                <p>I have gotten admission in GIK Electrical Engineering. Also I got selected in FAST Islamabad. It was a great experience to use entrytest4u. MCQs are conceptual and very helpful in solving questions in exam hall. I would recommend my juniors to use this site its very helpful and try understand each and every question. Don't even ignore a single question. I’m telling you because I really had no one to guide me and I suffered a lot. So I don't want any other to sufffer. SECONDLY you apply must for GIK test because most of the people don't apply for GIK because of the expenses. So take the test. Thanks to Team entrytest4u.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Shahrukh Sohail Janjua , UET Lahore (2017)</span></h3>
						                <p>I got admitted in UET Lahore. I used entrytest4u.com for my ECAT preparation. It helped me a lot. It has been a great experience using your site. It is a very great site especially its mock tests helped me a lot to understand ECAT pattern. Thank you entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Moeez Ahmed , UET Lahore (2017)</span></h3>
						                <p>My experience of using entrytest4u.com is great. I got selected in all universities I applied. I applied in COMSATS I was selected. I also got selected in UET Lahore and UET Taxila. I never went to any academy for preparation and highly recommended others to prepare entry tests from entrytest4u and don't waste your money on academies like kips etc.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Ayesha Tauqeer Aslam , FAST (2017)</span></h3>
						                <p>I got admission in FAST Islamabad. I prepared only through entrytest4u.com and it was super helpful.  </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Farem Khan , UET Peshawar (2017)</span></h3>
						                <p>I have secured my seat for UET Peshawar and also got good marks in NET because of this awesome website. I didn't get any time to practice all mcqs otherwise I would have been able to score better. Than you entrytest4u for helping me to make my future. I will suggest all students to register at entrytest4u now, no matter which class you are in 2nd year or 1st year. One again thanks! </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Zubair , UET Lahore (2017)</span></h3>
						                <p>I am Muhammad Zubair. I came to know about entrytest4u back in 2016 then I intended to Join entrytest4u. entrytest4u helped me in preparation of different competitive test. Though I had not joined any academy, with the use of entrytest4u and self-study I have made it into UET Lahore, UET Taxila (reserved seats for KPK), UET Peshawar, NUST and IST. I will suggest all Juniors to Join entrytest4u.com.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Syeda Hareeba Tirmizi  , NUST (2017)</span></h3>
						                <p>Preparing for the entry tests with the ongoing second year is quiet onerous. Joining the academy, though was helpful in a way, but it was really hectic. I learnt about the site from a senior at college and tried it out. This site is first rate concerning the quality of questions. It benefited me a lot so I recommended it to some of my friends too who were highly satisfied by it. Attempting different questions of the same concepts made my understanding good and I successfully got into NUST in my top preference. Hugely recommend entrytest4u.com. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Urooj Hamid , PIEAS (2017)</span></h3>
						                <p>I am UROOJ HAMID. I joined entrytest4u.com in June 2016 in connection with entrytest4u.com of entry tests for various top ranking Engineering universities. All praise is for ALLAH I successfully achieved my aim. I was offered admission both in PIEAS and NUST.  In PIEAS my merit number is 24 as a whole and 3rd in Electrical Engineering. So going to join PIEAS. I thank you team entrytest4u for polishing my potential.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Farhan Farooq , UET Peshawar (2017)</span></h3>
						                <p>ALLHAMDULLILAH by the grace of Almighty Allah and with the prayers of my beloved ones I have been selected for UET Peshawar. entrytest4u.com was very helpful to me the MCQ’s of entrytest4u are of great quality I enjoyed solving MCQ’s and believe me it helped me a lot in ETEA Engineering test. I am very thank full to team entrytest4u for providing me such quality MCQ’s and I also got good marks in NUST test but I have chosen UET Peshawar as my Engineering university. I will suggest all the fsc students to register for entrytest4u as soon as possible the best thing which I liked the most, entrytest4u will give you solution of questions which was very helpful in my point of view. At last I will again thanks entrytest4u and will suggest all the students to get register them self on entrytest4u.com. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Hannan Faryad , GIKI (2017)</span></h3>
						                <p>My name is Muhammad Hannan Faryad and I have been selected In both GIKI and NUST for the year 2017.Before using entrytest4u i had no idea what I was doing and couldn't get good marks in NET 1 and NET 2.But a month before NET 3 I came to know about entrytest4u.COM and registered there. My experience with entrytest4u was wonderful I practices each topic multiple times which enabled me to master perfection in MCQS.In just one month I had complete grip over MCQS.I gave GIKI entrance test and NET 3 and to my surprise I got admission in field of my choice (CS) In both universities. My merit number was 1629 and 1131 respectively.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Mashood Javaid , GIKI (2017)</span></h3>
						                <p>It was very helpful in preparing the entry test exams. I will recommend entrytest4u.com to every student preparing for entry test exams specially A level students. Mock test are very useful. I remember that I was solving the mock papers before the day I was going to appear in GIKI entry test next day. I only able to score 30 in the first section because there was negative marking in it. But after doing mock paper I decided to do only the answers which I know well. The rest I left in the entry test of GIKI. And I was able to get admission there. Thanks Team entrytest4u</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Omar Wajid , GIKI (2017)</span></h3>
						                <p>For some reasons, I only had almost a month to prepare for my university entry tests. entrytest4u.com was very helpful in practicing different type of questions mostly the type you'd see on the exam paper. Study the course thoroughly and practice MCQs on entrytest4u.com and you're good to go. GoodLuck! Thanks Team entrytest4u. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Bilal Ahmed ,   (2017)</span></h3>
						                <p>I did not get admitted in any universities except case university this site is amazing but I did not used it properly I was admitted in kips academy also my lots of time is wasted in doing so I recommend others to use entrytest4u.com only and do not waste your time on institutions because ECAT entry test preparation time is so limited you should practice every concept of Math and Physics on your register thanks.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Abdullah , Comsats Wah (2017)</span></h3>
						                <p>I belong to WAH Cantt. I used this website for two months and I think it is good and help me alot in preparation due to which I got 90 percent marks in NTS and got admission in Comsats Wah on big scholarship. Thanks team entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Abdullah Tahir , NUST (2017)</span></h3>
						                <p>I got 145 in Net 3 and got admission in Electrical Engineering at NUST.
 entrytest4u.com helped me out a lot in my preparation. Practice test session is a great course of clearing concepts. These are the certain things I would like to highlight. Thanks team entrytest4u.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Mahad Niazi , NUST (2017)</span></h3>
						                <p>Well entrytest4u.com proved to be very fruitful for my success in the days of entry test, especially the subjects of Maths and physics were quite useful, I attempted its practice MCQ’s and there were some MCQ’s which were almost same in net along with slight change of values. I got 158 marks in net 2, on the basis of which I got admission in Mechanical engineering at SMME NUST. But yes! Don't forget that I used entrytest4u.com for MCQ’s practice but first before it I prepared my books well, solved almost all question of Maths except the long questions and I go through each and every question that I practiced MCQ’s from entrytest4u.com, and same is the case with physics. On the test day stay calm and believe that u have prepared everything thing, and don't forget time management, divide each portion and give them their specified required time, BEST OF LUCK. Once again Thanks to Team entrytest4u. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Mehtab Jahan , PIEAS (2017)</span></h3>
						                <p>I got 281 marks in Ecat (1st position in Bahawalpur) and got selected in NUST Mechanical Engineering. I also selected in PIEAS for Mechanical Engineering. Finally I got admission in PIEAS in Mechanical Engineering. It is due to entrytest4u.com and blessing of Allah. Thanks Team entrytest4u. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Rehan Khalil , NUST (2017)</span></h3>
						                <p>By the grace of ALLAH I got selected in Mechatronics department in NUST.
My NUST merit number is 982. I also got selected in GIKI merit number is 401 and I got 205 marks in ECAT. Thanks to team entrytest4u.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Naqash Ahmed , Comsats Abbottabad (2017)</span></h3>
						                <p>I got admission in Comsats Abbotabad (Power Engineering) and also got Admission in UET (Telecom). Thanks to team entrytest4u for providing great material for the preparation. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Farahh Sayyar , UET Peshawar (2017)</span></h3>
						                <p>Salam I am selected in mechanical engineering in UET Peshawar. entrytest4u.com helped me a lot in my preparations. Thanks team entrytest4u</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Shah Nawaz , NUST (2017)</span></h3>
						                <p>I finally get admission in my dream university NUST. entrytest4u.com helped me a lot to get into my dream university in 2017. I am thankful to entrytest4u team. It is very best website I have ever seen on Internet. One best thing about entrytest4u website is there is no ad that confuse you during your preparation.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Junaid Saleem , Air University (2017)</span></h3>
						                <p>entrytest4u.com helped me alot in my preparation for test. I also selected in COMSATS LHR & ISB campuses and in FAST PWR campus but I prefer AIR UNIVERSITY ISLAMABAD because this university is working under the supervision of Pakistan Air Force.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sobhia Nizamani , University of Sindh (2017)</span></h3>
						                <p>I belong to rural Hyderbad, I never attended expensive private colleges and tuition centers. So I could not achieve good grades in matric and inter, but I continued learning online, and I had knowledge. I managed to make 68.4 cpn for admission and I was able to choose any department of Computer Science in University of Sindh. Thank You team entrytest4u for your sincere efforts for making distance learning possible.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Afaan khan , UET Peshawar (2017)</span></h3>
						                <p>I got admission in UET Peshawar in civil engineering program my merit no. is 75, where I wanted to be. I also got selected at GIKI in chemical engineering my merit no. was 455. entrytest4u.com helped me allot to achieving my goals. If someone wants to know how I get admission, I will probably prefer them to entrytest4u.com
well done and keep it up team entrytest4u. Thanks allot.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sadaqat Ali , NUST (2017)</span></h3>
						                <p>Assalamualaikum! My name is Sadaqat Ali and I am from Islamabad. I had selected in SEECS- NUST in BS Computer Science Program. It always was dream for me to take admission in NUST, but by grace of Allah Almighty, my parent’s best prayers for me and my hard work my dream came true. Also entrytest4u.com helped me a lot to prepare for NET because I didn't joined any academy or extra book. Just prepared through HSSC course books and practiced MCQ’s from entrytest4u.com. Thank You Team entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sualeh Hassan , ETEA Medical Topper (2016)</span></h3>
						                <p>I topped the ETEA medical test 2016 throughout the province. Thanks a lot to entrytest4u.com. Seriously it was quite helpful specially the ETEA mock tests and the chapter wise MCQs. It was a very good experience. Thankyou entrytest4u team.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Abdulbasit Chaaran , LUMHS (2016)</span></h3>
						                <p>The first thing worth highlighting is that the MCQs of entrytest4u.com aren't from a single board which makes the student more prepared for their respective entry test. After that even the most procrastinating person would end up studying something as studying through entrytest4u.com is much easier and comfortable than studying normal books (Though Book reading is very necessary). The reason I'm saying that it is more comfortable is because of the fact that we students find tablets, mobile phones, computers etc more comfortable than normal books. I got into the university of my pick and I'm pretty thankful to entrytest4u for making me learn new stuff which weren't in my regular books, surely I'm grateful.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Omer Wajid , GIKI (2016)</span></h3>
						                <p>For some reasons, I only had almost a month to prepare for my university entry tests.
entrytest4u.com was very helpful in practicing different type of questions mostly the type you'd see
on the exam paper. Study the course thoroughly and practice MCQs on entrytest4u.com and you're 
good to go. GoodLuck!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Mashhood Javaid , GIKI (2016)</span></h3>
						                <p>I am Mashhood Javaid from Shakargarh, Punjab. entrytest4u.com was very helpful in preparing for the entry test exams. I will recommend entrytest4u to every student preparing for entry test exams specially, A level students. Mock test in the website are very useful. I solved the mock papers a day before I was going to appear in GIKI entry test. I was only able to score 30 in the first section because there is negative marking in it. But after doing mock paper I decided to only attempt MCQs for which i know answers very well. The rest I left in the entry test of GIKI and I was able to get admission there.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Irfan Nasir , NED & Mehran University (2016)</span></h3>
						                <p>My Name is Irfan Nasir and I'm from Gilgit. I recommend this website for the students who want to appear in ECAT or MCAT exams. I've passed NED Entrance test, thanks to entrytest4u and Now I have gained admissions in Mehran University. Thanks a lot entrytest4u.com.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sheikh Nabeel , Nishtar Medical College (2016)</span></h3>
						                <p>I am Sheikh Nabeel Yaqoob from Azad Kashmir. Alhamdulillah, I have got admission in Nishtar Medical College Multan for MBBS.  I did my preparation via entrytest4u.com and it proved to be very productive and helpful. It is one of the best links by which one can achieve excellent results. In short, It provides everything that one requires in entry test. I will recommend all medical students to use this site for good results in UHS MCAT. After thanking to my Allah, mother, family, relatives, teachers and friends I am thankful to entrytest4u.com. I proud of and salute founder of entrytest4u.com who presented such a valuable and precious gift for the students. Marvelous job, thanks a lot again!!!!!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Shakeel Ahmed , NUST (2016)</span></h3>
						                <p>With the help of entrytest4u.com I have gotten admission at NUST. If I had known this site earlier, I am sure to have improved my merit number at NUST. Thanks to entrytest4u.com that I am able to pursue my engineering studies.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Syed Muhammad Zafar Rizvi , IST (2016)</span></h3>
						                <p>I prepared through entrytest4u.com and was accepted for Aerospace Engineering at Institute of Space Technology (IST). entrytest4u.com is an excellent website and much better than joining any crowded academy. Five starts to entrytest4u.com!!
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Daniyal Babar Warraich , NUST (2016)</span></h3>
						                <p>It was a really good experience of using entrytest4u.com for entry test preparation especially the mock tests. These tests give you an idea of how to manage your time during test which is the most important factor. entrytest4u team is doing a great job!!!
 </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Uzair Siddiq , UET Peshawar (2016)</span></h3>
						                <p>I hardly had 45 days to prepare for the entrance test. I didn't know what to do at all but then I found out about entrytest4u.com. Due to the MCQs practice from entrytest4u, I got admission in UET mechatronics engineering, NUST economics, FAST Islamabad computer science and COMSAT Islamabad electric engineering. I finally chose UET to pursue my higher education. I strongly recommend entrytest4u to all my juniors.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Rushmah Saleem , NUST (2016)</span></h3>
						                <p>I got admission in NUST in Computer Engineering and it is all because of entrytest4u.com. entrytest4u is really helpful and in my opinion it is the best website for entrance test preparation.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Talha , NUST (2016)</span></h3>
						                <p>Being an A levels students, I was worried about how to prepare for entrance tests which are based on FSc syllabus. I was recommended entrytest4u.com by one of my friends and wow…. it has been a great experience. I did not join academy and focused on self-study through entrytest4u.com. MCQs are very conceptual as per the needs of the entrance tests. I was able to practice difficult practice levels as per my needs. I found Mock test feature to be the best on the site. I would just say, go for it! </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Salman Zahid , NUST (2016)</span></h3>
						                <p>I am from Mandi Bahauddin and got selected in National University of Science and Technology (NUST) Islamabad. It's a good experience to prepare from entrytest4u. A great initiative by the team. At home I could study better and focused. Much better than joining any academy.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Noman Abbasi , NUST (2016)</span></h3>
						                <p>I am selected in NUST Roll no 112609163 in Geo Informatics Engineering due to entrytest4u.com. I was also selected in FAST and Air University. This site is very helpful. MCQs there are very important and I suggest all prospective university students to join entrytest4u.com. Thanks entrytest4u team.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Faizan Abid , NUST (2016)</span></h3>
						                <p>Team entrytest4u, I got admission in NUST and I used your site for my NET preparation. It's been a great experience for me to prepare from entrytest4u. It helped me in preparations regarding NET specially theyou’re your site takes the test. After practicing on entrytest4u.com I felt less stress in NET because I got used to the test Pattern. Thumbs up for entrytest4u!!!!  
 </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Abdul Shakoor , Islamic International University (2016)</span></h3>
						                <p>I am from Khushab. I prepared for my entry test through entrytest4u.com and got admission at IIU. The website is amazing and very useful for the students preparing for their entry tests.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Saad Qureshi , NUST (2016)</span></h3>
						                <p>My name is Muhammad Saad Qureshi from Karachi. I got selected in both PIEAS (25th position) and NUST (Electrical Engineering). It is all firstly because of the grace of my ALLAH and the prayers of my Parents and secondly because of the excellently prepared course outline of your tests. I practiced them a lot and even some MCQ's matched with exact figures. Your work no doubt is matchless to any test preparer. 0/0 (infinite cheers) to all the team of entrytest4u!!!... Thanx a lot J.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Ommair Ahmed , NUST (2016)</span></h3>
						                <p>Getting admission in NUST was my dream. When i gave NET 1st time I got only 96 marks. I was really disappointed but then I decided to join entrytest4u.com and didn't join any academy. The website helped me a lot. I learnt each and every question from entrytest4u and I found MCQs very suitable to NET standards. I also found mock tests to be very useful. Mock test gave me experience to solve NET paper and due to entrytest4u I scored 133 in NET and got admission in NUST PNEC Mechanical engineering ......Thank you entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>RIZ KHAN , Bahria University (2016)</span></h3>
						                <p>entrytest4u gives the platform in your house to focus on your weakness not like the other academies in which they have limited time and they are costly. Alhamdulilah i got admission in computer engineering at Bahria university. I have got admission in GIKI but due to expenses, I am going for CE at Bahria University. I would recommend students to use the website during the FSc. It is beneficial for 1st, 2nd year and entry test. entrytest4u gives the better platform than other academies</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Shahzaib Siddiqui , FAST NUCES (2016)</span></h3>
						                <p>This website is best for preparation of entry tests. I also joined academy but the day I gave tests I realized that joining academies is only a waste of time and money. Academies do teach some tricks and shortcuts in maths and physics but believe me none of them helped or benefited us in entry test. Just prepare thoroughly from your FSc books and for chemistry use Punjab textbook board's book. Just thoroughly read and learn your FSc books and solve MCQs on entrytest4u. Do practice each and every questions in maths section. And please please please don't waste your time and money in academies.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Syed Muhammad Ali , NUST (2016)</span></h3>
						                <p>"Say Not The Struggle Nought Availeth", this is my word of advice to all my fellow students who have been working hard to secure their future in one of the top universities of Pakistan. In my journey through the tough preparation of the entrance exam, entrytest4u has been a great companion. They provided me with not only relevant questions, but also the explanation of their answers. The mock tests proved very helpful in self- assessment, learning time management and boosting my confidence for the test. I was able to secure admission in Mech. Engg at NUST only by the grace of Allah, my constant hardwork and the guidance from my teachers and entrytest4u.  Trust me, the full membership fee is worth the buck :-)</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Talha Naveed , NUST (2016)</span></h3>
						                <p>entrytest4u provided me with all the relevant and valuable resources to prepare for NUST entry test. I tried the entry test two times before without any guideline but I could not score sufficient score. Third time, I scored 145/200 and easily got admission in the first merit list. The best part, entrytest4u covered all my needs and helped me cope up without any academy! Thanks :)
 </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Sarib Shahid , NUST (2016)</span></h3>
						                <p>I heard about this website from my cousin and I started studying from there and Alhamdullilah I got selected in NUST. This site is just amazing provides thousands of MCQs which helped me in developing my concepts and skills. Some shortcuts are also here which I found very useful. I advise all the students to check it out and you will realize that it is an amazing source.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Mehreen Qasim , AMC (2016)</span></h3>
						                <p>I solved the mcqs and mock tests prepared by entrytest4u.com. They helped me a lot and now I am a student of Army medical college Rawalpindi</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Fahad Ahmad Arsal , FAST NUCES (2016)</span></h3>
						                <p>I was worried about my entry test but then I got this amazing site. Each MCQ is explained brilliantly. This gives it an upper edge over all the other websites. I did not join any academy as it is only waste of money and time. I applied for FAST and NUST and got admitted in both. Thanks to Allah, My Concepts and entrytest4u.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Umer Javaid , NUST (2016)</span></h3>
						                <p>entrytest4u.com is remarkable and full of learning. I believe it is a great initiative by your administration to set up such website having relevant MCQs with commendable explanatory answers. What makes it more lucrative is its time orientated mock tests. I really extol your efforts and hope that you deliver quality work in days to come. I got into NUST, GIKI, FAST and LUMS. I finally enrolled in NUST Civil engineering program.
</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Kashif , NUST (2016)</span></h3>
						                <p>I got admission in Military College of Engineering NUST, department of Civil Engineering. I am thankful to entrytest4u.com team. I learned a lot from your site. I just went through the course books and practiced your MCQs and achieved my dream of getting into NUST. I also encourage other students to go through entrytest4u MCQs. Thanks a lot.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Moazin Khatti , NUST (2016)</span></h3>
						                <p>I started using entrytest4u back in May or June as far as I remember. I prepared only from Punjab Board Books and practiced through entrytest4u. I wanted to study Electrical Engineering. I applied to GIKI and got selected. My merit number was 67. I applied to PIEAS and got selected my merit number was 100. I also applied to NUST and got selected in EME College Rawalpindi. Merit number was 490. As far as my experience with entrytest4u is concerned, I would say it was helpful. To conclude I would say entrytest4us was nice and I am happy Alhamdulilah. I achieved what I wanted to achieve and entrytest4u helped a lot.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Seemab Zafar , NUST (2016)</span></h3>
						                <p>I have been selected in NUST as well as in UET and PIEAS (merit no 48).But I have selected NUST and will go for electrical engineering in SEEC school of NUST. It was a wonderful experience to be a part of entrytest4u.com. I really learnt a lot and my MCQs solving speed improved significantly. The explanations of answers and smart techniques were really helpful. Thanks entrytest4u.com</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sufyan Ahmad Qureshi , NED (2016)</span></h3>
						                <p>I didn't get admission in any center for test preparation I just joined entrytest4u.com. It was really helpful for me and I secured my seat in engineering university.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Usama Bin Nadeem , NUST (2016)</span></h3>
						                <p>I wanted to thank your team for providing such a great assistance to students.  I was a mediocre student in class. I scored 114 in NET-1, 125 in NET-2 (used entrytest4u) and 136 in NET-3 (used entrytest4u). Before my NET-3 I scored 150,145,131 in the entrytest4u NUST Mock tests. I surely could have scored a lot better than 136, but I was not feeling well during test because of travel sickness. Anyways, I just gave it one week for NET-3, took lots of tests and scored enough to get me a seat! :)</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Hamza Tanveer , GIKI (2015)</span></h3>
						                <p>I am very thankful to you because of your website. I secured admission in GIKI mechanical last year with a merit number of 286. I also scored 140 in net 3 and got selected in fast as well as Air university. This was all because of your efforts that you have put into your MCQs. I will recommend juniors to study from here because these 2000 rupees are worth more than anything.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Arooj Aslam , COMSATS (2015)</span></h3>
						                <p>After being disappointed by overcrowded academies, I switched to entrytest4u.com for my NTS preparation. I used some other books and entrytest4u.com to prepare for my entry tests. Neatly arranged MCQs, analytics of my performance, log of my mistakes, mock tests were really helpful in improving my preparation. Kudos to entrytest4u team and highly recommended for all students.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Hoorya Faatmaa , Nawaz Sharif Medical College  (2015)</span></h3>
						                <p>By the grace of Allah, I was able to secure admission in a medical college through UHS MCAT 2015, and entrytest4u helped me tremendously in preparation and achieving success. entrytest4u is a very well designed website, with tons of questions and answers, as well as easy to use navigation and comprehensive stats. It helped me to prepare daily with specific objective and constant progress. The mock test were highly helpful at the end of my preparation. The whole study planning offered by entrytest4u, contributed greatly toward my success in MCAT.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Azwa Rashid , Shifa Medical College (2015)</span></h3>
						                <p>After doing A-levels, I was looking for a good resource to prepare for my medical entry test preparation. My friend suggested to use entrytest4u.com. This is the best way to prepare for medical entry tests. The site structure is simple, easy to follow and MCQs are of good quality. Highly recommended!!!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Hayat Hamza , GIKI (2015)</span></h3>
						                <p>You people are doing great job. Questions are of good standard and mock tests very helpful.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Asif Haider , NUST (2015)</span></h3>
						                <p>Thank you Sir! This site proved very helpful for my NET-3 preparation. Difficulty level of NUST NET MCQs was similar to the ones in entrytest4u.com "Practice Tests". I am very thankful to you.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Bisma Haider , NUST (2015)</span></h3>
						                <p>This is the best project I have seen related to entry test preparation. I suggest entrytest4u.com to almost everyone who ask me on how to prepare for NUST NET. Keep working hard and please keep this project alive. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Noor ul Ain Munir , GIKI (2015)</span></h3>
						                <p>My Name is Noor ul Ain Munir, Allahmudullah I got Admission in GIKI in the Faculty of ES (Batch 25) and the credit for this goes only to entrytest4u,com. The best website for the preparation of entry test of any university. I didn't join any academy for the preparation of entry tests. I just practiced MCQ's from entrytest4u, It is the only website  which prepares you for any type of entry test and it is perfect in every way for the preparation of entry tests. If you use entrytest4u for the preparation of entry test you need not to join any acadmy e.g kips, Star etc which are only made for earning money by doing "Darr ka Business". I wish I could tell every student out there preparing for entry test that they don't need to join any academy. All they need is entrytest4u for the preparation of entry test. A salute to the team of entrytest4u for their sincere efforts.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Faran Saeed , GIKI (2015)</span></h3>
						                <p>(A.O.A) To all Members of entrytest4u.com. First of all I would like to thank dear ALLAH for my admission at GIKI in the faculty of Electrical Engineering. My merit position no is (23). I also applied in NUST where I was offered course at EME, Rawalpindi(merit no: 187). This all due to my parents prayers & brilliant efforts of entrytest4u team. I felt that their mock test helped me a lot in managing time during entry tests. I strongly recommend all the juniors to join entrytest4u.com as early as possible for their bright & successful future. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Shah Nawaz , FAST NU (2015)</span></h3>
						                <p>By the grace of Allah almighty I got selected in FAST NU Lahore. Thank you entrytest4u.com!!!!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Ahsan Zafar , NED (2015)</span></h3>
						                <p>I got admission in NED University Of Engineering and Technology, Karachi.. :). I did not study in any academy for ECAT but I did practice on entrytest4u and read my course books. It`s a good website, better than other all websites.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Raheel Masroor , NUST (2015)</span></h3>
						                <p>Alhamdulillah!  It has been a great entry test season for me. I secured 37 merit position in GIKI, 153 merit position in PIEAS and 233 merit position in NUST. Finally I got myself enrolled in NUST(SEECS) for BS EE(Electrical Engineering). I didn't join any Entrance test preparation academy. The two things which were most helpful for me in the Entrance tests were my two year concepts and entrytest4u.com MOCK Tests. These tests helped me manage my time during the real test. So the website was very helpful in my preparation.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Ibraheem Muhammad Nadeem , GIKI (2015)</span></h3>
						                <p>entrytest4u.com helped me a lot in my entry test preparations. I am an A levels student and I had a very short span of time to cover a huge course. Alhamdulillah through entrytest4u.com and my teachers I was able to excel. Thanks to Almighty Allah. Other universities in which I got accepted are NUST, FAST, Bahria and COMSATS.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Ahmer Asif , Ayub Medical College (2015)</span></h3>
						                <p>I am using entrytest4u online test preparation since January.  I got very good position in KPK entry test and got admitted in Ayub Medical College.  I will suggest all the students to get a entrytest4u.com account to get admission in the university or college of their choice. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Bilal Shahid , UET (2015)</span></h3>
						                <p>Sir I got Selected in GIKI , NUST , UET and PIEAS .... wherever I applied I got selected :). Its all because of the efforts of your team. Thanks to you for the excellent quality MCQs. I tried them a lot.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Abdullah Nasir , NUST (2015)</span></h3>
						                <p>For the first few days during my preparation for NUST and other entry tests I found it pretty difficult to deal with the heaps of syllabus that was yet to be revised. I already knew about entrytest4u.com but I hadn’t by then registered to it. Then one day one of my teachers told me about the site. I practiced a few MCQs and I found them great for my preparation. All I did was that I revised some subject and then practiced MCQs of various difficulty levels from the site. Alhamdullilah I got admitted in all renowned institutes namely NUST, GIKI, AIR and IST. Since I appeared in all three NETS I would suggest everyone that entrytest4u.com is the perfect option for preparing for NUST. In my personal opinion this is an option everyone should avail and refrain from wasting their time at the academies. And trust me I was a mediocre student. Well I did pass my matriculation and intermediate with good marks but I never regarded myself as one of the extraordinary students. I am thankful Alhamdullilah to both Allah Almighty and to entrytest4u.com for making me capable of passing the entry tests. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Rehan Ali , GIKI (2015)</span></h3>
						                <p>I have used entrytest4u.com for my entry test preparation. I found it very helpful and got admission in GIKI. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sohaib Siddiqui , GIKI (2015)</span></h3>
						                <p>I got admission in GIKI in electrical engineering. It was possible due to entrytest4u.com specially mock test, which give the idea of test, mind mapping and time. Again thanks a lot!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sahir Baig , GIKI (2015)</span></h3>
						                <p>Sir I got into GIKI in Faculty of Mechanical Engineering and your website entrytest4u.com helped me a lot, especially your mock tests and the analysis system for practice tests. I am here to thank all the team members for their sincere efforts. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Syed Sammer Abbas , NUST (2015)</span></h3>
						                <p>Alhumdulillah with grace of Allah Almighty, prayers of my parents, efforts of my teachers and specially efforts of entrytest4u.com team I with NUST ROLL NO: 105710153 from Gilgit have secured admission at NUST for Mechanical Engineering. I strongly recommend entrytest4u.com for juniors to take advantage of it and don’t waste their precious time in academies and other below standard resources.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Fiza Ashfaq , AIMC (2014)</span></h3>
						                <p>I got admission in the Allama Iqbal Medical College, Lahore. I complemented my preparation with entrytest4u.com. It is a great resource for self preparation for entry tests and I would recommend it for all students preparing for the Punjab MCAT.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Bilal Shahid , NUST (2014)</span></h3>
						                <p>With the help of your MCQs I got admitted in NUST, UET, GIKI and PIEAS. Thank you very much for this favor sir.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Sajjad Akbar , UET Lahore  (2014)</span></h3>
						                <p>I got admission at UET Lahore :). It is all because of the efforts of your team. Thanks to you for the excellent quality MCQs. I tried them a lot.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Muhammad Usman , UET Lahore  (2014)</span></h3>
						                <p>I got into Electrical Engineering at UET Lahore. Thanks to entrytest4u which helped me to prepare for my test. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Durga Shanker , Isra Univeristy Hyderabad  (2014)</span></h3>
						                <p>One of the best indeed!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Abdul Muiz , NUST (2014)</span></h3>
						                <p>This website is really help full. Highly recommended to everyone!!</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Hasan Shoaib Siddiqui , GIKI (2014)</span></h3>
						                <p>I am enrolled in Batch#24 of Mechanical Engineering in GIKI where I scored the merit no. 32. I also applied in NUST where I was offered admission at EME, Rawalpindi (Merit no. 512). The fact is that unlike most students, I did not study in any academy but thanks to my wonderful college professors, entrytest4u.com and a few entrance test books, I scored more than my classmates who prepared their entrance tests in various academies (namely KIPS). The feature of entrytest4u.com which I found most useful was the simulation of entrance tests (Mock tests) which helped me develop a rough idea of how to manage my time.</p>
				
                            </li>
                        
                            <li>
                		                <h3><span>M. Aibak Arshad , NUST (2014)</span></h3>
						                <p>I got selected in both NUST mechanical engineering and PIEAS mechanical engineering. I only prepared from entrytest4u.com. It is a very helpful site and I would recommend you guys to prepare from it rather than waste your time & money in useless academies. </p>
				
                            </li>
                        
                            <li>
                		                <h3><span>Farrukh Aqil , NUST (2014)</span></h3>
						                <p>entrytest4u.com was a great help to me. I have been admitted to NUST for mechanical engineering. I took practice MCQs from entrytest4u.com and solved the online test papers on the website. It was very helpful in terms of self-analysis or as progress tests. What I found most useful was that each question had a well-defined answer which helped me a lot to remove my confusion! </p>
				
                            </li>
                        </ul>
			    </section>
			</article>
      </article>
<aside>
    <section>
    <p class="img"><a href="account/register.html" title="register"><span class="img-border"><img alt="Register Now for Online Entry Test Prepration" src="{{asset('new/content/wc/images/insights.jpg')}}" title="Register Now for Online Entry Test Prepration" width="300"></span></a></p>
			</section>
            <section class="col3-content">
			<p class="img"><a href="account/register.html" title="Entry test preparation Pakistan"><span class="img-border"><img alt="Entry test preparation Pakistan" src="{{asset('new/content/wc/images/exams.jpg')}}" title="Entry test preparation Pakistan" width="300"></span></a></p>
			
			</section>
		
			
	</aside>
   
 
		
    
	<a class="go-top" href="#top" style="display: none;">Go to top of page</a>
       
</section>

@endsection
