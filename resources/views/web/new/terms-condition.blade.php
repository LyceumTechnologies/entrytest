<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
            
            
    <section class="content">
        <article class="main">
       <h1>TERMS AND CONDITIONS  OF WEBSITE USER’S AGREEMENT</h1>


<p>        PLEASE READ THE FOLLOWING TERMS AND CONDITIONS CAREFULLY BEFORE ENTERING THIS WEBSITE. </p>

<p>This is an Agreement between you acting on your own behalf or acting as a guardian on behalf of a minor (hereinafter referred to as "you" or "your") and entrytest4u (pvt) Limited, a Company incorporated in Pakistan under the Companies Ordinance 1984 (hereinafter referred to as "entrytest4u" or "we" or "our") that owns and governs the access to and use of www.entrytest4u.com website (hereinafter referred to as "Website"). When you access and/ or use the Website, you agree to be bound by these Terms and Conditions and the Privacy Policy of entrytest4u.com. </p>

<h2>PROPERTY OF entrytest4u</h2>
<p>This Website has been hosted by or on behalf of entrytest4u and you agree that you will be allowed to access and use the Website only if you agree to the terms and conditions as set out below and the privacy policy of entrytest4u. </p>

<h2>ELIGIBILITY </h2>
<p>In consideration of your use of the Website services, you represent that you are of legal age to form a binding contract as per the applicable laws in your jurisdiction. </p>

<h2>ACCEPTANCE OF THE TERMS AND CONDITIONS </h2>
<p>By registering to the website, you indicate acceptance of the terms and conditions of this Agreement and limitation of liability as set out in this Agreement and the privacy policy of entrytest4u. Such acceptance shall be either on your own behalf or on behalf of any corporate entity which employs you or which you represent or on behalf of a minor. 
You should read this Agreement carefully before clicking on the enter button. By clicking the register button you agreed and undertook that you have carefully read and understood the terms and conditions and privacy policy of entrytest4u and agree to be bound by the same. </p>

<h2>REGISTRATION DATA; ACCOUNT SECURITY </h2>
<p>In consideration of your use of the Website, you agree to (i) provide accurate, current and complete information about you as may be requested for by the Website (hereinafter referred to as "Personal Data"); (ii) maintain and promptly update the Personal Data; and (iii) be fully responsible for all use of your account and for any actions that take place using your account. </p>

<h2>DATA SECURITY </h2>
<p>The intent of entrytest4u is to strictly safeguard the security of your personal information; honor your choice for its intended use; and carefully protect your data from loss, misuse, unauthorized access, disclosure, alteration, or destruction. We have put in place appropriate physical, electronic and managerial procedures to safeguard and secure information we collect online, including the use of encryption, while collecting or transferring sensitive data such as credit card information. However, you understand and agree that when you approach or are guided to third party gateways for payment, entrytest4u cannot be liable for the information provided by you (credit card number, debit card information, bank account detail or any personal information) to the third party. </p>

<h2>KEEPING YOURSELF UPDATED </h2>
<p>entrytest4u may, at its sole discretion, modify or revise these Terms and Conditions of Use and policies at any time, and you agree to be bound by such modifications or revisions. Although we may attempt to notify you when major changes are made to these Terms and Conditions of Use, you should periodically review the most up-to-date version. Nothing in this Agreement shall be deemed to confer any third-party rights or benefits. You understand and agree that if you use the Services after the date on which the Terms have changed, entrytest4u will treat your use as acceptance of the updated Terms. Accordingly, we encourage you to read the terms and conditions and the privacy policy when you visit the Website. </p>

<h2>NOTIFICATION ON MISUSE OF ACCOUNT </h2>
<p>You must notify entrytest4u immediately, as and when you become aware of any breach of security or unauthorized use of your entrytest4u.com account. You agree that entrytest4u has no responsibility to you or to any third party for any breach of your obligations under the Terms and for the consequences (including any loss or damage that you or any third party may suffer) of any such breach. Although entrytest4u will not be liable for your losses caused by any unauthorized use of your account, you may be liable for the losses to entrytest4u or to others due to such unauthorized use. </p>

<h2>RIGHT TO DISCONTINUE </h2>
<p>entrytest4u reserves the right to discontinue (permanently or temporarily) any aspect of the Website at its sole discretion, at any time without prior notice to you. You acknowledge and agree that the form and nature of the Services that entrytest4u provides may change from time to time without prior notice to you. Further, while you may remove your User Content from the Website at any time, you acknowledge that Users may retain such Content in their own accounts. You also acknowledge that the entrytest4u may retain archived copies of your User  Content with the authority to use it in future for business purposes. </p>

<h2>OWNERSHIP OF MATERIALS </h2>
<p>entrytest4u company names and logos and all related products and service names, design marks and slogans are trademarks and service marks owned by entrytest4u. You are not authorized to use any name or mark of entrytest4u in any advertisements, publicity matter or in any other commercial manner without the prior written consent of entrytest4u. </p>

<h2>TERMS OF USE OF THE WEBSITE SERVICES </h2>
<p>
You are permitted to view and use the information and data hosted on this Website. However you are not permitted to copy any information for commercial or non-commercial purposes.<br />
The website services are for individual use only and may not be used for commercial purposes or group activities.<br />
You are prohibited from data mining, scraping, crawling, or using any process or processes that send automated queries to this Website. <br />
You agree not to use this Website or any information or data therein for any unsolicited commercial e-mail. <br />
You agree not to translate, modify, revise or create derivative works based on the information and data, except as expressly permitted by entrytest4u. <br />
You understand that this license is revocable at the sole discretion of entrytest4u at any time without notice and with or without any cause. </p>

<h2>DISCLAIMERS </h2>
<p>The information/ data and products on this Website, including but not limited to text, images, graphics, audio-visuals, videos and links is provided as-is and without warranties of any kind, whether express or implied, including but not limited to implied warranty of accuracy, completeness, reliability, merchantability or fitness for a particular purpose. <br />
Any information/ data provided in this Website about the separate third party service providers and/ or entrytest4u and/ or any conclusions based on the available information/ data does not necessarily reflect the view/s of entrytest4u and/ or its Affiliates. <br />
Visitors to this Website access the Website at their own risk. <br />
The photographs, images, audio-visuals and the videos used herein are purely for illustrative purposes only. <br />
This Website may include technical inaccuracies or typographical errors, and we make no guarantees, nor can we be responsible for any such information, including its authenticity, currency, content, quality, copyright compliance or legality, or any resulting loss or damage. <br />
Unauthorized access, reproduction, redistribution, transmission and/or dealing with any information contained in this Website in any other manner, either in whole or in part, are strictly prohibited, failing which strict legal action will be initiated against you as per applicable laws. <br />
You may have arrived on this website by means of some link provided by a third party. You understand that any such links are provided by third parties to this site and who have no connection to entrytest4u whatsoever. entrytest4u neither endorses the contents of such websites nor do we endorse the creators or the hosts of such websites, unless expressly stated in writing otherwise. <br />
We may also provide certain links to other third party websites within this Website as a convenience to you. You understand that this is purely for your understanding and convenience, and any decision that you might take on the basis of such reading is entirely your choice. We do not endorse the contents of such websites nor do we endorse the creators or the hosts of such website, unless expressly stated in writing otherwise. <br />
When you click on advertiser banners, sponsor links, or other external links from this Website, your browser automatically may direct you to a new browser window that is not hosted or controlled by entrytest4u. We are not responsible for the content, functionality, authenticity or technological safety of these external websites. We reserve the right to disable links to or from third-party sites to our Website, as we may deem it fit. This right to disable links includes links to or from advertisers, sponsors, and content partners that may use our trademarks as part of a co-branding relationship. <br />
Some external links may produce information that you may find objectionable, inappropriate, or offensive. We are not responsible for the accuracy, relevancy, copyright compliance, legality, or decency of material contained in any externally linked Websites. </p>

<h2>COPYRIGHT POLICY </h2>
<p>As a entrytest4u contributor you agree that you will not submit material that is copyrighted, protected by trade secret or otherwise subject to third party proprietary rights, including privacy and publicity rights, unless you are the owner of such rights or have express permission from their rightful owner to post the material and to grant entrytest4u all of the license rights granted herein. As part of entrytest4u copyright policy, entrytest4u will terminate user access to the Website if a user has been determined to be an infringer. Also if any third party sends us a copyright notice claiming your submitted material as his/her copyright, you shall be the only one who will be liable for the further action. entrytest4u will not be responsible for such copyright infringement on your part. You agree to indemnify entrytest4u from any case, suit, litigation or any other legal actions against entrytest4u. entrytest4u shall have the right to recover damages & legal costs for such actions on account of your breach. </p>
<p>If you believe that your work has been copied and posted on the Website in a way that constitutes copyright infringement, please provide our Copyright Agent with the following information: </p>
<li>an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest;</li>
<li>a description of the copyrighted work that you claim has been infringed;</li>
<li>a description of where the material that you claim is infringing is located on Website;</li>
<li>your address, telephone number, and email address;</li>
<li>a written statement by you that you have, in good faith, belief that the disputed use is not authorized by the copyright owner, his/her agent, or the law;</li>
<li>a statement by you, made under penalty for perjury, that the above information in your notice is accurate and that you are the copyright owner or are authorized to act on the copyright owner's behalf.</li>

<h2>LIMITATION ON LIABILITY </h2>
<p>You agree to access and use this Website and such other external link/s provided herein and the content thereof at your own risk. entrytest4u, its affiliates or service providers, shall not, be responsible or liable in any manner whatsoever for any direct, indirect, incidental, consequential or punitive damages arising out of any access, use or inability to use this Website or such other external link/s provided herein, including but not limited to any errors or omissions in the content thereof, loss of data, revenue, profits, property, attack by viruses, failure to provide Website Services and the like. 
entrytest4u does not assume any legal liability or responsibility for the accuracy, completeness, or usefulness of any information, data or process disclosed on this Website or other materials accessible from this Website 
You, the user of this Website assumes all responsibility and risk for the use of this Website and the internet generally. The foregoing limitations shall apply to the fullest extent permitted under applicable law. <p>

<h2>INDEMENITY </h2>
<p>You agree to defend, indemnify and hold harmless entrytest4u, and its franchises. Affiliates, agents, directors and employees from and against any and all claims, damages, obligations, losses, liabilities, expenses and costs, including, without limitation, regulatory penalties, attorneys' fees and costs incurred by entrytest4u in connection with any claim arising from: </p>
<ul>
<li>your use of and access to the Website;</li>
<li>your violation of any term of these Terms and Conditions of Use;</li>
<li>your violation of any third party right, including, without limitation, any copyright, property, privacy right, any proprietary rights and/or your violation of any other law whatsoever;?</li>
<li>any claim that one of your User Submissions caused damage to a third party.</li>
    </ul>
<p>This defense and indemnification obligation will survive these Terms and Conditions of Use and your use of the Website.</p>

<h2>GOVERNING LAW</h2>
<p>The Terms and Conditions of Use and the Privacy Policy of entrytest4u will be governed by and construed in accordance with the laws of Pakistan. The courts in Islamabad shall have sole and exclusive jurisdiction in respect of all matters arising herein.</p>

<h2>USER CONDUCT</h2>
<p>You agree not to use the Website to:</p>
<ul>
    <li>	post content or initiate communications that are unlawful, libelous, abusive, obscene, discriminatory, invasive of privacy or publicity rights, hateful, racially, ethnically or otherwise objectionable under the applicable laws.</li>
    <li>    upload, post, email, transmit or otherwise make available any unsolicited or unauthorized advertising, promotional materials, "junk mail", "spam", "chain letters", "pyramid schemes", or any other form of solicitation. This prohibition includes, but is not limited to :
            using the Website to connect to people who don't know you and then sending unsolicited promotional messages to those direct connections without their permission. sending messages to distribution lists, newsgroup aliases, or group aliases.</li>
     <li>   falsely state, impersonate, or otherwise misrepresent your identity, including but not limited to the use of a pseudonym, uploading photographs of celebrities in your profile, or misrepresenting your current or previous positions and qualifications, or your affiliations with a person or entity, past or present; upload, post, transmit, share, store or otherwise make publicly available on the Website any private information of any third party, including, without limitation, addresses, phone numbers, email addresses, Social Security numbers and credit card numbers.</li>
    </ul>

<p>entrytest4u shall not be responsible for the above mentioned misconducts of the users. If you are found guilty of the above mentioned facts, entrytest4u may terminate your membership; delete your profile and any content or information that you have posted on the Website; bar you from using or accessing the Service or the Website at any time at its sole discretion, with or without notice.</p>

<h2>SUBMISSIONS</h2>
<p>You acknowledge and agree that any questions, comments, suggestions, ideas, feedback or other information about the Website or the Service ("Submissions"), provided by you to entrytest4u are non-confidential, and shall become the sole property of the entrytest4u. The entrytest4u shall own exclusive rights, including all intellectual property rights, and shall be entitled to the unrestricted use and dissemination of these Submissions for any purpose, commercial or otherwise, without acknowledgment of or compensation to you.</p>

<h2>NO THIRD PARTY RIGHTS </h2>
<p>Neither this Agreement nor any provision hereof is intended to confer upon any person or third party any rights or remedies hereunder, save and except the Parties to this Agreement. </p>

<h2>SEVERABILITY</h2>
<p>The invalidity or unenforceability of any provisions of this Agreement in any jurisdiction shall not affect the validity, legality or enforceability of the remainder of this Agreement.</p>

<h2>ENTIRE AGREEMENT </h2>
<p>You acknowledge having read the terms and conditions mentioned in this Agreement, and agree to be bound by its terms and further agree that it is binding between you and entrytest4u, superseding all prior understandings and agreements, oral and/ or written entered into between us relating to the subject matter of this Agreement. The Privacy Policy outlined in this Website shall form part of this Agreement.</p>

<h2>CONTACTING US</h2>
<p>If you have any questions or comments about our privacy statement or practices, please contact us via email at support@entrytest4u.com with the words "USER AGREEMENT" in the subject line. entrytest4u reserves the right to modify or update this privacy statement at any time without prior notice.</p>



  </article>
        </section>


@endsection