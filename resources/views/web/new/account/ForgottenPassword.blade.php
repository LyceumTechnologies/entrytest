

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pakprep.com/account/ForgottenPassword.aspx by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 May 2019 16:30:30 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" /><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" /><meta name="msvalidate.01" content="590A625FBD65E499EE3FAEE35A28A3F4" /><title>
	 Entry Test Preparation MCQs for Medical and Engineering colleges - PakPrep - Forgotten Password
</title><meta name="robots" content="noodp,noydir" /><meta name="description" content="Pakprep.com is an online solution for entry test preparation and apptitude test preparation in Pakistan. PakPrep was developed with a mission to provide Simple, Innovative and Affordable Entry Test Preparation for All. " /><meta name="keywords" content="Pakistan entry test preparation, pakprep, Mcqs, Entry Test, FSc Punjab medical admission, Medical and Engineering entry test, Medical and Engineering colleges, Maths mcqs, Mathematics mcqs,test,entry,UET Entry Test, mcqs, tests, mcat, sample Physics MCQs, Sample Chemistry MCQs,Sample Biology MCQs, Biology mcqs for entry test, Engineering admission, Lums admission, aptitude test,nust,prepare, test, NUST entry test mcqs, FAST entry test 2015, Lums Entry test 2015, English Mcqs
, MCAT, ECAT, NTS, SAT2, Engineering, Medical, Physics Mcqs, Biology Mcqs,Maths Mcqs, English Mcqs, solved mcqs,mcqs,giki, sample tests,mock, mock tests, Entry test preparation,Entry test preparation Pakistan,Aptitude test preparation,Medical college entry test,Engineering Universities Pakistan,Medical and Engineering entry test,Medical and Engineering colleges,
FAST entry test,LUMS Entry test,FSc Punjab medical admission,GIKI entry test,Mathematics mcqs,sample Physics MCQs,Biology mcqs for entry test,Engineering admission
Lums admission,aptitude test,English Mcqs,solved mcqs,sample tests," />


<link rel="canonical" href=forgottenpassword-2.html />

<meta property="og:locale" content="en_US" /><meta property="og:type" content="website" />

<meta property="og:title" content=Forgotten Password />
<meta property="og:description" content="Pakprep.com is an online solution for entry test preparation and apptitude test preparation in Pakistan. PakPrep was developed with a mission to provide Simple, Innovative and Affordable Entry Test Preparation for All." />
<meta property="og:url" content=https://pakprep.com/account/forgottenpassword.aspx />
 <meta property="og:image" content="../images/500-fb.png" /><meta property="og:site_name" content="PakPrep.com | Entry Test Pakistan, Apptitude Test Pakistan" /><link href="https://fonts.googleapis.com/css?family=Signika:600,400,300" rel="stylesheet" type="text/css" /><link href="../content/wc/css/style5e1f.css?v=2" rel="stylesheet" type="text/css" media="screen" /><link href="../content/wc/css/style-headers3860.css?v=1" rel="stylesheet" type="text/css" media="screen" /><link href="../content/wc/css/style-colors3860.css?v=1" rel="stylesheet" type="text/css" media="screen" /><link href="https://pakprep.com/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	
		   
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<link href="../style-ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
   
    <script>
        document.onmousedown = disableclick;
        function disableclick(event) {
            if (event.button == 2) {
                return false;
            }
        }
	</script>  
    
    <!--  -->
         
</head>
<body class="home" oncontextmenu="return false" oncopy="return false" oncut="return false" >
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48685761-1', 'pakprep.com');
	  ga('send', 'pageview');
	  
	    var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-48685761-1']);
		_gaq.push(['_trackPageview']);

		(function() {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

	</script>


    <form method="post" action="https://pakprep.com/account/ForgottenPassword.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="ctl01">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTE2NzY3NjMxOQ9kFgJmDw8WBB4PX19BbnRpWHNyZlRva2VuBSBmM2Q0OWUxYWZiMTg0MTNlYmNkN2MyMDhlYWIzM2YxMB4SX19BbnRpWHNyZlVzZXJOYW1lZWQWAgIDD2QWBGYPZBYQZg8WAh4HVmlzaWJsZWhkAgIPFgIfAmhkAgQPFgIeBFRleHQFBzE5MSwzMDVkAgYPFgIfAwUHOTc0LDkwNWQCCA8WAh8DBQk3LDY3OSwxMjBkAgwPFgIfAwUHMTkxLDMwNWQCDg8WAh8DBQc5NzQsOTA1ZAIQDxYCHwMFCTcsNjc5LDEyMGQCAQ9kFgwCAQ8WAh4FY2xhc3NlZAIDD2QWBmYPFgIfBGVkAgEPFgIfBGVkAgIPFgIfBGVkAgUPFgIfBGVkAgcPFgIfBGVkAgkPFgIfBGVkAg0PFgIfAmcWBGYPFgIfBGVkAgEPFgIfBGVkZM6K2ZYFW0aCOZrJv08AKt93Fqg4LpQbchAH3qCXSPMx" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['ctl01'];
if (!theForm) {
    theForm = document.ctl01;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="https://pakprep.com/WebResource.axd?d=Wa1G2JgZ-GKiN0jxg9ID0HS9qIX3GHgVolPgIUquUtZBT96Ygu6lbSowUc1n82HywFbZL84XLrwabqo8tg-AR37-ogC0po7QZg6iPklavyo1&amp;t=636476119203255666" type="text/javascript"></script>


<script src="https://pakprep.com/WebResource.axd?d=ioLJuYek5lfbk5CFY1u_KPn6oFYeF-TgiSnJMOz5a8RKccEyWT2SxmNP2a9mhFoHyjDFoTrhblnNY4OuchZul_MWJ7U3h8jVe4AdAGlH_hE1&amp;t=636476119203255666" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5FF4FFD4" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAPF1oQ+P1dFQAd0vy9/TcFj/7WazomCT0sEHVkoZl6hRto+KhIsFOjveK8PX1MBnS3Or+GuYv1HuQPRbFGpbsMfOD01dzGlfM9UF9opLr8wdw==" />
</div>        
    <div class="root">
    <header class="h9 sticky-enabled no-topbar">
                     





 
<section class="main-header">
        <div id="logo">
                <p class="title">
                    <a href="https://pakprep.com/">
                            <img src="../content/wc/images/logo.png">
                    </a>
                    Tests Taken : 974,905 | MCQs Answered : 7,679,120 

               </p>
            
                           <span class="searchform"><a href="https://pakprep.com/pakprep-demo.aspx" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Demo Button']);">Try Demo</a><a href="register.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button']);">Register</a><a href="login.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Login Button']);">Login</a></span>
             
			
         </div>
        <div id="logo-mob">
		        <a href="https://pakprep.com/"> <img src="../content/wc/images/logo.png" style="margin:0px;"></a>
				<p>Tests Taken : 974,905 | MCQs Answered : 7,679,120 </p>
				
            
		      <p>  <a href="https://pakprep.com/pakprep-demo.aspx" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Demo Button Mobile']);">Try Demo</a><a href="register.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button Mobile']);">Register</a><a href="login.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Login Button Mobile']);">Login</a></p>
             
        </div>
		
</section>                       
              

         

<nav class="mainmenu">    
    
                    <ul>
                        <li id="ctl22_home" class=""><a href="https://pakprep.com/">Home</a></li>
                        

                        

                        <li id="ctl22_prepare" class=""><a href="https://pakprep.com/how-to-prepare.aspx">How to Prepare?</a></li>
                        <li id="ctl22_blogs" class=""><a href="http://pakprep.com/blogs/">Blog</a></li>
                        <li id="ctl22_faqs" class=""><a href="https://pakprep.com/faqs.aspx">FAQs</a></li>
                        <li id="ctl22_invite"><a href="https://pakprep.com/testimonials.aspx">Testimonials</a></li>
                        <li id="ctl22_about" class=""><a href="https://pakprep.com/past-papers.aspx">Past Papers</a></li><li id="ctl22_contact" class=""><a href="https://pakprep.com/contact-us.aspx">Contact Us</a></li>

                    </ul>
            <div class="clear"></div>
			    <select id="sec-nav" name="sec-nav">
				     <option value="/">Menu</option>
				     <option value="/default.aspx">Home</option>
                       
           
				    <option value="/how-to-prepare.aspx">How to Prepare?</option>
				    <option value="http://pakprep.com/blogs/">Blog</option>	
                    <option value="/faqs.aspx/">FAQs</option>			
                    <option value="/testimonials.aspx">Testimonials</option>			
				    <option value="/past-papers.aspx">Past Papers</option>  
				    <option value="/contact-us.aspx">Contact Us</option> 		
			    </select>		
</nav>               
					
		 
       
    </header>
      
   
            
            

    <section class="content">
                        <article class="main">
                       <h1>Forgotten Password</h1>
    
        <p>Enter your email address to receive a new password</p>
        
                <fieldset>
                    <legend>Enter your email address to receive a new password</legend>
                    <div class="forms">
                        <div>
                            <label for="MainContent_UserName" id="MainContent_Label1">Email</label>
                            <input name="ctl00$MainContent$UserName" type="text" maxlength="100" id="MainContent_UserName" autocomplete="off" />
                            <span id="MainContent_RequiredFieldValidator1" class="field-validation-error" style="visibility:hidden;">The user name field is required.</span>
                            
                        </div>
                        </div>
                    <input type="submit" name="ctl00$MainContent$ButtonForgottenPassword" value="Send me password" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$ButtonForgottenPassword&quot;, &quot;&quot;, true, &quot;Forgotten&quot;, &quot;&quot;, false, false))" id="MainContent_ButtonForgottenPassword" class="btn blue" />
                    
                    </fieldset>
                            </article>
        </section>
    


    
        <footer>
		<section class="widgets">
            <article>
				<h3><span>Contact Us</span></h3>
				<p>PakPrep (Pvt) Limited<br>Islamabad<br>Pakistan<br>E-mail: <a href="mailto:support@pakprep.com" title="contact Pakprep.com">support@pakprep.com</a><br/>Phone: 0335-5833265</p>
			</article>
            <article class="widget_links">
				<h3><span>Useful Links</span></h3>
				<ul>
					<li><a class="whitelink" href="https://pakprep.com/terms-and-conditions.aspx" title="Terms & Conditions">Terms & Conditions</a></li>
					<li><a class="whitelink" href="https://pakprep.com/privacy-policy.aspx" title="Privacy Policy :: Entry Test Preparation Pakistan">Privacy Policy</a></li>
					<li><a class="whitelink" href="https://pakprep.com/disclaimer.aspx" title="Disclaimer :: Aptitude Test  Preparation">Disclaimer</a></li>
					<li><a class="whitelink" href="http://pakprep.com/blogs/" title="Blog :: Medical and Engineering Entry Test ">Blog</a></li>
					<li><a class="whitelink" href="https://pakprep.com/faqs.aspx" title="FAQs :: Entry Test Preparation">FAQs</a></li>
					<li><a class="whitelink" href="https://pakprep.com/refer/invite-a-friend.aspx" title="Invite a friend :: Entry Test Preparation">Invite A Friend</a></li>
					
					
				</ul>
			</article>
			<article class="widget_links">
				<h3><span>Useful Links</span></h3>
				<ul>
					<li><a class="whitelink" href="https://pakprep.com/about-us.aspx" title="About us :: Entry Test Preparation Pakistan">About us</a></li>
					<li><a class="whitelink" href="https://pakprep.com/contact-us.aspx" title="Contact ::  Aptitude Test  Preparation ">Contact us</a></li>
					<li><a class="whitelink" href="login.html" title="Login :: Entry Test Preparation ">Login</a></li>
					<li><a class="whitelink" href="register.html" title="Register Now on Pakprep">Register</a></li>
					<li><a class="whitelink" href="forgottenpassword-2.html" title="Forgotten Password :: Entry test preparation">Forgotten password</a></li>
					<li><a class="whitelink" href="../practise/transactiondetails.html" title="Buy full version">Buy full version</a></li>
					
				</ul>
			</article>
            
		</section>
		<section class="bottom">
			<p style="font-size:13px;">&copy; 2019 - PakPrep.com</p>
			<nav class="social">
				<ul>
					<li><a href="https://www.facebook.com/pakprep" class="facebook" title="Pakprep.com on facebook">Facebook</a></li>
					<li><a href="https://twitter.com/pakprep" class="twitter" title="Pakprep.com on twitter">Twitter</a></li>
					<li><a href="https://plus.google.com/+Pakprepofficial" class="googleplus" title="Pakprep.com on Google+">Google+</a></li>					
					 
				</ul>
			</nav>
		</section>
	</footer>
    
    </div>
    
<script type="text/javascript">
//<![CDATA[
var Page_Validators =  new Array(document.getElementById("MainContent_RequiredFieldValidator1"));
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var MainContent_RequiredFieldValidator1 = document.all ? document.all["MainContent_RequiredFieldValidator1"] : document.getElementById("MainContent_RequiredFieldValidator1");
MainContent_RequiredFieldValidator1.controltovalidate = "MainContent_UserName";
MainContent_RequiredFieldValidator1.errormessage = "The user name field is required.";
MainContent_RequiredFieldValidator1.validationGroup = "Forgotten";
MainContent_RequiredFieldValidator1.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
MainContent_RequiredFieldValidator1.initialvalue = "";
//]]>
</script>


<script type="text/javascript">
//<![CDATA[

var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        //]]>
</script>
</form>
    <script type="text/javascript" src="../content/wc/js/jquery.js"></script>
	<script type="text/javascript" src="../content/wc/js/scripts.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="/content/wc/js/ie.js"></script>
	<![endif]-->
</body>
    

<!-- Mirrored from pakprep.com/account/ForgottenPassword.aspx by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 May 2019 16:30:30 GMT -->
</html>
