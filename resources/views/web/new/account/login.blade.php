<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
      
   
            
            
     <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$MainContent$ctl00', 'ctl01', ['tctl00$MainContent$LoginUpdatePanel','MainContent_LoginUpdatePanel'], [], [], 90, 'ctl00');
//]]>
</script>

	 
    <div id="MainContent_LoginUpdatePanel">
	
                    <section class="content">
                        <article class="main">
                       <h1>Log in</h1>
        

            
                        <div onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;MainContent_ButtonLogin&#39;)">
		
                                <fieldset>
                                    <legend>Log in Form</legend>
                                    <div class="forms">
                                        <div>
                                            <label for="MainContent_UserName">Email</label>
                                            <input name="ctl00$MainContent$UserName" type="text" maxlength="100" id="MainContent_UserName" autocomplete="off" />
                                            <span id="MainContent_ctl05" class="field-validation-error" style="display:none;">The user name field is required.</span>
                            
                                        </div>
                                        <div>
                                            <label for="MainContent_Password">Password</label>
                                            <input name="ctl00$MainContent$Password" type="password" maxlength="15" id="MainContent_Password" autocomplete="off" />
                                            <span id="MainContent_ctl07" class="field-validation-error" style="display:none;">The password field is required.</span>
                                        </div>                        
                                    </div>
                                    <input type="submit" name="ctl00$MainContent$ButtonLogin" value="Log in" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$ButtonLogin&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="MainContent_ButtonLogin" class="btn blue" /> 
                                    
                                </fieldset>
                        
	</div>
                        
                            <p><a href="ForgottenPassword.html" class="btn blue">Forgotten password?</a> | <a id="MainContent_RegisterHyperLink" class="btn blue" href="register.html">Register</a></p>
                    </article>
                       
        
</div>
        <div id="MainContent_ctl01" style="display:none;">
	
             <div id="progressBackgroundFilter"></div>
                    <div id="processMessage"> Loading...<br /><br />
             <img alt="Loading" src="../images/ajax_loader.gif" width="50" height="50" />
        </div>

        
</div>
   <!-- <section id="socialLoginForm">
      
    </section>
    -->


    @endsection