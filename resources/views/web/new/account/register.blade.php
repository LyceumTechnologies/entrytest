<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
      
   
            
            

   <section class="content">
                        <article class="main">
                       <h1>Register</h1>
        


                    <p class="validation-summary-errors">
                        
                    </p>

                    <fieldset>
                        <legend>Registration Form</legend>
                        <div class="forms">
                        <div>
                            <label for="MainContent_UserName" id="MainContent_Label1">Email</label>
                            <span class="small-print">Please make sure email address is correct.</span><br />
                            <input name="ctl00$MainContent$UserName" type="text" maxlength="100" id="MainContent_UserName" autocomplete="off" />
                            <span id="MainContent_RequiredFieldValidator1" class="field-validation-error" style="display:none;">The email field is required.</span>
                            <span id="MainContent_RegExpValUserName" class="field-validation-error" style="display:none;">The email field must be in a valid email format.</span>
                        </div>    
						<div>
                            <label for="MainContent_TextBoxReTypeEmail" id="MainContent_Label5">Re-type Email</label>
                            <span class="small-print">Please re-type your email address and make sure there are no spelling mistakes.</span><br />
                            <input name="ctl00$MainContent$TextBoxReTypeEmail" type="text" maxlength="100" id="MainContent_TextBoxReTypeEmail" autocomplete="off" />
                            <span id="MainContent_RequiredFieldValidator5" class="field-validation-error" style="display:none;">The re-type field is required.</span>
                            <span id="MainContent_ComapreValidator" class="field-validation-error" style="display:none;">The re-type email address must be same as email address.</span>
                            <span id="MainContent_RegularExpressionValidator2" class="field-validation-error" style="display:none;">The re-type field must be in a valid email format.</span>

                        </div>		                        
                        <div>

                            <label for="MainContent_Password" id="MainContent_Label2">Password</label>                            
                            <span class="small-print">Passwords are required to be a minimum of 8 characters in length.</span><br />
                            <input name="ctl00$MainContent$Password" type="password" maxlength="15" id="MainContent_Password" autocomplete="off" />
                            <span id="MainContent_RequiredFieldValidator2" class="field-validation-error" style="display:none;">The password field is required.</span>
                            <span id="MainContent_RegularExpressionValidator1" class="field-validation-error" style="display:none;">The password length must be 8 characters.</span>
                        </div>
                        
                        <div>
                            
                            <!-- <span class="small-print">Please select either pre-medical or pre-engineering group.</span><br /> -->
                            
                             <span id="MainContent_RequiredFieldValidator4" class="field-validation-error" style="visibility:hidden;">The study group field is required.</span>
                        </div>
                        <div>
                             <input name="ctl00$MainContent$TextBoxCaptcha" type="text" maxlength="8" id="TextBoxCaptcha" />
                          

                        </div>
                            <div>
                        <p class="small-print">
                            <input id="MainContent_TandC" type="checkbox" name="ctl00$MainContent$TandC" checked="checked" /> By clicking Register, you agree to our <a href="https://pakprep.com/terms-and-conditions.aspx" target="_blank">Terms and Conditions</a> and confirm that you have read our <a href="https://pakprep.com/disclaimer.aspx" target="_blank">Disclaimer</a> and <a href="https://pakprep.com/privacy-policy.aspx" target="_blank">Privacy Policy</a>.</p>
                                
                             <span id="MainContent_TandCValidation" class="field-validation-error" style="visibility:hidden;">Please accept terms & condition</span>
                            </div>
                      <div>
                          <input type="submit" name="ctl00$MainContent$ButtonRegister" value="Register" onclick="_gaq.push([&#39;_trackEvent&#39;, &#39;Register Page&#39;, &#39;click&#39;, &#39;Register Button Confirm&#39;]);WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$ButtonRegister&quot;, &quot;&quot;, true, &quot;Register&quot;, &quot;&quot;, false, false))" id="MainContent_ButtonRegister" class="btn blue" />
                          <p><span class="field-validation-error"></span></p>
                      </div>
                    </div>
                        
                    <br/><br/><p class="small-print">If you are having difficulties in creating your account, please send an email to <a href="mailto:support@pakprep.com">support@pakprep.com</a></p>
                    </fieldset>
    </article>
 </section>


    @endsection