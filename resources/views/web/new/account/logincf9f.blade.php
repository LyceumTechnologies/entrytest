

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pakprep.com/account/login.aspx?ReturnUrl=/refer/invite-a-friend-form.aspx by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 May 2019 16:35:44 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" /><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" /><meta name="msvalidate.01" content="590A625FBD65E499EE3FAEE35A28A3F4" /><title>
	 Entry Test Preparation MCQs for Medical and Engineering colleges - PakPrep - Log in
</title><meta name="robots" content="noodp,noydir" /><meta name="description" content="Pakprep.com is an online solution for entry test preparation and apptitude test preparation in Pakistan. PakPrep was developed with a mission to provide Simple, Innovative and Affordable Entry Test Preparation for All. " /><meta name="keywords" content="Pakistan entry test preparation, pakprep, Mcqs, Entry Test, FSc Punjab medical admission, Medical and Engineering entry test, Medical and Engineering colleges, Maths mcqs, Mathematics mcqs,test,entry,UET Entry Test, mcqs, tests, mcat, sample Physics MCQs, Sample Chemistry MCQs,Sample Biology MCQs, Biology mcqs for entry test, Engineering admission, Lums admission, aptitude test,nust,prepare, test, NUST entry test mcqs, FAST entry test 2015, Lums Entry test 2015, English Mcqs
, MCAT, ECAT, NTS, SAT2, Engineering, Medical, Physics Mcqs, Biology Mcqs,Maths Mcqs, English Mcqs, solved mcqs,mcqs,giki, sample tests,mock, mock tests, Entry test preparation,Entry test preparation Pakistan,Aptitude test preparation,Medical college entry test,Engineering Universities Pakistan,Medical and Engineering entry test,Medical and Engineering colleges,
FAST entry test,LUMS Entry test,FSc Punjab medical admission,GIKI entry test,Mathematics mcqs,sample Physics MCQs,Biology mcqs for entry test,Engineering admission
Lums admission,aptitude test,English Mcqs,solved mcqs,sample tests," />


<link rel="canonical" href=login0801.html?returnurl=/refer/invite-a-friend-form.aspx />

<meta property="og:locale" content="en_US" /><meta property="og:type" content="website" />

<meta property="og:title" content=Log in />
<meta property="og:description" content="Pakprep.com is an online solution for entry test preparation and apptitude test preparation in Pakistan. PakPrep was developed with a mission to provide Simple, Innovative and Affordable Entry Test Preparation for All." />
<meta property="og:url" content=https://pakprep.com/account/login.aspx?returnurl=/refer/invite-a-friend-form.aspx />
 <meta property="og:image" content="../images/500-fb.png" /><meta property="og:site_name" content="PakPrep.com | Entry Test Pakistan, Apptitude Test Pakistan" /><link href="https://fonts.googleapis.com/css?family=Signika:600,400,300" rel="stylesheet" type="text/css" /><link href="../content/wc/css/style5e1f.css?v=2" rel="stylesheet" type="text/css" media="screen" /><link href="../content/wc/css/style-headers3860.css?v=1" rel="stylesheet" type="text/css" media="screen" /><link href="../content/wc/css/style-colors3860.css?v=1" rel="stylesheet" type="text/css" media="screen" /><link href="../images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	
		   
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<link href="../style-ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
   
    <script>
        document.onmousedown = disableclick;
        function disableclick(event) {
            if (event.button == 2) {
                return false;
            }
        }
	</script>  
    
    <!--  -->
         
</head>
<body class="home" oncontextmenu="return false" oncopy="return false" oncut="return false" >
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-48685761-1', 'pakprep.com');
	  ga('send', 'pageview');
	  
	    var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-48685761-1']);
		_gaq.push(['_trackPageview']);

		(function() {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

	</script>


    <form method="post" action="https://pakprep.com/account/login.aspx?ReturnUrl=%2frefer%2finvite-a-friend-form.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="ctl01">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMjk4NzAxOTc3D2QWAmYPDxYEHg9fX0FudGlYc3JmVG9rZW4FIGYzZDQ5ZTFhZmIxODQxM2ViY2Q3YzIwOGVhYjMzZjEwHhJfX0FudGlYc3JmVXNlck5hbWVlZBYCAgMPZBYEZg9kFhBmDxYCHgdWaXNpYmxlaGQCAg8WAh8CaGQCBA8WAh4EVGV4dAUHMTkxLDMwNWQCBg8WAh8DBQc5NzQsOTA1ZAIIDxYCHwMFCTcsNjc5LDEyMGQCDA8WAh8DBQcxOTEsMzA1ZAIODxYCHwMFBzk3NCw5MDVkAhAPFgIfAwUJNyw2NzksMTIwZAIBD2QWDgIBDxYCHgVjbGFzc2VkAgMPZBYGZg8WAh8EZWQCAQ8WAh8EZWQCAg8WAh8EZWQCBQ8WAh8EZWQCBw8WAh8EZWQCCQ8WAh8EZWQCCw8WAh8EBRFjdXJyZW50LW1lbnUtaXRlbWQCDQ8WAh8CZxYEZg8WAh8EZWQCAQ8WAh8EZWRkfp6NGQ4jItz8ngqhRCtmM3imRnXHbbuw5QPzcEZ/8e4=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['ctl01'];
if (!theForm) {
    theForm = document.ctl01;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="https://pakprep.com/WebResource.axd?d=Wa1G2JgZ-GKiN0jxg9ID0HS9qIX3GHgVolPgIUquUtZBT96Ygu6lbSowUc1n82HywFbZL84XLrwabqo8tg-AR37-ogC0po7QZg6iPklavyo1&amp;t=636476119203255666" type="text/javascript"></script>


<script src="https://pakprep.com/ScriptResource.axd?d=adCTB6wdgJZttL2Bga8M1xlBO4GiH1ptPnE6-ZqM7OBQB_7NTI0GkKKDCbd8Vv2QSYZ5Jg8rCE8OZNYdCHfMfYxQaFav2ZwloAEvaAHHrnTdr83z5EqWbQAzXk7W5Qi1hTjdOasJWakbtjttvBGEzZJyJvMGQJkkvAs0bl-WLYs1&amp;t=ffffffff85b84bae" type="text/javascript"></script>
<script src="https://pakprep.com/ScriptResource.axd?d=UejaJU4N_vinn1Sfmj1xx9K0luaJxCEomJSWhKagJXSIZTTXAFohaCJ1cYyZXkmb7ecQFNG6q5uaEbR-5m1LNS4zDX90bzukxd1RdyECkynPiluuIF1-J0NeMeO1X6nOEXq1M8JxOFkW6Udx2GEPFYoVnAz0snUH5j49hdxCAtw1&amp;t=ffffffffad4b7194" type="text/javascript"></script>
<script src="https://pakprep.com/ScriptResource.axd?d=SfBq839y6rWN-Cz4wHcduQYYuxxDHdkigZlwvx53ErASz00jQ4DyqmP_MbrvDIJYoeLnnr9RsNMHYwqpBmb4cc5O-N-MoUoW8_oQjSKQhmmO9fzVsnlJflfHL3b0o2Ds6x2QsU0bAl-2p3qx4a79MPjc5HtXvi84tqw9XfOaEeqFfnfeK8aEAPCthrYiB-UV0&amp;t=ffffffffad4b7194" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CD85D8D2" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAQy6ff53oaJ0RhluFnu+/Ih/7WazomCT0sEHVkoZl6hRs+9P3s0w+MeGOPom7ExypBgxs0MNwtvfpWCAkWuFbDXIZ6N2pBHkZKgKh0HitEDy9wSOPEDYQDTDdaKnMyOVgc=" />
</div>        
    <div class="root">
    <header class="h9 sticky-enabled no-topbar">
                     





 
<section class="main-header">
        <div id="logo">
                <p class="title">
                    <a href="https://pakprep.com/">
                            <img src="../content/wc/images/logo.png">
                    </a>
                    Tests Taken : 974,905 | MCQs Answered : 7,679,120 

               </p>
            
                           <span class="searchform"><a href="../pakprep-demo.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Demo Button']);">Try Demo</a><a href="register.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button']);">Register</a><a href="login.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Login Button']);">Login</a></span>
             
			
         </div>
        <div id="logo-mob">
		        <a href="https://pakprep.com/"> <img src="../content/wc/images/logo.png" style="margin:0px;"></a>
				<p>Tests Taken : 974,905 | MCQs Answered : 7,679,120 </p>
				
            
		      <p>  <a href="../pakprep-demo.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Demo Button Mobile']);">Try Demo</a><a href="register.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button Mobile']);">Register</a><a href="login.html" class="btn blue" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Login Button Mobile']);">Login</a></p>
             
        </div>
		
</section>                       
              

         

<nav class="mainmenu">    
    
                    <ul>
                        <li id="ctl22_home" class=""><a href="https://pakprep.com/">Home</a></li>
                        

                        

                        <li id="ctl22_prepare" class=""><a href="../how-to-prepare.html">How to Prepare?</a></li>
                        <li id="ctl22_blogs" class=""><a href="http://pakprep.com/blogs/">Blog</a></li>
                        <li id="ctl22_faqs" class=""><a href="../faqs.html">FAQs</a></li>
                        <li id="ctl22_invite" class="current-menu-item"><a href="../testimonials.html">Testimonials</a></li>
                        <li id="ctl22_about" class=""><a href="../past-papers.html">Past Papers</a></li><li id="ctl22_contact" class=""><a href="../contact-us.html">Contact Us</a></li>

                    </ul>
            <div class="clear"></div>
			    <select id="sec-nav" name="sec-nav">
				     <option value="/">Menu</option>
				     <option value="/default.aspx">Home</option>
                       
           
				    <option value="/how-to-prepare.aspx">How to Prepare?</option>
				    <option value="http://pakprep.com/blogs/">Blog</option>	
                    <option value="/faqs.aspx/">FAQs</option>			
                    <option value="/testimonials.aspx">Testimonials</option>			
				    <option value="/past-papers.aspx">Past Papers</option>  
				    <option value="/contact-us.aspx">Contact Us</option> 		
			    </select>		
</nav>               
					
		 
       
    </header>
      
   
            
            
     <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$MainContent$ctl00', 'ctl01', ['tctl00$MainContent$LoginUpdatePanel','MainContent_LoginUpdatePanel'], [], [], 90, 'ctl00');
//]]>
</script>

	 
    <div id="MainContent_LoginUpdatePanel">
	
                    <section class="content">
                        <article class="main">
                       <h1>Log in</h1>
        

            
                        <div onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;MainContent_ButtonLogin&#39;)">
		
                                <fieldset>
                                    <legend>Log in Form</legend>
                                    <div class="forms">
                                        <div>
                                            <label for="MainContent_UserName">Email</label>
                                            <input name="ctl00$MainContent$UserName" type="text" maxlength="100" id="MainContent_UserName" autocomplete="off" />
                                            <span id="MainContent_ctl05" class="field-validation-error" style="display:none;">The user name field is required.</span>
                            
                                        </div>
                                        <div>
                                            <label for="MainContent_Password">Password</label>
                                            <input name="ctl00$MainContent$Password" type="password" maxlength="15" id="MainContent_Password" autocomplete="off" />
                                            <span id="MainContent_ctl07" class="field-validation-error" style="display:none;">The password field is required.</span>
                                        </div>                        
                                    </div>
                                    <input type="submit" name="ctl00$MainContent$ButtonLogin" value="Log in" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$ButtonLogin&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="MainContent_ButtonLogin" class="btn blue" /> 
                                    
                                </fieldset>
                        
	</div>
                        
                            <p><a href="ForgottenPassword.html" class="btn blue">Forgotten password?</a> | <a id="MainContent_RegisterHyperLink" class="btn blue" href="registerda62.html?ReturnUrl=%2frefer%2finvite-a-friend-form.aspx">Register</a></p>
                    </article>
                       
        
</div>
        <div id="MainContent_ctl01" style="display:none;">
	
             <div id="progressBackgroundFilter"></div>
                    <div id="processMessage"> Loading...<br /><br />
             <img alt="Loading" src="../images/ajax_loader.gif" width="50" height="50" />
        </div>

        
</div>
   <!-- <section id="socialLoginForm">
      
    </section>
    -->


    
        <footer>
		<section class="widgets">
            <article>
				<h3><span>Contact Us</span></h3>
				<p>PakPrep (Pvt) Limited<br>Islamabad<br>Pakistan<br>E-mail: <a href="mailto:support@pakprep.com" title="contact Pakprep.com">support@pakprep.com</a><br/>Phone: 0335-5833265</p>
			</article>
            <article class="widget_links">
				<h3><span>Useful Links</span></h3>
				<ul>
					<li><a class="whitelink" href="../terms-and-conditions.html" title="Terms & Conditions">Terms & Conditions</a></li>
					<li><a class="whitelink" href="../privacy-policy.html" title="Privacy Policy :: Entry Test Preparation Pakistan">Privacy Policy</a></li>
					<li><a class="whitelink" href="../disclaimer.html" title="Disclaimer :: Aptitude Test  Preparation">Disclaimer</a></li>
					<li><a class="whitelink" href="http://pakprep.com/blogs/" title="Blog :: Medical and Engineering Entry Test ">Blog</a></li>
					<li><a class="whitelink" href="../faqs.html" title="FAQs :: Entry Test Preparation">FAQs</a></li>
					<li><a class="whitelink" href="../refer/invite-a-friend.html" title="Invite a friend :: Entry Test Preparation">Invite A Friend</a></li>
					
					
				</ul>
			</article>
			<article class="widget_links">
				<h3><span>Useful Links</span></h3>
				<ul>
					<li><a class="whitelink" href="../about-us.html" title="About us :: Entry Test Preparation Pakistan">About us</a></li>
					<li><a class="whitelink" href="../contact-us.html" title="Contact ::  Aptitude Test  Preparation ">Contact us</a></li>
					<li><a class="whitelink" href="login.html" title="Login :: Entry Test Preparation ">Login</a></li>
					<li><a class="whitelink" href="register.html" title="Register Now on Pakprep">Register</a></li>
					<li><a class="whitelink" href="forgottenpassword-2.html" title="Forgotten Password :: Entry test preparation">Forgotten password</a></li>
					<li><a class="whitelink" href="../practise/transactiondetails.html" title="Buy full version">Buy full version</a></li>
					
				</ul>
			</article>
            
		</section>
		<section class="bottom">
			<p style="font-size:13px;">&copy; 2019 - PakPrep.com</p>
			<nav class="social">
				<ul>
					<li><a href="https://www.facebook.com/pakprep" class="facebook" title="Pakprep.com on facebook">Facebook</a></li>
					<li><a href="https://twitter.com/pakprep" class="twitter" title="Pakprep.com on twitter">Twitter</a></li>
					<li><a href="https://plus.google.com/+Pakprepofficial" class="googleplus" title="Pakprep.com on Google+">Google+</a></li>					
					 
				</ul>
			</nav>
		</section>
	</footer>
    
    </div>
    
<script type="text/javascript">
//<![CDATA[
var Page_Validators =  new Array(document.getElementById("MainContent_ctl05"), document.getElementById("MainContent_ctl07"));
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var MainContent_ctl05 = document.all ? document.all["MainContent_ctl05"] : document.getElementById("MainContent_ctl05");
MainContent_ctl05.controltovalidate = "MainContent_UserName";
MainContent_ctl05.errormessage = "The user name field is required.";
MainContent_ctl05.display = "Dynamic";
MainContent_ctl05.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
MainContent_ctl05.initialvalue = "";
var MainContent_ctl07 = document.all ? document.all["MainContent_ctl07"] : document.getElementById("MainContent_ctl07");
MainContent_ctl07.controltovalidate = "MainContent_Password";
MainContent_ctl07.errormessage = "The password field is required.";
MainContent_ctl07.display = "Dynamic";
MainContent_ctl07.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
MainContent_ctl07.initialvalue = "";
//]]>
</script>


<script type="text/javascript">
//<![CDATA[

var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        
document.getElementById('MainContent_ctl05').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('MainContent_ctl05'));
}

document.getElementById('MainContent_ctl07').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('MainContent_ctl07'));
}
Sys.Application.add_init(function() {
    $create(Sys.UI._UpdateProgress, {"associatedUpdatePanelId":"MainContent_LoginUpdatePanel","displayAfter":500,"dynamicLayout":true}, null, null, $get("MainContent_ctl01"));
});
//]]>
</script>
</form>
    <script type="text/javascript" src="../content/wc/js/jquery.js"></script>
	<script type="text/javascript" src="../content/wc/js/scripts.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="/content/wc/js/ie.js"></script>
	<![endif]-->
</body>
    

<!-- Mirrored from pakprep.com/account/login.aspx?ReturnUrl=/refer/invite-a-friend-form.aspx by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 May 2019 16:35:46 GMT -->
</html>
