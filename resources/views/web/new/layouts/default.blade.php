 <?php
/**
 * Project: EntryTest4u
 * User: SHAFQAT GHAFOOR
 * Date: 6/12/2018
 * Time: 6:30 PM
 */
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

  <base href="{{ url('/')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @section('metas')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  @show


  <title> @yield('title','') EntryTest</title>


  @stack('pre-styles')

  @section('styles')
    <link href="{{asset('new/content/wc/css/style5e1f.css?v=2"')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('new/content/wc/css/style-headers3860.css?v=1')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('new/content/wc/css/style-colors3860.css?v=1')}}" rel="stylesheet" type="text/css" media="screen" />
 @show

  @stack('post-styles')

  <script type="text/javascript">
    var BASE_URL = "{{ url('/')}}";
  </script>

</head>

<body>
<section class="body">
 <body class="home" oncontextmenu="return false" oncopy="return false" oncut="return false" >
          <div class="root">
            <header class="h9 sticky-enabled no-topbar">
              <section class="main-header">
                <div id="logo">
                  <p class="title">
                    <a href="{{url('new/index')}}">
                      <img src="images/logo.jpg" height="80" width="80" style="margin:0px;">
                      </a>
                    Tests Taken : 974,534 | MCQs Answered : 7,676,717 

               
                    </p>
                    <span class="searchform">
                     
                                    <a href="#" class="btn blue">Teacher.apk
                              <a href="#" class="btn blue">Student.apk</a>
                      <a href="{{url('new/register')}}" class="btn blue">Register</a>
                      <a href="{{url('new/login')}}" class="btn blue" >Login</a>
                    </span>
                  </div>
                  <div id="logo-mob">
                    <a href="{{url('new/index')}}">
                      <img src="{{asset('images/prep.jpg')}}" height="120" width="120" style="margin:0px;">
                      </a>
                      <p>Tests Taken : 974,534 | MCQs Answered : 7,676,717 </p>
                      <p>
                          <a href="#" class="btn blue">Teacher.apk
                              <a href="#" class="btn blue">Student.apk</a>
                       <a href="{{url('new/register')}}" class="btn blue">Register</a>
                      <a href="{{url('new/login')}}" class="btn blue" >Login</a>
                      </p>
                    </div>
                  </section>
                  <nav class="mainmenu">
                    <ul>
                      <li id="ctl22_home" class="current-menu-item">
                        <a href="{{url('new/index')}}">Home</a>
                      </li>
                      <li id="ctl22_prepare" class="">
                        <a href="{{url('new/how-to-prepare')}}">How to Prepare?</a>
                      </li>
                      <li id="ctl22_blogs" class="">
                        <a href="#">Blog</a>
                      </li>
                      <li id="ctl22_faqs" class="">
                        <a href="#">FAQs</a>
                      </li>
                      <li id="ctl22_invite">
                        <a href="{{url('new/testimonial')}}">Testimonials</a>
                      </li>
                      <li id="ctl22_about" class="">
                        <a href="#">Past Papers</a>
                      </li>
                      <li id="ctl22_contact" class="">
                        <a href="{{url('new/contact')}}">Contact Us</a>
                      </li>
                    </ul>
                    <div class="clear"></div>
                    <select id="sec-nav" name="sec-nav">
                      <option value="/">Menu</option>
                      <option value="{{url('new/index')}}">Home</option>
                      <option value="{{url('new/how-to-prepare')}}">How to Prepare?</option>
                      <option value="#">Blog</option>
                      <option value="#">FAQs</option>
                      <option value="{{url('new/testimonial')}}">Testimonials</option>
                      <option value="#">Past Papers</option>
                      <option value="{{url('new/contact')}}">Contact Us</option>
                    </select>
                  </nav>
                </header>
  
    @yield('content')
@show


<footer>
                                                  <section class="widgets">
                                                    <article>
                                                      <h3>
                                                        <span>Contact Us</span>
                                                      </h3>
                                                      <p>entrytest4u (Pvt) Limited
                                                        <br>Islamabad
                                                          <br>Pakistan
                                                            <br>E-mail: 
                                                              <a href="mailto:support@entrytest4u.com" title="contact entrytest4u.com">support@entrytest4u.com</a>
                                                              <br/>Phone: 0335-5833265
                                                            </p>
                                                          </article>
                                                          <article class="widget_links">
                                                            <h3>
                                                              <span>Useful Links</span>
                                                            </h3>
                                                            <ul>
                                                              <li>
                                                                <a class="whitelink" href="terms-and-conditions.html" title="Terms & Conditions">Terms & Conditions</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="privacy-policy.html" title="Privacy Policy :: Entry Test Preparation Pakistan">Privacy Policy</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="disclaimer.html" title="Disclaimer :: Aptitude Test  Preparation">Disclaimer</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="blogs/index.html" title="Blog :: Medical and Engineering Entry Test ">Blog</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="faqs.html" title="FAQs :: Entry Test Preparation">FAQs</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="refer/invite-a-friend.html" title="Invite a friend :: Entry Test Preparation">Invite A Friend</a>
                                                              </li>
                                                            </ul>
                                                          </article>
                                                          <article class="widget_links">
                                                            <h3>
                                                              <span>Useful Links</span>
                                                            </h3>
                                                            <ul>
                                                              <li>
                                                                <a class="whitelink" href="about-us.html" title="About us :: Entry Test Preparation Pakistan">About us</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="contact-us.html" title="Contact ::  Aptitude Test  Preparation ">Contact us</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="account/login.html" title="Login :: Entry Test Preparation ">Login</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="account/register.html" title="Register Now on entrytest4u">Register</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="account/forgottenpassword.html" title="Forgotten Password :: Entry test preparation">Forgotten password</a>
                                                              </li>
                                                              <li>
                                                                <a class="whitelink" href="practise/transactiondetails.html" title="Buy full version">Buy full version</a>
                                                              </li>
                                                            </ul>
                                                          </article>
                                                        </section>
                                                        <section class="bottom">
                                                          <p style="font-size:13px;">&copy; 2019 - entrytest4u.com</p>
                                                          <nav class="social">
                                                            <ul>
                                                              <li>
                                                                <a href="https://www.facebook.com/entrytest4u" class="facebook" title="entrytest4u.com on facebook">Facebook</a>
                                                              </li>
                                                              <li>
                                                                <a href="https://twitter.com/entrytest4u" class="twitter" title="entrytest4u.com on twitter">Twitter</a>
                                                              </li>
                                                              <li>
                                                                <a href="https://plus.google.com/+entrytest4uofficial" class="googleplus" title="entrytest4u.com on Google+">Google+</a>
                                                              </li>
                                                            </ul>
                                                          </nav>
                                                        </section>
                                                      </footer>
                                                    </div>
                                                  
                                                  </form>
                                                  
                                              
                                                </body>
                                                
                                              </html>

@stack('pre-scripts')

@section('scripts')

<script type="text/javascript" src="{{asset('new/content/wc/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('new/content/wc/js/scripts.js')}}"></script>




<script type="text/javascript">
  $.ajaxSetup({
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
 });



</script>





@show


@stack('post-scripts')

</body>
</html>