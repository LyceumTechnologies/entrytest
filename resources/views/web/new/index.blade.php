<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
								<section class="slider slider1">
									
										<article>
											<img src="{{asset('new/content/wc/images/mcq-05.png')}}" alt="Practise MCQs" title="Practise MCQs on entrytest4u">
												<div>
													<h3>Practise MCQs</h3>
													<p>15,000+ fully explained MCQs. Register and Try for Free!</p>
													<p class="cta">
														<a href="account/register.html" class="btn blue" title="register on entrytest4u.com" style="color:#fff;" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button Slider 2']);">Register!</a>
														<a href="#sample" style="color:#3f3f3f;" title="Practise MCQs on entrytest4u" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Learn More Button Slider 2']);">Learn more</a>
													</p>
												</div>
											</article>
											<article>
												<img src="{{asset('new/content/wc/images/analytics-04.png')}}" alt="Smart Analytics" title="Smart Analytics :: Medical and Engineering entry test">
													<div>
														<h3>Smart Analytics</h3>
														<p>Complete Log of Mistakes and Analytics to Help you Improve Performance.</p>
														<p class="cta">
															<a href="account/register.html" class="btn blue" title="register on entrytest4u.com" style="color:#fff;" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button Slider 3']);">Register!</a>
															<a href="#sample" style="color:#3f3f3f;" title="Mock Tests" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Learn More Button Slider 3']);">Learn more</a>
														</p>
													</div>
												</article>
												<article>
													<img src="{{asset('new/content/wc/images/mock-02.png')}}" alt="Mock Tests" title="Mock Tests">
														<div>
															<h3>Mock Tests</h3>
															<p> Simulation of Real Test Environment for GIKI, NUST, UET, MCAT and other leading Universities. Take the Simulated Test Before the Actual Exam!</p>
															<p class="cta">
																<a href="account/register.html" class="btn blue" title="Register Now :: Entry test preparation Pakistan" style="color:#fff;" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button Slider 4']);">Register!</a>
																<a href="#sample" style="color:#3f3f3f;" title="Learn More :: Entry test preparation Pakistan" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Learn More Button Slider 2']);">Learn more</a>
															</p>
														</div>
													</article>
													<article>
														<img src="{{asset('new/content/wc/images/analytics-03.png')}}" alt="Subjects,Math,Physics,Chemistry,English" title="Physics, Chemistry, Biology, Maths and English MCQS">
															<div>
																<h3>Subjects</h3>
																<p> MCQs available for Physics, Chemistry, Biology, Maths, Intelligence and English. </p>
																<p class="cta">
																	<a href="account/register.html" class="btn blue" title="Register Now" style="color:#fff;" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Register Button Slider 5']);">Register!</a>
																	<a href="#sample" style="color:#3f3f3f;" title="Learn More :: Entry test preparation Pakistan" onclick="_gaq.push(['_trackEvent', 'Home Page', 'click', 'Learn More Button Slider 5']);">Learn more</a>
																</p>
															</div>
														</article>
													</section>
													<section class="content">
														<article class="main">
															<h1>Welcome to entrytest4u.com</h1>
															<article class="col34" style="margin:0 2% 20px 0; width:95%;">
																<article class="tab-content">
																	<ul class="tick" style="float:left; margin:0px;">
																		<li>Prepare online for medical, engineering, Army, PAF, 
																			<br /> Navy and NTS admission tests.
																		</li>
																		<li>15,000+ MCQs for Maths, Biology, Physics, Chemistry, 
																			<br /> English and Intelligence.
																		</li>
																		<li>Every single question explained.</li>
																		<li>MCQs sorted by subject, topic and difficulty level.</li>
																		<li>Smart Analytics to help your prepare better.</li>
																		<li>Compare yourself with the other students.</li>
																		<li>Mock tests for GIKI, NUST, ECAT, MCAT etc format.</li>
																		<li>Complete personal log of how one has performed.</li>
																	</ul>
																	<div class="alignright">
																		<a href="account/register.html" title="Register yourself on entrytest4u.com">
																			<img alt="Register Now on entrytest4u" src="{{asset('new/content/wc/images/register-now1.png')}}">
																			</a>
																		</div>
																	</article>
																</article>
																
																<br/>
																<br/>
																<br/>
																<article class="col34 article-qofd">
																	<section class="product-list-full section-qofd">
																		<h2>
																			<span>Question Of The Day</span>
																		</h2>
																		<ol class="no-bullets-qofd radioButtonList-qofd">
																			<li>
																				<p>
																					<strong>Question : </strong>
	If magnitude of Force applied is increased, work done is?

																				</p>
																			</li>
																			<li>
																				<input id="RadioButtonAnswerA" type="radio" name="ctl00$MainContent$ctl00$Answers" value="RadioButtonAnswerA" />
																				<label for="RadioButtonAnswerA">A: </label>
																				<span class="answer_qofd">
	Increased
</span>
																			</li>
																			<li>
																				<input id="RadioButtonAnswerB" type="radio" name="ctl00$MainContent$ctl00$Answers" value="RadioButtonAnswerB" />
																				<label for="RadioButtonAnswerB">B: </label>
																				<span class="answer_qofd"> 
	Decreased
</span>
																			</li>
																			<li>
																				<input id="RadioButtonAnswerC" type="radio" name="ctl00$MainContent$ctl00$Answers" value="RadioButtonAnswerC" />
																				<label for="RadioButtonAnswerC">C: </label>
																				<span class="answer_qofd">
	No change
 </span>
																			</li>
																			<li>
																				<input id="RadioButtonAnswerD" type="radio" name="ctl00$MainContent$ctl00$Answers" value="RadioButtonAnswerD" />
																				<label for="RadioButtonAnswerD">D: </label>
																				<span class="answer_qofd">
	Becomes zero.
 </span>
																			</li>
																		</ol>
																		<span  style="padding-left:30px">
																			<input type="submit" name="ctl00$MainContent$ctl00$ButtonQuestionOfTheDay" value="Submit" id="MainContent_ctl00_ButtonQuestionOfTheDay" class="btn blue" />
																		</span>
																	</section>
																</article>
																<article class="col34" style="margin:0 2% 20px 0; width:95%;">
																	<section class="product-list-full">
																		<h2>
																			<span>Articles From entrytest4u.com Blog</span>
																		</h2>
																		<ul>
																			<li>
																				<h3>
																					<a href="blogs/2019/05/02/engineering-programs-offered-to-dae-candidates-in-nust-3/index.html" title="ENGINEERING PROGRAMS OFFERED TO DAE CANDIDATES IN NUST">ENGINEERING PROGRAMS OFFERED TO DAE CANDIDATES IN NUST</a>
																				</h3>
																				<dl class="product-data">
																					<dt>Category:</dt>
																					<dd>
																						<a href="#" title="NUST">NUST</a>
																					</dd>
																					<dt>Post Date:</dt>
																					<dd>
																						<a href="#" title="entrytest4u">02 May 2019</a>
																					</dd>
																				</dl>
																				<p>National University of Sciences and Technology NUST very well-known and top rated engineering university of Pakistan. NUST was founded in 1991 with mission of providing quality education and invest in research. So far in a very short period of time they are able to earn very good name in world. NUST provides wide range of ...
																					<a href="blogs/2019/05/02/engineering-programs-offered-to-dae-candidates-in-nust-3/index.html" title="ENGINEERING PROGRAMS OFFERED TO DAE CANDIDATES IN NUST">read more</a>
																				</p>
																			</li>
																			<li>
																				<h3>
																					<a href="blogs/2019/04/30/etea-sample-physics-questions-3/index.html" title="ETEA SAMPLE PHYSICS QUESTIONS">ETEA SAMPLE PHYSICS QUESTIONS</a>
																				</h3>
																				<dl class="product-data">
																					<dt>Category:</dt>
																					<dd>
																						<a href="#" title="ETEA">ETEA</a>
																					</dd>
																					<dt>Post Date:</dt>
																					<dd>
																						<a href="#" title="entrytest4u">30 April 2019</a>
																					</dd>
																				</dl>
																				<p>ETEA stands for Educational Testing and Evaluation Agency, it is an educational entity established in 1998 by KPK government. In order to get admission in the public sector Medical Colleges of KPK, the students have to undergo KPK ETEA entrance test, which is a requirement. Hundreds and thousands of students take this test each year. ...
																					<a href="blogs/2019/04/30/etea-sample-physics-questions-3/index.html" title="ETEA SAMPLE PHYSICS QUESTIONS">read more</a>
																				</p>
																			</li>
																			<li>
																				<h3>
																					<a href="blogs/2019/04/26/programs-offered-by-ned-university-of-engineering-technology-2/index.html" title="Programs Offered By NED University of Engineering & Technology">Programs Offered By NED University of Engineering & Technology</a>
																				</h3>
																				<dl class="product-data">
																					<dt>Category:</dt>
																					<dd>
																						<a href="#" title="NED">NED</a>
																					</dd>
																					<dt>Post Date:</dt>
																					<dd>
																						<a href="#" title="entrytest4u">26 April 2019</a>
																					</dd>
																				</dl>
																				<p>NED University of Engineering &#38; Technology is one of the oldest universities in Pakistan. NED is a public university established in 1921. NED was established by British government to provide civil engineering working in building Sukkur Barrage. Since then NED establishment worked hard and earned a very respectable name in Pakistan engineering society. Now NED ...
																					<a href="blogs/2019/04/26/programs-offered-by-ned-university-of-engineering-technology-2/index.html" title="Programs Offered By NED University of Engineering & Technology">read more</a>
																				</p>
																			</li>
																			<li>
																				<h3>
																					<a href="blogs/2019/04/16/nust-need-based-scholarship-3/index.html" title="NUST NEED BASED SCHOLARSHIP">NUST NEED BASED SCHOLARSHIP</a>
																				</h3>
																				<dl class="product-data">
																					<dt>Category:</dt>
																					<dd>
																						<a href="#" title="NUST">NUST</a>
																					</dd>
																					<dt>Post Date:</dt>
																					<dd>
																						<a href="#" title="entrytest4u">16 April 2019</a>
																					</dd>
																				</dl>
																				<p>National University of Science and Technology NUST is top rated engineering university of Pakistan. NUST was founded in 1991 with an aim of providing quality education. NUST grow up dramatically in a very little time period because of their focus and dedication toward their work. NUST provide vast range of undergraduate and Master’s programs for ...
																					<a href="blogs/2019/04/16/nust-need-based-scholarship-3/index.html" title="NUST NEED BASED SCHOLARSHIP">read more</a>
																				</p>
																			</li>
																			<li>
																				<h3>
																					<a href="blogs/2019/04/10/nust-undergraduate-fee-structure-2/index.html" title="NUST UNDERGRADUATE FEE STRUCTURE">NUST UNDERGRADUATE FEE STRUCTURE</a>
																				</h3>
																				<dl class="product-data">
																					<dt>Category:</dt>
																					<dd>
																						<a href="#" title="NUST">NUST</a>
																					</dd>
																					<dt>Post Date:</dt>
																					<dd>
																						<a href="#" title="entrytest4u">10 April 2019</a>
																					</dd>
																				</dl>
																				<p>National University of Science and Technology NUST is working in Pakistan since last 3 decades with a vision of “To evolve NUST into a world class Centre of Excellence among Higher Education Institutions, leading the transformation of Pakistan towards a rapidly developing Knowledge Economy to realize the national objective of a progressive and prosperous country ...
																					<a href="blogs/2019/04/10/nust-undergraduate-fee-structure-2/index.html" title="NUST UNDERGRADUATE FEE STRUCTURE">read more</a>
																				</p>
																			</li>
																			<li>
																				<h3>
																					<a href="blogs/2019/04/09/nust-need-based-scholarship-2/index.html" title="NUST NEED BASED SCHOLARSHIP">NUST NEED BASED SCHOLARSHIP</a>
																				</h3>
																				<dl class="product-data">
																					<dt>Category:</dt>
																					<dd>
																						<a href="#" title="NUST">NUST</a>
																					</dd>
																					<dt>Post Date:</dt>
																					<dd>
																						<a href="#" title="entrytest4u">09 April 2019</a>
																					</dd>
																				</dl>
																				<p>National University of Science and Technology NUST is top rated engineering university of Pakistan. NUST was founded in 1991 with an aim of providing quality education. NUST grow up dramatically in a very little time period because of their focus and dedication toward their work. NUST provide vast range of undergraduate and Master’s programs for ...
																					<a href="blogs/2019/04/09/nust-need-based-scholarship-2/index.html" title="NUST NEED BASED SCHOLARSHIP">read more</a>
																				</p>
																			</li>
																			<li>
																				<h3>
																					<a href="blogs/2019/04/05/etea-sample-physics-questions-2/index.html" title="ETEA SAMPLE PHYSICS QUESTIONS">ETEA SAMPLE PHYSICS QUESTIONS</a>
																				</h3>
																				<dl class="product-data">
																					<dt>Category:</dt>
																					<dd>
																						<a href="#" title="ETEA">ETEA</a>
																					</dd>
																					<dt>Post Date:</dt>
																					<dd>
																						<a href="#" title="entrytest4u">05 April 2019</a>
																					</dd>
																				</dl>
																				<p>ETEA stands for Educational Testing and Evaluation Agency, it is an educational entity established in 1998 by KPK government. In order to get admission in the public sector Medical Colleges of KPK, the students have to undergo KPK ETEA entrance test, which is a requirement. Hundreds and thousands of students take this test each year. ...
																					<a href="blogs/2019/04/05/etea-sample-physics-questions-2/index.html" title="ETEA SAMPLE PHYSICS QUESTIONS">read more</a>
																				</p>
																			</li>
																		</ul>
																	</section>
																</article>
															</article>
															<aside>
																<h3>
																	<a href="testimonials.html">
																		<span class="testimonial">Latest Testimonials (Read All)</span>
																	</a>
																</h3>
																<div class="testimonial">
																	<h3>
																		<span>Amir Taimur</span>
																	</h3>
																	<p>
																		<strong>UET Peshawar (2017) </strong>
																	</span>
																</p>
																<span></span>
																<div>
																	<p>I got admission in UET PESHAWAR. I did not attended any academy. MCQs of the entrytest4u helped me a lot to clear the ETEA engineering test.</p>
																</div>
															</div>
															<div class="testimonial">
																<h3>
																	<span>Bilal Shahid</span>
																</h3>
																<p>
																	<strong>UET (2015) </strong>
																</span>
															</p>
															<span></span>
															<div>
																<p>Sir I got Selected in GIKI , NUST , UET and PIEAS .... wherever I applied I got selected :). Its all because of the efforts of your team. Thanks to you for the excellent quality MCQs. I tried them a lot.</p>
															</div>
														</div>
														<div class="testimonial">
															<h3>
																<span>Shirza Qaiser</span>
															</h3>
															<p>
																<strong>Lahore Medical and Dental college (2017) </strong>
															</span>
														</p>
														<span></span>
														<div>
															<p>Thank you entrytest4u for providing me the platform where I studied and passed my test. Thank you so much.</p>
														</div>
													</div>
													<div class="testimonial">
														<h3>
															<span>Hoorya Faatmaa</span>
														</h3>
														<p>
															<strong>Nawaz Sharif Medical College  (2015) </strong>
														</span>
													</p>
													<span></span>
													<div>
														<p>By the grace of Allah, I was able to secure admission in a medical college through UHS MCAT 2015, and entrytest4u helped me tremendously in preparation and achieving success. entrytest4u is a very well designed website, with tons of questions and answers, as well as easy to use navigation and comprehensive stats. It helped me to prepare daily with specific objective and constant progress. The mock test were highly helpful at the end of my preparation. The whole study planning offered by entrytest4u, contributed greatly toward my success in MCAT.</p>
													</div>
												</div>
												<div class="testimonial">
													<h3>
														<span>Ommair Ahmed</span>
													</h3>
													<p>
														<strong>NUST (2016) </strong>
													</span>
												</p>
												<span></span>
												<div>
													<p>Getting admission in NUST was my dream. When i gave NET 1st time I got only 96 marks. I was really disappointed but then I decided to join entrytest4u.com and didn't join any academy. The website helped me a lot. I learnt each and every question from entrytest4u and I found MCQs very suitable to NET standards. I also found mock tests to be very useful. Mock test gave me experience to solve NET paper and due to entrytest4u I scored 133 in NET and got admission in NUST PNEC Mechanical engineering ......Thank you entrytest4u.</p>
												</div>
											</div>
											<div class="testimonial">
												<h3>
													<span>Suleman Hasan Khan</span>
												</h3>
												<p>
													<strong>NUST Chemical engineering  (2018) </strong>
												</span>
											</p>
											<span></span>
											<div>
												<p>I had a great experience of using entrytest4u.com. It prepared me for all sorts of MCQs.  I got 153 in NET3 and I am very grateful to entrytest4u for their friendly interface, variety of MCQs and Mock tests. </p>
											</div>
										</div>
										<div class="testimonial">
											<h3>
												<span>Noor ul Ain Munir</span>
											</h3>
											<p>
												<strong>GIKI (2015) </strong>
											</span>
										</p>
										<span></span>
										<div>
											<p>My Name is Noor ul Ain Munir, Allahmudullah I got Admission in GIKI in the Faculty of ES (Batch 25) and the credit for this goes only to entrytest4u,com. The best website for the preparation of entry test of any university. I didn't join any academy for the preparation of entry tests. I just practiced MCQ's from entrytest4u, It is the only website  which prepares you for any type of entry test and it is perfect in every way for the preparation of entry tests. If you use entrytest4u for the preparation of entry test you need not to join any acadmy e.g kips, Star etc which are only made for earning money by doing "Darr ka Business". I wish I could tell every student out there preparing for entry test that they don't need to join any academy. All they need is entrytest4u for the preparation of entry test. A salute to the team of entrytest4u for their sincere efforts.</p>
										</div>
									</div>
									<i>
										<a href="testimonials.html">...read all testimonials</a>
									</i>
									<h3>
										<span class="testimonial">Invite a friend</span>
									</h3>
									<div>
										<a href="refer/invite-a-friend.html">
											<img src="{{asset('new/content/wc/images/invite-promo.jpg')}}" id="invite-promo" alt="Invite a Friend and Earn Cash" />
										</a>
									</div>
									<h3>
										<span class="testimonial">Past Papers</span>
									</h3>
									<p>
										<a href="past-papers.html">
											<span>NUST Computer Science past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers.html">
											<span>NUST Engineering past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>NUST Bio-sciences past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers.html">
											<span>ECAT past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>ETEA past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>FMDC past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>SMB past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>DOW Medical College past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>Sindh MCAT past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>CMC Medical college past papers</span>
										</a>
									</p>
									<p>
										<a href="past-papers-med.html">
											<span>UHS MCAT past papers</span>
										</a>
									</p>
									
								</aside>
							
														
																		
																								</section>
																								@endsection
