
<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
            
            
   

    <section class="content">
        <article class="main">
       <h1>Contact Us</h1>

    
<p>If you have any questions or comments about our webiste, please contact us via email at <a href="mailto:support@entrytest4u.com" title="Feel free to contact for any Help">support@entrytest4u.com</a> or via the <a href="https://www.facebook.com/entrytest4u" title="Like entrytest4u Page on Facbook">entrytest4u.com Facebook Page</a></p>
<p>You can also contact us via phone on at 0335-5833265</p>
     </article>
            <aside>
		<section class="col3-content">
			<p class="img"><a href="account/register.html" title="register"><span class="img-border"><img alt="register" src="{{asset('new/content/wc/images/insights.jpg')}}" width="300" title="Entry Test Preparation Pakistan"></span></a></p>
			</section>
            
    
			
	</aside>
	<article class="col34" style="margin:0 2% 20px 0; width:95%;">
				<section class="product-list-full">
	        <h2><span>Book Shop Addresses</span></h2>
            <p>Our full membership scratch cards are available from the following stores:</p>		
			<ul>
			
			
			<li>
				<h3><span>IDRIS Book Bank</span></h3>
				<p>Bank road Saddar Rawalpindi. Phone # 051-5568272, 051-5568898. </p>
			</li>
			
			<li>
				<h3><span>IDRIS Book Bank 2</span></h3>
				<p>Shop#1, ALFEROZ MALL, Main PWD Road- Islamabad. </p>
			</li>	
<!--			
		   <li>
				<h3><span>Khurram Book Seller</span></h3>
				<p>Shop#2-55/4. Amin Plaza Behind Bread & Butter Bank Road Saddar Rawalpindi. Phone# 051-5528660. </p>
			</li>	
		   <li>
				<h3><span>SOHNEE Books & Uniforms </span></h3>
				<p>Old Iqbal Plaza, Commercial Market, Satellite Town, Rawalpindi</p>
			</li>	
			
			   <li>
				<h3><span>New & Old Book </span></h3>
				<p>6th Road Corner</p>
			</li>	
			
			   <li>
				<h3><span>Books & Books</span></h3>
				<p>Commercial Center, Satellite Town, Rawalpindi</p>
			</li>	
			
			   <li>
				<h3><span>Mr Books </span></h3>
				<p>F-6 Supper Market, Islamabad </p>
			</li>	
			
						   <li>
				<h3><span>Al-Fatimi Books</span></h3>
				<p>Commercial Area, Chaklala Scheme III, Rawalpindi.</p>
			</li>	
			
						   <li>
				<h3><span>Galaxy Books  </span></h3>
				<p>Opp. Master Fabric, Commercial Market, Chaklala Scheme III, Rawalpindi.</p>
			</li>	
			-->
			
			</ul>
			<p>If you are a book shop owner and interested in becoming our partner, please contact us on 0335-5833265</p>
			</article>
   
 
		
    
	<a class="go-top" href="#top" style="display: none;">Go to top of page</a>
    
            
    </section>


@endsection
