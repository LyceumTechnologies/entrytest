
<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
   
            
            
   <section class="content">
        <article class="main">
       <h1>Privacy Policy</h1>


<h2> Commitment to Privacy</h2>
<p>Your privacy is important to us and we, at entrytest4u respect the need to protect your private information. We understand the sensitivity of the personal information which may concern such diverse fields ranging from your career aspirations to your gender preferences. We shall take such reasonable steps as may be necessary to ensure that your private information remains confidential. 
This Privacy Policy outlines our online information practices and the choices you can make in giving information about yourself and how such information would be used by us.
This Website is hosted by or on behalf of entrytest4u and we are committed to a safe method of collecting and maintaining in a secure environment any personal information which may be requested from you and/or collected from you in accordance with this Privacy Policy and such laws, rules and regulations as may be applicable.</p>

<h2>Notice and Disclosure </h2>

<p>In general, you may visit this Website without identifying yourself or revealing any personal information. entrytest4u.com collects domain information from your visit to customize and improve your experience on our Website. Some portions of this Website may require you to give us personally identifiable information, which is information that enables us to identify you, such as your name, email, age, gender, place of work, contact number, location only when you voluntarily choose to submit it to us. If you opt to provide us that sort of information, we may also use it for purposes such as verifying your identity, sending you information or contacting you. For certain programs, we may make your information available to a third party, for marketing and other purposes which we believe may be beneficial to you. entrytest4u will not be responsible for any further agreement / sale / partnership between you and any other third party.</p>

<p>PLEASE NOTE THAT entrytest4u SHALL IN NO WAY BE HELD RESPONSIBLE OR LIABLE FOR ANY INFORMATION WHICH YOU PROVIDE TO A THIRD PARTY, WHICH MAY BE TABBED INTO OUR WEBSITE. THE INFORMATION BEING PROVIDED TO THE THIRD PARTY APPLICATIONS AND SITES SHALL BE GOVERNED BY THE INDIVIDUAL PRIVACY POLICIES OF THESE THIRD PARTY SERVICE PROVIDERS.</p>

<h2>Choice to Opt-Out</h2>
<p>It is our intent to inform you before we collect personally identifiable information, and tell you what we intend to do with it. You will have the option of not providing the information, in which case you may still be able to access other portions of this website, although you may not be able to access certain programs or services. In certain portions of this website, we also may enable you to "opt out" of certain uses of your information, or elect not to receive future communications or services.</p>

<h2>Data Security</h2>
<p>We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These may include such data collection, storage and processing practices and security measures, as well as physical security measures as may be necessary to guard against unauthorized access to systems where we store personal data. However, given that the nature of Internet is changing rapidly and more malicious code is being developed with every passing day, we do not guarantee the absolute protection of your information from being destroyed or stolen by means of introduction of such code into our servers, though we shall put our best efforts to prevent such an occurrence. However, you understand and agree that when you approach or are guided to third party gateways for payment, entrytest4u shall not be liable for the information provided by you (credit card number, debit card information, bank account detail or any personal information) to the third party.
We restrict access to personal information to entrytest4u employees, contractors and agents who need to know that information in order to operate, develop or improve our services. These individuals are bound by confidentiality obligations and may be subject to discipline, including termination of services, if they fail to meet these obligations.
However, in case of an unlikely eventuality of a data leakage, we shall endeavour to take all reasonable steps, as may be possible, to protect your information from being passed onto the hands of third parties, and shall proceed to recover such information, which, by means of physical or technological means may be deemed to be recoverable. </p>

<p>NOTWITHSTANDING ANYTHING CONTAINED HEREIN, entrytest4u SHALL IN NO WAY BE MADE LIABLE OR RESPONSIBLE FOR ANY LOSS OF INFORMATION AND/ OR DATA AND/ OR DIRECT OR INDIRECT DAMAGES OR LOSSES WHICH MAY OCCUR FROM SUCH ACTS AND/ OR DEEDS AND CONSEQUENCES, OF WHATSOEVER NATURE ARISING THEREFROM. </p>

<p>You may also note that your computer system shall have to be kept up-to-date with the latest anti-virus/ anti-spyware for your information which is residing in your system to be protected. </p>

<p>PLEASE NOTE THAT entrytest4u SHALL IN NO WAY BE RESPONSIBLE FOR ANY LOSS OF INFORMATION/ DATA WHICH MAY OCCUR FROM YOUR SYSTEM DUE TO INTRODUCTION OF ANY TROJANS, VIRUSES, BUGS, LOGIC BOMBS OR SPYWARE INTO YOUR SYSTEM.</p>

<h2>Data Quality and Access </h2>
<p>entrytest4u.com strives to keep your information and data accurate, complete and current. We will promptly take steps to correct any inaccuracies in your personally identifiable information that we are made aware of. </p>

<h2>Business Relationships</h2>
<p>The Website contains (or the Website or Service may provide) links to other websites ("Third Party Sites") as well as to articles, photographs, text, graphics, pictures, designs, presentations, music, sound, video, information, applications, software and other content or items belonging to or originating from third parties (the "Third Party Applications, Software or Content"). Such Third Party Sites or Third Party Applications, Software or Content are not investigated, monitored or checked for accuracy, appropriateness, or completeness by us. entrytest4u is not responsible for the privacy practices or the content of such websites. entrytest4u will not and cannot censor or edit the content of any third-party site. You understand and agree that by affording use of the Website, entrytest4u does not become liable for any loss or damage that you may incur by approaching such external site or resource, or by relying on the completeness, accuracy or existence of any advertising, product or material available on or from, such website or resource. Hence, you are advised to verify the contents at your own before acting thereon.
entrytest4u may use pixels, or transparent GIF files, to help manage online advertising. These GIF files are provided by our ad management department. These files enable the department to recognize a unique cookie on your Web browser, which in turn enables us to know which advertisements bring users to our Website. The cookie has been placed by us. With cookies technology, the information that we collect and share is anonymous and not personally identifiable. It does not contain your name, address, telephone number, or email address.</p>

<h2>Cookies</h2>
<p>Portions of this website use cookies to keep track of your visit, or to deliver content specific to your interests. A "Cookie" is a small amount of data transferred to your browser and read by the Web server that placed it there. It works as a sort of identification card, recording your preferences and previously entered information. You can set your browser to notify you when you receive a cookie, giving you the chance to accept or reject it.</p>

<h2>Contact Us</h2>
<p>If you have any questions or comments about our privacy statement or practices, please contact us via email at support@entrytest4u.com with the words "PRIVACY POLICY" in the subject line. entrytest4u reserves the right to modify or update this privacy statement at any time without prior notice.</p>



    </article>
       </section>
    



    @endsection
