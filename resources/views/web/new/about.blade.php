<?php
/**
 * Project: Entry Test 4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('web.new.layouts.default')
@section('content')
   
            
            


    <section class="content">
        <article class="main">
       <h1>About</h1>
            <p><a href="http://www.entrytest4u.com/" title="Entry Test">entrytest4u</a> was developed with a mission to provide "Simple, Innovative and Affordable Entry Test Preparation for All". </p>

<p><a href="http://www.entrytest4u.com/" title="Aptitude Test Preparation">entrytest4u</a> is not just another website. It is a complete intelligent system for the entry test preparation.</p>

<p>The idea evolved over a coffee conversation, where the two co-founders exchanged the ideas on a potential tech start-up to help the prospective students in their entry test preparation for the engineering and medical colleges.</p>

<p>During our student days, we always had a difficult time in getting access to a good quality MCQs database that was error free, challenging, and easy to use / navigate. The other problem is that individual students have no way to measure how he or she is performing as compared to the peers (no academy can do that). There is no existing solution in the market to track the progress and systematically help the students to channel their energies to the weakest areas. Finally, our experience is that taking mock tests and repeating wrongly answered questions is the most effective tool in the entry test preparation. </p>

<p><a href="http://www.entrytest4u.com/" title="Entry test preparation Pakistan">entrytest4u</a> is an innovative system that provides the solutions to all these challenges and more. It boasts,</p>

<ul>
<li>15,000+ good quality, error free MCQs.</li>
<li>Every single question explained.</li>
<li>MCQs sorted by subject, topic and difficulty level.</li>
<li>Smart Analytics to help your prepare better.</li>
<li>Compare yourself with the other students.</li>
<li>Mock tests for GIKI, NUST, ECAT, MCAT etc format.</li>
<li>Complete personal log of how one has performed.</li>
</ul>
<p>It is worth mentioning that every single person involved in the development of <a href="http://www.entrytest4u.com/" title="Medical and Engineering Entry Test">entrytest4u</a> has passed one or more competitive entry test. We have translated that wealth of first hand experience into this product to help you prepare more effectively.</p>

<p>We are confident that <a href="http://www.entrytest4u.com/" title="Engineering Entry Test Prepration in Pakistan">entrytest4u</a> provides the best entry test preparation experience in Pakistan. It will help the students in preparing better and getting an edge over the others. </p>


            </article>
            <aside>
		<section class="col3-content">
			<p class="img"><a href="account/register.html" title="register"><span class="img-border"><img alt="Register Now for Online Entry Test Prepration" src="{{asset('new/content/wc/images/insights.jpg')}}" title="Register Now for Online Entry Test Prepration" width="300"></span></a></p>
			</section>
            <section class="col3-content">
			<p class="img"><a href="account/register.html" title="Entry test preparation Pakistan"><span class="img-border"><img alt="Entry test preparation Pakistan" src="{{asset('new/content/wc/images/exams.jpg')}}" title="Entry test preparation Pakistan" width="300"></span></a></p>
			
			</section>
            <section class="col3-content">
			<p class="img"><a href="account/register.html" title="register"><span class="img-border"><img alt="Entry test preparation" title="Entry test preparation" src="{{asset('new/content/wc/images/hassan.jpg')}}" width="300"></span></a></p>
			</section>
            
			<section class="col3-content">
			<p class="img"><a href="account/register.html" title="register"><span class="img-border"><img alt="Entry test preparation" title="Sajjad Akbar, UET Lahore" src="{{asset('new/content/wc/images/sajjad.jpg')}}" width="300"></span></a></p>
			</section>
            
    
			
	</aside>
   
 
		
    
	<a class="go-top" href="#top" style="display: none;">Go to top of page</a>
    
        
    </section>
    


@endsection