 <html>
    <head>
        <meta charset="utf-8">
      

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{asset('web/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('web/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('web/css/bootstrap.mins.css')}}"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('web/css/bootstrap4_pyment.css')}}"> 
  
 
        <title>Education system</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <style>
            html {
              scroll-behavior: smooth;
            }
            span{
              position: absolute;
              color: rgb(253, 252, 252);
              left: 1%;
              bottom: 25%;
              right: 0%;
              margin: 1%;
              font-family: sans-serif;
              padding: 0.5% 0%;
              font-size: 10px;
              transform: translateY(50px);
              opacity: 1;
              transition: all 1s ease-in;
              font-weight: 300px;
            }
          </style>  
    </head>
 <body>
     <!--navBar-->
     <div class="container-fluid bg-black">

        <nav class="container navbar navbar-expand-lg navbar-dark bg-black">
          
   <a class="navbar-brand" href="">Home</a>
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
     <span class="navbar-toggler-icon"></span>
   </button>
 
   <div class="collapse navbar-collapse" id="navbarSupportedContent">
     <ul class="navbar-nav mr-auto">
       <li class="nav-item active">
         <a class="nav-link" href="{{route('about')}}">About Us</a>
       </li>
       <li class="nav-item active">
           <a class="nav-link" href="#" >Services </a>
         </li>
         <li class="nav-item active">
            <a class="nav-link" href="{{route('easy-paisa.index')}}" >Payment </a>
          </li>
         <li class="nav-item active">
					<a class="nav-link" href="#" >Terms & Condition </a>
				</li>
        <li class="nav-item active">
          <a class="nav-link" href="{{route('login')}}" >Sign in </a>
        </li>
     </ul>
   </div>
 </nav>
 </div>
     <!--slider-->
         </head>
      <body><div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
           <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
           <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
           <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
         </ol>
         <div class="carousel-inner">
           <div class="carousel-item active">
             <img class="d-block w-100" src="{{asset('web/images/img1.jpg')}}" alt="First slide">
             <div class="carousel-caption animated ">
                 <h1 style="color:white">STUDY ANYTIME, ANYWHERE</h1>
                 <h5 style="color:white">BUY EASY PAY EASY</h5>
                  <p style="color:white">Get Application from playstore.</p>
                  <button class="btn btn-info btn-lg">Teacher.apk</button>
                  <button class="btn btn-info btn-lg">Student.apk</button>
                </div>
           </div>
           <div class="carousel-item">
             <img class="d-block w-100" src="{{asset('web/images/img3.jpg')}}" alt="Second slide">
             <div class="carousel-caption animated ">
               <h1 style="color:white">OUTSTANDING ACADEMIC CONTENT</h1>
               <h5 style="color:white">BUY EASY PAY EASY</h5>
                  <p style="color:white">Get Application from playstore</p>
                  <button class="btn btn-info btn-lg">Teacher.apk</button>
                  <button class="btn btn-info btn-lg">Student.apk</button>
                </div>
           </div>
           <div class="carousel-item">
             <img class="d-block w-100" src="{{asset('web/images/img2.jpg')}}" alt="Third slide">
             <div class="carousel-caption animated ">
               <h1 style="color:rgb(8, 8, 8)">PERSONALIZED LEARNING EXPERIENCE</h1>
               <h5 style="color:rgb(8, 8, 8)">BUY EASY PAY EASY</h5>
                  <p style="color:rgb(8, 8, 8)">Get Application from playstore</p>
                  <button class="btn btn-info btn-lg">Teacher.apk</button>
                  <button class="btn btn-info btn-lg">Student.apk</button>
                </div>
           </div>
         </div>
         <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
           <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
           <span class="carousel-control-next-icon" aria-hidden="true"></span>
           <span class="sr-only">Next</span>
         </a>
       </div>

       <!--Payment-->
       <div class="container-fluid bg-light-gray">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
          <div class="container pt-5">
             
              <div class="row " id="pay">
                  <div class="col-md-12 text-center">
                      <h1>Payment methods</h1>
                      <div class="border"></div>
                    </div>
                    <div class="row">
                        
              </div>
                 
              </div>
          </div>
          <div class="container mt-5">
                  <div class="row">
                      
                      <div class="col-md-6">
                          <div class="card wow animated fadeInUp">
                              <img src="{{asset('web/images/img16.jpg')}}" alt="card-1" class="card-img-top">
                              <div >
                                     <span> <h2 class="card-body"> Payment</h2>
<br>
                                      <ul>
                                        <li class="hero-text wow animated fadeInleft">Free trail(for one week)</li>
                                        <li class="hero-text wow animated fadeInleft">Pay for One month - 2000/ month</li>
                                        <li class="hero-text wow animated fadeInleft">Pay for Three month - 1500/ month (4500) </li>
                                        <li class="hero-text wow animated fadeInleft">Pay for Six month - 1000/ month (6000) </li>
                                        <li class="hero-text wow animated fadeInleft">Pay for one year - 590/ month (7080)</li>
                                     
                                      </ul> 
                                      <button class="btn btn-danger" > One Year</button>
                                     
                                      <button class="btn btn-danger" > Six Month</button>
                                      <button class="btn btn-danger" > Three Month</button>
                                      <button class="btn btn-danger" > One Month</button>
                                  
                                      
                                      
                                  </span>
                   
                              </div>
                              
                          </div>
                          
                      </div>
                      
                      <div class="col-md-6">
                              <div class="card wow animated fadeInRight">
                                  <img src="{{asset('web/images/img17.jpg')}}" alt="card-1" class="card-img-top">
                                  <div >
                                         <span> <h2 class="card-body">Money Back Guarantee</h2>
                                      <br>
                                      <p class="wow animated fadeInRight hero-text ">If you daily spent four hours on prepaid and complete atleast one diagnostic test daily we gurantee you will get marks in Enterance exams. Otherwise your fee will be returned afteer deducing admission charges.  </p>
                                  <br>
                                  <br>
                                  <br>
                                  </span>  
                              </div>
                          </div>
                          
                      </div>
                      
                      
                     
              </div>
        </div>
      </div>





       <!--Services-->
       
       <div class="container-fluid bg-light-gray">

            <div class="container pt-5">
               
                <div class="row " id="ser">
                    <div class="col-md-12 text-center">
                        <h1>Services</h1>
                        <div class="border"></div>
                      </div>
                      <div class="row">
                          
                </div>
                   
                </div>
            </div>
            <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card wow animated fadeInUp">
                                <img src="{{asset('web/images/img5.jpg')}}" alt="card-1" class="card-img-top">
                                <div class="card-body">
                                    <h5> STUDENT</h5>
                                    <p class="wow animated fadeInUp" >In order to write a 5 paragraph essay you need to know necessary components it should include. Professional writers may not use this form of writing as often, but it still considered a useful academic writing exercise given to students.  </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="card wow animated fadeInLeft">
                                <img src="{{asset('web/images/img6.jpg')}}" alt="card-1" class="card-img-top">
                                <div class="card-body">
                                    <h5>TEACHER</h5>
                                    <p class="wow animated fadeInLeft ">In order to write a 5 paragraph essay you need to know necessary components it should include. Professional writers may not use this form of writing as often, but it still considered a useful academic writing exercise given to students.  </p>
                                    
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="card wow animated fadeInRight">
                                <img src="{{asset('web/images/img7.jpg')}}" alt="card-1" class="card-img-top">
                                <div class="card-body">
                                    <h5>LEADERBOARD</h5>
                                    <p class="wow animated fadeInRight">In order to write a 5 paragraph essay you need to know necessary components it should include. Professional writers may not use this form of writing as often, but it still considered a useful academic writing exercise given to students.  </p>
                            </div>
                        </div>
                        
                    </div>
                </div>
          </div>
        </div>
      </div>
        
        <!--perralex-->
  <style>
    body{
        margin: 0;
        padding: 0;
    }    
    
    #box2{
        height: 50vh;
        width: 100%;
        background-image: url({{asset('web/slider/img5.jpg')}});
        background-size: cover;
        display: table;
        background-attachment: fixed;
    }

</style>
</head>
<body>
    
    <div id="box2">
        <h1></h1>
    </div>
   
 <!--How we work -->
 <div class="container-fluid bg-light-gray">

  <div class="container pt-5">
     
      <div class="row ">
          <div class="col-md-12 text-center">
              <h1>How We work</h1>
            </div>
            <div class="row">
                
      </div>
         
      </div>
  </div>
  <div class="container mt-5">
          <div class="row">
              <div class="col-md-2">
                  <div class="card wow animated fadeInUp">
                      <img src="{{asset('web/images/img29.jpg')}}" alt="card-1" class="card-img-top">
                      <div class="card-body">
                          <h5>order</h5>
                      </div>
                  </div>
              </div>
              
              
              <div class="col-md-2">
                  <div class="card wow animated fadeInRight">
                      <img src="{{asset('web/images/img30.jpg')}}" alt="card-1" class="card-img-top">
                      <div class="card-body">
                          <h5>Information</h5>
                  </div>
              </div>
              
          </div>
        <div class="col-md-2">
          <div class="card wow animated fadeInLeft">
              <img src="{{asset('web/images/img31.jpg')}}" alt="card-1" class="card-img-top">
              <div class="card-body">
                  <h5>Meeting</h5>
                  
              </div>
          </div>
      </div>
          <div class="col-md-2">
            <div class="card wow animated fadeInLeft">
                <img src="{{asset('web/images/img32.jpg')}}" alt="card-1" class="card-img-top">
                <div class="card-body">
                    <h5>Payment</h5>                      
                </div>
            </div>
        </div>
        <div class="col-md-2">
          <div class="card wow animated fadeInLeft">
              <img src="{{asset('web/images/img33.jpg')}}" alt="card-1" class="card-img-top">
              <div class="card-body">
                  <h5>Delivery</h5>
                  
              </div>
          </div>
      </div>
<div class="col-md-2">
                  <div class="card wow animated fadeInLeft">
                      <img src="{{asset('web/images/img34.jpg')}}" alt="card-1" class="card-img-top">
                      <div class="card-body">
                          <h5>FeedBack</h5>
                          

                          
                      </div>
                  </div>
              </div>
      
</div>
</div>
<br>
<br>
<!--Footer-->
<section>
<footer>
  <div class="container-fluid pt-5 pb-5 bg-dark text-light">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
        <div class="row">
          <h5>My Mart</h5>
        </div>
        <div class="row mb-4">
        <div class="underline bg-light" style="width: 50px;"></div>
        </div>
          
            <p><i class="fa fa-chevron-right" aria-hidden="true"></i> Register</p>
            <p><i class="fa fa-chevron-right" aria-hidden="true"></i> Carrer </p>
            <p><i class="fa fa-chevron-right" aria-hidden="true"></i> JoBs</p>
            <p><i class="fa fa-chevron-right" aria-hidden="true"></i> Terms</p>
            <p><i class="fa fa-chevron-right" aria-hidden="true"></i> Policy</p>
          
        </div>
        
        
        
      
        
        <div class="col-md-3 pl-4">
          <div class="row">
          <h5>About</h5>
        </div>
        <div class="row mb-4">
        <div class="underline bg-light" style="width: 50px;"></div>
        </div>
        <div class="row">
          <p>We are providing all ectronics in easy Installments at your doorstep.</p>
        </div>
        </div>
        
        <div class="col-md-3">
          <div class="row">
          <h5>APPs</h5>
        </div>
        <div class="row mb-4">
        <div class="underline bg-light" style="width: 50px;"></div>
        </div>
       
        </div>
        <form action="">
          <form>
              <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your Name">
                  
                </div>
              <div class="form-group">
                <label>Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                
              </div>
              
             
              <div class="form-group">
                  <label >Message</label>
                  <textarea placeholder="Type your Message" class="form-control" id="exampleFormControlTextarea1" rows="1"></textarea>
                </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
      </form>
    </div>

      </div>
    </div>
  </div>
</footer>
</section>
<!--JS-->
<script src="wow.min.js"></script>
<script>
  new WOW().init();
  </script> 
       <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
</body>

</html>