<!DOCTYPE html>
<html>

<!-- Mirrored from www.oliveboard.in/login.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 22 May 2019 17:16:05 GMT -->
<head>

	<meta charset="utf-8">
	<title>Login | PrepOn</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<link href="{{asset('public-web/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('public-web/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('public-web/css/stylelr.css')}}" rel="stylesheet">

</head>

<body id="home">

	<div class="wrapper">
		<!-- Header Area -->
		<div class="header">
			<!-- Navigation Menu -->
			<nav class="navbar navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="{{url('/')}}">
							<img src="{{asset('images/logo.png')}}" height="50" width="50" style="margin:0px;"/>
						</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
								 <!-- <li class="active"><a id="remove-home" href="{{url('/')}}">Home</a></li>
								<li><a href="#">Features</a></li>
								<li><a href="#">Exams</a></li>
								<li><a href="#" >Live Course</a></li>
								<li><a href="#">App</a></li>
								<li><a href="#" >Blog</a></li>
								<li><a href="#">Contact</a></li>  -->
								<li><a href="{{url('/package')}}"  >Packages</a></li>
								<li><a href="{{route('user_web.login')}}"  >Login</a></li>
								<li><a href="{{route('user_web.register')}}"  class="sign-up">Register</a></li>
							</ul>
						</div>
					</div>
				</nav>
				<!-- Navigation End -->		
			</div>




			<!-- Main Content -->
			<div class="main-content">
				<!-- custom login starts -->
				<section id="signin">
					<div class="signup padd">
						<div class="container">
							<div class="contain">
								<div class="signup-content">
									<!-- Heading -->
									<h5 class="sign-title text-center">Login</h5>
									<div class="bor"></div>
									
									<div class="row col-xs-offset-1">
										<!-- Sign IN form here -->

										<div class="signin-page">
											<div class="col-md-6 col-sm-6 content col-centered">
												<form method="post"  action="{{route('user.submitLogin')}}" >
													@csrf
													@component('_components.alerts-default')
													@endcomponent

													
													@isset($message)
							                        <div class="alert alert-@isset($status){{$status}}@endisset" role="alert">
							                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							                                <span aria-hidden="true">×</span>
							                                <span class="sr-only">Close</span>
							                            </button>
							                            <strong>@isset($status){{$status}}@endisset!</strong>  {{ $message }}
							                        </div>
							                        @endisset
							                        @if(Session::has('forgot_success_message'))
														<div class="alert alert-success" role="alert">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true">×</span>
																<span class="sr-only">Close</span>
															</button>
															<strong>Error!</strong>  {{ Session::get('forgot_success_message') }}
														</div>
														@endif
														@if(Session::has('forgot_danger'))
														<div class="alert alert-danger" role="alert">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true">×</span>
																<span class="sr-only">Close</span>
															</button>
															<strong>Error!</strong>  {{ Session::get('forgot_danger') }}
														</div>
														@endif
													<div class="row">
														<div class="col-md-10 col-sm-10 errMsg">
															<p class="text-left">&nbsp;&nbsp;</p>
														</div>

													</div>
													<div class="row">
														<div class="col-md-10 col-sm-10">
															<div class="form-group">
																<input type="text" required="" value="" placeholder="Enter Email or phone" name="email" id="lemail" class="form-control" aria-required="true">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-10 col-sm-10">
															<div class="form-group">
																<input type="password" required="" value="" placeholder="Enter Password..." name="password" id="lpwd" class="form-control" aria-required="true">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-10 col-sm-10">
															<p><a class="forgot-password" href="javascript:;">Forgot Password?</a></p>

														</div>
													</div>

													<div class="row">
														<div class="col-md-10 col-sm-10 ">	
															<button type="submit" class="btn btn-primary btn-theme signn-bt btn-block" >Login</button>
														</div>

													</div>
													<!-- <div class="row">
														<div class="col-md-2 col-sm-2 col-xs-offset-5 col-md-offset-4">
															<p class="or-divider">or</p>
														</div>
													</div> -->
											<!-- <div class="row">
												<div class="col-md-10 col-sm-10">
													<a onclick="obfacebook();" class="btn btn-block  btn-lg btn-social btn-facebook">
                                                     <i class="fa fa-facebook"></i> Login with facebook
                                                   </a>
												</div>
											</div> -->


											<div class="row">
												<div class="col-mod-12 col-sm-12">
													<div class="sign-up-new">
														<p>Dont have an account?<a class="gsign" href="{{route('user_web.register')}}"> Register</a></p>
													</div>
												</div>
											</div>

										</form>
									</div>
								</div>

								<!-- end of Sign in form -->
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Forget password -->
		<section id="forgotpass">	
			<div class="forgotpass-wrap">
				<div class="container">
					<div class="forgotpass">
						<div class="heading text-center less-padd">
							<h2>Forgot Password</h2>
							<div class="bor"></div>
						</div>
					</div>
					<div class="row">
						<div class=" col-sm-8 col-md-8 col-centered">
							<h4 class="text-center resetemail less-padd">Enter email ID and we will send you a link to reset your password.</h4>
							<h4 class="text-center resetpass">Password reset details have been mailed to you.</h4>

							@if(Session::has('forgot_success_message'))
							<div class="alert alert-success" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>Error!</strong>  {{ Session::get('forgot_success_message') }}
							</div>
							@endif
							@if(Session::has('forgot_danger'))
							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>Error!</strong>  {{ Session::get('forgot_danger') }}
							</div>
							@endif

							<!-- Reset Form -->
							<form role="form" action="{{route('user.create_token')}}" method="post">
								@csrf
								<div class="col-md-10 col-sm-10">
									<div class="form-group">
										<input type="email" class="form-control" id="remail" name="email" placeholder="Enter Email..." value="" required>
									</div>
								</div>

								<div class="col-md-2 col-sm-2">

									<button type="submit" class="btn btn-theme">Submit</button>
								</div>
								<div class="col-md-8 col-sm-8">
									<div class="rerrMsg">
										<p>&nbsp;</p>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>				
		</section>
	</div>
	<!-- Main Content End -->		
	<!-- Footer start -->
	<div class="footer text-center">
		<div class="container link-footer">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<p><img src="{{asset('images/logo.png')}}" height="80" width="80" style="margin:0px;"></p>
					<p>M. Shaukat Ali Road, Lahore, Pakistan</p>
					<p>prepon.entrytests@gmail.com</p>
					<p>03334242069</p>
					<div class="footer-social social">
						<!--<a href="#" onclick="window.open('', '_blank'); return false;" class="facebook"><i class="fa fa-facebook"></i></a>-->
						<a href="https://www.facebook.com/PrepOn-636371010108789/?modal=admin_todo_tour"  class="facebook"><i class="fa fa-facebook"></i></a>
						{{-- <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a> --}}
						{{-- <a href="#" onclick="#" class="google-plus"><i class="fa fa-google-plus"></i></a> --}}
						{{-- <a href="#" onclick="#" class="linkedin"><i class="fa fa-linkedin"></i></a> --}}
						{{-- <a href="#" onclick="#" class="youtube"><i class="fa fa-youtube"></i></a>      --}}
					</div>    
				</div>
				<div class="col-md-2 col-sm-6 col-xs-12">
					{{-- <div class="footer-social social"> --}}
						{{-- <a href="#" onclick="#" class="facebook"><i class="fa fa-facebook"></i></a> --}}
						{{-- <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a> --}}
					{{-- </div> --}}
					<h3>Social</h3>
					<ul>
						<li><a href="https://www.facebook.com/PrepOn-636371010108789/?modal=admin_todo_tour" disabled ><i class="right isDisabled"></i><span>Facebook</span></a></li>
						{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Twitter</span></a></li> --}}
						{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Careers</span></a></li> --}}
						{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Testimonials</span></a></li> --}}
					</ul>

				</div>
				<div class="col-md-2 col-sm-6 col-xs-12">
					{{-- <h3>Resources</h3> --}}
					{{-- <ul>
						<li><a href="#"><i class="right"></i><span>Live Practice</span></a></li>
						<li><a href="#"><i class="right"></i><span>Video Course</span></a></li>
						<li><a href="#"><i class="right"></i><span>Free eBooks</span></a></li>
						<li><a href="#"><i class="right"></i><span>Blog</span></a></li>
						<li><a href="#"><i class="right"></i><span>Discuss Forum</span></a></li>
						<li><a href="#"><i class="right"></i><span>Current Affairs</span></a></li>
					</ul> --}}

				</div>
				<div class="col-md-2 col-sm-6 col-xs-12">
					{{-- <h3>Popular Exams</h3>
						<ul>
							<li><a href="#"><i class="right"></i><span>ECAT</span></a></li>
							<li><a href="#"><i class="right"></i><span>MCAT</span></a></li>
							<li><a href="#"><i class="right"></i><span>PPSC</span></a></li>
							<li><a href="#"><i class="right"></i><span>CSS</span></a></li>
							<li><a href="#"><i class="right"></i><span>NTS</span></a></li>
							<li><a href="#"><i class="right"></i><span>PTS</span></a></li>

						</ul> --}}

					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<h3>Download the App</h3>
						<ul>

							<li><a href="https://play.google.com/store/apps/details?id=com.project.prepon" /><img src="{{asset('public-web/img/play.png')}}"></a></li>
						</ul>

					</div>
				</div> 
			</div>

			<div class="container">

				<!-- Footer Copyright -->
				<div class="copyright">
					{{-- <p><a class="footerlinks" href="#">Careers</a> | --}} <a class="footerlinks" href="{{url('/privacy/policy')}}">Privacy Policy</a> {{-- | <a class="footerlinks" href="#">Terms</a> --}}</p>
					<p>&copy; Copyright <a href="#">entrytest4u.com</a> - All Rights Reserved.</p>
				</div>
			</div>
		</div>
		<!-- Footer End -->
		<!-- Footer End -->
	</div>
	<!-- Wrapper End -->

	<!-- Javascript files -->
	<script src="{{asset('public-web/js/jquery.js')}}"></script>
	<script src="{{asset('public-web/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('public-web/js/jquery.nav.js')}}"></script>


	<script src="{{asset('public-web/js/commonscript.js')}}"></script>
	@if(Session::has('forgot_success_message') || Session::has('forgot_danger'))
		<script type="text/javascript">
			$('#signin').css("display", "none");
			$('.forgotpass-wrap').css("display", "block");
		</script>
	@endif
	
	<script type="text/javascript">
		$('.forgot-password').on('click',function(){
			$('#signin').css("display", "none");
			$('.forgotpass-wrap').css("display", "block");
		})
	</script>
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-33831595-1']);
		_gaq.push(['_setDomainName', 'oliveboard.in']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>
	
</body>	

<!-- Mirrored from www.oliveboard.in/login.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 22 May 2019 17:16:06 GMT -->
</html>


