<!DOCTYPE html>
<?php
/**
 * Project: Entrytest4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')
<style>
	.Welcome_text{text-align: center;
        background: #06b;
        color: #fff;
        font-size: 26px;
        text-shadow: 0px 4px 4px #004893;
        padding: 8px 10px;}
    </style>
    <div id="main-sec">
       <section id="mainWrap-sec" class="mainWrap-sec col-md-12"><!-- body -->
          <h2 class="Welcome_text">Welcome To PrepOn </h2>
          <div class="row row-features">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rmv-grph-txt">
                <div class="portlet light bordered rec-mthd">
                    <div class="portlet-body">
                      
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="checkbox_sec">
        <div class="portlet">
            <div class="portlet-body" data-step="3" data-intro="Watch your imortant digits and channelize your efforts accordingly" data-position="right" data-scrollto="tooltip">
                <!--new2-->

                <!---->

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2">
                        <div class="display">
                            <div class="number">
                                <h3 class="text-white">
                                    <span data-counter="counterup" data-value="489">489</span>
                                </h3>
                                <small class="text-white">Total  Questoin</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="text-white">
                                    <span data-counter="counterup" data-value="0">0</span>
                                </h3>
                                <small class="text-white">Watched Question</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="text-white">
                                    <span data-counter="counterup" data-value="489">489</span>
                                </h3>
                                <small class="text-white">Remaining  Question</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <!--end new2-->
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 video-frn-table">
        <div class="checkbox_sec">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption ">
                       <i class=" icon-layers font-green"></i>
                       <span class="caption-subject font-green bold uppercase" data-step="4" data-intro="View stats of your Subject wise , Section wise and Subsection wise Progress in Video Lectures" data-position="right" data-scrollto="tooltip">Your Stats</span>
                   </div>
               </div>
               <div class="portlet-body">
                <div id="main">
                    <table id="example-advanced" class="treetable" width="100%;">
                        <thead class="dummy">
                            <tr>
                                <th style="width:50%;">Subject / Section / Sub-Section</th>
                                <th class="text-center" style="width:15%;">Total Question</th>
                                <th class="text-center" style="width:15%;">Watched Question</th>
                                <th class="text-center" style="width:15%;">Remaining Question</th>
                            </tr>
                        </thead>
                        <tbody>              
                            <tr data-tt-id="subject-1" id="sbjct" class="branch selected collapsed">
                                <td colspan="5"><span class="indenter" style="padding-left: 0px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <span class="folder">Physics</span>
                                </td>
                            </tr>
                            <tr class="">
                                <td>
                                    <div class="progress active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>

                                    </div>
                                </td>
                                <!--<td>106 (100%)</td>-->
                                <td class="text-center">106</td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td style="text-align:center;">106&nbsp;
                                    <small class="font-weight-bold bg-red rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-1" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Measurement</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-1" data-tt-parent-id="subject-1" class="branch expanded" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Collapse">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">8</td>
                                <!--<td>8 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">8&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-1-subsection-965" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Introduction to Physics  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '1', null, 'physics', 'Physics') ">Introduction to Physics</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-1-subsection-965" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-1-subsection-1" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Physical Quantities and SI Units  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '2', null, 'physics', 'Physics') ">Physical Quantities and SI Units</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-1-subsection-1" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-1-subsection-2" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Errors and Uncertainties  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '3', null, 'physics', 'Physics') ">Errors and Uncertainties</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-1-subsection-2" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-1-subsection-3" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Precision and Accuracy  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '5', null, 'physics', 'Physics') ">Precision and Accuracy</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-1-subsection-3" data-tt-parent-id="subject-1-section-1" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-15" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Motion &amp; Force</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-15" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">15</td>
                                <!--<td>15 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">15&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-6" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Displacement, Velocity &amp; Acceleration  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '11', null, 'physics', 'Physics') ">Displacement, Velocity &amp; Acceleration</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-6" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-7" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Velocity-Time Graph  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '12', null, 'physics', 'Physics') ">Velocity-Time Graph</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-7" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-8" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Equations of Motion  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '13', null, 'physics', 'Physics') ">Equations of Motion</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-8" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-9" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Newton's Laws of Motion  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '14', null, 'physics', 'Physics') ">Newton's Laws of Motion</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-9" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-10" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Momentum and its Conservation  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '15', null, 'physics', 'Physics') ">Momentum and its Conservation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-10" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-11" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Projectile Motion   Question" onclick="LoadQuestion('https://app.topgrade.pk/', '19', null, 'physics', 'Physics') ">Projectile Motion </strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-11" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-4" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Torque  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '9', null, 'physics', 'Physics') ">Torque</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-4" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-15-subsection-5" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Equilibrium  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '10', null, 'physics', 'Physics') ">Equilibrium</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-15-subsection-5" data-tt-parent-id="subject-1-section-15" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-2" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Work, Energy &amp; Power</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-2" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">8</td>
                                <!--<td>8 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">8&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-2-subsection-12" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Work Done by a Constant Force  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '20', null, 'physics', 'Physics') ">Work Done by a Constant Force</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-2-subsection-12" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-2-subsection-13" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Power  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '23', null, 'physics', 'Physics') ">Power</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-2-subsection-13" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-2-subsection-14" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Energy and its Conservation  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '24', null, 'physics', 'Physics') ">Energy and its Conservation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-2-subsection-14" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-2-subsection-15" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Interconversion of Potential &amp; Kinetic Energy  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '25', null, 'physics', 'Physics') ">Interconversion of Potential &amp; Kinetic Energy</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-2-subsection-15" data-tt-parent-id="subject-1-section-2" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-3" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Circular Motion</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-3" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-3-subsection-16" data-tt-parent-id="subject-1-section-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Circular Motion  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '27', null, 'physics', 'Physics') ">Introduction to Circular Motion</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-3-subsection-16" data-tt-parent-id="subject-1-section-3" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-3-subsection-17" data-tt-parent-id="subject-1-section-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Centripetal Force  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '28', null, 'physics', 'Physics') ">Centripetal Force</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-3-subsection-17" data-tt-parent-id="subject-1-section-3" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-3-subsection-18" data-tt-parent-id="subject-1-section-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Geostationary Orbits &amp; Communication Satellites  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '36', null, 'physics', 'Physics') ">Geostationary Orbits &amp; Communication Satellites</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-3-subsection-18" data-tt-parent-id="subject-1-section-3" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-4" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Oscillations</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-4" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">10</td>
                                <!--<td>10 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">10&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-4-subsection-20" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Simple Harmonic Motion  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '45', null, 'physics', 'Physics') ">Simple Harmonic Motion</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-4-subsection-20" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-4-subsection-22" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Horizontal Mass Spring System  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '48', null, 'physics', 'Physics') ">Horizontal Mass Spring System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-4-subsection-22" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-4-subsection-23" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Simple Pendulum  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '49', null, 'physics', 'Physics') ">Simple Pendulum</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-4-subsection-23" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-4-subsection-24" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Energy Conservation in SHM  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '50', null, 'physics', 'Physics') ">Energy Conservation in SHM</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-4-subsection-24" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-4-subsection-25" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Free and Forced Oscillations  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '51', null, 'physics', 'Physics') ">Free and Forced Oscillations</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-4-subsection-25" data-tt-parent-id="subject-1-section-4" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-5" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Waves</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-5" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-5-subsection-29" data-tt-parent-id="subject-1-section-5" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Waves  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '55', null, 'physics', 'Physics') ">Basic Concepts of Waves</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-5-subsection-29" data-tt-parent-id="subject-1-section-5" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-5-subsection-30" data-tt-parent-id="subject-1-section-5" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Stationary Waves  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '61', null, 'physics', 'Physics') ">Stationary Waves</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-5-subsection-30" data-tt-parent-id="subject-1-section-5" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-5-subsection-33" data-tt-parent-id="subject-1-section-5" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Doppler Effect  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '64', null, 'physics', 'Physics') ">Doppler Effect</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-5-subsection-33" data-tt-parent-id="subject-1-section-5" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-6" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Light</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-6" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-6-subsection-34" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Wave fronts &amp; Huygens’s Principle  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '65', null, 'physics', 'Physics') ">Wave fronts &amp; Huygens’s Principle</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-6-subsection-34" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-6-subsection-36" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Interference of Light Waves  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '67', null, 'physics', 'Physics') ">Interference of Light Waves</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-6-subsection-36" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-6-subsection-37" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Diffraction of Light  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '71', null, 'physics', 'Physics') ">Diffraction of Light</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-6-subsection-37" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-6-subsection-39" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch  Fiber Optics  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '80', null, 'physics', 'Physics') "> Fiber Optics</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-6-subsection-39" data-tt-parent-id="subject-1-section-6" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-16" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Heat and Thermodynamics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-16" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-16-subsection-40" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Kinetic Theory of Gases  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '81', null, 'physics', 'Physics') ">Kinetic Theory of Gases</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-16-subsection-40" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-16-subsection-41" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Internal Energy, Work &amp; Heat  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '82', null, 'physics', 'Physics') ">Internal Energy, Work &amp; Heat</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-16-subsection-41" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-16-subsection-43" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch First Law of Thermodynamics  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '84', null, 'physics', 'Physics') ">First Law of Thermodynamics</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-16-subsection-43" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-16-subsection-44" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Molar Specific Heat of a Gas  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '85', null, 'physics', 'Physics') ">Molar Specific Heat of a Gas</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-16-subsection-44" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-16-subsection-45" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Thermodynamic Scale of Temperature  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '89', null, 'physics', 'Physics') ">Thermodynamic Scale of Temperature</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-16-subsection-45" data-tt-parent-id="subject-1-section-16" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-7" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Electrostatics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-7" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-7-subsection-46" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Coulomb's Law  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '92', null, 'physics', 'Physics') ">Coulomb's Law</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-7-subsection-46" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-7-subsection-47" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Fields of Force  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '93', null, 'physics', 'Physics') ">Fields of Force</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-7-subsection-47" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-7-subsection-48" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electric Field Lines  Question" onclick="LoadQuestion('https://app.topgrade.pk/', '94', null, 'physics', 'Physics') ">Electric Field Lines</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-7-subsection-48" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-7-subsection-49" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electric Potential &amp; Electron Volt  Question" onclick="LoadVideos('https://app.topgrade.pk/', '99', null, 'physics', 'Physics') ">Electric Potential &amp; Electron Volt</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-7-subsection-49" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-7-subsection-51" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Capacitor and Capacitance of a Parallel Plate Capacitor  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '103', null, 'physics', 'Physics') ">Capacitor and Capacitance of a Parallel Plate Capacitor</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-7-subsection-51" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-7-subsection-52" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Energy Stored in a Capacitor  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '105', null, 'physics', 'Physics') ">Energy Stored in a Capacitor</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-7-subsection-52" data-tt-parent-id="subject-1-section-7" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-8" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Current Electricity</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-8" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-8-subsection-53" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Electric Current  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '107', null, 'physics', 'Physics') ">Basic Concepts of Electric Current</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-8-subsection-53" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-8-subsection-54" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Ohm's Law  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '108', null, 'physics', 'Physics') ">Ohm's Law</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-8-subsection-54" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-8-subsection-55" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Resistance &amp; Resistivity  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '109', null, 'physics', 'Physics') ">Resistance &amp; Resistivity</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-8-subsection-55" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-8-subsection-56" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electrical Power &amp; Power Dissipation in Resistors  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '111', null, 'physics', 'Physics') ">Electrical Power &amp; Power Dissipation in Resistors</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-8-subsection-56" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-8-subsection-57" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch EMF &amp; Potential Difference  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '112', null, 'physics', 'Physics') ">EMF &amp; Potential Difference</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-8-subsection-57" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-8-subsection-58" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Kirchhoff's Rules  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '113', null, 'physics', 'Physics') ">Kirchhoff's Rules</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-8-subsection-58" data-tt-parent-id="subject-1-section-8" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-9" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Electromagnetism</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-9" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">7</td>
                                <!--<td>7 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">7&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-9-subsection-59" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Magnetic Field Due to Current in a Long Straight Wire  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '116', null, 'physics', 'Physics') ">Magnetic Field Due to Current in a Long Straight Wire</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-9-subsection-59" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-9-subsection-60" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Force on a Current Carrying Conductor in a Uniform Magnetic Field  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '117', null, 'physics', 'Physics') ">Force on a Current Carrying Conductor in a Uniform Magnetic Field</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-9-subsection-60" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-9-subsection-62" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Ampere's Law &amp; Determination of Flux Density B  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '119', null, 'physics', 'Physics') ">Ampere's Law &amp; Determination of Flux Density B</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-9-subsection-62" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-9-subsection-63" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Force on a Moving Charge in a Magnetic Field  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '120', null, 'physics', 'Physics') ">Force on a Moving Charge in a Magnetic Field</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-9-subsection-63" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-9-subsection-64" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Motion of Charged Particle in an Electric &amp; Magnetic Field  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '121', null, 'physics', 'Physics') ">Motion of Charged Particle in an Electric &amp; Magnetic Field</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-9-subsection-64" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-9-subsection-65" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Determination of e/m of an Electron  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '122', null, 'physics', 'Physics') ">Determination of e/m of an Electron</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-9-subsection-65" data-tt-parent-id="subject-1-section-9" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-10" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Electromagnetic Induction</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-10" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-10-subsection-61" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Magnetic Flux &amp; Flux Density  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '118', null, 'physics', 'Physics') ">Magnetic Flux &amp; Flux Density</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-10-subsection-61" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-10-subsection-66" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Induced EMF &amp; Current  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '127', null, 'physics', 'Physics') ">Introduction to Induced EMF &amp; Current</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-10-subsection-66" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-10-subsection-67" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Faraday's Law and Induced EMF  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '129', null, 'physics', 'Physics') ">Faraday's Law and Induced EMF</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-10-subsection-67" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-10-subsection-68" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Lenz's Law in Direction of Induced EMF  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '130', null, 'physics', 'Physics') ">Lenz's Law in Direction of Induced EMF</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-10-subsection-68" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-10-subsection-69" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Transformer  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '139', null, 'physics', 'Physics') ">Transformer</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-10-subsection-69" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-10-subsection-70" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Alternating Current Generator  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '140', null, 'physics', 'Physics') ">Alternating Current Generator</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-10-subsection-70" data-tt-parent-id="subject-1-section-10" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-11" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Deformation of Solids</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-11" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-11-subsection-71" data-tt-parent-id="subject-1-section-11" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Mechanical Properties of Solids  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '158', null, 'physics', 'Physics') ">Mechanical Properties of Solids</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-11-subsection-71" data-tt-parent-id="subject-1-section-11" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-13" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Electronics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-13" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-13-subsection-73" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Rectification  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '163', null, 'physics', 'Physics') ">Rectification</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-13-subsection-73" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-13-subsection-74" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Operational Amplifier  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '168', null, 'physics', 'Physics') ">Operational Amplifier</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-13-subsection-74" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-13-subsection-75" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Op-Amp as an Inverting Amplifier  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '169', null, 'physics', 'Physics') ">Op-Amp as an Inverting Amplifier</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-13-subsection-75" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-13-subsection-76" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Op-Amp as a Non-Inverting Amplifier  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '170', null, 'physics', 'Physics') ">Op-Amp as a Non-Inverting Amplifier</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-13-subsection-76" data-tt-parent-id="subject-1-section-13" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-12" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Modern Physics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-12" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-12-subsection-72" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electrical Properties of Solids  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '159', null, 'physics', 'Physics') ">Electrical Properties of Solids</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-12-subsection-72" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-12-subsection-77" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Energy of Photon  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '182', null, 'physics', 'Physics') ">Energy of Photon</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-12-subsection-77" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-12-subsection-78" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Photoelectric Effect  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '183', null, 'physics', 'Physics') ">Photoelectric Effect</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-12-subsection-78" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-12-subsection-79" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Wave Nature of Particles  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '185', null, 'physics', 'Physics') ">Wave Nature of Particles</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-12-subsection-79" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-12-subsection-80" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Atomic Spectra  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '187', null, 'physics', 'Physics') ">Introduction to Atomic Spectra</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-12-subsection-80" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-12-subsection-81" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch X Rays  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '190', null, 'physics', 'Physics') ">X Rays</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-12-subsection-81" data-tt-parent-id="subject-1-section-12" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-1-section-14" data-tt-parent-id="subject-1" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Nuclear Physics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-1-section-14" data-tt-parent-id="subject-1" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">9</td>
                                <!--<td>9 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">9&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-82" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Atomic Nucleus  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '193', null, 'physics', 'Physics') ">Atomic Nucleus</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-82" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-83" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Isotopes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '194', null, 'physics', 'Physics') ">Isotopes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-83" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-84" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Mass Defect and Binding Energy  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '195', null, 'physics', 'Physics') ">Mass Defect and Binding Energy</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-84" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-85" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Radioactivity  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '196', null, 'physics', 'Physics') ">Radioactivity</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-85" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-86" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Half Life  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '197', null, 'physics', 'Physics') ">Half Life</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-86" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-87" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nuclear Reactions  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '200', null, 'physics', 'Physics') ">Nuclear Reactions</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-87" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-88" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nuclear Fission  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '201', null, 'physics', 'Physics') ">Nuclear Fission</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-88" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-89" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Fusion Reaction  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '202', null, 'physics', 'Physics') ">Fusion Reaction</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-89" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-1-section-14-subsection-90" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Forces of Nature &amp; Building Blocks of Matter  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '206', null, 'physics', 'Physics') ">Basic Forces of Nature &amp; Building Blocks of Matter</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-1-section-14-subsection-90" data-tt-parent-id="subject-1-section-14" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>


                            <!--// subject-->
                            <tr data-tt-id="subject-2" id="sbjct" class="branch collapsed">
                                <td colspan="5"><span class="indenter" style="padding-left: 0px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <span class="folder">Chemistry</span>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    <div class="progress active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>

                                    </div>
                                </td>
                                <!--<td>103 (100%)</td>-->
                                <td class="text-center">103</td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td style="text-align:center;">103&nbsp;
                                    <small class="font-weight-bold bg-red rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-148" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Fundamental Concepts</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-148" data-tt-parent-id="subject-2" class="branch expanded" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Collapse">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">11</td>
                                <!--<td>11 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">11&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-662" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Atom, Molecules &amp; Ions  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '335', null, 'chemistry', 'Chemistry') ">Atom, Molecules &amp; Ions</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-662" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-742" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Relative Atomic Mass  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '336', null, 'chemistry', 'Chemistry') ">Relative Atomic Mass</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-742" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-719" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Isotopes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '337', null, 'chemistry', 'Chemistry') ">Isotopes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-719" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-660" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Analysis of a Compound - Empirical &amp; Molecular Formulas  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '338', null, 'chemistry', 'Chemistry') ">Analysis of a Compound - Empirical &amp; Molecular Formulas</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-660" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-674" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Concept of Mole &amp; Avogadro's No.  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '339', null, 'chemistry', 'Chemistry') ">Concept of Mole &amp; Avogadro's No.</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-674" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-745" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Stoichiometry  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '340', null, 'chemistry', 'Chemistry') ">Stoichiometry</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-745" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-724" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Limiting Reactant  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '341', null, 'chemistry', 'Chemistry') ">Limiting Reactant</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-724" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-758" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Yield  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '342', null, 'chemistry', 'Chemistry') ">Yield</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-758" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-148-subsection-673" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 38px;"></span><span class="file"><strong title="Watch Concentration Units of Solutions  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '405', null, 'chemistry', 'Chemistry') ">Concentration Units of Solutions</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-148-subsection-673" data-tt-parent-id="subject-2-section-148" class="leaf collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 38px;"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-149" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">States of Matter</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-149" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">9</td>
                                <!--<td>9 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">9&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-711" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to States of Matter  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '348', null, 'chemistry', 'Chemistry') ">Introduction to States of Matter</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-711" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-694" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Ideal Gas Laws &amp; Ideal Gas Equation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '349', null, 'chemistry', 'Chemistry') ">Ideal Gas Laws &amp; Ideal Gas Equation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-694" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-721" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Kinetic Molecular Theory of Gases  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '354', null, 'chemistry', 'Chemistry') ">Kinetic Molecular Theory of Gases</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-721" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-726" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Non-Ideal Behaviour of Gases  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '357', null, 'chemistry', 'Chemistry') ">Non-Ideal Behaviour of Gases</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-726" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-697" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Intermolecular Forces  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '359', null, 'chemistry', 'Chemistry') ">Intermolecular Forces</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-697" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-687" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Evaporation, Vapour Pressure &amp; Boiling Point  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '360', null, 'chemistry', 'Chemistry') ">Evaporation, Vapour Pressure &amp; Boiling Point</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-687" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-743" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Solids  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '362', null, 'chemistry', 'Chemistry') ">Solids</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-743" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-149-subsection-669" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Classification of Solids  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '365', null, 'chemistry', 'Chemistry') ">Classification of Solids</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-149-subsection-669" data-tt-parent-id="subject-2-section-149" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-138" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Atomic Structure</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-138" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-138-subsection-4859" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Discovery Of Sub-Atomic Particles of Atom &amp; Millikan's Oil Drop Method  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '766', null, 'chemistry', 'Chemistry') ">Discovery Of Sub-Atomic Particles of Atom &amp; Millikan's Oil Drop Method</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-138-subsection-4859" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-138-subsection-748" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Sub-Atomic Particles of Atom  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '367', null, 'chemistry', 'Chemistry') ">Sub-Atomic Particles of Atom</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-138-subsection-748" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-138-subsection-682" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electronic Distribution  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '375', null, 'chemistry', 'Chemistry') ">Electronic Distribution</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-138-subsection-682" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-138-subsection-717" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Ionization Energy  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '378', null, 'chemistry', 'Chemistry') ">Ionization Energy</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-138-subsection-717" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-138-subsection-680" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electron Affinity  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '379', null, 'chemistry', 'Chemistry') ">Electron Affinity</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-138-subsection-680" data-tt-parent-id="subject-2-section-138" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-139" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Chemical Bonding</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-139" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-139-subsection-706" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Chemical Bonding  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '376', null, 'chemistry', 'Chemistry') ">Introduction to Chemical Bonding</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-139-subsection-706" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-139-subsection-752" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Types of Bonds  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '381', null, 'chemistry', 'Chemistry') ">Types of Bonds</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-139-subsection-752" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-139-subsection-757" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch VSEPR Theory  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '383', null, 'chemistry', 'Chemistry') ">VSEPR Theory</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-139-subsection-757" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-139-subsection-756" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Valence Bond Theory  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '384', null, 'chemistry', 'Chemistry') ">Valence Bond Theory</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-139-subsection-756" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-139-subsection-664" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Bond Properties  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '386', null, 'chemistry', 'Chemistry') ">Bond Properties</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-139-subsection-664" data-tt-parent-id="subject-2-section-139" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-150" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Chemical Energetics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-150" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-150-subsection-712" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Thermochemistry  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '389', null, 'chemistry', 'Chemistry') ">Introduction to Thermochemistry</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-150-subsection-712" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-150-subsection-698" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Internal Energy &amp; First Law of Thermodynamics  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '392', null, 'chemistry', 'Chemistry') ">Internal Energy &amp; First Law of Thermodynamics</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-150-subsection-698" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-150-subsection-685" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Enthalpy  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '393', null, 'chemistry', 'Chemistry') ">Enthalpy</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-150-subsection-685" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-150-subsection-693" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Hess's Law of Constant Heat Summation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '394', null, 'chemistry', 'Chemistry') ">Hess's Law of Constant Heat Summation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-150-subsection-693" data-tt-parent-id="subject-2-section-150" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-141" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Electrochemistry</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-141" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-141-subsection-708" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Electrochemistry  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '413', null, 'chemistry', 'Chemistry') ">Introduction to Electrochemistry</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-141-subsection-708" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-141-subsection-741" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Redox Reactions &amp; Electrical Conductance  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '414', null, 'chemistry', 'Chemistry') ">Redox Reactions &amp; Electrical Conductance</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-141-subsection-741" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-141-subsection-678" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electrochemical Cells &amp; Electrolysis  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '415', null, 'chemistry', 'Chemistry') ">Electrochemical Cells &amp; Electrolysis</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-141-subsection-678" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-141-subsection-679" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electrode Potential  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '416', null, 'chemistry', 'Chemistry') ">Electrode Potential</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-141-subsection-679" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-141-subsection-751" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch The Electrochemical Series  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '417', null, 'chemistry', 'Chemistry') ">The Electrochemical Series</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-141-subsection-751" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-141-subsection-689" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Fuel Cells  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '419', null, 'chemistry', 'Chemistry') ">Fuel Cells</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-141-subsection-689" data-tt-parent-id="subject-2-section-141" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-140" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Chemical Equilibrium</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-140" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">8</td>
                                <!--<td>8 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">8&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-707" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Chemical Equilibrium  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '387', null, 'chemistry', 'Chemistry') ">Introduction to Chemical Equilibrium</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-707" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-722" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Law of Mass Action &amp; Equilibrium Constant  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '395', null, 'chemistry', 'Chemistry') ">Law of Mass Action &amp; Equilibrium Constant</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-722" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-723" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Le-Chatelier Principle  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '398', null, 'chemistry', 'Chemistry') ">Le-Chatelier Principle</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-723" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-715" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Ionic Product of Water  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '397', null, 'chemistry', 'Chemistry') ">Ionic Product of Water</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-715" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-954" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch pH &amp; pOH  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '785', null, 'chemistry', 'Chemistry') ">pH &amp; pOH</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-954" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-716" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Ionization Constant PKa &amp; PKb, Common Ion Effect  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '399', null, 'chemistry', 'Chemistry') ">Ionization Constant PKa &amp; PKb, Common Ion Effect</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-716" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-665" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Buffer Solutions  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '402', null, 'chemistry', 'Chemistry') ">Buffer Solutions</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-665" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-140-subsection-686" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Equilibria of Slightly Soluble Ionic Compounds  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '403', null, 'chemistry', 'Chemistry') ">Equilibria of Slightly Soluble Ionic Compounds</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-140-subsection-686" data-tt-parent-id="subject-2-section-140" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-152" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Reaction Kinetics / Chemical Kinetics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-152" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-152-subsection-734" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Rate of Reaction  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '421', null, 'chemistry', 'Chemistry') ">Rate of Reaction</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-152-subsection-734" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-152-subsection-727" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Order of a Reaction  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '420', null, 'chemistry', 'Chemistry') ">Order of a Reaction</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-152-subsection-727" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-152-subsection-684" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Energy of Activation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '423', null, 'chemistry', 'Chemistry') ">Energy of Activation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-152-subsection-684" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-152-subsection-688" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Factors Affecting Rate of Reaction and Methods for Determining Rate of Reaction  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '425', null, 'chemistry', 'Chemistry') ">Factors Affecting Rate of Reaction and Methods for Determining Rate of Reaction</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-152-subsection-688" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-152-subsection-666" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Catalysis  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '426', null, 'chemistry', 'Chemistry') ">Catalysis</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-152-subsection-666" data-tt-parent-id="subject-2-section-152" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-153" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Periods</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-153" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-153-subsection-730" data-tt-parent-id="subject-2-section-153" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Periodic Trends  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '424', null, 'chemistry', 'Chemistry') ">Periodic Trends</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-153-subsection-730" data-tt-parent-id="subject-2-section-153" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-154" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Groups</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-154" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-154-subsection-691" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch General Behaviour of Alkaline Earth Metals  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '435', null, 'chemistry', 'Chemistry') ">General Behaviour of Alkaline Earth Metals</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-154-subsection-691" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-154-subsection-709" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Properties of Halogens &amp; Uses Of Chlorine  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '454', null, 'chemistry', 'Chemistry') ">Properties of Halogens &amp; Uses Of Chlorine</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-154-subsection-709" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-154-subsection-672" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Reactions of Chlorine with NaOH  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '458', null, 'chemistry', 'Chemistry') ">Reactions of Chlorine with NaOH</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-154-subsection-672" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-154-subsection-725" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Applications of Noble Gases   Videos" onclick="LoadVideos('https://app.topgrade.pk/', '463', null, 'chemistry', 'Chemistry') ">Applications of Noble Gases </strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-154-subsection-725" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-154-subsection-754" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Water Disinfection by Chlorine  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '550', null, 'chemistry', 'Chemistry') ">Water Disinfection by Chlorine</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-154-subsection-754" data-tt-parent-id="subject-2-section-154" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-142" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Transition Elements</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-142" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-142-subsection-681" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electronic Configurations  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '465', null, 'chemistry', 'Chemistry') ">Electronic Configurations</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-142-subsection-681" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-142-subsection-729" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Oxidation States &amp; Colour of Transition Elements  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '467', null, 'chemistry', 'Chemistry') ">Oxidation States &amp; Colour of Transition Elements</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-142-subsection-729" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-142-subsection-671" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Complex Compounds of Transition Elements  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '468', null, 'chemistry', 'Chemistry') ">Complex Compounds of Transition Elements</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-142-subsection-671" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-142-subsection-966" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Use of Transition Elements as Catalyst  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '469', null, 'chemistry', 'Chemistry') ">Use of Transition Elements as Catalyst</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-142-subsection-966" data-tt-parent-id="subject-2-section-142" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-151" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Elements of Biological Importance</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-151" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-151-subsection-661" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Applications of Chemical Equilibrium in Industry  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '396', null, 'chemistry', 'Chemistry') ">Applications of Chemical Equilibrium in Industry</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-151-subsection-661" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-151-subsection-696" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Inertness of Nitrogen  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '449', null, 'chemistry', 'Chemistry') ">Inertness of Nitrogen</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-151-subsection-696" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-151-subsection-749" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Manufacturing of Sulphuric Acid   Videos" onclick="LoadVideos('https://app.topgrade.pk/', '452', null, 'chemistry', 'Chemistry') ">Manufacturing of Sulphuric Acid </strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-151-subsection-749" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-151-subsection-667" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nucleic Acids   Videos" onclick="LoadVideos('https://app.topgrade.pk/', '543', null, 'chemistry', 'Chemistry') ">Nucleic Acids </strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-151-subsection-667" data-tt-parent-id="subject-2-section-151" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-656" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Fundamental Principles of Organic Chemistry </span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-656" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-656-subsection-720" data-tt-parent-id="subject-2-section-656" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch IUPAC Nomenclature  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '484', null, 'chemistry', 'Chemistry') ">IUPAC Nomenclature</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-656-subsection-720" data-tt-parent-id="subject-2-section-656" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-155" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Fundamental Principles Of Organic Chemistry </span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-155" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">8</td>
                                <!--<td>8 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">8&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-155-subsection-676" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Cracking of Petroleum  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '477', null, 'chemistry', 'Chemistry') ">Cracking of Petroleum</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-155-subsection-676" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-155-subsection-668" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Classification of Organic Compounds  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '479', null, 'chemistry', 'Chemistry') ">Classification of Organic Compounds</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-155-subsection-668" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-155-subsection-690" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Functional Group  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '480', null, 'chemistry', 'Chemistry') ">Functional Group</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-155-subsection-690" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-155-subsection-718" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Isomerism  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '482', null, 'chemistry', 'Chemistry') ">Isomerism</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-155-subsection-718" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-155-subsection-720" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch IUPAC Nomenclature  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '484', null, 'chemistry', 'Chemistry') ">IUPAC Nomenclature</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-155-subsection-720" data-tt-parent-id="subject-2-section-155" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-156" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Hydrocarbons</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-156" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-156-subsection-659" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Reactions of Alkanes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '486', null, 'chemistry', 'Chemistry') ">Reactions of Alkanes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-156-subsection-659" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-156-subsection-733" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Preparation of Alkenes (Dehydrohalogenation of Alkyl Halides, Dehydration of Alcohols)  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '488', null, 'chemistry', 'Chemistry') ">Preparation of Alkenes (Dehydrohalogenation of Alkyl Halides, Dehydration of Alcohols)</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-156-subsection-733" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-156-subsection-703" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Aromatic Hydrocarbons  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '492', null, 'chemistry', 'Chemistry') ">Introduction to Aromatic Hydrocarbons</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-156-subsection-703" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-156-subsection-704" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Benzene  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '494', null, 'chemistry', 'Chemistry') ">Introduction to Benzene</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-156-subsection-704" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-156-subsection-683" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Electrophilic Substitution Reactions, Side Chain Oxidation, Orientation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '493', null, 'chemistry', 'Chemistry') ">Electrophilic Substitution Reactions, Side Chain Oxidation, Orientation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-156-subsection-683" data-tt-parent-id="subject-2-section-156" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-143" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Alkyl Halides</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-143" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-143-subsection-702" data-tt-parent-id="subject-2-section-143" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction &amp; Classification of Alkyl Halides  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '499', null, 'chemistry', 'Chemistry') ">Introduction &amp; Classification of Alkyl Halides</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-143-subsection-702" data-tt-parent-id="subject-2-section-143" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-143-subsection-737" data-tt-parent-id="subject-2-section-143" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Reactions of Alkyl Halides  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '503', null, 'chemistry', 'Chemistry') ">Reactions of Alkyl Halides</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-143-subsection-737" data-tt-parent-id="subject-2-section-143" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-157" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Alcohols &amp; Phenols</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-157" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-157-subsection-699" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction &amp; Classification of Alcohols  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '505', null, 'chemistry', 'Chemistry') ">Introduction &amp; Classification of Alcohols</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-157-subsection-699" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-157-subsection-735" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Reactions and Uses of Alcohols  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '508', null, 'chemistry', 'Chemistry') ">Reactions and Uses of Alcohols</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-157-subsection-735" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-157-subsection-677" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Distinction between Primary, Secondary and Tertiary Alcohols  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '510', null, 'chemistry', 'Chemistry') ">Distinction between Primary, Secondary and Tertiary Alcohols</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-157-subsection-677" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-157-subsection-731" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Phenol (Acidic Behaviour, Salt Formation, Nitration &amp; Halogenation)  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '513', null, 'chemistry', 'Chemistry') ">Phenol (Acidic Behaviour, Salt Formation, Nitration &amp; Halogenation)</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-157-subsection-731" data-tt-parent-id="subject-2-section-157" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-144" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Aldehydes &amp; Ketones</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-144" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-144-subsection-701" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Carbonyl Compounds  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '515', null, 'chemistry', 'Chemistry') ">Introduction to Carbonyl Compounds</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-144-subsection-701" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-144-subsection-732" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Preparation of Carbonyl Compounds  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '517', null, 'chemistry', 'Chemistry') ">Preparation of Carbonyl Compounds</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-144-subsection-732" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-144-subsection-738" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Reactions of Carbonyl Compounds with HCN, 2,4 DNPH, Na Borohydride &amp; Oxidation Reactions  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '520', null, 'chemistry', 'Chemistry') ">Reactions of Carbonyl Compounds with HCN, 2,4 DNPH, Na Borohydride &amp; Oxidation Reactions</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-144-subsection-738" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-144-subsection-695" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Tollen's Test &amp; Fehling's Solution Test  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '522', null, 'chemistry', 'Chemistry') ">Tollen's Test &amp; Fehling's Solution Test</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-144-subsection-695" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-144-subsection-714" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Iodoform Test  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '525', null, 'chemistry', 'Chemistry') ">Iodoform Test</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-144-subsection-714" data-tt-parent-id="subject-2-section-144" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-145" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Carboxylic Acids</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-145" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-145-subsection-740" data-tt-parent-id="subject-2-section-145" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Reactivity of Carboxyl Group  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '529', null, 'chemistry', 'Chemistry') ">Reactivity of Carboxyl Group</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-145-subsection-740" data-tt-parent-id="subject-2-section-145" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-145-subsection-956" data-tt-parent-id="subject-2-section-145" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Esters  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '788', null, 'chemistry', 'Chemistry') ">Esters</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-145-subsection-956" data-tt-parent-id="subject-2-section-145" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-158" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Amino Acids</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-158" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-158-subsection-746" data-tt-parent-id="subject-2-section-158" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Structure &amp; Classification of Amino Acids  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '531', null, 'chemistry', 'Chemistry') ">Structure &amp; Classification of Amino Acids</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-158-subsection-746" data-tt-parent-id="subject-2-section-158" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-146" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Macromolecules</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-146" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-146-subsection-747" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Structure &amp; Types of Polymers  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '533', null, 'chemistry', 'Chemistry') ">Structure &amp; Types of Polymers</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-146-subsection-747" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-146-subsection-658" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Addition Polymers  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '534', null, 'chemistry', 'Chemistry') ">Addition Polymers</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-146-subsection-658" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-146-subsection-675" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Condensation Polymers  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '535', null, 'chemistry', 'Chemistry') ">Condensation Polymers</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-146-subsection-675" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-146-subsection-663" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Proteins  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '538', null, 'chemistry', 'Chemistry') ">Proteins</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-146-subsection-663" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-2-section-146-subsection-2824" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nucleic Acids   Videos" onclick="LoadVideos('https://app.topgrade.pk/', '798', null, 'chemistry', 'Chemistry') ">Nucleic Acids </strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-146-subsection-2824" data-tt-parent-id="subject-2-section-146" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-2-section-147" data-tt-parent-id="subject-2" class="leaf collapsed" style="display: none;">
                                <td colspan="5"><span class="indenter" style="padding-left: 19px;"></span>
                                    <span class="folder">Environmental Chemistry</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-2-section-147" data-tt-parent-id="subject-2" class="branch collapsed" style="display: none;">
                                <td><span class="indenter" style="padding-left: 19px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-2-section-147-subsection-993" data-tt-parent-id="subject-2-section-147" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Air Pollution  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '548', null, 'chemistry', 'Chemistry') ">Air Pollution</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-2-section-147-subsection-993" data-tt-parent-id="subject-2-section-147" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>


                            <!--// subject-->
                            <tr data-tt-id="subject-3" id="sbjct" class="branch collapsed">
                                <td colspan="5"><span class="indenter" style="padding-left: 0px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <span class="folder">Biology </span>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    <div class="progress active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>

                                    </div>
                                </td>
                                <!--<td>205 (100%)</td>-->
                                <td class="text-center">205</td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td style="text-align:center;">205&nbsp;
                                    <small class="font-weight-bold bg-red rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-38" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Cell Biology</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-38" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">23</td>
                                <!--<td>23 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">23&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-322" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Cell Biology  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '225', null, 'biology', 'Biology ') ">Basic Concepts of Cell Biology</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-322" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-323" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Cell Membrane  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '226', null, 'biology', 'Biology ') ">Cell Membrane</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-323" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-324" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nucleus  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '227', null, 'biology', 'Biology ') ">Nucleus</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-324" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-325" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Mitochondria  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '228', null, 'biology', 'Biology ') ">Mitochondria</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-325" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-326" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Endoplasmic Reticulum  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '229', null, 'biology', 'Biology ') ">Endoplasmic Reticulum</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-326" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-327" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Golgi Apparatus  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '230', null, 'biology', 'Biology ') ">Golgi Apparatus</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-327" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-328" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Peroxisomes, Glyoxisomes and Lysosomes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '231', null, 'biology', 'Biology ') ">Peroxisomes, Glyoxisomes and Lysosomes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-328" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-329" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Ribosomes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '232', null, 'biology', 'Biology ') ">Ribosomes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-329" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-330" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Centrioles  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '233', null, 'biology', 'Biology ') ">Centrioles</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-330" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-331" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Cytoskeleton  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '234', null, 'biology', 'Biology ') ">Cytoskeleton</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-331" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-38-subsection-942" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Cell Comparisons  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '769', null, 'biology', 'Biology ') ">Cell Comparisons</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-38-subsection-942" data-tt-parent-id="subject-3-section-38" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-39" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Biological Molecules</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-39" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">20</td>
                                <!--<td>20 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">20&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-39-subsection-332" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Biological Molecules  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '235', null, 'biology', 'Biology ') ">Basic Concepts of Biological Molecules</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-39-subsection-332" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-39-subsection-333" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Carbohydrates  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '236', null, 'biology', 'Biology ') ">Carbohydrates</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-39-subsection-333" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-39-subsection-334" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Proteins  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '237', null, 'biology', 'Biology ') ">Proteins</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-39-subsection-334" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-39-subsection-335" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Lipids  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '238', null, 'biology', 'Biology ') ">Lipids</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-39-subsection-335" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-39-subsection-336" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nucleic Acids  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '239', null, 'biology', 'Biology ') ">Nucleic Acids</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-39-subsection-336" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-39-subsection-337" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Enzymes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '240', null, 'biology', 'Biology ') ">Enzymes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-39-subsection-337" data-tt-parent-id="subject-3-section-39" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-40" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Microbiology</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-40" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">10</td>
                                <!--<td>10 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">10&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-40-subsection-338" data-tt-parent-id="subject-3-section-40" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Viruses  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '241', null, 'biology', 'Biology ') ">Viruses</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-40-subsection-338" data-tt-parent-id="subject-3-section-40" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-40-subsection-339" data-tt-parent-id="subject-3-section-40" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Bacteria  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '242', null, 'biology', 'Biology ') ">Bacteria</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-40-subsection-339" data-tt-parent-id="subject-3-section-40" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">5</td>
                                <!--<td>5 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">5&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-40-subsection-340" data-tt-parent-id="subject-3-section-40" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Fungi  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '243', null, 'biology', 'Biology ') ">Fungi</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-40-subsection-340" data-tt-parent-id="subject-3-section-40" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:0%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">0</td>
                                <!--<td>0 (0%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-41" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Kingdom Animalia</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-41" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-41-subsection-341" data-tt-parent-id="subject-3-section-41" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Terminology Related to Kingdom Animalia  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '244', null, 'biology', 'Biology ') ">Basic Terminology Related to Kingdom Animalia</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-41-subsection-341" data-tt-parent-id="subject-3-section-41" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-41-subsection-342" data-tt-parent-id="subject-3-section-41" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Medically Important Phyla  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '245', null, 'biology', 'Biology ') ">Medically Important Phyla</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-41-subsection-342" data-tt-parent-id="subject-3-section-41" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-42" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Digestive System</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-42" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">12</td>
                                <!--<td>12 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">12&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-42-subsection-343" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Introduction to Digestive System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '246', null, 'biology', 'Biology ') ">Introduction to Digestive System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-42-subsection-343" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-42-subsection-344" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Digestion in Oral Cavity, Pharynx and Esophagus  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '247', null, 'biology', 'Biology ') ">Digestion in Oral Cavity, Pharynx and Esophagus</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-42-subsection-344" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-42-subsection-345" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Digestion in Stomach  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '248', null, 'biology', 'Biology ') ">Digestion in Stomach</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-42-subsection-345" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-42-subsection-346" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Digestion in Small Intestine  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '249', null, 'biology', 'Biology ') ">Digestion in Small Intestine</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-42-subsection-346" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-42-subsection-347" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Digestion in Large Intestine  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '250', null, 'biology', 'Biology ') ">Digestion in Large Intestine</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-42-subsection-347" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-42-subsection-348" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Disorders Related to Nutrition  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '251', null, 'biology', 'Biology ') ">Disorders Related to Nutrition</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-42-subsection-348" data-tt-parent-id="subject-3-section-42" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-43" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Gas Exchange</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-43" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">10</td>
                                <!--<td>10 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">10&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-43-subsection-349" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Anatomy of Respiratory System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '252', null, 'biology', 'Biology ') ">Anatomy of Respiratory System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-43-subsection-349" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-43-subsection-350" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Mechanism of Breathing  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '253', null, 'biology', 'Biology ') ">Mechanism of Breathing</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-43-subsection-350" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-43-subsection-351" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Transport of Respiratory Gases  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '254', null, 'biology', 'Biology ') ">Transport of Respiratory Gases</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-43-subsection-351" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-43-subsection-352" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Respiratory Pigments  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '255', null, 'biology', 'Biology ') ">Respiratory Pigments</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-43-subsection-352" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-43-subsection-353" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Respiratory Disorders  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '256', null, 'biology', 'Biology ') ">Respiratory Disorders</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-43-subsection-353" data-tt-parent-id="subject-3-section-43" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-44" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Transport</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-44" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">12</td>
                                <!--<td>12 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">12&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-44-subsection-354" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Transport  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '257', null, 'biology', 'Biology ') ">Basic Concepts of Transport</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-44-subsection-354" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-44-subsection-355" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Blood  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '258', null, 'biology', 'Biology ') ">Blood</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-44-subsection-355" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-44-subsection-356" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Heart  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '259', null, 'biology', 'Biology ') ">Heart</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-44-subsection-356" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-44-subsection-357" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Blood Vessels  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '260', null, 'biology', 'Biology ') ">Blood Vessels</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-44-subsection-357" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-44-subsection-358" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Circulatory Disorders  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '261', null, 'biology', 'Biology ') ">Circulatory Disorders</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-44-subsection-358" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-44-subsection-359" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Lymphatic System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '262', null, 'biology', 'Biology ') ">Lymphatic System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-44-subsection-359" data-tt-parent-id="subject-3-section-44" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-45" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Homeostasis</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-45" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">11</td>
                                <!--<td>11 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">11&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-45-subsection-360" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Homeostasis  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '263', null, 'biology', 'Biology ') ">Basic Concepts of Homeostasis</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-45-subsection-360" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-45-subsection-361" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Structure and Functioning of Kidney  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '264', null, 'biology', 'Biology ') ">Structure and Functioning of Kidney</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-45-subsection-361" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-45-subsection-362" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Kidney Problems and Cure  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '265', null, 'biology', 'Biology ') ">Kidney Problems and Cure</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-45-subsection-362" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-45-subsection-363" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Osmoregulation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '266', null, 'biology', 'Biology ') ">Osmoregulation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-45-subsection-363" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-45-subsection-364" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Thermoregulation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '267', null, 'biology', 'Biology ') ">Thermoregulation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-45-subsection-364" data-tt-parent-id="subject-3-section-45" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-46" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Nervous System</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-46" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">14</td>
                                <!--<td>14 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">14&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-46-subsection-365" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Comparison of Nervous and Hormonal Control  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '268', null, 'biology', 'Biology ') ">Comparison of Nervous and Hormonal Control</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-46-subsection-365" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-46-subsection-366" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Elements of Nervous System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '269', null, 'biology', 'Biology ') ">Elements of Nervous System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-46-subsection-366" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-46-subsection-367" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nerve Impulse and Synapse  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '270', null, 'biology', 'Biology ') ">Nerve Impulse and Synapse</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-46-subsection-367" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-46-subsection-368" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Central Nervous System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '271', null, 'biology', 'Biology ') ">Central Nervous System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-46-subsection-368" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-46-subsection-369" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Peripheral Nervous System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '272', null, 'biology', 'Biology ') ">Peripheral Nervous System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-46-subsection-369" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-46-subsection-370" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nervous Disorders  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '273', null, 'biology', 'Biology ') ">Nervous Disorders</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-46-subsection-370" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-46-subsection-371" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Biological Rhythms  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '274', null, 'biology', 'Biology ') ">Biological Rhythms</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-46-subsection-371" data-tt-parent-id="subject-3-section-46" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-47" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Reproduction</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-47" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">8</td>
                                <!--<td>8 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">8&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-47-subsection-372" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Gametogenesis  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '275', null, 'biology', 'Biology ') ">Gametogenesis</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-47-subsection-372" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-47-subsection-373" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Male Reproductive System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '276', null, 'biology', 'Biology ') ">Male Reproductive System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-47-subsection-373" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-47-subsection-374" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Female Reproductive System  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '277', null, 'biology', 'Biology ') ">Female Reproductive System</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-47-subsection-374" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-47-subsection-375" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Sexually Transmitted Diseases  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '278', null, 'biology', 'Biology ') ">Sexually Transmitted Diseases</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-47-subsection-375" data-tt-parent-id="subject-3-section-47" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-48" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Support and Movement</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-48" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">12</td>
                                <!--<td>12 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">12&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-48-subsection-376" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Support and Movement  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '279', null, 'biology', 'Biology ') ">Basic Concepts of Support and Movement</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-48-subsection-376" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-48-subsection-377" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Axial and Appendicular Skeleton  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '280', null, 'biology', 'Biology ') ">Axial and Appendicular Skeleton</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-48-subsection-377" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-48-subsection-378" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Joints  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '281', null, 'biology', 'Biology ') ">Joints</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-48-subsection-378" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-48-subsection-379" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Skeletal Muscle and Related Concepts  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '282', null, 'biology', 'Biology ') ">Skeletal Muscle and Related Concepts</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-48-subsection-379" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-48-subsection-380" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Muscle Disorders  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '283', null, 'biology', 'Biology ') ">Muscle Disorders</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-48-subsection-380" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-48-subsection-381" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Comparison of Smooth, Cardiac and Skeletal Muscles  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '284', null, 'biology', 'Biology ') ">Comparison of Smooth, Cardiac and Skeletal Muscles</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-48-subsection-381" data-tt-parent-id="subject-3-section-48" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-49" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Hormonal Control</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-49" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">22</td>
                                <!--<td>22 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">22&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-382" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Hormonal Control  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '285', null, 'biology', 'Biology ') ">Basic Concepts of Hormonal Control</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-382" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-383" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Chemical Composition of Hormones  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '286', null, 'biology', 'Biology ') ">Chemical Composition of Hormones</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-383" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-384" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Hypothalamus  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '287', null, 'biology', 'Biology ') ">Hypothalamus</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-384" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-385" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Pituitary Gland  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '288', null, 'biology', 'Biology ') ">Pituitary Gland</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-385" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-386" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Thyroid Gland  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '289', null, 'biology', 'Biology ') ">Thyroid Gland</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-386" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-387" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Parathyroid Gland  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '290', null, 'biology', 'Biology ') ">Parathyroid Gland</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-387" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-388" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Adrenal Gland  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '291', null, 'biology', 'Biology ') ">Adrenal Gland</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-388" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-389" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Islets of Langerhans  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '292', null, 'biology', 'Biology ') ">Islets of Langerhans</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-389" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-390" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Hormones of Alimentary Canal  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '293', null, 'biology', 'Biology ') ">Hormones of Alimentary Canal</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-390" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-391" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Hormones of Ovaries and Testes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '294', null, 'biology', 'Biology ') ">Hormones of Ovaries and Testes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-391" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-49-subsection-392" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Endocrine Disorders  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '295', null, 'biology', 'Biology ') ">Endocrine Disorders</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-49-subsection-392" data-tt-parent-id="subject-3-section-49" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-50" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Human Physiology: Immunity</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-50" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:0%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">0</td>
                                <!--<td>0 (0%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-50-subsection-393" data-tt-parent-id="subject-3-section-50" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch The Immunity  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '296', null, 'biology', 'Biology ') ">The Immunity</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-50-subsection-393" data-tt-parent-id="subject-3-section-50" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:0%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">0</td>
                                <!--<td>0 (0%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-51" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Bioenergetics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-51" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-51-subsection-395" data-tt-parent-id="subject-3-section-51" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Bioenergetics and Photosynthesis  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '298', null, 'biology', 'Biology ') ">Bioenergetics and Photosynthesis</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-51-subsection-395" data-tt-parent-id="subject-3-section-51" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-51-subsection-396" data-tt-parent-id="subject-3-section-51" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Cellular Respiration  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '299', null, 'biology', 'Biology ') ">Cellular Respiration</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-51-subsection-396" data-tt-parent-id="subject-3-section-51" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-52" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Biotechnology</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-52" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">9</td>
                                <!--<td>9 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">9&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-52-subsection-397" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Recombinant DNA Technology  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '300', null, 'biology', 'Biology ') ">Recombinant DNA Technology</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-52-subsection-397" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-52-subsection-398" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Polymerase Chain Reaction  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '301', null, 'biology', 'Biology ') ">Polymerase Chain Reaction</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-52-subsection-398" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-52-subsection-399" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch DNA Analysis  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '302', null, 'biology', 'Biology ') ">DNA Analysis</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-52-subsection-399" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-52-subsection-400" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Gene Therapy  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '303', null, 'biology', 'Biology ') ">Gene Therapy</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-52-subsection-400" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-52-subsection-401" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Transgenic Organisms  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '304', null, 'biology', 'Biology ') ">Transgenic Organisms</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-52-subsection-401" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:0%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">0</td>
                                <!--<td>0 (0%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-52-subsection-402" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Tissue Culture  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '305', null, 'biology', 'Biology ') ">Tissue Culture</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-52-subsection-402" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-52-subsection-403" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Cloning  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '306', null, 'biology', 'Biology ') ">Cloning</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-52-subsection-403" data-tt-parent-id="subject-3-section-52" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-53" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Ecosystem</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-53" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">7</td>
                                <!--<td>7 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">7&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-53-subsection-404" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Biological Succession  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '307', null, 'biology', 'Biology ') ">Biological Succession</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-53-subsection-404" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-53-subsection-405" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Impacts of Human Activity on Ecosystem  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '308', null, 'biology', 'Biology ') ">Impacts of Human Activity on Ecosystem</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-53-subsection-405" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-53-subsection-406" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Nitrogen Cycle  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '309', null, 'biology', 'Biology ') ">Nitrogen Cycle</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-53-subsection-406" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-53-subsection-407" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Energy Flow in Ecosystem  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '310', null, 'biology', 'Biology ') ">Energy Flow in Ecosystem</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-53-subsection-407" data-tt-parent-id="subject-3-section-53" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-54" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Evolution</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-54" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">6</td>
                                <!--<td>6 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">6&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-54-subsection-408" data-tt-parent-id="subject-3-section-54" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Lamarck’s &amp; Darwin’s Theories  of Evolution  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '311', null, 'biology', 'Biology ') ">Lamarck’s &amp; Darwin’s Theories  of Evolution</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-54-subsection-408" data-tt-parent-id="subject-3-section-54" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-54-subsection-409" data-tt-parent-id="subject-3-section-54" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Evidences of Evolution  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '312', null, 'biology', 'Biology ') ">Evidences of Evolution</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-54-subsection-409" data-tt-parent-id="subject-3-section-54" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-54-subsection-410" data-tt-parent-id="subject-3-section-54" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Hardy-Weinberg Theorem  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '313', null, 'biology', 'Biology ') ">Hardy-Weinberg Theorem</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-54-subsection-410" data-tt-parent-id="subject-3-section-54" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-3-section-55" data-tt-parent-id="subject-3" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Genetics</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-3-section-55" data-tt-parent-id="subject-3" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">19</td>
                                <!--<td>19 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">19&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-411" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Basic Concepts of Genetics  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '314', null, 'biology', 'Biology ') ">Basic Concepts of Genetics</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-411" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:0%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">0</td>
                                <!--<td>0 (0%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-412" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Mendelian Inheritance  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '315', null, 'biology', 'Biology ') ">Mendelian Inheritance</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-412" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-413" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Genetic Linkage and Sex Linkage  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '316', null, 'biology', 'Biology ') ">Genetic Linkage and Sex Linkage</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-413" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-414" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch DNA Replication  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '317', null, 'biology', 'Biology ') ">DNA Replication</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-414" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-415" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Mechanism of Gene Expression  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '318', null, 'biology', 'Biology ') ">Mechanism of Gene Expression</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-415" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-416" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Sex Determination  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '319', null, 'biology', 'Biology ') ">Sex Determination</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-416" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-417" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Cell Cycle  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '320', null, 'biology', 'Biology ') ">Cell Cycle</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-417" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-3-section-55-subsection-418" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Mitosis and Meiosis  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '321', null, 'biology', 'Biology ') ">Mitosis and Meiosis</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-3-section-55-subsection-418" data-tt-parent-id="subject-3-section-55" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">4</td>
                                <!--<td>4 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">4&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>


                            <!--// subject-->
                            <tr data-tt-id="subject-4" id="sbjct" class="branch collapsed">
                                <td colspan="5"><span class="indenter" style="padding-left: 0px;"><a href="#" title="Expand">&nbsp;</a></span>
                                    <span class="folder">English</span>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    <div class="progress active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>

                                    </div>
                                </td>
                                <!--<td>75 (100%)</td>-->
                                <td class="text-center">75</td>
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td style="text-align:center;">75&nbsp;
                                    <small class="font-weight-bold bg-red rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-4-section-104" data-tt-parent-id="subject-4" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Sentence Completion/Synonyms</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-4-section-104" data-tt-parent-id="subject-4" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">35</td>
                                <!--<td>35 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">35&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-943" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Orientation of Vocabulary  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '770', null, 'english', 'English') ">Orientation of Vocabulary</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-943" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-604" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  A  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '726', null, 'english', 'English') ">UHS Essential Word Power -  A</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-604" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-605" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  B  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '727', null, 'english', 'English') ">UHS Essential Word Power -  B</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-605" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-606" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  C  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '728', null, 'english', 'English') ">UHS Essential Word Power -  C</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-606" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-607" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  D  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '729', null, 'english', 'English') ">UHS Essential Word Power -  D</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-607" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-608" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  E  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '730', null, 'english', 'English') ">UHS Essential Word Power -  E</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-608" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-609" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  F  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '731', null, 'english', 'English') ">UHS Essential Word Power -  F</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-609" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-610" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  G,H  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '732', null, 'english', 'English') ">UHS Essential Word Power -  G,H</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-610" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-611" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  I  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '733', null, 'english', 'English') ">UHS Essential Word Power -  I</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-611" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-612" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  J, K , L  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '734', null, 'english', 'English') ">UHS Essential Word Power -  J, K , L</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-612" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-613" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  M  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '735', null, 'english', 'English') ">UHS Essential Word Power -  M</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-613" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-614" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  N, O  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '736', null, 'english', 'English') ">UHS Essential Word Power -  N, O</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-614" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-615" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  P   Videos" onclick="LoadVideos('https://app.topgrade.pk/', '737', null, 'english', 'English') ">UHS Essential Word Power -  P </strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-615" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-616" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  Q,R  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '738', null, 'english', 'English') ">UHS Essential Word Power -  Q,R</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-616" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-617" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  S  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '739', null, 'english', 'English') ">UHS Essential Word Power -  S</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-617" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-618" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  T  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '740', null, 'english', 'English') ">UHS Essential Word Power -  T</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-618" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-619" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  U,V  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '741', null, 'english', 'English') ">UHS Essential Word Power -  U,V</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-619" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">1</td>
                                <!--<td>1 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">1&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-104-subsection-620" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch UHS Essential Word Power -  W,X,Y,Z  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '742', null, 'english', 'English') ">UHS Essential Word Power -  W,X,Y,Z</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-104-subsection-620" data-tt-parent-id="subject-4-section-104" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--chapter-->


                            <tr data-tt-id="subject-4-section-105" data-tt-parent-id="subject-4" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span>
                                    <span class="folder">Spot the Error/Sentence Correction</span>
                                </td>

                            </tr>


                            <tr id="sbjct-child" data-tt-id="subject-4-section-105" data-tt-parent-id="subject-4" class="branch" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-40 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">40</td>
                                <!--<td>40 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">40&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>

                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-944" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Orientation of Grammar  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '771', null, 'english', 'English') ">Orientation of Grammar</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-944" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-621" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Concord Mistakes  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '743', null, 'english', 'English') ">Concord Mistakes</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-621" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-622" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Noun  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '744', null, 'english', 'English') ">Noun</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-622" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-623" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Pronoun  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '745', null, 'english', 'English') ">Pronoun</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-623" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-624" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Adjective  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '746', null, 'english', 'English') ">Adjective</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-624" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-625" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Verb  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '747', null, 'english', 'English') ">Verb</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-625" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-626" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Adverb  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '748', null, 'english', 'English') ">Adverb</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-626" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-627" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Modifiers  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '749', null, 'english', 'English') ">Modifiers</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-627" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-628" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Preposition  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '750', null, 'english', 'English') ">Preposition</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-628" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-629" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Conjunctions  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '751', null, 'english', 'English') ">Conjunctions</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-629" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">3</td>
                                <!--<td>3 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">3&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-630" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Article  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '752', null, 'english', 'English') ">Article</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-630" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-631" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Redundancy  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '753', null, 'english', 'English') ">Redundancy</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-631" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-632" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Parallelism  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '754', null, 'english', 'English') ">Parallelism</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-632" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-633" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Collocation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '755', null, 'english', 'English') ">Collocation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-633" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-634" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Punctuation  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '756', null, 'english', 'English') ">Punctuation</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-634" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-961" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Diction  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '789', null, 'english', 'English') ">Diction</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-961" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-969" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Tenses  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '791', null, 'english', 'English') ">Tenses</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-969" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-963" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Narration  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '764', null, 'english', 'English') ">Narration</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-963" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>
                            <!--topic-->

                            <tr data-tt-id="subject-4-section-105-subsection-964" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td colspan="5"><span class="indenter"></span><span class="file"><strong title="Watch Voice  Videos" onclick="LoadVideos('https://app.topgrade.pk/', '765', null, 'english', 'English') ">Voice</strong></span></td>

                            </tr>
                            <tr data-tt-id="subject-4-section-105-subsection-964" data-tt-parent-id="subject-4-section-105" class="leaf" style="display: none;">
                                <td><span class="indenter"></span>
                                    <div class="progress ml-80 active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" style=";width:0%">
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" style=";width:100%">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">2</td>
                                <!--<td>2 (100%)</td>-->
                                <td class="text-center">0&nbsp;
                                    <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small>
                                </td>
                                <td class="text-center">2&nbsp;
                                    <small class="font-weight-bold bg-red text-white rounded badge badge-plain ng-binding">100%</small>
                                </td>
                            </tr>
                            <tr style="outline: thin solid #d5d3d4;"></tr>



                            <!--tfooter-->
                        </tbody><thead>
                            <tr>
                                <th style="width:50%; padding-left:13px;">Total</th>
                                <th style="width:15%;text-align: center;">489</th>
                                <th style="width:15%;text-align: center;">0&nbsp; <small class="font-weight-bold bg-green text-white rounded badge badge-plain ng-binding">0%</small></th>
                                <th style="width:15%px;text-align: center;">489&nbsp; <small class="font-weight-bold bg-red rounded badge badge-plain ng-binding">100%</small></th>
                            </tr>
                        </thead>
                        <!--end tfooter-->                        
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>
</section>
</div>
@endsection
