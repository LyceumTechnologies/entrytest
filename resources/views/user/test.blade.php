<!DOCTYPE html>
<?php
/**
 * Project: Entrytest4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')

<div id="main-sec">
	<section id="mainWrap-sec" class="mainWrap-sec"><!-- body -->
		<div id="wrap-leftSide" class="wrap-leftSide hidden-xs testHasCourse" style="display: block;">
			<ul id="tests-group" class="tests-group learn-tstgroup">
				<li class="tests-item"><a class="testsItem-div main-active">Entry Test<span class="du-arrows tabscaretall">&nbsp;</span></a>

					<ul id="lessonslist" class="examsnames-collapse examshd-collapsefirst sin ">

						
						@foreach($course as $cour)
						<li><a href="javascript:;" id="{{preg_replace('/\s/', '', $cour->name)}}{{$cour->id}}" class="sub_active tpSecName-1 dt-tpSecName getSubject" data-subject="{{preg_replace('/\s/', '', $cour->name)}}{{$cour->id}}"><span class="subactive-dot"></span>{{$cour->name}}</a></li>
						@endforeach
						{{-- <li><a href="#scrolltotop" class="sub_active tpSecName-1 dt-tpSecName"><span class="subactive-dot"></span>{{$cour->name}}</a></li>
						<li><a class="tpSecName-1 dt-tpSecName" href="#scrolltotop" onclick="filtertopics(1);">Quantitative Ability 1</a></li>
						<li><a class="tpSecName-2 dt-tpSecName" href="#scrolltotop1" onclick="filtertopics(2);">Quantitative Ability 2</a></li>
						<li><a class="tpSecName-3 dt-tpSecName" href="#scrolltotop2" onclick="filtertopics(3);">Data Interpretation</a></li>
						<li><a class="tpSecName-4 dt-tpSecName" href="#scrolltotop3" onclick="filtertopics(4);">Logical Reasoning</a></li>
						<li><a class="tpSecName-5 dt-tpSecName" href="#scrolltotop4" onclick="filtertopics(5);">Verbal Ability</a></li> -   --}}      
					</ul>
				</li>
				<li class="tests-item"><a href='javascript:;' class=" testsItem-div cpoint"> <!-- here is feeter-->     <span class="du-greyarr greytaball">&nbsp;</span></a>
				</li> 
			</ul>
		</div>
		{{-- /////////////////////////////// courseHassubject change//////////////////// --}}
		@foreach($course as $cour)
		<div id="wrap-leftSide" class="wrap-leftSide hidden-xs CourseHassubjects  {{ preg_replace('/\s/', '', $cour->name) }} {{$cour->id}}" style="display: none">
			<ul id="tests-group" class="tests-group learn-tstgroup">
				<li class="tests-item"><a class="testsItem-div main-active">{{$cour->name}}<span class="du-arrows tabscaretall">&nbsp;</span></a>
					<ul id="lessonslist" class="examsnames-collapse examshd-collapsefirst sin ">
						@foreach($cour->couseSubject as $subjectC)
						<li><a href="javascript:;" data-ids="{{$subjectC->subject->id}}"  id="{{preg_replace('/\s/', '', $subjectC->subject->sb_name)}}{{$subjectC->subject->id}}" onclick="subjectTopic(this)" class="sub_active tpSecName-1 dt-tpSecName getSubject"><span class="subactive-dot"></span>{{$subjectC->subject->sb_name}}</a></li>
						@endforeach
						     
					</ul>
				</li>
				<li class="tests-item"><a href='javascript:;' class=" testsItem-div cpoint"> <!-- here is feeter-->     <span class="du-greyarr greytaball">&nbsp;</span></a>
				</li> 
			</ul>
		</div>
		@endforeach
		{{-- ///////////////////////////////////////////////////////////////////////////////// --}}

		<div class="wrap-rightSide">
			@foreach($course as $cour)
			<div class="{{preg_replace('/\s/', '', $cour->name)}}{{$cour->id}} allBookContent" style="@if ($loop->first) display: block; @else display:none; @endif">
				<div class="section-header hidden-xs">
					<h3>{{$cour->name}} - Books  <span id="topheader-txt">All Books</span>
					</h3>
				</div>
				@foreach($cour->couseSubject as $subjectC)
				<div id="sec-topicTst" class="sec-topicTst sec-pca">
					<div class="row tptst-row tp1 cpoint" data-ids="{{$subjectC->subject->id}}" onclick="subjectTopic(this)">
						<!-- tst row --><span class="mtmob-borcl mtmob-bor-available">&nbsp;</span>
						<div class="col-xs-12 tptst-col-left">
							<!-- tst col left --><span class="mt-borcl mt-bor-available">&nbsp;</span>
							<div class="tpic-tstname">
								<span class="mt-status mt-available">&nbsp;</span>
								<h3>{{$subjectC->subject->sb_name}}</h3>
							</div>
							<div class="tptst-col-right">
								<button class="btn btn-mt btn-taketest" data-ids="{{$subjectC->subject->id}}" onclick="subjectTopic(this)" type="button">View</button>
							</div>
						</div><!-- /.tst col left -->
					</div>
				</div>
				@endforeach
			</div>
			@endforeach
			<div id="topics-show"></div>
		</div>
	</section><!-- /. body -->
</div>
<!--Footer page (includes closing body and html tag). Add common JS code in the footer subjectTopic -->


@endsection
@push('post-scripts')
<script type="text/javascript">
	$('.getSubject').on('click',function(e){
		var obj=$(this).attr('id');
		$('.allBookContent').css('display','none');
		console.log($('.'+obj).css("display", "block"));
		$('.'+obj).show();
	});
	$(function(){
		$('.allBookContent').css('display','none');
		
	});
	function subjectTopic(obj){
		var id=$(obj).attr('data-ids');
		$.ajax({
                url: "{{route('subjectTopic')}}",
                type: 'post',
                data: {
                    'id': id
                },
                success: function (response) {
                	$('.allBookContent').css("display",'none');
                    $("#topics-show").html(response);

                },
                error: function (e) {
                    console.log('error', e);
                }
            });
	}
</script>
@endpush