@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')
<div id="main-sec">
<section id="mainWrap-sec" class="mainWrap-sec col-md-12">
<div class="row row-features">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light bordered notes">
            <div class="portlet-title">
                <div class="caption ">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green bold" data-step="2" data-intro="Search Notes" data-position="right" data-scrollto="tooltip">Search Notes</span>
                </div>
            </div>
            <div class="portlet-body d-flex flex-wrap">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="flashnotes_form" method="get" action="https://app.topgrade.pk/notes-filter">
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>Notes</label>
                                <select class="form-control" id="notes-type" onchange="get_notetype(this.value)" name="notes-type">
                                    <option value="">Notes Type</option>
                                    <option value="0">Video Lectures</option>
                                    <option value="1">Fast Revision Notes</option>
                                    <option value="2">Question Bank</option>
                                    <option value="3">General Notes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12" id="subject_list">
                            <div class="form-group">
                                <label>Subject</label>
                                <select class="form-control" id="flashnote_subject_list" name="flashnote_subject_list" onchange="get_flashnotes_section('https://app.topgrade.pk/', this.value)">
                                    <option value="0">Select Subject</option>
                                    <option value="249576623">Physics</option>
                                    <option value="645586696">Chemistry</option>
                                    <option value="448566652">Biology </option>
                                    <option value="945556719">English</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12" id="section_list">
                            <div class="form-group">
                                <label>Section</label>
                                <select class="form-control select2 select2-hidden-accessible" onchange="get_flashnotes_subsection(this.value, 'https://app.topgrade.pk/')" name="flashnote_section_list" id="flashnote_section_list" tabindex="-1" aria-hidden="true">
                                    <option value="0">Select Section</option>
                                </select>
                                <span class="select2 select2-container select2-container--bootstrap" dir="ltr" style="width: 411px;">
                                    <span class="selection">
                                        <span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-flashnote_section_list-container">
                                            <span class="select2-selection__rendered" id="select2-flashnote_section_list-container" title="Select Section">Select Section</span>
                                            <span class="select2-selection__arrow" role="presentation">
                                                <b role="presentation"></b>
                                            </span>
                                        </span>
                                    </span>
                                    <span class="dropdown-wrapper" aria-hidden="true"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 w-m-50" id="subsection_list">
                            <div class="form-group">
                                <label>Sub-Section</label>
                                <select class="form-control select2 select2-hidden-accessible" id="flashnotes_subsection_list" name="flashnotes_subsection_list" tabindex="-1" aria-hidden="true">
                                    <option value="0">Select Subsection</option>
                                </select>
                                <span class="select2 select2-container select2-container--bootstrap" dir="ltr" style="width: 411px;">
                                    <span class="selection">
                                        <span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-flashnotes_subsection_list-container">
                                            <span class="select2-selection__rendered" id="select2-flashnotes_subsection_list-container" title="Select Subsection">Select Subsection</span>
                                            <span class="select2-selection__arrow" role="presentation">
                                                <b role="presentation"></b>
                                            </span>
                                        </span>
                                    </span>
                                    <span class="dropdown-wrapper" aria-hidden="true"></span>
                                </span>
                            </div>
                        </div>
                        <div class="clearfix d-none"></div>
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 mt-24 pre-tst">
                            <div class="form-actions">
                                <button type="button" onclick="notes_filter()" class="btn green rounded">Filter</button>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3 mt-24 pre-tst">
                            <div class="form-actions">
                                <a href="https://app.topgrade.pk/my-notes" class="btn green rounded">Clear Filters</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!--                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 tst-id"><form id="flashnotes_search_form" method="get" action="https://app.topgrade.pk/notes-filter"><div class="col-lg-8 col-md-8 col-sm-6 col-xs-12"><div class="form-group"><label>Question ID</label><input type="text" placeholder="Search By Q.ID" name="flashnotes_question" id="flashnotes_question" class="form-control" value="" placeholder=""></div></div><div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 mt-24 search"><div class="form-actions"><button type="button" onclick="notes_search()" class="btn green rounded">Search</button></div></div></form></div>-->
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
   @endsection