<!DOCTYPE html>
<?php
/**
* Project: Entrytest4u.
* User: shafqat bhatti
* Date: 12/03/2019
* Time: 1:44 PM
*/
?>

@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('public-web/css/user/mcqz.css')}}">
<div id="main-sec" >
	<section id="mainWrap-sec" class="mainWrap-sec"><!-- body -->
		<div id="mtq_question_container-1" style="display: block;">

			@include('_layouts.user.pagination_question')
			@if(count($question))
				<div class="mtq_results_request mtq_scroll_item-1" id="mtq_results_request-1" style="width: 605px; margin: 0 auto;">
					<table style="width: 100%;" border="0">
				<div id="watupro_quiz" class="quiz-area "><script>
					window.fbAsyncInit = function() {
						FB.init({
							appId            : '1499612433621269',
							autoLogAppEvents : true,
							xfbml            : true,
							version          : 'v3.1'
						});
					};

					(function(d, s, id){
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) {return;}
						js = d.createElement(s); js.id = id;
						js.src = "https://connect.facebook.net/en_US/sdk.js";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
				</script>
				<div id="startOutput">&nbsp;</div>
				<table style="width: 100%;" border="0">
					<tbody>
						<tr>
							<td class="testresult_tbl_coltop" colspan="3">
								<center><p></p>
									<h3>Your Test Result Card</h3>
									<p></p></center>
								</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Subject Name</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3">{{$topic->name}}</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Total Questions</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3 ">{{count($question)}}</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Total Attempt Questions</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3 totalAttempt">0</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Your Right Answers</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3 totalRightAns">0</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Percentage</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3 percentageCount">0 %</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Grade</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3">
									<p>None</p>
								</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Rating</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3">Failed</td>
							</tr>
							<tr>
								<td class="testresult_tbl_col1">Points</td>
								<td class="testresult_tbl_col2">:</td>
								<td class="testresult_tbl_col3">0</td>
							</tr>
							<tr>
								<td style="text-align: center;" colspan="3"><strong></strong></td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>
			@else@ksd
			<div class="mtq_results_request mtq_scroll_item-1" id="mtq_results_request-1" style="width: 605px;margin:0 auto;    box-shadow: 0px 2px 5px #bbb;
			border: 1px solid #ccc; padding:10px;">
			Oop! Record not found. 
			
			</div>
			@endif
		</div>
		<div class="mtq_quiz_status" id="mtq_quiz_status-1" style="display: none;">
			You have completed 2/10  questions .<br>
			Your score is 50%.</div>
		</div>
	</section>
</div>
<style type="text/css">
	.testresult_tbl_coltop {
	    width: 100%;
	    font-size: large;
	    color: #ffffff;
	    background-color: #008000;
	    font-family: arial black,avant garde;
	}
	.entry table tr:nth-child(even) {
    background: #F9F9F9;
}
</style>
@endsection
@push('post-scripts')
<script type="text/x-mathjax-config">
	MathJax.Hub.Config({
	tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
});
</script>
<script type="text/javascript" async src="path-to-mathjax/MathJax.js?config=TeX-AMS_CHTML"></script>
<script type="text/javascript">
	var index=1;
	var totalRightAns=0;
	var totalAttempt=0;
	function mtq_button_click(obj,ans){
		console.log('question',obj);
		var ids=$(obj).attr('id');
		$(obj).parent().parent('.mtq_answer_table').find('.answerRemove').first().empty();
		$(obj).parent().parent('.mtq_answer_table').find('.mtq_correct_marker').show();
		$(obj).find('.mtq_css_letter_button').first().css("display",'none');
		$(obj).find('.mtq_marker').show();
		if(ans){
			totalRightAns++;
		}
		totalAttempt++;
		console.log('answer',totalRightAns);
		$('.totalRightAns').html('');
		$('.totalRightAns').html(totalRightAns);
		$('.totalAttempt').html('');
		$('.totalAttempt').html(totalAttempt);
		$('.percentageCount').html('');
		$('.percentageCount').html((totalRightAns/totalAttempt)*100);
		$(obj).parent().parent('.mtq_answer_table').find('.mtq_clickable').unbind('click');
		$(obj).parent().parent('.mtq_answer_table').find('.mtq_clickable').click(false);
		

	}
	function previosQuestion(obj){
		console.log('previous');
		$('.question'+index).css('display','none');
		index--;
		$('.mtq_question_label').html('');
		$('.mtq_question_label').text(`Question ${index}`);
		$('.question'+index).css('display','block');
		preButton();
	}
	function nextQuestion(obj){

		$('.question'+index).css('display','none');
		$('.mtq_question_label').html('');

		
		index++;
		$('.mtq_question_label').text(`Question ${index}`);
		$('.question'+index).css('display','block');

		preButton();
	}
	function preButton(){
		if(index>1){
			$('#prev-question').show();
		}else{
			$('#prev-question').css('display','none');
		}
	}
</script>

@endpush 