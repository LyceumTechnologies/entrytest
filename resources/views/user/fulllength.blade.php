@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')
<div id="main-sec">
<section id="mainWrap-sec" class="mainWrap-sec col-md-12">
<div class="row row-features flp_home">
	<div class="col-lg-3 col-md-2 col-sm-12 col-xs-12"></div>
	<div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								<th> Test</th>
								<th colspan="2">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr class="odd gradeX">
								<td style="max-width: 100%;"> Full Length Practice Exams 
									<b>1</b>
								</td>
								<td>
									<a href="https://app.topgrade.pk/full-length-practice-exams/full-length-practice-test-review/548566939" class="btn yellow-crusta">Resume Test</a>
								</td>
							</tr>
							<tr class="odd gradeX">
								<td style="max-width: 100%;"> Full Length Practice Exams 
									<b>2</b>
								</td>
								<td>
									<a disabled="" class="btn blue-madison">Launch</a>
								</td>
							</tr>
							<tr class="odd gradeX">
								<td style="max-width: 100%;"> Full Length Practice Exams 
									<b>3</b>
								</td>
								<td>
									<a disabled="" class="btn blue-madison">Launch</a>
								</td>
							</tr>
							<tr class="odd gradeX">
								<td style="max-width: 100%;"> Full Length Practice Exams 
									<b>4</b>
								</td>
								<td>
									<a disabled="" class="btn blue-madison">Launch</a>
								</td>
							</tr>
							<tr class="odd gradeX">
								<td style="max-width: 100%;"> Full Length Practice Exams 
									<b>5</b>
								</td>
								<td>
									<a disabled="" class="btn blue-madison">Launch</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-2 col-sm-12 col-xs-12"></div>
	<div class="clearfix"></div>
</div>
@endsection