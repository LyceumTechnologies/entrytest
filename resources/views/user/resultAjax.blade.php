<div class="col-md-12 tst-rslt">
        <div class="checkbox_sec">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption ">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase" data-step="1" data-intro="View stats of your test importance here" data-position="right" data-scrollto="tooltip">
                        Your Test Performance</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                        <div id="chart_6_analysis" class="chart" style="overflow: hidden; text-align: left;">
                            <div class="amcharts-main-div" style="position: relative;">
                                <div class="amcharts-chart-div" >
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="form-group text-center d-flex justify-content-center align-items-center h-270">
                        <div class="form-group text-center" style="margin-top:20px;margin-bottom: 30px;">
                            <div class="mt-checkbox-inline">
                                <div class="text-left mb-20">
                                    <div class="box-green"></div>
                                    <span class="correctionPercentage">{{round(($result->correctQuestionCount()/$result->totalQuestionCount())*100,2)}}</span>
                                </div>
                                <div class="text-left mb-20">
                                    <div class="box-red"></div>
                                    <span class="inCorrectPercentage">{{round(($result->wrongQuestionCount()/$result->totalQuestionCount())*100,2)}}</span>
                                </div>
                                <div class="text-left mb-20">
                                    <div class="box-blue"></div>
                                    <span class="unAnswerPercentage">{{round(($result->SkipQuestionCount()/$result->totalQuestionCount())*100,2)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center p-0">
                    <div class="gift">
                        <p>
                           Your Score
                       </p>
                       <figure class="p-20">
                          <img id="img" src="">
                      </figure>
                      <div class="gift-detail">
                        <div class="col-lg-12">
                            <small class="score">{{$result->correctQuestionCount()}} out of {{$result->totalQuestionCount()}}</small>
                            <small class="bg-red text-white rounded">{{round($result->correctQuestionCount()/$result->totalQuestionCount()*100,2)}}</small>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="stats">
                    <p class="text-center">
                        Stats
                    </p>
                    <div class="p-10-15 border-gray-bottom mb-0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            Total Questions:
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            <small class="bg-gray rounded font-weight-bold border-gray questionCount" style="padding: 5px 10px;">{{$result->totalQuestionCount()}}</small>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="p-10-15 border-gray-bottom mb-0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            Answered:
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            <small class="bg-gray rounded font-weight-bold border-gray answerCount" style="padding: 5px 10px;">{{$result->correctQuestionCount() + $result->wrongQuestionCount()}}</small> <span class="bg-purple rounded font-weight-bold text-white answerCountPer" style="padding:5px 5px;"> {{round((($result->correctQuestionCount() + $result->wrongQuestionCount())/$result->totalQuestionCount())*100,2)}} % </span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="p-10-15 border-gray-bottom mb-0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            Correct:
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            <small class="bg-gray rounded font-weight-bold border-gray correctCount" style="padding: 5px 10px;">{{$result->correctQuestionCount()}}</small> <span class="bg-green rounded font-weight-bold text-white correctCountPer" style="padding:5px 5px;">{{round($result->correctQuestionCount()/$result->totalQuestionCount()*100,2)}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="p-10-15 border-gray-bottom mb-0 p-0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            Incorrect:
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            <small class="bg-gray rounded font-weight-bold border-gray inCorrectCount" style="padding: 5px 10px;">{{$result->wrongQuestionCount()}}</small> <span class="bg-red rounded font-weight-bold text-white inCorrectCountPer" style="padding:5px 5px;">{{round($result->wrongQuestionCount()/$result->totalQuestionCount()*100,2)}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>


                    <div class="p-10-15 border-gray-bottom mb-0">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            Unanswered:
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                            <small class="bg-gray rounded font-weight-bold border-gray unAnswerCount" style="padding: 5px 10px;">{{$result->SkipQuestionCount()}}</small> <span class="bg-blue rounded font-weight-bold text-white unAnswerCountPer" style="padding:5px 5px;">{{round($result->SkipQuestionCount()/$result->totalQuestionCount()*100,2)}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>