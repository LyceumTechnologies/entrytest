@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')
<div id="main-sec">
<section id="mainWrap-sec" class="mainWrap-sec col-md-12">
	<div class="row row-features">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="portlet light bordered notes">
                   <div class="portlet-title">
                      <div class="caption">
                         <i class=" icon-layers font-green"></i>
                               <span class="caption-subject font-green bold uppercase" data-step="1" data-intro="SEARCH TESTS" data-position="right" data-scrollto="tooltip">Search Tests</span>
                                    </div>
                                </div>
                                <div class="portlet-body d-flex flex-wrap">
                                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                                        <form id="previoustest_form" method="get" action="https://app.topgrade.pk/question-bank-tests/previous-test-filter">
                                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <label>Subject</label>
                                                    <select class="form-control" id="subject_list" name="subject_list">
                                                        <option value="0">Select Subject</option>
                                                        <option value="249576623">Physics</option>
                                                        <option value="645586696">Chemistry</option>
                                                        <option value="448566652">Biology </option>
                                                        <option value="945556719">English</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group">
                                                    <label>Test Status</label>
                                                    <select class="form-control" id="test_status" name="test_status">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Incomplete</option>
                                                        <option value="2">Complete</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 w-m-50">
                                                <div class="form-group">
                                                    <label>Date Range</label>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                                                        <div class="input-group" id="">
                                                            <input type="date" class="form-control"  name="date_range"  id="#">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix d-none"></div>
                                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 mt-24 pre-tst">
                                                <div class="form-actions">
                                                    <button type="button" onclick="test_filter()" class="btn green rounded">Filter</button>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 mt-24 pre-tst">
                                                <div class="form-actions">
                                                    <a href="https://app.topgrade.pk/question-bank-tests/previous-test" class="btn green rounded">Clear Filters</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 tst-id">
                                        <form id="previoustest_search_form" method="get" action="https://app.topgrade.pk/question-bank-tests/previous-test-filter">
                                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Test ID</label>
                                                    <input type="text" name="test_id" placeholder="Search by Test ID" id="test_id" class="form-control" value="">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 mt-24 search">
                                                <div class="form-actions">
                                                    <button type="button" onclick="test_search()" class="btn green rounded">Search</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-features">
                        <div class="checkbox_sec">
                            <div class="portlet">
                                <div class="portlet-body" data-step="2" data-intro="Watch your imortant digits and channelize your efforts accordingly" data-position="right" data-scrollto="tooltip">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="dashboard-stat2">
                                            <div class="display">
                                                <div class="number">
                                                    <h3 class="font-blue-sharp">
                                                        <span data-counter="counterup" data-value="0">0</span>
                                                    </h3>
                                                    <small>Total Taken</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="dashboard-stat2">
                                            <div class="display">
                                                <div class="number">
                                                    <h3 class="font-blue-sharp">
                                                        <span data-counter="counterup" data-value="0">0</span>
                                                    </h3>
                                                    <small>Total Complete</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="dashboard-stat2 ">
                                            <div class="display">
                                                <div class="number">
                                                    <h3 class="font-blue-sharp">
                                                        <span data-counter="counterup" data-value="0">0</span>
                                                    </h3>
                                                    <small>Total Uncomplete</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!--end new2-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-features">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-green"></i>
                                        <span class="caption-subject font-green bold uppercase" data-step="3" data-intro="PREVIOUS TESTS" data-position="right" data-scrollto="tooltip"> Previous Tests</span>
                                    </div>
                                </div>
<div class="portlet-body">
	<div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
		<div class="row">
			<div class="col-md-6 col-sm-6"></div>
			<div class="col-md-6 col-sm-6"></div>
		</div>
		<div class="table-scrollable">
			<table class="table table-striped table-bordered table-hover table-checkable order-column prev-test dataTable no-footer" id="sample_1" role="grid">
				<thead>
					<tr role="row">
						<th style="width: 240px;" class="sorting_desc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-sort="descending" aria-label=" Date &amp;amp; Time : activate to sort column ascending"> Date &amp; Time </th>
						<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Test ID : activate to sort column ascending" style="width: 245px;"> Test ID </th>
						<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Subject : activate to sort column ascending" style="width: 260px;"> Subject </th>
						<th style="width: 50px;" class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" No. Q's : activate to sort column ascending"> No. Q's </th>
						<th style="width: 102px;" class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Question Mode : activate to sort column ascending"> Question Mode </th>
						<th style="text-align: center; width: 332px;" class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Your Score: activate to sort column ascending"> Your Score</th>
						<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 238px;">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr class="odd">
						<td valign="top" colspan="7" class="dataTables_empty">No data available in table</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-5 col-sm-5"></div>
			<div class="col-md-7 col-sm-7"></div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
   @endsection