<div class="sec-topicTst sec-pca" id="sec-topicTst">
		<div class="vl-inner">
			<div class="vl-row tpall tp1">
				<h3>{{$subject->sb_name}}</h3>
				<div class="vl-row-inner">
					@foreach($data as $chap)
					<div class="vl-col ">
						<a class="sproutvideo-lightbox" data-height="360" data-width="640" href="{{route('user.question',$chap->id)}}">
						<div class="vl-videocard">
							<div class="overlay">
								{{-- <span><img src="{{asset('images/info.svg')}}" ></span> --}}
							</div>
							<div class="img-videocard">
								<div class="top">
									<img src="{{asset('images/logo.png')}}" height="30" width="40">
									<p>{{$subject->sb_name}}</p>
								</div>
								<div class="bottom">
									<p>{{$chap->name}}</p>
								</div>
							</div>
						</div></a>
					</div>
					@endforeach
				
					
				</div>
			</div>
			
		</div><!--./ vl-inner -->
	</div>