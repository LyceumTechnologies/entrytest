@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')
<div id="main-sec">
<section id="mainWrap-sec" class="mainWrap-sec col-md-12">
<div id="flashnotes" class="modal fade in" style="display: block;">
	<div class="modal-dialog">
		<div class="modal-content rounded">
			<!-- Modal Header -->
			<div class="modal-header">  
		 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-lg-12 text-center">
						<figure>
							<img class="img-fluid" src="https://app.topgrade.pk/assets/images/xnotes.png.pagespeed.ic.G8KsfCAJM8.webp" data-pagespeed-url-hash="98223009" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
							</figure>
							<h3 class="mt-0">
								<b id="thanks_title">Make a Note</b>
							</h3>
							<div class="form-group">
								<textarea class="form-control shadow" placeholder="Note down any thing that you want to recall later. You will be able to find it in My Notes" name="flashnotes_text" id="flashnotes_text" rows="7"></textarea>
							</div>
							<a class="bg-blue text-white d-block rounded" onclick="">Save Notes</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	   @endsection