<!DOCTYPE html>
<?php
/**
* Project: Entrytest4u.
* User: shafqat bhatti
* Date: 12/03/2019
* Time: 1:44 PM
*/
?>

@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')

<div id="main-sec">
	<section id="mainWrap-sec" class="mainWrap-sec"><!-- body -->
		@foreach($data as $book)
		<div class="sec-topicTst sec-pca" id="sec-topicTst">
			<div class="vl-inner">
				<div class="vl-row tpall tp1">
					<button class="btn-dark"><h2>{{$book->sb_name}}</h2></button>
					<div class="vl-row-inner">
						@foreach($book->filterChapters as $chap)
						<div class="vl-col ">
							<a class="sproutvideo-lightbox" data-height="360" data-width="640" href="{{route('user.question',$chap->id)}}">
								<div class="vl-videocard">
									<div class="overlay">
										{{-- <span><img src="{{asset('images/info.svg')}}" ></span> --}}
									</div>
									<div class="img-videocard">
										<div class="top">
											<img src="{{asset('images/logo.png')}}" height="30" width="40">
											<p>{{$book->sb_name}}</p>
										</div>
										<div class="bottom">
											<p>{{$chap->name}}</p>
										</div>
									</div>
								</div></a>
							</div>
							@endforeach
						</div>
					</div>
				</div><!--./ vl-inner -->
			</div>
			@endforeach
		</section><!-- /. body -->
	</div>
	<!--Footer page (includes closing body and html tag). Add common JS code in the footer subjectTopic -->


	@endsection
	@push('post-scripts')
	<script type="text/javascript">
		$('.getSubject').on('click',function(e){
			var obj=$(this).attr('id');
			$('.allBookContent').css('display','none');
			console.log($('.'+obj).css("display", "block"));
			$('.'+obj).show()
		});
		function subjectTopic(obj){
			var id=$(obj).attr('data-ids');
			$.ajax({
				url: "{{route('subjectTopic')}}",
				type: 'post',
				data: {
					'id': id
				},
				success: function (response) {
					$('.allBookContent').css("display",'none');
					$("#topics-show").html(response);

				},
				error: function (e) {
					console.log('error', e);
				}
			});
		}
	</script>
	@endpush