<!DOCTYPE html>
<?php
/**
* Project: Entrytest4u.
* User: shafqat bhatti
* Date: 12/03/2019
* Time: 1:44 PM
*/
?>

@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')

<div id="main-sec">
	<section id="mainWrap-sec" class="mainWrap-sec"><!-- body -->
		@if(isset($ques) && count($ques))
		@php($questionIndex=1)
		@foreach($ques as $q)
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 questioncontent questionCount{{$questionIndex}}" data-question-id="{{$q->id}}" data-counter="{{$questionIndex}}" style="@if ($loop->first) display: block; @else display:none; @endif">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pl-0">
				<p><b data-step="3" data-intro="You may note down Test ID for future reference if needed" data-position="right" data-scrollto="tooltip">Test ID : @isset($testId){{$testId}}@endisset</b></p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center" data-step="4" data-intro="Answer question given below according to this command" data-position="right" data-scrollto="tooltip">
				<p>
					<b>
                       Choose the single best answer                                
                   </b>
               </p>
           </div>

           <div class="clearfix"></div>
           <div class="right_side_bar questionAnswerTest" style="background-image: url(http://localhost:8000/images/PrepOn_icon.png); baseline-shift:all;">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-right">
               <input type="hidden" value="433241" name="test_question_link_id" id="test_question_link_id">
               <h3> <b class="question-label">1 </b><b> of  {{count($ques)}}</b> <small>(QID: {{$q->id}})</small> 
                  <span class="label label-info" style="display:none">Past Paper Question</span>
                  <i class="fa fa-bookmark-o fs-32" id="marked" style="display:none" aria-hidden="true"></i></h3>
                  <div class="peragraph">
                      <p><span style="font-size:16px"><strong>{!! $q->question !!} </strong></span></p>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 tst-scrn-qustn-grp">
               <div class="radio_btn answers">
                  @php($ansId=0)
                  @foreach($q->answers as $ans)
                  <div class="form-check answer disabledAnswer"  onclick="mtq_button_click(this,@if($ans->is_correct==1) 1 @else 0 @endif)" 
                    data-ans-id="{{$ans->id}}" >
                    <label>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 d-flex align-items-center">
                           @if($ans->is_correct==1)
                           {{-- <div class="mtq_marker mtq_correct_marker @if($ans->is_correct==1) answerRemove @endif" id="mtq_marker-1-2-1" style="display: none;"></div> --}}
                           <i class="fa fa-check green mtq_ans mts_correct_ans mtq_correct_marker @if($ans->is_correct==1) mts_correct_ans @endif" style="display: none"></i>
                           @else
                           {{-- <div /class="mtq_marker mtq_wrong_marker" id="mtq_marker-1-1-1" style="display: none;"></div> --}}
                           <i class="fa fa-times mtq_ans red " style="display: none"></i>
                           @endif

                       </div>
                       <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                           <input type="radio" name="answer_val" id="ans{{$ans->id}}" value="{{$ans->id}}"> 
                           <span class="label-text " id="ans{{$ans->id}}">{{(toNumAlpha($ansId))}}.</span>
                       </div>
                       <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 p-0">
                           <small style="font-weight:normal;"> {!! $ans->ans !!} </small>
                       </div>
                       <div class="clearfix"></div>
                   </label>
               </div>
               @php($ansId++)
               @endforeach

           </div>
       </div>
       <div class="clearfix"></div>
       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
				{{-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center submit-view-detail mt-30 mb-10">
					<a title="Correct Answer" data-step="7" data-intro="View relevant details of this question" data-position="left" data-scrollto="tooltip" class="white bg-blue rounded p-10 d-block m-auto" onclick="submit_answer('https://app.topgrade.pk/', true)" id="submit" style="cursor: pointer"> View Details <i class="fa fa-check-circle"></i></a>
					<a onclick="ReportIssueModal()" class="text-black d-block" style="cursor: pointer;margin: 20px auto;font-size: 16px;text-decoration: underline;"> Report an issue </a>
				</div> --}}
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
				<div class="clearfix"></div>

				<!--modal-->
				<div class="modal fade" id="report_issue_model">
					<div class="modal-dialog">
						<div class="modal-content">

							<!-- Modal Header -->
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
							</div>

							<!-- Modal body -->
							<div class="modal-body">
								<div class="row clearfix">
									<div class="col-lg-12 text-center">
										<figure>
											{{-- <img class="img-fluid" src="#" data-pagespeed-url-hash="3982054518"> --}}
										</figure>
										<h3 class="mt-0"><b>Report an Issue</b></h3>
										<p>Select one of the issues to let us know</p>
										<hr>
										<p class="text-left">
											<input type="radio" id="featured-1" name="issue" value=" Question Seems Invalid" checked="">
											<label class="w-100" for="featured-1"> Question Seems Invalid</label>
											<input type="radio" id="featured-2" name="issue" value="Given Answer/Explanation Seems Wrong">
											<label class="w-100" for="featured-2">Given Answer/Explanation Seems Wrong</label>
											<input type="radio" id="featured-3" name="issue" value="Request an Explanation">
											<label class="w-100" for="featured-3">Request an Explanation</label>
											<input type="radio" id="featured-4" name="issue" value="0">
											<label class="w-100" for="featured-4">Other</label>
										</p><div class="form-group">
											<textarea class="form-control" style="display:none" id="issue_msg" name="issue_msg" rows="3"></textarea>
										</div>
										<p></p>
										<hr>
										<a onclick="SendIssue('#')" class="bg-blue text-white d-block rounded">Send Issue</a>
									</div>
								</div>
							</div>

							<!-- Modal footer -->
							<div class="modal-footer">
                                            <!--                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            	<button type="button" class="btn btn-primary" onclick="ReferFrnd()">Proceed</button>-->
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="report_issue_model-dumy">
                                	<div class="modal-dialog">
                                		<div class="modal-content">

                                			<!-- Modal Header -->
                                			<div class="modal-header">
                                				<button type="button" class="close" data-dismiss="modal">×</button>
                                			</div>

                                			<!-- Modal body -->
                                			<div class="modal-body">
                                				<div class="row clearfix">
                                					<div class="col-lg-12 text-center">
                                						<figure>
                                							{{-- <img class="img-fluid" src="https://app.topgrade.pk/assets/images/xfresh-repeater.jpg.pagespeed.ic.9MOvgXW4Ib.webp" data-pagespeed-url-hash="534631301" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"> --}}
                                						</figure>
                                						<h3 class="mt-0"><b>Welcome to Topgrade.pk Family</b></h3>
                                						<p>Select one of the Following</p>
                                						<hr>
                                						<p class="text-left">
                                							<input type="radio" id="featured-11" name="issue" value=" Question Seems Invalid" checked="">
                                							<label class="w-100" for="featured-11"> Fresh</label>
                                							<input type="radio" id="featured-12" name="issue" value="Given Answer/Explanation Seems Wrong">
                                							<label class="w-100" for="featured-12">Repeater</label>
                                							<input type="radio" id="featured-13" name="issue" value="Request an Explanation">

                                							<label class="w-100" for="featured-13">Ist Year</label>
                                							<input type="radio" id="featured-14" name="issue" value="0">
                                							<label class="w-100" for="featured-14">2nd Year</label>
                                						</p><div class="form-group">
                                							<textarea class="form-control" style="display:none" id="issue_msg" name="issue_msg" rows="3"></textarea>
                                						</div>
                                						<p></p>
                                						<p class="text-left">Fsc Obtained Marks:<strong>800</strong></p>
                                						<p class="text-left">Fsc Total Marks: <strong>1100</strong></p>

                                						<p class="text-left">Ist Year Obtained Marks:<strong>364</strong></p>
                                						<p class="text-left">Ist Year Total Marks:<strong>560</strong></p>

                                						<p class="text-left">2nd Year Obtained Marks:<strong>436</strong></p>
                                						<p class="text-left">2nd Year Total Marks:<strong>540</strong></p>
                                						<hr>
                                						<a onclick="SendIssue('https://app.topgrade.pk/')" class="bg-blue text-white d-block rounded">Send Issue</a>
                                					</div>
                                				</div>
                                			</div>

                                			<!-- Modal footer -->
                                			<div class="modal-footer">
                                            <!--                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            	<button type="button" class="btn btn-primary" onclick="ReferFrnd()">Proceed</button>-->
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!--end fresh/repeater modal-->
                                <div class="modal fade" id="report_issue_model_thanks">
                                	<div class="modal-dialog">
                                		<div class="modal-content">
                                			<!-- Modal Header -->
                                			<div class="modal-header">
                                				<button type="button" class="close" data-dismiss="modal">×</button>
                                			</div>
                                			<!-- Modal body -->
                                			<div class="modal-body">
                                				<div class="row clearfix">
                                					<div class="col-lg-12 text-center">
                                						<figure>
                                							{{-- <img class="img-fluid" src="https://app.topgrade.pk/assets/images/xthanks.png.pagespeed.ic.OkkRPm69HW.webp" data-pagespeed-url-hash="2591262323" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"> --}}
                                						</figure>
                                						<!--<h3 class="mt-0"><b>Report an Issue</b></h3>-->
                                						<p>We will review your issue and get back to you as soon as possible.</p>

                                						<!--<hr></hr>-->
                                						<!--<a  class="bg-blue text-white d-block rounded" data-dismiss="modal" >Ok</a>-->
                                					</div>
                                				</div>
                                			</div>
                                			<!-- Modal footer -->
                                			<div class="modal-footer">
                                            <!--                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            	<button type="button" class="btn btn-primary" onclick="ReferFrnd()">Proceed</button>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="empty-div">
                                	&nbsp;
                                </div>
                            </div>
                            <!--tabs-->
                            <!--new tabs-->
                            <div class="portlet box blue" id="desktop">
    <!--    <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Left Tabs </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            </div>
        </div>-->
        <div class="portlet-body">
        	<div class="row">
        		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        			<ul class="nav nav-tabs tabs-left">
        				<li class="active">
        					<a data-toggle="tab" href="#home"><i class="fa fa-caret-right black" aria-hidden="true"></i><i class="d-none fa fa-caret-down black" aria-hidden="true"></i>&nbsp; Text Explanation</a>
        				</li>

        				<li>
        					<a data-toggle="tab" href="#menu6"><i class="fa fa-caret-right black" aria-hidden="true"></i><i class="d-none fa fa-caret-down black" aria-hidden="true"></i>&nbsp; Other Options </a>
        				</li>
        				<li>
        					<a data-toggle="tab" href="#menu8"><i class="fa fa-caret-right black" aria-hidden="true"></i><i class="d-none fa fa-caret-down black" aria-hidden="true"></i>&nbsp; Question Details </a>
        				</li>

        			</ul>
        		</div>
        		<input type="hidden" name="_q_r" id="_q_r" value="46733">
        		<input type="hidden" name="_fill_v" id="_fill_v" value="0">
        		<input type="hidden" name="_fill_r" id="_fill_r" value="0">
        		<input type="hidden" name="_q_id" id="_q_id" value="43047">
        		<input type="hidden" name="_sub_s_id" id="_sub_s_id" value="335">
        		<input type="hidden" name="_t_id" id="_t_id" value="14089">
        		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        			<div class="tab-content py-20">

        				<div id="home" class="tab-pane fade in active">
        					<p style="text-align:justify"><span style="font-size:16px">The branch of science concerned with the substances of which matter is composed, the investigation of their properties and reactions, and the use of such reactions to form new substances.</span></p>
        				</div>


        				<div id="menu6" class="tab-pane fade ">
        					<div class="col-lg-12 mb-10"><strong style="font-size: 16px;line-height: 22px;">OPENS IN NEW TAB</strong></div>
        					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 text-center">
        						<a class="white bg-blue rounded p-10 d-block text-center text-decoration cursor-pointer" onclick="LoadVideos('https://app.topgrade.pk/', '335', '110', 'chemistry', 'Chemistry')">Watch  Videos</a>
        					</div>
        					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 text-center">
        						<a class="white bg-blue rounded p-10 d-block text-center text-decoration cursor-pointer" onclick="LoadSlides('https://app.topgrade.pk/', '335', '789', 'chemistry', 'Chemistry')">View Slides</a>

        					</div>

        				</div>
        				<div id="menu8" class="tab-pane fade ">
        					<div class="blue-bg">
        						<ul>
        							<li>
        								<strong>Your Result:</strong>&nbsp;
        								<span id="user_answer_d">Unanswered</span>
        							</li>

        							<li>
        								<strong>Subject:</strong>&nbsp;
        								<span>Chemistry</span>
        							</li>
        							<li>
        								<strong>Section:</strong>&nbsp;
        								<span>Stoichiometry</span>
        							</li>

        							<li>
        								<strong>Sub Section:</strong>&nbsp;
        								<span>Atom, Molecules &amp; Ions</span>
        							</li>
        							<li>
        								<strong>Your Pace:</strong>&nbsp;
        								<span id="user_pace_d">0 sec</span>
        							</li>
        							<li>
        								<strong>Recommended Pace:</strong>&nbsp;
        								<span>40 sec</span>
        							</li>
        							<li>
        								<strong>Difficulty:</strong>&nbsp;
        								<span>Low</span>
        							</li>
        						</ul>
        					</div>
        				</div>


        			</div>
        		</div>
        	</div>
        </div>
    </div>
    <!--end new tabs-->


    

    <div class="empty-div">

    	&nbsp;
    </div>
    
    {{-- ........................ here question detail  .............. --}}
    {{-- <div class="portlet-body" style="display: block;"> --}}
    {{-- 	<div class="row">
    		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    			<ul class="nav nav-tabs tabs-left">
    				<li class="active">
    					<a data-toggle="tab" href="#home"><i class="fa fa-caret-right black" aria-hidden="true"></i><i class="d-none fa fa-caret-down black" aria-hidden="true"></i>&nbsp; Text Explanation</a>
    				</li>
    				
    				<li>
    					<a data-toggle="tab" href="#menu6"><i class="fa fa-caret-right black" aria-hidden="true"></i><i class="d-none fa fa-caret-down black" aria-hidden="true"></i>&nbsp; Other Options </a>
    				</li>
    				<li>
    					<a data-toggle="tab" href="#menu8"><i class="fa fa-caret-right black" aria-hidden="true"></i><i class="d-none fa fa-caret-down black" aria-hidden="true"></i>&nbsp; Question Details </a>
    				</li>

    			</ul>
    		</div>
    		<input type="hidden" name="_q_r" id="_q_r" value="2690">
    		<input type="hidden" name="_fill_v" id="_fill_v" value="0">
    		<input type="hidden" name="_fill_r" id="_fill_r" value="0">
    		<input type="hidden" name="_q_id" id="_q_id" value="2677">
    		<input type="hidden" name="_sub_s_id" id="_sub_s_id" value="272">
    		<input type="hidden" name="_t_id" id="_t_id" value="14099">
    		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
    			<div class="tab-content py-20">

    				<div id="home" class="tab-pane fade in active">
    					<p style="text-align:justify"><span style="font-size:16px">Most ganglion fibres of the sympathetic system arise from a middle portion of the spinal cord and almost terminate in ganglia that lie near the cord. This system is important during emergency situations and is associated with “fight and flight”. This system accelerates the heartbeat, dilates the pupil and inhibits the digestion of food.</span></p>

    					<p style="text-align:justify"><span style="font-size:16px"> --}}
                         {{--    <img alt="" src="https://www.topgrade.pk/manager/assets/images/userfiles/image/1.Subjects/biology/peripheral-nervous-system/para%20nd%20sys.png" style="height:464px; width:469px" data-pagespeed-url-hash="1703811487" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                        </span></p>
    				</div>
    				

    				<div id="menu6" class="tab-pane fade ">
    					<div class="col-lg-12 mb-10"><strong style="font-size: 16px;line-height: 22px;">OPENS IN NEW TAB</strong></div>
    					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 text-center">
    						<a class="white bg-blue rounded p-10 d-block text-center text-decoration cursor-pointer" onclick="LoadVideos('https://app.topgrade.pk/', '272', '619', 'biology', 'Biology')">Watch  Videos</a>
    					</div>
    					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 text-center">
    						<a class="white bg-blue rounded p-10 d-block text-center text-decoration cursor-pointer" onclick="LoadSlides('https://app.topgrade.pk/', '272', '382', 'biology', 'Biology')">View Slides</a>

    					</div>
    					
    				</div>
    				<div id="menu8" class="tab-pane fade ">
    					<div class="blue-bg">
    						<ul>
    							<li>
    								<strong>Your Result:</strong>&nbsp;
    								<span id="user_answer_d" class="userpace_red">Incorrect</span>
    							</li>

    							<li>
    								<strong>Subject:</strong>&nbsp;
    								<span>Biology</span>
    							</li>
    							<li>
    								<strong>Section:</strong>&nbsp;
    								<span>Nervous Coordination</span>
    							</li>

    							<li>
    								<strong>Sub Section:</strong>&nbsp;
    								<span>Peripheral Nervous System</span>
    							</li>
    							<li>
    								<strong>Your Pace:</strong>&nbsp;
    								<span id="user_pace_d">7109 sec</span>
    							</li>
    							<li>
    								<strong>Recommended Pace:</strong>&nbsp;
    								<span>40 sec</span>
    							</li>
    							<li>
    								<strong>Difficulty:</strong>&nbsp;
    								<span>Low</span>
    							</li>
    						</ul>
    					</div>
    				</div>


    			</div>
    		</div>
    	</div>
    </div>
    {{-- ................................ ehere question detail end /////////////////////////////////////////--}}



    <div class="clearfix"></div>

    
    <div class="yellow-bg fixed-bottom bg-gray">
    	<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
    		<a title="Previous" class="white " onclick="previosQuestion(this)" data-step="8" data-intro="Go to previous question" id="prev-question" data-position="left previous"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Prev</a>
    	</div>
    	<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
    		<a title="Book Mark" id="mark_question" data-step="9" style="cursor: pointer;display: " onclick="book_mark()" class="dark">Bookmark <i class="fa fa-pencil"></i></a>
    		<a title="Remove Bookmark" id="unmark_question" style="cursor: pointer; display: none" onclick="book_mark()" class="dark"> Unbookmark <i class="fa fa-pencil"></i></a>
    	</div>


    	<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
    		<a title="Test Overview" data-step="10" data-intro="Have an overview of entire test" onclick="performanceResult(@isset($testId){{$testId}}@endisset)" data-position="left" data-scrollto="tooltip" class="white" onclick="TestOverView(this)">Test Overview <i class="fa fa-book" aria-hidden="true"></i></a>
    	</div>

    	<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
    		<a title="Finish Test" data-step="11" data-intro="Finish test and move to test result" data-position="left" data-scrollto="tooltip" style="cursor: pointer" onclick="performanceResult(@isset($testId){{$testId}}@endisset)" class="bg-red rounded white">Finish <i class="fa fa-flag-o" aria-hidden="true"></i> </a>

    	</div>
    	<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
    		<a title="Next" data-step="12" data-intro="Go to next question" data-position="left" data-scrollto="tooltip" style="cursor: pointer" onclick="nextQuestion(this);"  class="white curved next-question next" >Next <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> </a>

    	</div>
    	<div class="clearfix"></div>
    </div>

</div>
@php($questionIndex++)
@endforeach
@else


@endif
<div class="recordNotFound" style="display: none">
    <h2>Record not found</h2>
</div>
{{-- ////////////////////// ????????????????????????????????????????????????????????????????????? --}}
<div class="row " id="performanceContent" style="display: block" >

</div>
</div>

{{-- ////////////////////////////// ?????????????????????????????????????????????????????????????????? --}}
</section>
</div>
@endsection

@push('post-scripts')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
<script type="text/javascript">
	var index=1;
	var totalRightAns=0;
	var totalAttempt=0;
    var totalQuestion={{count($ques)}};

    ///////////// Attempted Question /////////////////////
    var userCorrectionAns=0;
    var userWrongAns=0;
    var userUnanswer=0;

    function book_mark(){
        console.log('bookmarked');
        var question_id=$('.questionCount'+index).attr('data-question-id');
        var test_id=@isset($testId){{$testId}}@endisset ;
        $.ajax({
          method:"POST",
          url:"{{route('BookMark')}}",
          data : {question_id:question_id,test_id:test_id},
          dataType:"json",
          success:function(data){
            console.log('book marked inserted',data.message);
            if(data.status){
                var priority = 'success';
                var title = 'Success';
                var message = data.message;

                toastr.success(message, {timeOut: 5000})
            }else{
                var priority = 'error';
                var title = 'Warning';
                var message = data.message;

                toastr.error(message,{timeOut: 5000});
            }


        }
    });
    }
    function performanceGraph(){


        console.log(userCorrectionAns,'userCorrectionAns');
        console.log(userWrongAns,'userWrongAns');
        console.log(userUnanswer,'userUnanswer');
        console.log(totalQuestion,'totalQuestion');
        $('.correctionPercentage').html(``);
        $('.inCorrectPercentage').html(``);
        $('.unAnswerPercentage').html(``);
        $('.score').html(``);
        console.log('Session user correction ans count',userCorrectionAns);
        ///////////////////////////////// now update result////////////
        $('.correctionPercentage').html(`Correct (${(userCorrectionAns/totalQuestion)*100}%)`);
        $('.inCorrectPercentage').html(`Incorrect (${(userWrongAns/totalQuestion)*100}%)`);
        $('.unAnswerPercentage').html(`Unanswered (${(userUnanswer/totalQuestion)*100}%)`);
        $('.score').html(`${userUnanswer} out of ${totalQuestion}`);

        $('.questionCount').html(`${totalQuestion}`);
        $('.answerCount').html(`${userWrongAns+userCorrectionAns}`);
        $('.answerCountPer').html(`${(userWrongAns+userCorrectionAns/totalQuestion)*100}%`);
        $('.correctCount').html(`${userCorrectionAns}`);
        $('.correctCountPer').html(`${(userCorrectionAns/totalQuestion)*100}%`);
        $('.inCorrectCount').html(`${userWrongAns}`);
        $('.inCorrectCountPer').html(`${(userWrongAns/totalQuestion)*100}%`);
        $('.unAnswerCount').html(`${userUnanswer}`);
        $('.unAnswerCountPer').html(`${(userUnanswer/totalQuestion)*100}%`);
    }
    function questionInserted(question_id,test_id,user_ans,is_correct,obj){
        console.log('parent html',obj);
        $.ajax({
          method:"POST",
          url:"{{route('testQuestion')}}",
          data : {question_id:question_id,test_id:test_id,user_ans:user_ans,is_correct:is_correct},
          dataType:"json",
          success:function(data){
            console.log('data inserted',data);
            console.log(data.userCorrectionAns,'response');
            userCorrectionAns=data.userCorrectionAns;
            userWrongAns=data.userWrongAns;
            userUnanswer=data.userUnanswer;
            console.log('userCorrectionAns response',userCorrectionAns);
            console.log('userCorrectionAns response',userWrongAns);
            console.log('userCorrectionAns response',userUnanswer);
            performanceGraph();

        }
    });
        performanceGraph();
    }
    $(function(){
        performanceGraph();
    });
    function mtq_button_click(obj,ans){
      console.log('question',obj);
      var ids=$(obj).attr('id');
      $(obj).find('.mtq_ans').first().show();
      $(obj).parent('.answers').find('.mts_correct_ans').first().show();

      $(obj).parent().parent().parent('.questionAnswerTest').find('.disabledAnswer').css('pointer-events','none');


      var question_id=$('.questionCount'+index).attr('data-question-id');
      var test_id=@isset($testId){{$testId}}@endisset ;
      var user_ans=$(obj).attr('data-ans-id');
      var is_correct=ans;

      questionInserted(question_id,test_id,user_ans,ans,obj=null);
      if(ans){
         totalRightAns++;
     }
     totalAttempt++;
     $('.next-question').attr("data-answer-checked", "1");
     console.log('answer',totalRightAns);

 }
 function previosQuestion(obj){
  console.log('previous');
  if(index<=1){
     return false;
 }
 $('.questionCount'+index).css('display','none');
 index--;
 $('.question-label').html('');
 $('.question-label').text(`${index}`);
 $('.questionCount'+index).css('display','block');
 var counter=$('.questionCount'+index).attr('data-counter');
 index=parseInt(counter);
 console.log('counter',index);
 preButton();
}



function performanceResult(obj){
    // console.log('result button pres',obj);return false;

    $.ajax({
      method:"POST",
      url:"{{route('testResult')}}",
      data : {test_id:obj},
      dataType:"json",
      success:function(response){
        console.log('result response',response.data);
        $('#performanceContent').html(response.data);
        // performanceGraph();

    }
});
// return false;

  
    // performanceGraph();
    $('.questioncontent').css('display','none');
    $('#performanceContent').css('display','block');
}

function nextQuestion(obj){
  console.log('next button click');
  console.log('index',index);
  var counter=$('.questionCount'+index).attr('data-counter');
  $('.questionCount'+index).css('display','none');
  $('.question-label').html('');
  index=parseInt(counter);
  console.log('counter',index);
  var question_id=$('.questionCount'+index).attr('data-question-id');
  var test_id=@isset($testId){{$testId}}@endisset ;
  var user_ans=null;
  var is_correct=null;
  var ans=null;
  var unAnaswer=$('.next-question').attr("data-answer-checked");

  if(!unAnaswer){
    console.log('when answer skip');
    questionInserted(question_id,test_id,user_ans,ans);
}

index++;
console.log('next count',index,'total count',totalQuestion);
if(index==totalQuestion){
    console.log('inner');
    $('.next-question').css('pointer-events','none');
    $('.next-question').addClass('dull');
}
$('.question-label').text(`${index}`);
var question_id=$('.questionCount'+index).attr('data-question-id');
if(question_id){
    $('.questionCount'+index).css('display','block');
}else{
    $('.recordNotFound').css('display','none');
}
$('.next-question').removeAttr("data-answer-checked");

preButton();
}
$(function(){
  preButton();
});
function preButton(){
  if(index>1){
     $('#prev-question').removeClass('dull');
 }else{
     $('#prev-question').addClass('dull');
 }
}


</script>
@endpush 