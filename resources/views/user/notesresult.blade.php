@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')
<div class="row">
	<div class="col-md-12 tst-rslt">
		<div class="checkbox_sec">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption ">
						<i class=" icon-layers font-green"></i>
						<span class="caption-subject font-green bold uppercase" data-step="1" data-intro="View stats of your test importance here" data-position="right" data-scrollto="tooltip">
                                                Your Test Performance</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
						<div id="chart_6_analysis" class="chart" style="overflow: hidden; text-align: left;">
							<div class="amcharts-main-div" style="position: relative;">
								<div class="amcharts-chart-div" style="overflow: hidden; position: relative; text-align: left; width: 414px; height: 270px; padding: 0px;">
									<svg version="1.1" style="position: absolute; width: 414px; height: 270px; top: 0px; left: 0px;">
										<desc>JavaScript chart by amCharts 3.17.1</desc>
										<g>
											<path cs="100,100" d="M0.5,0.5 L413.5,0.5 L413.5,269.5 L0.5,269.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0"></path>
										</g>
										<g></g>
										<g></g>
										<g></g>
										<g></g>
										<g></g>
										<g>
											<g opacity="1">
												<path cs="1000,1000" d=" M207,135 L207,55 A80,80,0,1,1,206.916224211216,55.00004386490444 L207,135 Z" fill="#689bf6" stroke="#FFFFFF" stroke-width="2" stroke-opacity="1" fill-opacity="1"></path>
											</g>
										</g>
										<g></g>
										<g></g>
										<g></g>
										<g></g>
										<g></g>
										<g></g>
										<g>
											<g></g>
										</g>
										<g></g>
										<g></g>
										<g></g>
										<g></g>
									</svg>
									<a href="http://www.amcharts.com/javascript-charts/" title="JavaScript charts" style="position: absolute; text-decoration: none; color: rgb(136, 136, 136); font-family: &quot;Open Sans&quot;; font-size: 11px; opacity: 0.7; display: block; left: 5px; top: 5px;"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
						<div class="form-group text-center d-flex justify-content-center align-items-center h-270">
							<div class="form-group text-center" style="margin-top:20px;margin-bottom: 30px;">
								<div class="mt-checkbox-inline">
									<div class="text-left mb-20">
										<div class="box-green"></div>
										<span>Correct (0%)</span>
									</div>
									<div class="text-left mb-20">
										<div class="box-red"></div>
										<span>Incorrect (0%)</span>
									</div>
									<div class="text-left mb-20">
										<div class="box-blue"></div>
										<span>Unanswered (100%)</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center p-0">
						<div class="gift">
							<p>
                                                    Your Score
                                                </p>
							<figure class="p-20">
								<img id="img" src="">
								</figure>
								<div class="gift-detail">
									<div class="col-lg-12">
										<small>0 out of 220</small>
										<small class="bg-red text-white rounded">0%</small>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="stats">
								<p class="text-center">
                                                    Stats
                                                </p>
								<div class="p-10-15 border-gray-bottom mb-0">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                                                        Total Questions:
                                                    </div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
										<small class="bg-gray rounded font-weight-bold border-gray" style="padding: 5px 10px;">220</small>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="p-10-15 border-gray-bottom mb-0">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                                                        Answered:
                                                    </div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
										<small class="bg-gray rounded font-weight-bold border-gray" style="padding: 5px 10px;">0</small>
										<span class="bg-purple rounded font-weight-bold text-white" style="padding:5px 5px;">0%</span>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="p-10-15 border-gray-bottom mb-0">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                                                        Correct:
                                                    </div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
										<small class="bg-gray rounded font-weight-bold border-gray" style="padding: 5px 10px;">0</small>
										<span class="bg-green rounded font-weight-bold text-white" style="padding:5px 5px;">0%</span>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="p-10-15 border-gray-bottom mb-0 p-0">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        Incorrect:
                                                    </div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
										<small class="bg-gray rounded font-weight-bold border-gray" style="padding: 5px 10px;">0</small>
										<span class="bg-red rounded font-weight-bold text-white" style="padding:5px 5px;">0%</span>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="p-10-15 border-gray-bottom mb-0">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                                                        Unanswered:
                                                    </div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
										<small class="bg-gray rounded font-weight-bold border-gray" style="padding: 5px 10px;">220</small>
										<span class="bg-blue rounded font-weight-bold text-white" style="padding:5px 5px;">100%</span>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
	</div>
	<div class="row row-features test-result" style="padding-top:0;">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" icon-layers font-green"></i>
					<span class="caption-subject font-green bold uppercase">Choose Question Type</span>
				</div>
			</div>
			<div class="portlet-title">
				<div class="form-group form-md-radios">
					<div class="md-radio-list">
						<div class="checkbox_sec" style="margin:0;">
							<!--new-->
							<ul class="filter text-right center-xs top-margin40 ques_filter" data-step="2" data-intro="Select a specific group of questions to work on" data-position="right" data-scrollto="tooltip">
								<li>
									<div id="t1">
										<div class="md-radio">
											<input type="radio" id="radio1" name="radio" value="" checked="" onclick="practice_response_filter(this, 'https://app.topgrade.pk/', '548566939')" class="md-radiobtn">
												<label for="radio1">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>All
                                                                
												</label>
											</div>
										</div>
									</li>
									<li>
										<div id="t1">
											<div class="md-radio">
												<input type="radio" id="radio4" name="radio" value="2" onclick="practice_response_filter(this, 'https://app.topgrade.pk/', '548566939')" class="md-radiobtn">
													<label for="radio4">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>Unanswered
                                                                
													</label>
												</div>
											</div>
										</li>
										<li>
											<div id="t1">
												<div class="md-radio">
													<input type="radio" id="radio3" name="radio" value="1" onclick="practice_response_filter(this, 'https://app.topgrade.pk/', '548566939')" class="md-radiobtn">
														<label for="radio3">
															<span></span>
															<span class="check"></span>
															<span class="box"></span>Incorrect
                                                                
														</label>
													</div>
												</div>
											</li>
											<li>
												<div id="t1">
													<div class="md-radio">
														<input type="radio" id="radio2" name="radio" value="0" onclick="practice_response_filter(this, 'https://app.topgrade.pk/', '548566939')" class="md-radiobtn">
															<label for="radio2">
																<span></span>
																<span class="check"></span>
																<span class="box"></span>Correct
                                                                
															</label>
														</div>
													</div>
												</li>
												<li>
													<div id="t1">
														<div class="md-radio">
															<input type="radio" id="radio5" name="radio" value="3" onclick="practice_response_filter(this, 'https://app.topgrade.pk/', '548566939')" class="md-radiobtn">
																<label for="radio5">
																	<span></span>
																	<span class="check"></span>
																	<span class="box"></span>Bookmarked
                                                                
																</label>
															</div>
														</div>
													</li>
												</ul>
												<!--end new-->
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="portlet-body">
									<div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
										<div class="row">
											<div class="col-md-6 col-sm-6"></div>
											<div class="col-md-6 col-sm-6"></div>
										</div>
										<div class="table-scrollable">
											<table class="table table-striped table-bordered table-hover table-checkable order-column test-result-table dataTable no-footer" id="sample_1" role="grid">
												<thead>
													<tr role="row">
														<th style="text-align:left;padding:8px;width: 50px;" class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Sr.#: activate to sort column descending">Sr.#</th>
														<th style="text-align:left;width: 120px;" class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="QID: activate to sort column ascending">QID</th>
														<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Subject: activate to sort column ascending" style="width: 51px;">Subject</th>
														<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Section: activate to sort column ascending" style="width: 75px;">Section</th>
														<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Sub-Section: activate to sort column ascending" style="width: 209px;">Sub-Section</th>
														<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Statement: activate to sort column ascending" style="width: 611px;">Statement</th>
														<th style="text-align:center;width: 50px;" class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th>
														<th style="width:100px;" class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Your Time: activate to sort column ascending">Your Time</th>
														<th style="width:100px;" class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
													</tr>
												</thead>
												<tbody>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  1</td>
														<td style="text-align:left !important">
															<umair>943576756</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('943576756', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Modern Physics</td>
														<td style="text-align:left !important">Electrical Properties of Solids</td>
														<td style="text-align:left !important" title="The superconductive material will have:

(i) Zero resistance 

(ii) Maximum efficiency 

(iii) No loss of power
">  The superconductive material will have:

(i) Zero resistance 

(ii) Maximum efficiency 

(iii) No loss of power
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/1 " data-step="3" data-intro="View details of respective question here" data-position="right" data-scrollto="tooltip">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  2</td>
														<td style="text-align:left !important">
															<umair>747546846</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('747546846', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Modern Physics</td>
														<td style="text-align:left !important">X Rays</td>
														<td style="text-align:left !important" title="\[\text{If }~\text{&nbsp; }~\text{&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }\lambda {{\text{ }}_{\text{L}}}\text{&nbsp; and }~\text{&nbsp; }~\text{&nbsp;&nbsp; }\lambda {{\text{ }}_{\text{k}}}\]are the wavelengths are L and K series X-ray lines responding then
">  \[\text{If }~\text{ }~\text{ }\lambda {{\text{ }}_{\text{L}}}\text{ and }~\text{ }~\text{ }\lambda {{\text{ }}_{\text{k}}}\]are the wavelengths are L and K series X-ray lines responding then
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/2 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  3</td>
														<td style="text-align:left !important">
															<umair>645526145</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('645526145', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Modern Physics</td>
														<td style="text-align:left !important">Introduction to Atomic Spectra</td>
														<td style="text-align:left !important" title="Two bottoms and one top quark will form
">  Two bottoms and one top quark will form
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/3 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  4</td>
														<td style="text-align:left !important">
															<umair>242536359</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('242536359', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Modern Physics</td>
														<td style="text-align:left !important">X Rays</td>
														<td style="text-align:left !important" title="The minimum wavelength of X-rays produced by the bombardment of the accelerating potential is 20KV, will be
">  The minimum wavelength of X-rays produced by the bombardment of the accelerating potential is 20KV, will be
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/4 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  5</td>
														<td style="text-align:left !important">
															<umair>645516349</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('645516349', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Waves</td>
														<td style="text-align:left !important">Basic Concepts of Waves</td>
														<td style="text-align:left !important" title="Transverse waves are moving past a small boat on a lake. Which way will the waves make the boat move?


">  Transverse waves are moving past a small boat on a lake. Which way will the waves make the boat move?


 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/5 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  6</td>
														<td style="text-align:left !important">
															<umair>947526346</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('947526346', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Waves</td>
														<td style="text-align:left !important">Stationary Waves</td>
														<td style="text-align:left !important" title="A stationary sound wave has frequency&nbsp;165 Hz (Speed of sound in air = 330 m/s) than the distance between two consecutive nodes is&nbsp;&nbsp;
">  A stationary sound wave has frequency165 Hz (Speed of sound in air = 330 m/s) than the distance between two consecutive nodes is
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/6 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  7</td>
														<td style="text-align:left !important">
															<umair>645536161</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('645536161', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electromagnetism</td>
														<td style="text-align:left !important">Ampere's Law &amp; Determination of Flux Density B</td>
														<td style="text-align:left !important" title="The permeability of free space μo has a value
">  The permeability of free space μo has a value
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/7 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  8</td>
														<td style="text-align:left !important">
															<umair>547566928</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('547566928', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electromagnetism</td>
														<td style="text-align:left !important">Force on a Current Carrying Conductor in a Uniform Magnetic Field</td>
														<td style="text-align:left !important" title="If a long copper rod carries a direct current, the magnetic field associated with the current will be
">  If a long copper rod carries a direct current, the magnetic field associated with the current will be
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/8 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  9</td>
														<td style="text-align:left !important">
															<umair>246556743</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('246556743', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electromagnetism</td>
														<td style="text-align:left !important">Force on a Moving Charge in a Magnetic Field</td>
														<td style="text-align:left !important" title="Work done by magnetic force on a charged particle is
">  Work done by magnetic force on a charged particle is
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/9 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  10</td>
														<td style="text-align:left !important">
															<umair>841516819</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('841516819', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electronics</td>
														<td style="text-align:left !important">Rectification</td>
														<td style="text-align:left !important" title="In the process of rectification, the current received across the load resistance is
">  In the process of rectification, the current received across the load resistance is
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/10 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  11</td>
														<td style="text-align:left !important">
															<umair>142516629</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('142516629', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electronics</td>
														<td style="text-align:left !important">Rectification</td>
														<td style="text-align:left !important" title="The slope of diode characteristics graph shows&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
">  The slope of diode characteristics graph shows
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/11 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  12</td>
														<td style="text-align:left !important">
															<umair>443556972</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('443556972', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electronics</td>
														<td style="text-align:left !important">Op-Amp as a Non-Inverting Amplifier</td>
														<td style="text-align:left !important" title="For same values of resistances used in inverting and non-inverting amplifier, which has more gain.
">  For same values of resistances used in inverting and non-inverting amplifier, which has more gain.
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/12 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  13</td>
														<td style="text-align:left !important">
															<umair>545566224</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('545566224', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electrostatics</td>
														<td style="text-align:left !important">Capacitor and Capacitance of a Parallel Plate Capacitor</td>
														<td style="text-align:left !important" title="The electric intensity&nbsp;between two plates of a capacitor equals&nbsp;
">  The electric intensitybetween two plates of a capacitor equals
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/13 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  14</td>
														<td style="text-align:left !important">
															<umair>945556826</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('945556826', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electrostatics</td>
														<td style="text-align:left !important">Coulomb's Law</td>
														<td style="text-align:left !important" title="The force of attraction between two charges is 40 N. if the magnitude of charges is doubled and a dielectric of strength 8 is placed between charges then the force &nbsp;&nbsp;&nbsp;
">  The force of attraction between two charges is 40 N. if the magnitude of charges is doubled and a dielectric of strength 8 is placed between charges then the force 
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/14 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  15</td>
														<td style="text-align:left !important">
															<umair>144516441</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('144516441', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Electrostatics</td>
														<td style="text-align:left !important">Coulomb's Law</td>
														<td style="text-align:left !important" title="The figure shows three charges. Find the net force on q1:



&nbsp;
">  The figure shows three charges. Find the net force on q1:




 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/15 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  16</td>
														<td style="text-align:left !important">
															<umair>944556137</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('944556137', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Work, Energy &amp; Power</td>
														<td style="text-align:left !important">Work Done by a Constant Force</td>
														<td style="text-align:left !important" title="Work done during isothermal expansion depends on change in
">  Work done during isothermal expansion depends on change in
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/16 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  17</td>
														<td style="text-align:left !important">
															<umair>349566573</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('349566573', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Work, Energy &amp; Power</td>
														<td style="text-align:left !important">Energy and its Conservation</td>
														<td style="text-align:left !important" title="1810 Joule of energy is absorbed by 10&shy;gram mass from a radioactive source. What is the absorbed dose?
">  1810 Joule of energy is absorbed by 10&shy;gram mass from a radioactive source. What is the absorbed dose?
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/17 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  18</td>
														<td style="text-align:left !important">
															<umair>647596339</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('647596339', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Work, Energy &amp; Power</td>
														<td style="text-align:left !important">Work Done by a Constant Force</td>
														<td style="text-align:left !important" title="A body of mass 1 kg falls from rest through the air a distance of 100 m. Its velocity when it reaches the ground is 30 ms-1 then work done against friction is (take g = 10 ms-2)
">  A body of mass 1 kg falls from rest through the air a distance of 100 m. Its velocity when it reaches the ground is 30 ms-1 then work done against friction is (take g = 10 ms-2)
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/18 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX odd" role="row">
														<td style="text-align:left !important" class="sorting_1">  19</td>
														<td style="text-align:left !important">
															<umair>348546568</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('348546568', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Circular Motion</td>
														<td style="text-align:left !important">Geostationary Orbits &amp; Communication Satellites</td>
														<td style="text-align:left !important" title="At&nbsp; what depth the value of g will become one fourth of its value at surface of&nbsp; earth
">  At what depth the value of g will become one fourth of its value at surface of earth
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/19 ">View Details</a>
															</b>
														</td>
													</tr>
													<tr class="gradeX even" role="row">
														<td style="text-align:left !important" class="sorting_1">  20</td>
														<td style="text-align:left !important">
															<umair>547586918</umair>
															<i title="Copy QID to clipboard" onclick="CopytoClipboard('547586918', 0)" class="fa fa-clipboard cursor-pointer"></i>
														</td>
														<td style="text-align:left !important">Physics</td>
														<td style="text-align:left !important">Circular Motion</td>
														<td style="text-align:left !important">Geostationary Orbits &amp; Communication Satellites</td>
														<td style="text-align:left !important" title="&nbsp;Stars are not visible in day-time because?&nbsp;
">  Stars are not visible in day-time because?
 </td>
														<td>
															<i title="Unanswered" class="fa fa-minus-circle blue"></i>
														</td>
														<td style="text-align:left !important"> -  </td>
														<td style="text-align:left !important">
															<b>
																<a style="text-decoration:none;" class="text-underline" href="https://app.topgrade.pk/full-length-practice-exams/full-length-test-explanation/548566939/20 ">View Details</a>
															</b>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="row">
											<div class="col-md-5 col-sm-5"></div>
											<div class="col-md-7 col-sm-7"></div>
										</div>
									</div>
                                    Showing 1 to 20  of 220 records
									<br>
										<ul class="pagination">
											<li class="active">
												<a href="#">1</a>
											</li>
											<li>
												<a href="https://app.topgrade.pk/full-length-practice-exams/full-length-practice-test-result/2?test_id=548566939" data-ci-pagination-page="2">2</a>
											</li>
											<li>
												<a href="https://app.topgrade.pk/full-length-practice-exams/full-length-practice-test-result/3?test_id=548566939" data-ci-pagination-page="3">3</a>
											</li>
											<li>
												<a href="https://app.topgrade.pk/full-length-practice-exams/full-length-practice-test-result/2?test_id=548566939" data-ci-pagination-page="2" rel="next">»</a>
											</li>
										</ul>
									</div>
								</div>
								<!-- END EXAMPLE TABLE PORTLET-->
							</div>
						</div>
	   @endsection