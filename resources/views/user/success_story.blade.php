<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<title>EntryTest</title>
	<meta name="description" content="EntryTest.">
	<meta name="keywords" content="Entrytest, Ecat, MCAT">
	<meta name="google-site-verification" content="w5V6AC46YbTlolvDWbldcKsluUm4vO4hBRq9yextC_g" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="{{ url('/')}}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="{{asset('public-web/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('public-web/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('public-web/css/user/mystyle.css" rel="stylesheet">
	
	<script>
		function goto(url){  window.stop(); window.location = url; }
	</script>
</head>
<body id="home">
	<div class="wrapper">
		<!-- Header Area -->
		<div class="header">
			<!-- Navigation Menu -->
			<nav class="navbar navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="#home"><img src="https://s3-ap-southeast-1.amazonaws.com/sg2.oliveboard.in/static/img/logo.png"></a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="#home" id="remove-home">Home</a>
							</li>
							<li>
								<a href="#">App</a>
							</li>
							<li>
								<a href="#">Blog</a>
							</li>
							<li class="active">
								<a href="#">Success Stories</a>
							</li>
							<li>
								<a class="login re-hover">Login</a>
							</li>
							<li>
								<a class="sign-up" href="#">Register</a>
							</li>
						</ul>
					</div>
				</div>
			</nav><!-- Navigation End -->
		</div><!-- Header End --><!-- Main Content -->
		<div class="main-content">
			<section id="tm-banner">
				<div class="banner-box">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<h1>Success Stories</h1>
							</div>
							<div class="col-md-6 col-sm-6 text-right">
								<a class="btn btn-theme btn-testimonials" href="#sb-testimonial" id="submit" name="submit">Add your Success Story</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="testimonials">
				<!-- testimonials -->
				<div class="paddtop">
					<div class="container tmContainer">
						<div class="row tmRow">
							<div class="col-md-4 col-sm-4 tmMain-vcol">
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.
											<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
							</div><!-- /. row-box-1 -->
							<div class="col-md-4 col-sm-4 tmMain-vcol">
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="row tmMain-vrow">
								</div>
							</div>
							<div class="col-md-4 col-sm-4 tmMain-vcol">
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<!-- box -->
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 tmcol-box">
									<div class="tmbox">
										<div class="tmbox-header">
											<table>
												<tbody>
													<tr>
														<td><span class="tmimg tmimg-ele tm-img113"></span></td>
													</tr>
													<tr>
														<td class="tmtd-bor">
															<h3>Akshay Vohra</h3>
															<p>IBPS CLERK</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tmbox-body">
											Hello Everyone,<br>
											I am Akshay Vohra, currently working in Punjab &amp; Sind Bank.<br>
											I would love to share my story of cracking IBPS exam through self-study &amp; help of test series by Olive board.<br>
											I completed my graduation in the year 2015 and was placed in HERO cycles. I worked for 3 months b<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>ut was never satisfied so I decided to quit my job and started preparing for banking exams. I took guidance from my elder brother who is currently working in PNB and started with self-study. I joined many test series but found the Oliveboard Mock Test Series to be the best.<br>
												They have maintained very good standards and quality is also remarkable because their question papers are of the same pattern similar to the new banking exams pattern.<br>
												I gave many exams like SBI, IBPS, LIC and much more and was unable to crack any of them. Still, i did not lose my hope and continue to study and give many other exams<br>
												After days of hard work, I cracked IBPS clerk 2016 and was allotted in Punjab and Sind Bank. I think the best strategy of preparing for these exams is, first of all, clear your basic concepts and make your strong areas, even more, stronger and bring weaker areas to average.<br>
												MATHS - I used books only to clear my conceptual basics, for e.g Arihant Publications, Arun Sharma. Rest, I only practised on various websites and tried to give as many quizzes so that I could encounter different types of questions.<br>
												REASONING - This was my strength. For reasoning, practice is the only key. Practice as many questions possible and give maximum quizzes online. I will only suggest you the Oliveboard mock test series because here you will find quality questions.<br>
												General Awareness - I used to read newspapers on a daily basis and had read many current affairs from various websites, then made daily notes and revised them daily. This is the one section where you can score maximum marks<br>
												ENGLISH= This was my weakest area, I used to read editorial section of the newspaper and devoted my maximum time to this subject by solving more and more quizzes.<br>
												I allotted time for all subjects in a day. This is what I had done :<br>
												Maths - 3 hrs<br>
												Reasoning - 2 hrs<br>
												GA - 2hrs<br>
												English - 3 hrs<br>
												and used to give mock papers on weekends to analyse my progress.<br>
												NEVER GIVING UP is the key to success.<br>
											All the best.</span>&nbsp;&nbsp;<a class="morelink" href="">Read More</a></span>
										</div>
									</div>
								</div>
							</div><!-- /. row-box-3 -->
						</div>
					</div>
				</div>
			</section><!-- /.testimonials -->
			<section id="sb-testimonial">
				<div class="contact padd">
					<div class="container">
						<div class="contact-content" id="contactcontainer">
							<h5 class="text-center">Submit Your Success story</h5>
							<div class="bor"></div><br>
							<label class="testimonialUp">&nbsp;</label>
							<form id="testimonialForm" method="post" name="testimonialForm" role="form">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<input class="form-control" id="t-name" name="t-name" placeholder="Enter Name..." type="text" value="">
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<input class="form-control" 
											id="t-cexam" name="t-cexam" placeholder="Enter Exam you cracked..." type="text" value="">
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<input class="form-control" id="t-email" name="t-email" placeholder="Enter Email..." type="email" value="">
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<input class="form-control" id="t-phone" name="t-phone" placeholder="Enter Mobile..." type="text" value="">
										</div>
									</div>
									<div class="col-xs-12">
										<div class="form-group">
											<label class="btn btn-file"><i aria-hidden="true" class="fa fa-upload"></i>Upload Profile Photo<input id="upload_file" name="upload_file" style="display:none;" type="file"></label> <span id="uploadFileName">No file</span>
										</div>
									</div>
									<div class="col-xs-12">
										<div class="form-group">
											<textarea class="form-control" id="testimonial-message" name="testimonial-message" placeholder="Enter Your Success Story (min 200 characters)..." rows="6"></textarea>
										</div><a class="btn btn-theme" href="javascript:submitTestimonial();" id="sMsg" name="submit">Submit Success Story</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>
		   <div class="footer text-center">
            	<div class="container link-footer">
            		<div class="row">
            			<div class="col-md-3 col-sm-6 col-xs-12">
            				<p><img src="<?php asset('images/logo.png') ?>" height="80" width="80" style="margin:0px;"></p>
            				<p>M. Shaukat Ali Road, Lahore, Pakistan</p>
            				<p>prepon.entrytests@gmail.com</p>
            				<p>03334242069</p>
            				<div class="footer-social social">
            					<!--<a href="#" onclick="window.open('', '_blank'); return false;" class="facebook"><i class="fa fa-facebook"></i></a>-->
            					<a href="https://www.facebook.com/PrepOn-636371010108789/?modal=admin_todo_tour"  class="facebook"><i class="fa fa-facebook"></i></a>
            					{{-- <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="google-plus"><i class="fa fa-google-plus"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="linkedin"><i class="fa fa-linkedin"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="youtube"><i class="fa fa-youtube"></i></a>      --}}
            				</div>    
            			</div>
            			<div class="col-md-2 col-sm-6 col-xs-12">
            				{{-- <div class="footer-social social"> --}}
            					{{-- <a href="#" onclick="#" class="facebook"><i class="fa fa-facebook"></i></a> --}}
            					{{-- <a href="#" onclick="#" class="twitter"><i class="fa fa-twitter"></i></a> --}}
            				{{-- </div> --}}
            				<h3>Social</h3>
            				<ul>
            					<li><a href="https://www.facebook.com/PrepOn-636371010108789/?modal=admin_todo_tour" disabled ><i class="right isDisabled"></i><span>Facebook</span></a></li>
            					{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Twitter</span></a></li> --}}
            					{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Careers</span></a></li> --}}
            					{{-- <li><a href="#" disabled ><i class="right isDisabled"></i><span>Testimonials</span></a></li> --}}
            				</ul>

            			</div>
            			<div class="col-md-2 col-sm-6 col-xs-12">
            				{{-- <h3>Resources</h3> --}}
                            {{-- <ul>
                                <li><a href="#"><i class="right"></i><span>Live Practice</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Video Course</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Free eBooks</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Blog</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Discuss Forum</span></a></li>
                                <li><a href="#"><i class="right"></i><span>Current Affairs</span></a></li>
                            </ul> --}}
                            
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            {{-- <h3>Popular Exams</h3>
                            <ul>
                                <li><a href="#"><i class="right"></i><span>ECAT</span></a></li>
                                <li><a href="#"><i class="right"></i><span>MCAT</span></a></li>
                                <li><a href="#"><i class="right"></i><span>PPSC</span></a></li>
                                <li><a href="#"><i class="right"></i><span>CSS</span></a></li>
                                <li><a href="#"><i class="right"></i><span>NTS</span></a></li>
                                <li><a href="#"><i class="right"></i><span>PTS</span></a></li>
                               
                            </ul> --}}
                            
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                        	<h3>Download the App</h3>
                        	<ul>

                        		<li><a href="https://play.google.com/store/apps/details?id=com.project.prepon" /><img src="{{asset('public-web/img/play.png')}}"></a></li>
                        	</ul>

                        </div>
                    </div> 
                </div>

                <div class="container">

                	<!-- Footer Copyright -->
                	<div class="copyright">
                		{{-- <p><a class="footerlinks" href="#">Careers</a> | --}} <a class="footerlinks" href="{{url('/privacy/policy')}}">Privacy Policy</a> {{-- | <a class="footerlinks" href="#">Terms</a> --}}</p>
                		<p>&copy; Copyright <a href="#">entrytest4u.com</a> - All Rights Reserved.</p>
                	</div>
                </div>
            </div>
            <!-- Footer End -->
		<span class="totop"><a href=""><i class="fa fa-chevron-up"></i></a></span>
	</div>
	<script src="{{asset('public-web/js/jquery.js')}}"></script>
	<script src="{{asset('public-web/js/bootstrap.min.js')}}"></script>
	
</body>
</html>