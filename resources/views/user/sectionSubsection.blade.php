<!DOCTYPE html>
<?php
/**
* Project: Entrytest4u.
* User: shafqat bhatti
* Date: 12/03/2019
* Time: 1:44 PM
*/
?>

@extends('_layouts.user.default')
@section('title', 'User Dashboard')
@section('content')

<div id="main-sec">
	<section id="mainWrap-sec" class="mainWrap-sec"><!-- body -->
		<form name="form" method="post" id="formtest" action="{{route('getQuestion')}}">
			@csrf
			<input type="hidden" name="selected_course_id" id="selected_course_id" value="@isset($course_id){{$course_id}}@endisset">

			<input type="hidden" name="sub_id" id="sub_id" value="@isset($id){{$id}}@endisset">
                       
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="print_test">
				<div class="portlet light portlet-fit bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class=" icon-layers font-green"></i>
							<span class="caption-subject font-green bold uppercase">Choose Question Type</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="checkbox_sec q-mod">
							<div class="form-group form-md-radios">
								<div class="md-radio-list">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio10" checked="" value="1" name="question_mode" class="md-radiobtn">
												<label for="radio10">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>All
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio8" value="2" name="question_mode" class="md-radiobtn">
												<label for="radio8">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>Unseen
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio9" value="3" name="question_mode" class="md-radiobtn">
												<label for="radio9">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>Past Papers
												</label>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" id="t6">
							</div>

							<div style="clear:both;"></div>

						</div>

					</div>

				</div>

			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="take_test">

				<div class="portlet light portlet-fit bordered">

					<div class="portlet-title">

						<div class="caption">

							<i class=" icon-layers font-green"></i>

							<span class="caption-subject font-green bold uppercase" data-step="1" data-intro="Select a specific group of questions to work on" data-position="right" data-scrollto="tooltip">Choose Question Type</span>

						</div>

					</div>

					<div class="portlet-body">

						<div class="checkbox_sec q-mod">

							<div class="form-group form-md-radios">
								<div class="md-radio-list">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio4" checked="1" value="1" name="question_mode" class="md-radiobtn">
												<label for="radio4">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>All
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio2" value="2" name="question_mode" class="md-radiobtn">
												<label for="radio2">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>Incorrect
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio5" value="3" name="question_mode" class="md-radiobtn">
												<label for="radio5">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>Unseen
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio1" value="4" name="question_mode" class="md-radiobtn">
												<label for="radio1">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>Unanswered
												</label>
											</div>
										</div>

										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio3" value="5" name="question_mode" class="md-radiobtn">
												<label for="radio3">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>Bookmarked
												</label>
											</div>
										</div>
										<div class="col-lg-2 col-md-6 col-sm-4 col-xs-6">
											<div class="md-radio">
												<input type="radio" id="radio6" value="6" name="question_mode" class="md-radiobtn">

												<label for="radio6">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>Past Questions
												</label>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" id="t6">

							</div>

							<div style="clear:both;"></div>

						</div>

					</div>

				</div>

			</div>

			<div class="clearfix"></div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="past_question_div" style="display:none;">

				<div class="portlet light portlet-fit bordered">

					<div class="portlet-title">

						<div class="caption">

							<i class=" icon-layers font-green"></i>

							<span class="caption-subject font-green bold uppercase">Past Questions</span>

						</div>

					</div>

					<div class="portlet-body">

						<div class="checkbox_sec q-mod">

							<div class="form-group form-md-radios">
								<div class="md-radio-list">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="past_question_body">
									</div>
									<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>

								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" id="t6">
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
				</div>

			</div>
			<div class="clearfix"></div>

			<div class="col-md-12 col-sm-12 col-xs-12" id="chapter" style="float:none;">

				<div class="portlet light portlet-fit bordered">

					<div class="portlet-title">

						<div class="caption">

							<i class=" icon-layers font-green"></i>

							<span class="caption-subject font-green bold uppercase" data-step="2" data-intro="Choose section/s from which you want to take test" data-position="right" data-scrollto="tooltip">Choose Section</span>

						</div>

					</div>

					<div class="portlet-body mb-15">
						<!---->
						<div class="checkbox_sec" id="section_list">
							<div class="form-group form-md-checkboxes">
								<div class="md-checkbox-list">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<div class="md-checkbox">
												<input type="checkbox" id="checkbox2000" onclick="selectall(this)" class="md-check">
												<label for="checkbox2000">
													<span class="inc"></span>
													<span class="check"></span>
													<span class="box"></span> Select All
												</label>
											</div>

										</div>
									</div>
									@foreach($data->chapters as $chaps)
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="md-checkbox ">
											<input type="checkbox" id="checkbox{{$chaps->id}}" name="section_id[]" onclick="get_sub_section({{$chaps->id}})" value="{{$chaps->id}}" class="md-check">
											<label for="checkbox{{$chaps->id}}">
												<span class="inc"></span>
												<span class="check"></span>
												<span class="box"></span> 
												{{$chaps->name}} {{-- [684] --}}
											</label>
										</div>
									</div>
									@endforeach


								</div>
							</div>


						</div>
						<div class="clearfix"></div>

					</div>

				</div>

			</div>

			<!---->

			<!---->

			<div class="col-md-12 col-sm-12 col-xs-12" id="sub_topic" style="float:none; display: none;">

				<div class="portlet light portlet-fit bordered">

					<div class="portlet-title">

						<div class="caption">
							<i class=" icon-layers font-green"></i>
							<span class="caption-subject font-green bold uppercase" data-step="3" data-intro="Choose sub-section/s from which you want to take test" data-position="right" data-scrollto="tooltip" id="guided-tour-text">Choose Sub-Section</span>

						</div>
					</div>

					<div class="portlet-body">

						<div class="checkbox_sec" id="subsection_list">

							<div class="clearfix"></div>

						</div>

					</div>

				</div>
			</div>

			
			<div class="col-md-12 col-sm-12 col-xs-12" id="no-of-questions">
				<div class="clearfix"></div>
				<div class="clearfix"></div>
				<div class="clearfix"></div>
				<div class="portlet light portlet-fit bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class=" icon-layers font-green"></i>
							<span class="caption-subject font-green bold uppercase">No. of Questions</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="checkbox_sec d-flex flex-wrap align-items-center number-ov-question">
							<input style="width:60px;" type="number" value="0" min="0" max="50" data-step="4"  data-intro="Specify the number of questions you want to include in the test" data-position="right" data-scrollto="tooltip" name="no_of_question" id="no_of_question"> &nbsp; Maximum allowed &nbsp;
							<span class="rounded text-white bg-black">50</span>
							<a>
								<button type="submit"  data-step="5" data-intro="Click to start your test." data-position="right" data-scrollto="tooltip"  class="btn green d-block w-100 rounded">Start Your Test</button>
							</a>
							<span id="q_err" class="display-none"></span>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>

		</form>
	</section>
</div>
@endsection
@push('post-scripts')
<script type="text/javascript">
	function selectall(obj){
		console.log('select all');
		$('input[name=section_id]').prop('checked', true).triggerHandler('click');

	}
	function get_sub_section(obj){
		console.log('subject');
		$.ajax({
			url: "{{route('subSection')}}",
			type: 'post',
			data: {
				'id': obj
			},
			success: function (response) {
				console.log(response);
				if(response.status){
					$('#sub_topic').css("display",'block');
					var html_content=``;

					$.each(response.data,function(index,value){
						html_content+=`<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="md-checkbox ">
											<input type="checkbox" id="checkbox${value.id}" value="${value.id}" name="sub_section_id[]" class="md-check">
											<label for="checkbox${value.id}">
												<span class="inc"></span>
												<span class="check"></span>
												<span class="box"></span> 
												 ${value.sub_topic_name} {{-- [684] --}}
											</label>
										</div>
									</div>`;
					});
					// $.each(response.data, function(index, value) { 
					// 	console.log('index',value);
					// 	html_content +=	`<div class="form-group form-md-radios">
					// 	<div class="md-radio-list">
					// 	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					// 	<div class="md-radio ">
					// 	<input type="radio" id="radio${value.id}" value="${value.id}"  name="sub_section_id" class="md-radiobtn">
					// 	<label for="radio${value.id}">
					// 	<span></span>
					// 	<span class="check"></span>
					// 	<span class="box"></span> ${value.sub_topic_name}
					// 	</label>
					// 	</div>
					// 	</div>
					// 	</div>
					// 	</div>`;
					// });
					html_content+=`<div class="clearfix"></div>`;
					$("#subsection_list").html(html_content);
				}else{
					$('#sub_topic').css("display",'none');
					$("#subsection_list").html('');
				}


			},
			error: function (e) {
				console.log('error', e);
			}
		});
	}
</script>
@endpush