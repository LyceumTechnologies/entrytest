<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <base href="{{ url('/')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @section('metas')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    @show

    <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

    <title> @yield('title','Login') - ALLONS ENTERPRISES</title>

    @stack('pre-styles')

    @section('styles')

        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">

    @show

    @stack('post-styles')
     <script type="text/javascript" src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/app.js')}}"></script>

    <script type="text/javascript">
        var BASE_URL = "{{ url('/')}}";
    </script>
</head>

<body class="signwrapper">
@yield('content')

</body>
</html>