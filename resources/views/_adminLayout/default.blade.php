 <?php
/**
 * Project: Entry Test For you.
 * User: SHAFQAT GHAFOOR
 * Date: 6/12/2018
 * Time: 6:30 PM
 */
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

  <base href="{{ url('/')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @section('metas')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  @show


  <title> @yield('title','Dashboard')- EntryTest</title>


  @stack('pre-styles')

  @section('styles')
  <link rel="shortcut icon" href="favicon.ico">
  <link rel="icon" href="favicon.ico" type="image/x-icon">
  

  <link href="{{asset('assets/adminAsset/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
  
  <!-- Toast CSS -->
  <link href="{{asset('assets/adminAsset/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
  
  <!-- Morris Charts CSS -->
    <link href="{{asset('assets/adminAsset/vendors/bower_components/morris.js/morris.css')}}" rel="stylesheet" type="text/css"/>
  
  <!-- Chartist CSS -->
  <link href="{{asset('assets/adminAsset/vendors/bower_components/chartist/dist/chartist.min.css')}}" rel="stylesheet" type="text/css"/>
  
  
  <!-- vector map CSS -->
  <link href="{{asset('assets/adminAsset/vendors/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" type="text/css"/>
  
  <!-- Custom CSS -->
  <link href="{{asset('assets/adminAsset/admintres/dist/css/style.css')}}" rel="stylesheet" type="text/css">

  


 @show

  @stack('post-styles')

  <script type="text/javascript">
    var BASE_URL = "{{ url('/')}}";
  </script>

</head>

<body>
<div class="preloader-it">
    <div class="la-anim-1"></div>
  </div>
  <!-- /Preloader -->
    <div class="wrapper theme-2-active navbar-top-light">

   @section('header-base')
    @include('_adminLayout.partials._header-base')
    @show

    @section('aside-left')
    @include('_adminLayout.partials._aside-left')

   
    
      @yield('content')
      @show

    @section('footer')
    @include('_adminLayout.partials._footer')
 
</div>

@stack('pre-scripts')

@section('scripts')

<script src="{{asset('assets/adminAsset/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('assets/adminAsset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    
  <!-- Data table JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
  
  <!-- Slimscroll JavaScript -->
  <script src="{{asset('assets/adminAsset/admintres/dist/js/jquery.slimscroll.js')}}"></script>
  
  <!-- Progressbar Animation JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('assets/adminAsset/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>
  
  <!-- Fancy Dropdown JS -->
  <script src="{{asset('assets/adminAsset/admintres/dist/js/dropdown-bootstrap-extended.js')}}"></script>
  
  <!-- Sparkline JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>
  
  <!-- Owl JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>
  
  <!-- Switchery JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>
  
  <!-- Vector Maps JavaScript -->
    <script src="{{asset('assets/adminAsset/vendors/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
  <script src="{{asset('assets/adminAsset/vendors/vectormap/jquery-jvectormap-us-aea-en.js')}}"></script>
    <script src="{{asset('assets/adminAsset/vendors/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <script src="{{asset('assets/adminAsset/admintres/dist/js/vectormap-data.js')}}"></script>
  
  <!-- Toast JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
  
  <!-- Piety JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/bower_components/peity/jquery.peity.min.js')}}"></script>
  <script src="{{asset('assets/adminAsset/admintres/dist/js/peity-data.js')}}"></script>
  
  <!-- Chartist JavaScript -->
  <script src="{{asset('assets/adminAsset/vendors/bower_components/chartist/dist/chartist.min.js')}}"></script>
  
  <!-- Morris Charts JavaScript -->
    <script src="{{asset('assets/adminAsset/vendors/bower_components/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('assets/adminAsset/vendors/bower_components/morris.js/morris.min.js')}}"></script>
    <script src="{{asset('assets/adminAsset/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
  
  <!-- ChartJS JavaScript -->
  <script src="{{asset('assets/adminAsset//vendors/chart.js/Chart.min.js')}}"></script>
  
  <!-- Init JavaScript -->
  <script src="{{asset('assets/adminAsset/admintres/dist/js/init.js')}}"></script>
  <script src="{{asset('assets/adminAsset/admintres/dist/js/dashboard-data.js')}}"></script>




<script type="text/javascript">
  $.ajaxSetup({
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
 });



</script>





@show


@stack('post-scripts')

</body>
</html>