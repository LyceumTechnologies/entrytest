<header>
	<div class="navbar navbar-postlogin navbar-fixed-side navbar-fixed-side-left " role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" id="navbar-toggle" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="navbar-collapse collapse">
				<div id="mainmenu-navbar">
					<ul class="nav navbar-nav">
					</ul>
					<ul class="nav navbar-nav">
						{{-- <li class="active"><a href="javascript:void(0);"><span class="tabs-icon icon_liveclass"></span>Learn</a>
							<ul class="menu-practice showlearnmenu">

								<li><a class="pmenu-active" href="/mba/lessons.php"><span class=""></span>Lessons</a></li>

								<li><a class="" href="videolessons.php"><span class=""></span>Video Lessons</a></li>
								<li><a class="" href="/mba/ebooks.php"><span class=""></span>Free eBooks</a></li>				                    		
							</ul>

						</li> --}}
						<li class="{{ areActiveRoutes(['user.dashboard'])}}">
							<a href="{{route('user.dashboard')}}"><span class="tabs-icon icon_practice"></span>Dashboard</a>
						</li>
						{{-- <li class="{{ areActiveRoutes(['user.test'])}}">
							<a href="{{route('user.test')}}"><span class="tabs-icon icon_practice"></span>Tests
							</a>
						</li> --}}
						@php($course=\App\Models\Course::where('status',1)->with('couseSubject.subject')->get())
						@foreach($course as $testing)
						<div class="sidebar left ">
						  <ul class="list-sidebar ">
						    <li> 
						    	<a href="javascript:;" data-toggle="collapse" data-target="#{{$testing->id}}" class="collapsed active" >
						    	 <i class="fa fa-th-large"></i> 
						    	 <span class="nav-label"> {{$testing->name}} </span> 
						    	 <span class="fa fa-chevron-left pull-right"></span> 
						    	</a>
							    <ul class="sub-menu collapse" id="{{$testing->id}}">
							    	@foreach($testing->couseSubject as $log)
							        <li><a href="{{url('user/subject/'.$log->sub_id.'/'.$testing->id)}}"> {{$log->subject->sb_name}} </a></li>
							        @endforeach
							        {{-- <li>
								    	<a href="{{route('previous.test')}}"> Previous Test</a>
								    </li>
							        <li>
								    	<a href="{{route('fulllength.test')}}"> fulllength</a>
								    </li>
								     <li>
								    	<a href="{{route('viewnotes.test')}}"> view Notes</a>
								    </li>
								     <li>
								    	<a href="{{route('writenotes.test')}}"> write Notes</a>
								    </li> --}}
							    </ul>
						    </li>
						    
						    
						  </ul>
						</div>
						@endforeach
						{{-- <li class="{{ areActiveRoutes(['user.practice'])}}"><a href="{{route('user.practice')}}"><span class="tabs-icon icon_practice"></span>Practice Tests</a> --}}

							{{-- <ul class="menu-practice "> --}}

								{{-- <li><a class="" href="#"><span class=""></span>Mock Tests</a></li> --}}

								{{-- <li><a class="" href="{{route('user.practice')}}">Practice Tests</a></li> --}}

								{{-- <li><a class="" href="#">GK Tests</a></li>

								<li><a class="" href="#">Decision Making</a></li> --}}
							{{-- </ul>
						</li> --}}
						<!--<li class="< ? php echo $active_improve; ?>"><a href="< ? php echo $url_improve; ?>"><span class="tabs-icon icon_improve"></span>Improve</a></li>-->
					<ul class="nav navbar-nav">
						<!--<li class="menu-header">Resources</li>-->
						<!--<li class="< ?php echo $active_studyplan; ? >"><a href="< ?php echo $spurl; ? >" target="_blank"><span class="tabs-icon icon_studyplan"></span>Study Plan</a></li> -->
						{{-- <li class=""><a href="#"><span class="tabs-icon icon_discuss"></span>Discuss Forum</a></li> --}}
						{{-- <li class=""><a href="#" target="_blank"><span class="tabs-icon icon_sstories"></span>Success Stories</a></li> --}}

						<li><a href="{{route('user.logout')}}">
							<span class="tabs-icon icon_logout"></span>Logout</a></li>
						</ul>
					</div><!--/.main tabs -->

				</div>
				<!--/.nav-collapse -->
			</div><!--/.main tabs -->

		</div>
<script>
	$(document).ready(function(){
   $('button').click(function(){
       $('.sidebar').toggleClass('fliph');
   });
  
  
   
});
</script>
			