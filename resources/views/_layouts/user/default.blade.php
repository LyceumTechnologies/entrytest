 <?php
/**
 * Project: Entry Test For you.
 * User: SHAFQAT GHAFOOR
 * Date: 6/12/2018
 * Time: 6:30 PM
 */
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

  <base href="{{ url('/')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @section('metas')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  @show


  <title> @yield('title','User Dashboard')- EntryTest</title>


  @stack('pre-styles')

  @section('styles')
  <link href="{{asset('public-web/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('public-web/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('public-web/css/user/style.css')}}" rel="stylesheet">
  <link href="{{asset('public-web/css/user/style_1.css')}}" rel="stylesheet">
  <link href="{{asset('public-web/css/user/icon.css')}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
<script src="{{asset('public-web/js/jquery.js')}}"></script>

  @show

  @stack('post-styles')

  <script type="text/javascript">
    var BASE_URL = "{{ url('/')}}";
  </script>

</head>

<body id="home">
  <div class="sidebar-open-bg"></div>
  <div id="wrapper">

    @section('aside-left')
    @include('_layouts.user.partials._aside-left')

    @section('header-base')
    @include('_layouts.user.partials._header-base')
    @show



    @yield('content')
    @show



    <div class="modal fade buyModal" id="buyModal" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog buydialog" role="document">

        <div class="modal-content" id="buyContent">
          <div class="modal-header buy-header">
            <button  type="button" class="close buyclose" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div id="buyBody" class="modal-body buy-body">
            <div class="iconsbox">
              <span class="ic_buy"></span>
            </div>
            <h5 class="modal-title">Please make a purchase to access the remaining tests</h5>
            <div class="btngroup"><button onclick="#" type="button" class="btn-upgrade">Buy Now</button></div>
          </div>
        </div>
      </div>
    </div><!-- Summary Modal -->

  </div>
  
  
</body>
</html>

@stack('pre-scripts')

@section('scripts')


<script src="{{asset('public-web/js/jquery.js')}}"></script>
  <script src="{{asset('public-web/js/bootstrap.min.js')}}"></script>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<!-- Examples -->
<script type="text/javascript" async
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML">
</script>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
 });


</script>
<script>
    $(document).ready(function(){
      $("#hide").click(function(){
        $("p").hide();
      });
      $("#sec-topicTst").click(function(){
        $("p").show();
      });
    });
  </script>




@show


@stack('post-scripts')

</body>
</html> 