<div id="tag_container">
	@php($index=1)
	@foreach($question as $q)
	<div class="mtq_question mtq_scroll_item-1 question{{$index}}" id="mtq_question-1-1" style="width: 605px;margin:0 auto;    box-shadow: 0px 2px 5px #bbb;
	border: 1px solid #ccc; padding:10px; @if ($loop->first) display: block; @else display:none; @endif" >
	<table class="mtq_question_heading_table questionCorrection">
		<tbody>
			<tr>
				<td>
					<div class="mtq_question_label">
						Question 1 
					</div>
					<div class="mtq_stamp mtq_correct_stamp" id="mtq_stamp-1-1">
						
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="mtq_question_text" id="mtq_question_text-1-1">
		<a href="javascript:;">
			<strong>{!!$q->question !!}</strong>
		</a>
	</div>
	<table class="mtq_answer_table">
		<colgroup>
			<col class="mtq_oce_first">
		</colgroup>
		<tbody>
			@php($ansId=0)
			@foreach($q->answers as $ansd)
			<tr class="mtq_clickable answerId{{$ansd}} " id="answerId{{$ansd}}" onclick="mtq_button_click(this,@if($ansd->is_correct==1) 1 @else 0 @endif)">
				<td class="mtq_letter_button_td">
					<div class="mtq_css_letter_button mtq_letter_button_0 @if($ansd->is_correct==1) answerRemove @endif" id="mtq_button-1-1-1">
						{{(toNumAlpha($ansId))}}
					</div>
					@if($ansd->is_correct==1)
					<div class="mtq_marker mtq_correct_marker @if($ansd->is_correct==1) answerRemove @endif" id="mtq_marker-1-2-1" style="display: none;"></div>
					@else
					<div class="mtq_marker mtq_wrong_marker" id="mtq_marker-1-1-1" style="display: none;"></div>
					@endif
				</td>
				<td class="mtq_answer_td">
					<div class="mtq_answer_text" id="mtq_answer_text-1-1-1">
						{!! $ansd->ans !!}
					</div>
				</td>
			</tr>
			@php($ansId++)
			@endforeach

		</tbody>
	</table>
</div>
@php($index++)
@endforeach
<div class="watupro_buttons flex" id="watuPROButtons2226">
	<div id="prev-question" style="display:none;">
		<input type="button" class="btn btn-warning" value="< Previous" data-ids=1 onclick="previosQuestion(this)">
	</div> 
	<div id="next-question">
		<input type="button" class="btn btn-info" value="Next >" data-ids='1' onclick="nextQuestion(this);">
	</div> 

	{{-- <div><input type="button" class="btn btn-success" name="action" onclick="javascript:;" id="action-button" value="Submit"> --}}
	{{-- </div> --}}
</div>
</div>

<style type="text/css">
	.watupro_buttons.flex {
    display: flex;
    flex-flow: row wrap;
    justify-content: center;
    clear: both;
}
.watupro_buttons, .watupro_buttons td {
    border: none!important;
}
.watupro_buttons.flex div {
    margin: .5rem;
}
</style>