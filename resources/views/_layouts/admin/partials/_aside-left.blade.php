<style>
  li{
    list-style-type: none;
  }
  .navew {
    border-bottom: 2px solid #000;
    padding: 10px 51px;
    border-bottom: 1px solid #fdfdfd;
  }
.nave1{
 min-height: 40px;
     padding-top: 4px;
       text-decoration: unset;
}
.nave1:hover, ul.nave1:focus {
    background-color: #21262d;
    border-top: 2px solid #fff;
        padding-top: 4px;
        margin-bottom: 4px;
        text-decoration: unset;
    border-bottom: 2px solid #fff;
}
 .nave1:hover {
    text-decoration: none;
    vertical-align: middle;
   text-decoration: unset;
}
</style>
<div class="inner-wrapper">
  <!-- start: sidebar -->
  <aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
      <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>            </div>
      </div>

      <div class="nano">
        <div class="nano-content">
          <nav id="menu" class="nav-main" role="navigation">
            <ul class="nav nav-main">
              <li class="nav-active">
                <a href="index.html">
                  <i class="fa fa-home" aria-hidden="true"></i>
                  <span>Dashboard</span>
                </a>
              </li>
              <li>
                @if(Auth::check())
                @if( Auth::user()->role=='Super Admin' )
                <a href="{{route('admin.index')}}">
                  <i class="fa fa-group" aria-hidden="true"></i>
                  <span>Admin Management</span>
                </a>
              </li>
              <li>
                <a href="{{route('teacher.index')}}">
                  <i class="fa fa-group" aria-hidden="true"></i>
                  <span>Teacher Management</span>
                </a>
              </li>

              <li>
                <a href="{{route('user.index')}}">
                  <i class="fa fa-user" aria-hidden="true"></i>
                  <span>User Management</span>
                </a>
              </li>

              
              <li>
                <a href="{{route('question-export.index')}}">
                  <i class="fa fa-book" aria-hidden="true"></i>
                  <span>Question Export</span>
                </a>
              </li>

              <li>
                <a href="{{route('subject.index')}}">
                  <i class="fa fa-book" aria-hidden="true"></i>
                  <span>Subject</span>
                </a>
              </li>

               <li>
                <a href="{{route('past-paper.index')}}">
                  <i class="fa fa-book" aria-hidden="true"></i>
                  <span>Past Paper</span>
                </a>
              </li>

              <li>
                <a href="{{route('notes.index')}}">
                  <i class="fa fa-book" aria-hidden="true"></i>
                  <span>Notes</span>
                </a>
              </li>

              <li>
                <a href="{{route('video.index')}}">
                  <i class="fa fa-book" aria-hidden="true"></i>
                  <span>Video </span>
                </a>
              </li>

               <div class="sidebar left ">
                    <ul class="list-sidebar bg-defoult nave1">
                    <li> 
                      <a href="javascript:;" data-toggle="collapse" data-target="#challengeMangement" class="collapsed active" > <i class="fa fa-th-large"></i> <span class="nav-label">Challenge Management  </span> <span class="fa fa-chevron-left pull-right"></span> </a>
                      <ul class="sub-menu collapse" id="challengeMangement">
                        <li class="navew"><a href="{{route('challenge.index')}}">       Challenge    </a></li>
                        <li class="navew"><a href="#"> Challenge invites    </a></li>
                        <li class="navew"><a href="#"> Challenge Joins    </a></li>




                    
                      </ul>
                    </li>
                 
                  </ul>
                </div>



                 
                  {{-- <li>
                    <a href="{{route('course.index')}}">
                      <i class="fa fa-book" aria-hidden="true"></i>
                      <span>Course</span>
                    </a>
                  </li> --}}

                  <div class="sidebar left ">
                    <ul class="list-sidebar bg-defoult nave1">
                    <li> 
                      <a href="javascript:;" data-toggle="collapse" data-target="#courseMangement" class="collapsed active" > <i class="fa fa-th-large"></i> <span class="nav-label">Course Mangement  </span> <span class="fa fa-chevron-left pull-right"></span> </a>
                      <ul class="sub-menu collapse" id="courseMangement">
                        <li class="navew"><a href="{{route('course.index')}}">       Courses    </a></li>
                        <li class="navew"><a href="{{route('courseAddTopic')}}"> Course Add Subject     </a></li>




                    
                      </ul>
                    </li>
                 
                  </ul>
                </div>
                  @endif
                  @endif
                  @if(Auth::check())
                @if(Auth::user()->role=='Super Admin' or Auth::user()->role=='Data Entry' )
                  <div class="sidebar left ">
                    <ul class="list-sidebar bg-defoult nave1">
                    <li> 
                      <a href="javascript:;" data-toggle="collapse" data-target="#dashboard" class="collapsed active" > <i class="fa fa-th-large"></i> <span class="nav-label">Question Diffination  </span> <span class="fa fa-chevron-left pull-right"></span> </a>
                      <ul class="sub-menu collapse" id="dashboard">
                        <li class="navew"><a href="{{route('difficulty.index')}}">Difficulty Level</a></li>
                        <li class="navew"><a href="{{route('grade.index')}}">       Class    </a></li>
                         <li class="navew"><a href="{{route('chapters.index')}}">       Chapter    </a></li>


                        <li class="navew"><a href="{{route('chapter.index')}}"> Topic    </a></li>
                        <li class="navew"><a href="{{route('topic.index')}}"> Sub Topic</a></li>
                        

                      </ul>
                    </li>
                 
                  </ul>
                </div>
  
                @endif
                @endif
                @if(Auth::check())
                @if(Auth::user()->role=='Super Admin' or Auth::user()->role=='Data Entry' )
                {{-- <li>
                  <a href="{{route('chapter.index')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span>Topic </span>
                  </a>
                </li> --}}
                @endif
                @endif 
                @if(Auth::check())
                @if(Auth::user()->role=='Super Admin' or Auth::user()->role=='Data Entry' )
               {{--  <li>
                  <a href="{{route('difficulty.index')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span>Difficulty Level </span>
                  </a>
                </li> --}}
                @endif
                @endif 
                @if(Auth::check())
                @if(Auth::user()->role=='Super Admin' or Auth::user()->role=='Data Entry' )
                {{-- <li>
                  <a href="{{route('topic.index')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span>Sub Topic</span>
                  </a>
                </li> --}}
                @endif
                @endif

                @if(Auth::check())
                @if(Auth::user()->role=='Super Admin' or Auth::user()->role=='Data Entry' )
                <div class="sidebar left ">
                    <ul class="list-sidebar bg-defoult nave1">
                    <li> 
                      <a href="javascript:;" data-toggle="collapse" data-target="#question" class="collapsed active" > <i class="fa fa-th-large"></i> <span class="nav-label">Question Mangement  </span> <span class="fa fa-chevron-left pull-right"></span> </a>
                      <ul class="sub-menu collapse" id="question">
                        <li class="navew"><a href="{{route('question.create')}}">       Add New Question    </a></li>
                        <li class="navew"><a href="{{route('question-edit.index')}}">       Question Edit  </a></li>
                        <li class="navew"><a href="{{route('bulk-question-edit.index')}}">      Bulk Question Edit  </a></li>



                        <li class="navew"><a href="{{route('own-question.index')}}"> Own Question     </a></li>
                        <li class="navew"><a href="{{route('question.index')}}"> Question Approve</a></li>
                        <li class="navew"><a href="{{route('approved-question.create')}}">Approved</a></li>

                      </ul>
                    </li>
                 
                  </ul>
                </div>

                {{-- <li>
                  <a href="{{route('question.create')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span>Add New Question </span>
                  </a>
                </li> --}}
                @endif
                @endif

                @if(Auth::check())
                @if(Auth::user()->role=='Super Admin')
                <li>
                  <a href="{{route('question-remove.index')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span> Question Remove</span>
                  </a>
                </li>
                @endif 

                @endif
                @if(Auth::check())
                @if(Auth::user()->role=='Super Admin')

                <div class="sidebar left ">
                    <ul class="list-sidebar bg-defoult nave1">
                    <li> 
                      <a href="javascript:;" data-toggle="collapse" data-target="#report" class="collapsed active" > <i class="fa fa-th-large"></i> <span class="nav-label">Report  </span> <span class="fa fa-chevron-left pull-right"></span> </a>
                      <ul class="sub-menu collapse" id="report">
                        <li class="navew"><a href="{{route('report.index')}}">      Data entry Report   </a></li>
                        <li class="navew"><a href="{{route('approvedReport')}}">Approved Question Report     </a></li>
                       

                      </ul>
                    </li>
                 
                  </ul>
                </div>

              {{--   <li class="menu-item dropdown dropdown-submenu">

                  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-book" aria-hidden="true"></i> <span>Report</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="menu-item ">
                      <a href="{{route('report.index')}}">Data entry Report </a>
                    </li>
                    <li class="menu-item ">
                      <a href="{{route('approvedReport')}}">Approved Question Report </a>
                    </li>

                  </ul>
                </li> --}}
                @endif
                @endif
                @if(Auth::check())
                @if( Auth::user()->role=='Super Admin' or Auth::user()->role=='Admin' )
               {{--  <li>
                  <a href="{{route('question.index')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span>Question Approve</span>
                  </a>
                </li> --}}

                {{-- <li>
                  <a href="{{route('approved-question.create')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span> Approved</span>
                  </a>
                </li> --}}

                @endif
                @endif
                 {{--  <li>
                    <a href="#">
                      <i class="fa fa-laptop" aria-hidden="true"></i>
                      <span>Question of the Day</span>
                    </a>
                  </li> --}}
                  @if(Auth::check())
                  @if(Auth::user()->role=='Super Admin' )
                  <!-- <li>
                    <a href="{{route('user-package.create')}}">
                      <i class="fa fa-book" aria-hidden="true"></i>
                      <span>Packages Update </span>
                    </a>
                  </li> -->
                  @endif
                  @endif
                  @if(Auth::check())
                  @if(Auth::user()->role=='Super Admin' )
                  <!-- <li>
                    <a href="{{route('user-package.index')}}">
                      <i class="fa fa-book" aria-hidden="true"></i>
                      <span>Purchase Package history </span>
                    </a>
                  </li> -->
                  @endif
                  @endif
                  @if(Auth::check())
                  @if(Auth::user()->role=='Super Admin' )
                  <li>
                    <a href="{{route('notification.index')}}">
                      <i class="fa fa-book" aria-hidden="true"></i>
                      <span>Notifications </span>
                    </a>
                  </li>
                  @endif
                  @endif

                  

                <div class="sidebar left ">
                    <ul class="list-sidebar bg-defoult nave1">
                    <li> 
                      <a href="javascript:;" data-toggle="collapse" data-target="#marketing" class="collapsed active" > <i class="fa fa-th-large"></i> <span class="nav-label">Marketing  </span> <span class="fa fa-chevron-left pull-right"></span> </a>
                      <ul class="sub-menu collapse" id="marketing">
                        <li class="navew"><a href="{{route('non-verified.index')}}"> Non Verified User   </a></li>
                      </ul>
                    </li>
                 
                  </ul>
                </div>




                  
                </ul>
              </nav>

            </div>

          </div>

        </aside>