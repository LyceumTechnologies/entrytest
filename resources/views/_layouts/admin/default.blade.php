 <?php
/**
 * Project: Entry Test For you.
 * User: SHAFQAT GHAFOOR
 * Date: 6/12/2018
 * Time: 6:30 PM
 */
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

  <base href="{{ url('/')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @section('metas')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  @show


  <title> @yield('title','Dashboard')- EntryTest</title>


  @stack('pre-styles')

  @section('styles')
  <link rel="stylesheet" href="{{asset('web/assets/vendor/bootstrap/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('web/assets/vendor/font-awesome/css/font-awesome.css')}}" />
    <link rel="stylesheet" href="{{asset('web/assets/vendor/magnific-popup/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{asset('web/assets/vendor/bootstrap-datepicker/css/datepicker3.css')}}" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{asset('web/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css')}}" />
    <link rel="stylesheet" href="{{asset('web/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
    <link rel="stylesheet" href="{{asset('web/assets/vendor/morris/morris.css')}}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('web/assets/stylesheets/theme.css')}}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{asset('web/assets/stylesheets/skins/default.css')}}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{asset('web/assets/stylesheets/theme-custom.css')}}">

    <!-- Head Libs -->
    <script src="{{asset('web/assets/vendor/modernizr/modernizr.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">


 @show

  @stack('post-styles')

  <script type="text/javascript">
    var BASE_URL = "{{ url('/')}}";
  </script>

</head>

<body>
<section class="body">
  @section('header-base')
  @include('_layouts.admin.partials._header-base')
  @show

  @section('aside-left')
  @include('_layouts.admin.partials._aside-left')
  
    @yield('content')
@show


</section>
</div>

@stack('pre-scripts')

@section('scripts')

<script src="{{asset('web/assets/vendor/jquery/jquery.js')}}"></script>
    <script src="{{asset('web/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
    <script src="{{asset('web/assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('web/assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
    <script src="{{asset('web/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('web/assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
    <script src="{{asset('web/assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
    
    <!-- Specific Page Vendor -->
    <script src="{{asset('web/assets/vendor/select2/select2.js')}}"></script>
    <script src="{{asset('web/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('web/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="{{asset('web/assets/javascripts/theme.js')}}"></script>
    
    <!-- Theme Custom -->
    <script src="{{asset('web/assets/javascripts/theme.custom.js')}}"></script>
    
    <!-- Theme Initialization Files -->
    <script src="{{asset('web/assets/javascripts/theme.init.js')}}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
    <!-- Examples -->
    <script src="{{asset('web/assets/javascripts/tables/examples.datatables.editable.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML' async></script>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({
  tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
});
</script>
{{-- <script type="text/javascript" async src="path-to-mathjax/MathJax.js?config=TeX-AMS_CHTML"></script>
 --}}


<script type="text/javascript">
  $.ajaxSetup({
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
 });



</script>





@show


@stack('post-scripts')

</body>
</html> 