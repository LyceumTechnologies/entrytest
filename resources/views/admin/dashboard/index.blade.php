<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Admin')
@section('content')
 <section role="main" class="content-body">
					<header class="page-header">
						<h2>Dashboard</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Dashboard</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open=""></a>
						</div>
					</header>

						
					<!-- start: page -->
					<div class="row">
						<a href="ManageTeacher.html">
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left panel-featured-primary">
								<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary">
														<i class="fa fa-life-ring"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">&nbsp;</h4>
														<div class="info">
															<strong class="amount">Manage Teacher</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase">(View Detail)</a>
													</div>
												</div>
											</div>
								</div>
							</section>
						</div>
						</a>

					<a href="ManageCourse.html">
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left panel-featured-primary">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-primary">
												<i class="fa fa-life-ring"></i>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">&nbsp;</h4>
												<div class="info">
													<strong class="amount">Manage Course</strong>
												</div>
											</div>
											<div class="summary-footer">
												<a class="text-muted text-uppercase">(View Detail)</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</a>
				</div>

					<div class="row">
						<a href="ManageUser.html">
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left panel-featured-primary">
								<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary">
														<i class="fa fa-life-ring"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">&nbsp;</h4>
														<div class="info">
															<strong class="amount">Manage User</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase">(View Detail)</a>
													</div>
												</div>
											</div>
								</div>
							</section>
						</div>
						</a>

						<a href="QuestionOfTheDay.html">
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left panel-featured-primary">
								<div class="panel-body">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-primary">
												<i class="fa fa-life-ring"></i>
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
												<h4 class="title">&nbsp;</h4>
												<div class="info">
													<strong class="amount">Question of the Day</strong>
												</div>
											</div>
											<div class="summary-footer">
												<a class="text-muted text-uppercase">(View Detail)</a>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						</a>
					</div>

					<div class="row">
						<a href="Event.html">
						<div class="col-md-12 col-lg-6 col-xl-6">
							<section class="panel panel-featured-left panel-featured-primary">
								<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary">
														<i class="fa fa-life-ring"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">&nbsp;</h4>
														<div class="info">
															<strong class="amount">Events</strong>
														</div>
													</div>
													<div class="summary-footer">
														<a class="text-muted text-uppercase">(View Detail)</a>
													</div>
												</div>
											</div>
								</div>
							</section>
						</div>
						</a>
					</div>
											
					<!-- end: page -->
				</section>
			</div>
@endsection
@push('post-styles')
			
@endpush
@push('post-scripts')
			
@endpush