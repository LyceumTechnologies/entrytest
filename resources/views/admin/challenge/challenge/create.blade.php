<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Challenge')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
   <h2>Manage Challenge</h2>
   <div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
     <li>
      <a href="index.html">
       <i class="fa fa-home"></i>
     </a>
   </li>
   <li><span>Index</span></li>
 </ol>
 <a class="sidebar-right-toggle" ></a>
</div>
</header>
<section class="panel">
 <header class="panel-heading">
  <h2 class="panel-title">Manage Challenge</h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="panel panel-primary basic_info_panel">
    <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
    text-transform: uppercase;">Basic Info</b></h4></div>
    @component('_components.alerts-default')
    @endcomponent
    <form method="POST" action="{{route('challenge.store')}}" enctype = "multipart/form-data">
     @csrf
     <div class="panel-body">
      <div class="row">


        <div class="col-md-9" style="margin-bottom: 20px;">
          <div class="row" style="margin: 10px 0px;">

            <div class="col-md-12">
             <label class="control-label">Course Name</label>
             <select name="course_id" id="course_id" type="text"  class="form-control course_id">
              @foreach($courses as $sub)
              <option value="{{$sub->id}}">{{$sub->name}}</option>
              @endforeach
            </select>
            @if ($errors->has('sb_name'))
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
              </button>
              <strong>Warning!</strong> {{$errors->first('sb_name')}}
            </div>
            @endif
          </div>



          <div class="col-md-12">
           <label class="control-label"> Challenge Name</label>
           <input name="challenge_name" value="{{ old('challenge_name') }}" type="text" placeholder="Challenge Name" class="form-control">
           @if ($errors->has('challenge_name'))
           <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('challenge_name')}}
          </div>
          @endif
        </div>


        <div class="col-md-12">
         <label class="control-label"> Challenge Join Price </label>
         <input name="challenge_price" value="{{ old('challenge_price') }}" type="number" min="0" placeholder="0" class="form-control">
         @if ($errors->has('challenge_price'))
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('challenge_price')}}
          </div>
            @endif
        </div>

        <div class="col-md-12">
         <label class="control-label"> Challenge Date </label>
         <input name="challenge_date" value="{{ now() }}" type="date" min="0" placeholder="0" class="form-control">
         @if ($errors->has('challenge_date'))
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('challenge_date')}}
          </div>
            @endif
        </div>

        <div class="col-md-12">
         <label class="control-label"> Reward Challenge </label>
         <input name="reward_challenge" value="{{ old('reward_challenge') }}" type="number" min="0" placeholder="0" class="form-control">
         @if ($errors->has('reward_challenge'))
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('reward_challenge')}}
          </div>
            @endif
        </div>

    </div>
    
    <div class="row">


      
          </div>

          <div class="col-md-12">
            <label class="control-label">Description: <span class="req"></span></label>
            <textarea  class="form-control" name="challenge_description" id="challenge_description" maxlength="150"></textarea>

          </div><!--form-group ends-->
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-actions">
  <div class="row">
    <div class="col-md-offset-3 col-md-9">

     <button class="btn btn-success" type="Submit" name="">Save</button>
   </form>
   <a href="{{route('subject.index')}}" class="btn  btn-primary" type="reset">Cancel</a>

 </div>
</div>
</div>
</div>
</div>
</div>
</section>
</section>
@endsection
@push('post-styles')

@endpush

@push('post-scripts')
<script type="text/javascript">

  function SomeDeleteRowFunction(o) {
    console.log(o);
    $(o).parents('.addChapterName').remove();
    var numItems = $('.addChapterName').length;
    console.log('count',numItems);
    if(numItems==0){
      console.log(numItems,'add More');
      $('.addMore').show();
    }

  }


  $(".addMore").click(function() {
    $("#subjectId").attr('readonly', 'true');
    option = `<div class="addChapterName">
    <div class="col-md-10">
    <label class="control-label">Sub Subject Name</label>

    <input name="sub_subject[]" type="text" placeholder="Sub Subject Name" class="form-control">
    </div>
    <div class="col-md-2" style="padding-top: 25px"> 
    <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
    <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
    </div>
    </div>`;


    $('.addMoreInput').html(option);
    $('.addMore').css('display', 'none');
  });
  function  SomeAddRowFunction(obj){
    console.log('add more',obj);
    option = `<div class="addChapterName">
    <div class="col-md-10">
    <label class="control-label">Sub Subject Name</label>

    <input name="sub_subject[]" type="text" placeholder="Sub Subject Name" class="form-control">
    </div>
    <div class="col-md-2" style="padding-top: 25px"> 
    <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
    <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
    </div>
    </div>`;

    console.log('add more data',option);
    $('.addMoreInput').append(option);
  }
</script>



@endpush