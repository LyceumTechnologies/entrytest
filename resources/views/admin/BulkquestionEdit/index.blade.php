<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', ' Question Edit')
@section('content')
<section role="main" class="content-body">
  <style>
    .tpic-tstname{
      float: left;
    }
    .tpic-tstname h3{
      margin-top: 0px;
    }
    .tptst-col-left {
      width: 100%;
      background: #fdfdfd;
      /* border: 1px solid; */
      padding: 4px;
      border-left: 4px solid #08c;
      box-shadow: 1px 0px 4px #444;
    }
    .btn-taketest{
      float: right;
    }
  </style>
  <header class="page-header">
    <h2>Manage  Question Edit</h2>
    <div class="right-wrapper pull-right">
      <ol class="breadcrumbs">
        <li>
          <a href="index.html">
            <i class="fa fa-home"></i>
          </a>
        </li>
        <li><span>Index</span></li>
      </ol>
      <a class="sidebar-right-toggle" ></a>
    </div>
  </header>
  <section class="panel">
    <header class="panel-heading">
      <h2 class="panel-title">Manage  Question Edit</h2>
    </header>
    <div class="panel-body">
      <div class="row">

        <div class="panel panel-primary basic_info_panel">
          <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
          text-transform: uppercase;"> Question Edit Info</b></h4></div>
          @component('_components.alerts-default')
          @endcomponent
<form method="POST" action="{{route('bulk-question-edit.store')}}">
  @csrf
          <div class="panel-body">
            <div class="row">
              <div class="col-md-9" style="margin-bottom: 20px;">
                <div class="row" style="margin: 10px 0px;">
                  <div class="col-md-12">
                   <label class="control-label">Subject Name</label>
                   <select name="sub_id"  type="text"  class="form-control sub_id" onchange="selectTopic(this)">
                    <option disabled="disabled" selected="selected">Choose the subject</option>
                    @foreach($subject as $sub)
                    <option value="{{$sub->id}}" @if($sub->id==Session()->get('sub_id')) selected @endif>{{$sub->sb_name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('name'))
                  <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('name')}}
                  </div>
                  @endif
                </div>
                
              </div> 
            </div> 

             
           </div>
           <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('teacher.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
         </div>
       </div>

     </div>

   </div>
 </div>
</div>
</section>
</section>
<style type="text/css">
  img{
    width: 100px,
    height:100px;

  }
</style>
@endsection
@push('post-styles')

@endpush
@push('post-scripts')

@endpush