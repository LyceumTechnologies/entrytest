<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Question Bulk Edit')
@section('content')

<section role="main" class="content-body">
	<header class="page-header">
		<h2>Manage Question Bulk Edit</h2>
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="{{url('admin')}}">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Index</span></li>
			</ol>
			<a class="sidebar-right-toggle" ></a>
		</div>
	</header>
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Manage Question Bulk Edit</h2>
		</header>
		<div class="panel-body">
			<div class="row">
				<form method="POST" action="{{route('question.editQuestionTopic')}}">
					@csrf
					<div class="col-md-9" style="margin-bottom: 20px;">
						<div class="row" style="margin: 10px 0px;">

							<div class="col-md-4">
								<label class="control-label">Subject Name</label>
								<select name="sub_id" id="sub_id"  required  type="text"  class="form-control sub_id" >
									<option disabled="disabled" selected="selected">Choose the subject</option>
									@foreach($subject as $sub)
									<option value="{{$sub->id}}" >{{$sub->sb_name}}</option>
									@endforeach
								</select>
								@if ($errors->has('name'))
								<div class="alert alert-danger" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">×</span>
										<span class="sr-only">Close</span>
									</button>
									<strong>Warning!</strong> {{$errors->first('name')}}
								</div>
								@endif
							</div>
							<div class="col-md-4">
								<label class="control-label"> Grade Name</label>
								<select name="grade_id" id="grade_id" value="{{ old('grade_id') }}" type="text" placeholder="Grade Name" class="form-control grade_id" required>
									<option  selected="selected" value="0">All Grade</option>
									@foreach($grade as $to)
									<option value="{{$to->id}}" >{{$to->grade_name}}</option>
									@endforeach
								</select>

							</div>
							<div class="col-md-4">
								<label class="control-label"> Chapter</label>
								<select name="chapter_id" id="chapter_id" value="{{ old('chapter_id') }}" type="text" class="form-control chapter_id" required>


								

								</select>

							</div>



						</div>
						<div class="row" style="margin: 10px 0px;">
							<div class="col-md-4">
								<label class="control-label"> Topic Name</label>
								<select name="topic_id" id="topic_id" value="{{ old('topic_id') }}" onchange="selectSubTopic(this)" type="text" placeholder="First Name" class="form-control">


								</select>

							</div>
							<div class="col-md-4">
								<label class="control-label"> Sub Topic Name</label >
									<select name="sub_topic_id" id="sub_topic_id" value="{{ old('sub_topic_id') }}" type="text" placeholder="First Name" class="form-control sub_topic_id">
									</select>
								</div>
								<div class="col-md-4">
									<label class="control-label"> Difficulty Level</label>
									<select name="dif_id" value="{{ old('dif_id') }}" type="text" placeholder="First Name" class="form-control dif_id" required>
										<option  selected="selected" value="0">All Difficulty</option>
										@foreach($difficulty as $tops)
										<option value="{{$tops->id}}" >{{$tops->difficulty}}</option>
										@endforeach
									</select>

								</div>
							</div>

							@component('_components.alerts-default')
							@endcomponent
							<h2>Select question for edit</h2>
							<table id="questionTable" class="table table-bordered table-striped mb-none">
								@php($index=1)
								@foreach($questions as $q)
								<tr class="{{$q->id}}"  style="margin-bottom: 15px" for="checkedInput_$q->id">



									<td >
										<div class="row answerApproved{{$q->id}}">
											<div class="" style="margin-bottom: 20px;max-width: 1024px;">
												<div class="row" style="margin: 10px 0px;">
													<div class="col-md-4">
														<label class="control-label">Subject Name</label> 
														<input   type="text"  class="form-control" readonly="" value="@isset($q->subject->sb_name){{$q->subject->sb_name}} @endisset" >

													</div>
													<div class="col-md-4">
														<label class="control-label"> Grade Name</label>
														<input   type="text"  class="form-control" readonly="" value="@isset($q->course->grade_name){{$q->course->grade_name}} @endisset" >

													</div>
													<div class="col-md-4">
														<label class="control-label"> Chapter Name</label>
														<input   type="text"  class="form-control " readonly="" value="@isset($q->chapter->chaper_name){{$q->chapter->chaper_name}} @endisset" >
													</div>


													<div class="col-md-4">
														<label class="control-label"> Topic Name</label>
														<input   type="text"  class="form-control" readonly="" value="@isset($q->topic->name){{$q->topic->name}} @endisset" >


													</div>

													<div class="col-md-4">
														<label class="control-label"> Sub Topic Name</label>
														<input   type="text"  class="form-control" readonly="" value="@isset($q->SubTopic->sub_topic_name){{$q->SubTopic->sub_topic_name}} @endisset" >
													</div>

													<div class="col-md-4">
														<label class="control-label"> Difficulty Level</label>

														<input   type="text"  class="form-control" readonly="" value="@isset($q->difficulty->difficulty){{$q->difficulty->difficulty}} @endisset" >


													</div>

												</div>
											</div> 
										</div> 

										<div>
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"> <input name="question_ids[]"   type="checkbox" class="checkboxes custom-check  checkedInput_$q->id" id="checkedInput_$q->id" value="{{$q->id}}" />  <span></span></label>
											<strong>Question {{$index}}:</strong> {!! $q->question !!} </h4>

											<div><a class="btn btn-warning" href="javascript:;" onclick="getAnswer({{$q->id}})">Answers</a></div>
											<div class="question_answer_{{$q->id}}">

											</div>
										</div>




									</td>
								</tr>
							</form>
							@php($index++)
							@endforeach



						</table>
					</div>
					<div class="row">
						<button type="submit" class="btn btn-success">Update</button>
					</div>
				</form>
			</div>
		</section>


		@endsection

		@push('post-styles')
		<link href="{{asset('assets/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('assets/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
		@endpush

		@push('post-scripts')
		<script src="{{asset('assets/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/jszip.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
		<script src="{{asset('assets/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.js"></script>

		<script type="text/javascript">


			var old_topic_id;
			var old_chapter_id;
			var old_sub_topic_id;

			function  getAnswer(obj){
				$.ajax({
					method:"POST",
					url:"{{route('question.questionGetAnswer')}}",
					data : {question_id:obj},
					dataType:"json",
					success:function(res){
						var option=``;
						if(res.status){
							res.data.forEach(function(val,ind){
								var id = val.id;
								var name = val.ans;
								 option += `<p>${name}</p>`;
							});

							$('.question_answer_'+obj).html(option);
						}

					}
				});

			}

			$("[name='sub_id']").add("[name='grade_id']").on('on change',function() {
				SelectChapter();
			});

			$("[name='sub_id']").add("[name='grade_id']").add("[name='chapter_id']").on('on change',function() {
				SelectTopic();
			});

			function SelectChapter(obj){
				console.log(obj);
				$("[name='chapter_id']").html(` <option selected="selected" disabled='disabled' value="0"> Select Chapter  </option>`);
				var sub_id  = $("#sub_id").val();
				var grade_id  = $("#grade_id").val();

				$.ajax({
					method:"POST",
					url:"{{route('SubjectChapter')}}",
					data : {sub_id:sub_id,grade_id:grade_id},
					dataType:"json",
					success:function(res){
						if(res.status){
							res.data.forEach(function(val,ind){
								var id = val.id;
								var name = val.chaper_name;
								var option = `<option value="${id}" ${id==old_chapter_id?'selected':''}>${name}</option>`;
								$("[name='chapter_id']").append(option);
							});


						}

					}
				});
			}

			function SelectTopic(){
				$("[name='topic_id']").html(` <option selected="selected" disabled='disabled' value="0"> Select Topic  </option>`);
				var sub_id  = $("#sub_id").val();
				var grade_id  = $("#grade_id").val();
				var chapter_id  = $("#chapter_id").val();

				$.ajax({
					method:"POST",
					url:"{{route('ChapterHasTopic')}}",
					data : {sub_id:sub_id,grade_id:grade_id,chapter_id:chapter_id},
					dataType:"json",
					success:function(res){
						console.log('old_topic_id',old_topic_id);
						if(res.status){
							res.data.forEach(function(val,ind){
								var id = val.id;
								var name = val.name;
								var option = `<option value="${id}" ${id==old_topic_id?'selected':''}>${name}</option>`;
								$("[name='topic_id']").append(option);
							});
						}

					}
				});
			}


			function selectSubTopic(obj){
				$("#sub_topic_id").first().html(` <option value="0" selected='selected' value="0"> Select sub topic  </option>`);
				var topic_id  = $('#topic_id').val();

				console.log('topic change value',topic_id);
				$.ajax({
					method:"POST",
					url:"{{route('TopicHasSubtopic')}}",
					data : {id:topic_id},
					dataType:"json",
					success:function(res){
						console.log('sub topic response',res);
						if(res.status){
							res.data.forEach(function(val,ind){
								var id = val.id;
								var name = val.sub_topic_name;
								var option = `<option value="${id}" ${id==old_sub_topic_id?'selected':''}>${name}</option>`;
								$("#sub_topic_id").append(option);
							});
						}
						
					}
				});

			}



			$(function(){
				var sub_id=$('.sub_id').val();
				selectTopic();
				var sub_id=$('.sub_id').val();
				var topic_id=$('.sub_id').attr('data-topic_id');


				SelectChapter();
				SelectTopic();
				selectSubTopic(topic_id);
			})


		</script>
		@endpush


