<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Edit Past Paper')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
   <h2>Edit Past Paper</h2>
   <div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
     <li>
      <a href="index.html">
       <i class="fa fa-home"></i>
     </a>
   </li>
   <li><span>Index</span></li>
 </ol>
 <a class="sidebar-right-toggle" ></a>
</div>
</header>
<section class="panel">
 <header class="panel-heading">
  <h2 class="panel-title">Edit Past Paper</h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="panel panel-primary basic_info_panel">
    <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
    text-transform: uppercase;">Basic Info</b></h4></div>
    @component('_components.alerts-default')
    @endcomponent
    <form method="POST" action="{{route('past-paper.update',$data->id)}}" enctype = "multipart/form-data">
     @csrf
     @method('PATCH')
     <div class="panel-body">
      <div class="row">


        <div class="col-md-9" style="margin-bottom: 20px;">
          <div class="row" style="margin: 10px 0px;">

            <div class="col-md-12">
             <label class="control-label">Course Name</label>
             <select name="course_id" id="course_id" type="text"  class="form-control course_id">
              @foreach($courses as $sub)
              <option value="{{$sub->id}}" @if($data->course_id==$sub->id) selected @endif>{{$sub->name}}</option>
              @endforeach
            </select>
            @if ($errors->has('sb_name'))
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
              </button>
              <strong>Warning!</strong> {{$errors->first('sb_name')}}
            </div>
            @endif
          </div>

          <div class="col-md-12">
           <label class="control-label"> Past Paper Name</label>
           <input name="past_paper_name" value="{{ $data->past_paper_name }}" type="text" placeholder="Past Paper Name" class="form-control">
           @if ($errors->has('past_paper_name'))
           <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('past_paper_name')}}
          </div>
          @endif
        </div>


        <div class="col-md-12">
         <label class="control-label"> Year </label>
         <input name="year" value="{{ $data->year}}" type="number" min="0" placeholder="Past Paper Name" class="form-control">
         @if ($errors->has('year'))
         <div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <strong>Warning!</strong> {{$errors->first('year')}}
        </div>
        @endif
      </div>

    </div>
    

  </div>
</div>
</div>
</div>

<div class="form-actions">
  <div class="row">
    <div class="col-md-offset-3 col-md-9">

     <button class="btn btn-success" type="Submit" name="">Save</button>
   </form>

 </div>
</div>
</div>
</div>
</div>
</div>
</section>
</section>
@endsection
@push('post-styles')

@endpush

@push('post-scripts')




@endpush