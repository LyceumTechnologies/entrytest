<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'User Package')
@section('content')
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage User Package</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage User Package</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="panel panel-primary basic_info_panel">
              
              @component('_components.alerts-default')
        @endcomponent
              <form method="POST" action="{{route('user-package.store')}}" id="UserPackage" enctype = "multipart/form-data">
              @CSRF              
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-9" style="margin-bottom: 20px;">
                    

                    <div class="row" style="margin: 10px 0px;">
                      <div class="col-md-8">
                        <label class="control-label">Email</label>
                        <input name="email" type="email" value="{{ old('email') }}" class="form-control email" placeholder="abc123@example.com">
                        <input type="hidden" name="user_id" class="user_id">
                        @if ($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('email')}}
                        </div>
                        @endif
                      </div><!--form-group ends-->
                    </div> 

                 <div class="row" style="margin: 10px 0px;">
                  <div class="col-md-8">
                    <label class="control-label">Package</label>
                    <select name="packageId" type="text" value="{{ old('packageId') }}" class="form-control packageId">
                      <option disabled="disabled" selected="">please select the package</option>
                      @foreach($package as $use)
                        <option value={{$use->id}}>{{$use->name}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('packageId'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('packageId')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->
              </div> 

                <div class="row" style="margin: 10px 0px;">
                  <div class="col-md-8">
                    <label class="control-label">Transaction Id</label>
                    <input name="transactionId" type="text" value="{{ old('transactionId') }}" class="form-control transactionId" placeholder="transactionId">
                    @if ($errors->has('transactionId'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('transactionId')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->
              </div> 
             

              <div class="row" style="margin: 10px 0px;">

                <div class="col-md-8">
                  <label class="control-label">Amount</label>
                  <input type="number" placeholder="00000" class="form-control amount" name="amount">
                  @if ($errors->has('amount'))
                  <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('amount')}}
                  </div>
                  @endif
                </div><!--form-group ends-->

              
            	</div> 
          	</div>
        </div>

      </div>
            </div>
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success UserPackageSubmit" type="Submit" name="">Save</button>
                     </form>
                  <a href="{{route('teacher.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
				</div>
			</div>
		</section>
	</section>
@endsection
@push('post-styles')
			
@endpush
@push('post-scripts')
		<script type="text/javascript">
 $(function(){
    $("#UserPackage :input").prop("disabled", true);
    $('.email').prop("disabled",false);
    $(".UserPackageSubmit").attr("disabled", true);

    // $('.UserPackageSubmit"]').prop('disabled', false);

  });
  $(".email").on('keyup',function(e){
    $.ajax({
      method:'POST',
      url:"{{route('userInformation')}}",
      dataType:'json',
      data:{email:$('.email').val()},
      success:function(data){
        console.log('application form',data);
        if(data.status){
          $('.user_id').val(data.record.id);
          $(".UserPackageSubmit").attr("disabled", false);;
          $("#UserPackage :input").prop("disabled", false);
        
          
        }else{
          
        $(".amount").prop("disabled", true);
        $(".transactionId").prop("disabled", true);
        $(".packageId").prop("disabled", true);
        $(".UserPackageSubmit").attr("disabled", true);
          console.log('print not done');
        }
      }

    });
  });

</script>

@endpush