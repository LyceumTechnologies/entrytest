<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Course Subject')
@section('content')
<section role="main" class="content-body">
	<style>
		.tpic-tstname{
			float: left;
		}
		.tpic-tstname h3{
			margin-top: 0px;
		}
		.tptst-col-left {
			width: 100%;
			background: #fdfdfd;
			/* border: 1px solid; */
			padding: 4px;
			border-left: 4px solid #08c;
			box-shadow: 1px 0px 4px #444;
		}
		.btn-taketest{
			float: right;
		}
	</style>
	<header class="page-header">
		<h2>Manage Course Subject</h2>
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.html">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Index</span></li>
			</ol>
			<a class="sidebar-right-toggle" ></a>
		</div>
	</header>
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Manage Course Subject</h2>
		</header>
		<div class="panel-body">
			<div class="row">
				<div class="content">
					@component('_components.alerts-default')
					@endcomponent
					<div id="accordion">
						<div class="panel list-group">
							<!-- panel class must be in -->
							@foreach($section as $topic)
							<a href="#web{{$topic->id}}" data-parent="#accordion" data-toggle="collapse" class="list-group-item" aria-expanded="true">
								<h4>{{$topic->name}}
									<span aria-hidden="true" style="float: right;">
										<i class="ti-angle-right"></i>
									</span></h4>
								</a>
								
								<div class="collapse " id="web{{$topic->id}}" style="">
									<ul class="list-group-item-text">
										@foreach($topic->couseSubject as $subTopic)
										<a href="{{url('admin/subject/topic-section/'.$topic->id.'/'.$subTopic->subject->id)}}"><li>{{$subTopic->subject->sb_name}}</li></a>
										@endforeach
										
									</ul>
								</div>
								@endforeach



							</div>
						</div>
					</div>


				</div>
				<div class="row">
					<table id="example" class="table border table-bordered">
						<thead>
							<tr style=" color: white; background: linear-gradient(#0088cc, #fff) !important; font-weight: unset;">
								<th>#id</th>
								<th>Course Name</th>
								<th>Subject</th>
								<th>Topics</th>
							</tr>
						</thead>
						<tbody>
							@isset($section)
							@foreach($section as $pro)
							<tr>
								<td>@isset($pro->id){{$pro->id}}@endisset</td>
								<td>@isset($pro->name){{$pro->name}}@endisset</td>
								<?php 
								$subjectArray=array();
								foreach($pro->couseSubject as $courSubject){
									array_push($subjectArray, $courSubject->subject->sb_name);
								}
								?>
								<td>@if(count($subjectArray)){{implode(',',$subjectArray)}}@endif</td>
								<td></td>

								
							</tr>
							@endforeach
							@endisset
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</section>

@endsection
@push('post-styles')
	<link href="{{asset('assets/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
@endpush

@push('post-scripts')
<script src="{{asset('assets/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>

<script>
	$(document).ready(function() {
     //Default data table
     $('#default-datatable').DataTable();


     var table = $('#example').DataTable( {
     	"order": [[ 0, "desc" ]],
     	lengthChange: false,
     	buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
     } );

     table.buttons().container()
     .appendTo( '#example_wrapper .col-md-6:eq(0)' );

 } );
</script>
@endpush

