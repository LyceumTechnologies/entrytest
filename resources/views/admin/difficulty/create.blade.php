<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Difficulty')
@section('content')
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage Difficulty</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage Difficulty</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="panel panel-primary basic_info_panel">
             
              @component('_components.alerts-default')
        @endcomponent
              <form method="POST" action="{{route('difficulty.store')}}" enctype = "multipart/form-data">
              	@csrf
              <div class="panel-body">
                <div class="row">
                  

                  <div class="col-md-9" style="margin-bottom: 20px;">
                  
                  <div class="row" style="margin: 10px 0px;">
                      <div class="col-md-12">
                        <label class="control-label">Difficulty Name</label>
                          <input name="difficulty" type="text" placeholder="Difficulty Name" class="form-control" required>
                         
                      </div>
                     
                  </div>
                
                </div>
              </div>
            </div>
        </div>
              
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('difficulty.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
				</div>
			</div>
		</section>
	</section>
@endsection
@push('post-styles')
			
@endpush
@push('post-scripts')
		<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 
        function SomeDeleteRowFunction(o) {
          console.log(o);
            $(o).parents('.addChapterName').remove();
            var numItems = $('.addChapterName').length;
            console.log('count',numItems);
            if(numItems==0){
              console.log(numItems,'add More');
              $('.addMore').show();
            }

        }

       
        $(".addMore").click(function() {
            $("#subjectId").attr('readonly', 'true');
            option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Grade Name</label>
                              <input name="grade_name[]" type="text" placeholder="Grade Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            

            $('.addMoreInput').html(option);
            $('.addMore').css('display', 'none');
        });
       function  SomeAddRowFunction(obj){
          console.log('add more',obj);
          option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Grade Name</label>
                              <input name="grade_name[]" type="text" placeholder="Grade Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            
            console.log('add more data',option);
            $('.addMoreInput').append(option);
       }
</script>

@endpush