<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Topics')
@section('content')
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage Topics</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage Topics</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="panel panel-primary basic_info_panel">
             
              @component('_components.alerts-default')
              @endcomponent
              <form method="POST" action="{{route('chapter.store')}}" enctype = "multipart/form-data">
              	@csrf
              <div class="panel-body">
                <div class="row">
                  

                  <div class="col-md-9" style="margin-bottom: 20px;">
                    <div class="row" style="margin: 10px 0px;">
                       <div class="col-md-12">
                   <label class="control-label"> Grade Name</label>
                   <select name="grade_id" value="{{ old('grade_id') }}" type="text" placeholder="Grade Name" class="form-control grade_id" required>
                     <option  selected="selected" value="0">All Grade</option>
                     @foreach($grade as $to)
                     <option value="{{$to->id}}" >{{$to->grade_name}}</option>
                     @endforeach
                   </select>

                 </div>

                      <div class="col-md-12">
                       <label class="control-label">Subject Name</label>
                       <select name="sb_name" id="subjectId" type="text" placeholder="First Name" class="form-control">
                        <option disabled="disabled" selected>Choose the subject name</option>
                        @foreach($subject as $sub)
                        <option value="{{$sub->id}}">{{$sub->sb_name}}</option>
                        @endforeach
                       </select>
                       @if ($errors->has('sb_name'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('sb_name')}}
                      </div>
                      @endif
                    </div>
                    <div class="col-md-12">
                       <label class="control-label">Chapter Name</label>
                       <select name="chapter_id" id="chapter_id" type="text" placeholder="First Name" class="form-control">
                        
                       </select>
                       @if ($errors->has('chapter_id'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('chapter_id')}}
                      </div>
                      @endif
                    </div>
                  </div>
                  <div class="row" style="margin: 10px 0px;">
                      <div class="col-md-10">
                        <label class="control-label">Topic Name Name</label>
                          <input name="name[]" type="text" placeholder="Topic Name" class="form-control">
                          @if ($errors->has('name'))
                            <div class="alert alert-danger" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                              </button>
                              <strong>Warning!</strong> {{$errors->first('name')}}
                            </div>
                          @endif
                      </div>
                      <div class="col-md-2" style="padding-top: 25px"> 
                        <a class="addMore btn btn-success firstBtn" href="javascript:void(0)">+</a>
                      </div>
                      <div class=" addMoreInput">
                        
                      </div>
                  </div>
                
                </div>
              </div>
            </div>
        </div>
              
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('chapter.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
				</div>
			</div>
		</section>
	</section>
@endsection
@push('post-styles')
			
@endpush
@push('post-scripts')
		<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 

  $("#subjectId").add("[name='grade_id']").on('on change',function() {

    $("[name='chapter_id']").html(` <option selected="selected" disabled='disabled'> Select chapter  </option>`);
    var sub_id  = $("#subjectId").val();
    var grade_id  = $("[name='grade_id']").val();

    $.ajax({
      method:"POST",
      url:"{{route('SubjectHasChapter')}}",
      data : {sub_id:sub_id,grade_id:grade_id},
      dataType:"json",
      success:function(res){
        console.log(res);
        res.data.forEach(function(val,ind){
          var id = val.id;
          var name = val.chaper_name;
          var option = `<option value="${id}">${name}</option>`;
          $("[name='chapter_id']").append(option);
        });
      }
    });

  });


        function SomeDeleteRowFunction(o) {
          console.log(o);
            $(o).parents('.addChapterName').remove();
            var numItems = $('.addChapterName').length;
            console.log('count',numItems);
            if(numItems==0){
              console.log(numItems,'add More');
              $('.addMore').show();
            }

        }

       
        $(".addMore").click(function() {
            $("#subjectId").attr('readonly', 'true');
            option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Topic Name Name</label>
                              <input name="name[]" type="text" placeholder="Topic Name Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            

            $('.addMoreInput').html(option);
            $('.addMore').css('display', 'none');
        });
       function  SomeAddRowFunction(obj){
          console.log('add more',obj);
          option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Topic Name Name</label>
                              <input name="name[]" type="text" placeholder="Topic Name Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            
            console.log('add more data',option);
            $('.addMoreInput').append(option);
       }
</script>

@endpush