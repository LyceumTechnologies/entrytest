<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Reports')
@section('content')

	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage Reports</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage Reports</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="mb-md" style="float:left;">
<!-- 							<a class="btn btn-primary" href="{{route('admin.create')}}">Add Admin &nbsp;<i class="fa fa-plus"></i></a>
 -->						</div>
					</div>
				</div>
			
					@component('_components.alerts-default')
        		@endcomponent
					<table id="example" class="table border table-bordered">
						<thead>
							<tr style=" color: white; background: linear-gradient(#0088cc, #fff) !important; font-weight: unset;">
								<th>#id</th>
								<th>Name</th>
								<th>Today Entries</th>
								<th>Total Entries</th>

								<th>Today past Entries</th>
								<th>Today past Entries</th>

								<th>Total Approved</th>
								<th>Dis Approved</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
									@isset($user)
									@foreach($user as $pro)
									<tr>
										<td>@isset($pro->id){{$pro->id}}@endisset</td>
									
										<td>@isset($pro->name){{$pro->name}}@endisset</td>
										<td>{{$pro->todayCountQuestion()}}</td>
										
										<td>{{$pro->UserHasQuestion()}}</td>

										<td>{{$pro->todayCountPastQuestion()}}</td>
										<td>{{$pro->UserHasPastQuestion()}}</td>


										<td>{{$pro->UserApprovedQuestion()}}</td>
										<td>{{$pro->ownDisApprovedCount()}}</td>
										<td>{{-- <a href="{{route('report.show',$pro->id)}}" class="btn btn-info btn-sm">Detail</a> --}}</td>
									</tr>
									@endforeach
									@endisset
						</tbody>
					</table>
				</div>
			</div>
		</section>


@endsection

@push('post-styles')
	<link href="{{asset('assets/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
@endpush

@push('post-scripts')
<script src="{{asset('assets/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>

<script>
	$(document).ready(function() {
     //Default data table
     $('#default-datatable').DataTable();


     var table = $('#example').DataTable( {
     	"order": [[ 0, "desc" ]],
     	lengthChange: false,
     	"pageLength": 25,
     	buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
     } );

     table.buttons().container()
     .appendTo( '#example_wrapper .col-md-6:eq(0)' );

 } );
</script>
@endpush


