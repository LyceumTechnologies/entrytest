<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Note')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
   <h2>Manage Note</h2>
   <div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
     <li>
      <a href="index.html">
       <i class="fa fa-home"></i>
     </a>
   </li>
   <li><span>Index</span></li>
 </ol>
 <a class="sidebar-right-toggle" ></a>
</div>
</header>
<section class="panel">
 <header class="panel-heading">
  <h2 class="panel-title">Manage Note</h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="panel panel-primary basic_info_panel">
    <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
    text-transform: uppercase;">Basic Info</b></h4></div>
    @component('_components.alerts-default')
    @endcomponent
    <form method="POST" action="{{route('notes.store')}}" enctype = "multipart/form-data">
     @csrf
     <div class="panel-body">
      <div class="row">


        <div class="col-md-9" style="margin-bottom: 20px;">
          <div class="row" style="margin: 10px 0px;">
            <div class="col-md-6">
             <label class="control-label"> Grade Name</label>
             <select name="grade_id" value="{{ old('grade_id') }}" id="grade_id" type="text" placeholder="Grade Name" class="form-control grade_id" required>
               <option  selected="selected" value="0">All Grade</option>
               @foreach($grade as $to)
               <option value="{{$to->id}}" >{{$to->grade_name}}</option>
               @endforeach
             </select>

           </div>

           <div class="col-md-6">
             <label class="control-label">Subject Name</label>
             <select name="sub_id"  type="text"  class="form-control" id="sub_id" required>
              <option disabled="disabled" selected="selected">Choose the subject</option>
              @foreach($subject as $sub)
              <option value="{{$sub->id}}" @if($sub->id==Session()->get('sub_id')) selected @endif>{{$sub->sb_name}}</option>
              @endforeach
            </select>
          </div>

          <div class="col-md-6">
           <label class="control-label"> Chapter</label>
           <select name="chapter_id" id="chapter_id" value="{{ old('chapter_id') }}" type="text" class="form-control chapter_id" required>

           </select>

         </div>

         <div class="col-md-6">
           <label class="control-label"> Topic Name</label>
           <select name="topic_id" id="topic_id" value="{{ old('topic_id') }}" onchange="selectSubTopic()" type="text" placeholder="First Name" class="form-control">

           
          </select>

        </div>

        <div class="col-md-6">
          <label class="control-label"> Sub Topic Name</label required>
            <select name="sub_topic_id" id="sub_topic_id" value="{{ old('sub_topic_id') }}" type="text" placeholder="First Name" class="form-control sub_topic_id">
            </select>
          </div>


          <div class="col-md-6">
           <label class="control-label">Notes File</label>
           <input name="notes_file" value="{{ old('notes_file') }}" type="file" placeholder="Notes Title" class="form-control">
           @if ($errors->has('notes_file'))
           <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('notes_file')}}
          </div>
          @endif
        </div>




          <div class="col-md-12">
           <label class="control-label">Notes Title</label>
           <input name="notes_title" value="{{ old('notes_title') }}" type="text" placeholder="Notes Title" class="form-control">
           @if ($errors->has('notes_title'))
           <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('notes_title')}}
          </div>
          @endif
        </div>

      </div>



      <div class="col-md-12">
        <label class="control-label">Note Description: <span class="req"></span></label>
        <textarea  class="form-control" name="notes_description" id="notes_description" maxlength="150"></textarea>

      </div>
    </div>
  </div>
</div>
</div>
</div>

<div class="form-actions">
  <div class="row">
    <div class="col-md-offset-3 col-md-9">

     <button class="btn btn-success" type="Submit" name="">Save</button>
   </form>
   <a href="{{route('subject.index')}}" class="btn  btn-primary" type="reset">Cancel</a>

 </div>
</div>
</div>
</div>
</div>
</div>
</section>
</section>
@endsection
@push('post-styles')

@endpush

@push('post-scripts')
<script type="text/javascript">

    $("[name='sub_id']").add("[name='grade_id']").on('on change',function() {
        SelectChapter();
    });

    $("[name='sub_id']").add("[name='grade_id']").add("[name='chapter_id']").on('on change',function() {
        SelectTopic();
    });

    function SelectChapter(obj){
        console.log(obj);
        $("[name='chapter_id']").html(` <option selected="selected" disabled='disabled' value="0"> Select Chapter  </option>`);
        var sub_id  = $("#sub_id").val();
        var grade_id  = $("#grade_id").val();
        
        $.ajax({
          method:"POST",
          url:"{{route('SubjectChapter')}}",
          data : {sub_id:sub_id,grade_id:grade_id},
          dataType:"json",
          success:function(res){
            if(res.status){
                 res.data.forEach(function(val,ind){
                  var id = val.id;
                  var name = val.chaper_name;
                  var option = `<option value="${id}">${name}</option>`;
                  $("[name='chapter_id']").append(option);
                });
            }
           
        }
    });
  }

  function SelectTopic(){
        $("[name='topic_id']").html(` <option selected="selected" disabled='disabled' value="0"> Select Topic  </option>`);
        var sub_id  = $("#sub_id").val();
        var grade_id  = $("#grade_id").val();
        var chapter_id  = $("#chapter_id").val();
        
        $.ajax({
          method:"POST",
          url:"{{route('ChapterHasTopic')}}",
          data : {sub_id:sub_id,grade_id:grade_id,chapter_id:chapter_id},
          dataType:"json",
          success:function(res){
            console.log(res);
            if(res.status){
                res.data.forEach(function(val,ind){
                  var id = val.id;
                  var name = val.name;
                  var option = `<option value="${id}">${name}</option>`;
                  $("[name='topic_id']").append(option);
                });
            }
            
          }
        });
  }
 function selectSubTopic(obj){
    $("#sub_topic_id").first().html(` <option value="0" selected='selected' value="0"> Select sub topic  </option>`);
    var topic_id  = $('#topic_id').val();
    
    console.log('topic change value',topic_id);
    $.ajax({
      method:"POST",
      url:"{{route('TopicHasSubtopic')}}",
      data : {id:topic_id},
      dataType:"json",
      success:function(data){
        console.log('sub topic response',data);
        data.forEach(function(val,ind){
          var id = val.id;
          var name = val.sub_topic_name;
          var option = `<option value="${id}">${name}</option>`;
          $("#sub_topic_id").append(option);
        });
      }
    });
    
  }






</script>

@endpush