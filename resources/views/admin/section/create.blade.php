<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Topic')
@section('content')
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage Topic</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage Topic</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="panel panel-primary basic_info_panel">
             
              @component('_components.alerts-default')
        @endcomponent
              <form method="POST" action="{{route('topic.store')}}" enctype = "multipart/form-data">
              	@csrf
              <div class="panel-body">
                <div class="row">
                  

                  <div class="col-md-9" style="margin-bottom: 20px;">
                    <div class="row" style="margin: 10px 0px;">

                      <div class="col-md-12">
                       <label class="control-label">Topic Name</label>
                       <select name="topic_id"  type="text" placeholder="Subject Name" class="form-control" required>
                        <option disabled="disabled" selected>Choose the Topic name</option>
                        @foreach($subject as $sub)
                        <option value="{{$sub->id}}">{{ (isset($sub->subject->sb_name)?$sub->subject->sb_name:'').' - '. $sub->name}}</option>
                        @endforeach
                       </select>
                       
                    </div>
                  </div>
                 

                  <div class="row" style="margin: 10px 0px;">
                      <div class="col-md-10">
                        <label class="control-label">Sub Topic Name</label>
                          <input name="sub_topic[]" type="text" placeholder="Sub Topic Name" class="form-control">
                          
                      </div>
                      <div class="col-md-2" style="padding-top: 25px"> 
                        <a class="addMore btn btn-success firstBtn" href="javascript:void(0)">+</a>
                      </div>
                      <div class=" addMoreInput">
                        
                      </div>
                  </div>
                  

                
                </div>
              </div>
            </div>
        </div>
              
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('topic.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
				</div>
			</div>
		</section>
	</section>
@endsection
@push('post-styles')
@endpush

@push('post-scripts')
		<script type="text/javascript">
 
        function SomeDeleteRowFunction(o) {
          console.log(o);
            $(o).parents('.addChapterName').remove();
            var numItems = $('.addChapterName').length;
            console.log('count',numItems);
            if(numItems==0){
              console.log(numItems,'add More');
              $('.addMore').show();
            }

        }

       
        $(".addMore").click(function() {
            $("#subjectId").attr('readonly', 'true');
            option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Topic Name</label>
                              <input name="sub_topic[]" type="text" placeholder="Topic Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            

            $('.addMoreInput').html(option);
            $('.addMore').css('display', 'none');
        });
       function  SomeAddRowFunction(obj){
          console.log('add more',obj);
          option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Topic Name</label>
                              <input name="sub_topic[]" type="text" placeholder="Topic Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            
            console.log('add more data',option);
            $('.addMoreInput').append(option);
       }
</script>

@endpush