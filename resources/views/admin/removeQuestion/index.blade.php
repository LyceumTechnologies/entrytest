<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>
@extends('_layouts.admin.default')
@section('title', 'Question')
@section('content')
<style>
p {
    margin: 0 0 10px;
    max-width: 100px;
}
p img{
    margin: 0 0 10px;
    max-width: 100px;
}
</style>
<section role="main" class="content-body">
	<header class="page-header">
		<h2>Manage Question</h2>
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="{{url('admin')}}">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Index</span></li>
			</ol>
			<a class="sidebar-right-toggle" ></a>
		</div>
	</header>
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Manage Question</h2>
		</header>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-12">
					
				</div>
			</div>
			
			@component('_components.alerts-default')
			@endcomponent
			<table id="example" class="table border table-bordered">
				<thead>
					<tr style=" color: white; background: linear-gradient(#0088cc, #fff) !important; font-weight: unset;">
						<th>#id</th>
						<th>Question</th>
						<th>Ans A</th>
						<th>Ans B</th>
						<th>Ans C</th>
						<th>Ans D</th>
						<th>Correct</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@isset($questions)
					@foreach($questions as $pro)
					<tr class="question{{$pro->id}}">
						<td>@isset($pro->id){{$pro->id}}@endisset</td>
						<td>@isset($pro->question){!! $pro->question !!}@endisset</td>
						<td>@isset($pro->answers[0]){!! $pro->answers[0]->ans !!}@endisset</td>
						<td>@isset($pro->answers[1]){!! $pro->answers[1]->ans !!}@endisset</td>
						<td>@isset($pro->answers[2]){!! $pro->answers[2]->ans !!}@endisset</td>
						<td>@isset($pro->answers[3]){!! $pro->answers[3]->ans !!}@endisset</td>
						<td>@isset($pro->answers){!! AnserCorrect($pro->answers) !!}@endisset</td>
						<td><a href="javascript:;" onclick="deleteQuestion({{$pro->id}})" class="btn btn-info btn-sm">Remove</a></td>
					</tr>
					@endforeach
					@endisset
				</tbody>
			</table>
		</div>
	</div>
</section>


@endsection

@push('post-styles')
<link href="{{asset('assets/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
@endpush

@push('post-scripts')
<script src="{{asset('assets/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>

<script>
	$(document).ready(function() {
     //Default data table
     $('#default-datatable').DataTable();


     var table = $('#example').DataTable( {
     	"order": [[ 0, "desc" ]],
     	lengthChange: false,
     	"pageLength": 50,
     	buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
     } );

     table.buttons().container()
     .appendTo( '#example_wrapper .col-md-6:eq(0)' );

 } );

	function showConfirmDelete() {
            return confirm("Are You Sure You Want To Delete This Data?");
    }

	function deleteQuestion(id){
		var val = showConfirmDelete();
        if (val) {
			$.ajax({
			    method:"POST",
			    url:"{{route('deleteQuestion')}}",
			    data : {id:id},
			    dataType:"json",
			    success:function(data){
			    console.log('response',data);
			      	if(data.status){
				       $('.question'+id).closest('tr').remove();

				       swal("Question Delete Successfully", "Well done", "success");
			     	}else{
			     		swal("Failed to delete question", "danger");
			     	}
			   }
			});
		}
	}
</script>
@endpush


