<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>
@extends('_layouts.admin.default')
@section('title', 'Teacher Edit')
@section('content')
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage Teacher Edit</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage Teacher Edit</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="panel panel-primary basic_info_panel">
              <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
              text-transform: uppercase;">Basic Info</b></h4></div>
              @component('_components.alerts-default')
        @endcomponent
              <form method="POST" action="{{route('teacher.update',$teacher->id)}}" enctype = "multipart/form-data">
              	@csrf
              	@method('PATCH')
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-3">
                    <div class="fileinput fileinput-new" data-provides="fileinput" >
                      <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;">
                          <img src="@if($teacher->images){{asset('images/teacher/'.$teacher->images)}}@else{{asset('images/no-image.png')}}@endif" id="profile-img-tag" height="250" width = "250">
                        </div>
                      </div>
                      <div class="form-group">
                       <label for="images" class="btn btn-primary" style="position: relative;left: 50px;top: 20px;">Upload Profile</label>
                       <input type="file" id="images" value="{{ old('images') }}" name="images" class="hide" style="opacity: 0;">
                       @if ($errors->has('images'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('images')}}
                      </div>
                      @endif
                    </div>
                    <!-- cnic image upload here -->
                    <div class="col-md-4">
                      <div class="form-group" style="padding: 0px; ">
                        <div class = "gallery"></div>
                      </div>
                    </div>
                  </div><!--form-group ends-->

                  <div class="col-md-9" style="margin-bottom: 20px;">
                    <div class="row" style="margin: 10px 0px;">

                      <div class="col-md-12">
                       <label class="control-label"> Name</label>
                       <input name="name" value="@isset($teacher->name){{$teacher->name}}@endisset" type="text" placeholder="First Name" class="form-control">
                       @if ($errors->has('name'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('name')}}
                      </div>
                      @endif
                    </div>

                </div>

                <div class="row" style="margin: 10px 0px;">

                  <div class="col-md-6">
                    <label class="control-label">Email</label>
                    <input name="email" type="email" value="@isset($teacher->email){{$teacher->email}}@endisset" class="form-control" placeholder="abc123@example.com">
                    @if ($errors->has('email'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('email')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->

                  <div class="col-md-6">
                    <label class="control-label">phone</label>
                     <input type="text" name="phone" value="@isset($teacher->phone){{$teacher->phone}}@endisset" placeholder="Mobile No" class="form-control">

                   @if ($errors->has('phone'))
                   <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('phone')}}
                  </div>
                  @endif


                </div><!--form-group ends-->

              </div> 
              <div class="row" style="margin: 10px 0px;">

                  <div class="col-md-6">
                    <label class="control-label">Subject</label>
                    <input name="subject" type="text" value="@isset($teacher->subject){{$teacher->subject}}@endisset" class="form-control" placeholder="subject">
                    @if ($errors->has('subject'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('subject')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->

                  <div class="col-md-6">
                    <label class="control-label">Score</label>
                     <input type="text" name="phone" value="@isset($teacher->score){{$teacher->score}}@endisset" placeholder="Score" class="form-control">

                   @if ($errors->has('score'))
                   <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('score')}}
                  </div>
                  @endif


                </div><!--form-group ends-->

              </div> 


              <div class="row" style="margin: 10px 0px;">

                <div class="col-md-6">
                  <label class="control-label">Password</label>
                  <input type="password" placeholder="******" class="form-control" name="password">
                  @if ($errors->has('password'))
                  <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('password')}}
                  </div>
                  @endif
                </div><!--form-group ends-->

                <div class="col-md-6">
                  <label class="control-label">Confirm Password</label>
                  <input name="password_confirmation" type="password" placeholder="******" class="form-control">
                </div><!--form-group ends-->

              </div> 
               

            	</div> 
          	</div>
        </div>

      </div>
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('teacher.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
				</div>
			</div>
		</section>
	</section>
@endsection
@push('post-styles')
			
@endpush
@push('post-scripts')
		<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 

</script>

@endpush