<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Approved')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
    <h2>Manage Approved</h2>
    <div class="right-wrapper pull-right">
      <ol class="breadcrumbs">
        <li>
          <a href="index.html">
            <i class="fa fa-home"></i>
          </a>
        </li>
        <li><span>Index</span></li>
      </ol>
      <a class="sidebar-right-toggle" ></a>
    </div>
  </header>
  <section class="panel">
    <header class="panel-heading">
      <h2 class="panel-title">Manage Approved</h2>
    </header>
    <div class="panel-body">
      <div class="row">

        <div class="panel panel-primary basic_info_panel">
          <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
          text-transform: uppercase;">Approved Question</b></h4></div>
          @component('_components.alerts-default')
          @endcomponent
        
          <div class="panel-body">
            <div class="row">
              <div class="col-md-9" style="margin-bottom: 20px;">
                <div class="row" style="margin: 10px 0px;">
                  <div class="col-md-6">
                   <label class="control-label">Subject Name</label>
                   <select name="sub_id"  type="text"  class="form-control sub_id" onchange="selectTopic(this)">
                    <option disabled="disabled" selected="selected">Choose the subject</option>
                    @foreach($subject as $sub)
                    <option value="{{$sub->id}}" @if($sub->id==Session()->get('sub_id')) selected @endif>{{$sub->sb_name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('name'))
                  <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('name')}}
                  </div>
                  @endif
                </div>
                <div class="col-md-6">
                 <label class="control-label"> Topic Name</label>
                 <select name="topic_id" value="{{ old('topic_id') }}" type="text" placeholder="First Name" class="form-control topic_id">
                  @if(Session()->get('topic_id'))
                  @foreach($topic as $top)
                  <option value="{{$top->id}}" @if($top->id==Session()->get('topic_id')) selected @endif>{{$top->name}}</option>
                  @endforeach
                  @endif
                </select>
                @if ($errors->has('topic_id'))
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                  </button>
                  <strong>Warning!</strong> {{$errors->first('topic_id')}}
                </div>
                @endif
              </div>

            </div>
          </div> 
        </div> 
        {{-- ///////////////////////////////// Table Show here in row////////////////// --}}
        <div class="row" >
          <div class="col-md-12">
            <table id="questionTable" class="table table-bordered table-striped mb-none">




            </table>
          </div>
        </div>
        {{-- /////////////////////////////////////// End Table here ///////////////////////////// --}}
      </div>
    </div>

  </div>

</div>
</div>
</div>
</section>
</section>
@endsection
@push('post-styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">

@endpush
@push('post-scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 

</script>
<script type="text/javascript">

  $('.sub_id, .topic_id').on('change', function () {
    console.log('approved create');
    var sub_id  = $("[name='sub_id']").val();
    var topic_id  = $("[name='topic_id']").val();

    $.ajax({
      method:"POST",
      url:"{{route('approvedQuestion')}}",
      data : {sub_id:sub_id,topic_id:topic_id},
      dataType:"json",
      success:function(data){
        console.log('response',data);
        $('#questionTable').html('');
        if(data.status){
          var tableContent=``;
          data.data.forEach(function(val,ind){
            tableContent+=`<tr class="${val.id}" style="margin-bottom: 15px">

            <form method="POST" action="javascript:;"  id="answerApproved${val.id}">
            <input type="hidden" name="id" value="${val.id}">
            <input type="hidden" name='question_id' value="${val.id}"><td>
            <div><strong>Question</strong><a href='admin/question/${val.id}/edit' class="btn btn-info btn-sm"> Edit</a>`;

            if(val.type==2){
              tableContent+=`<img src="/images/question/${val.question}" id="profile-img-tag" height="60" width = "60" style="margin-bottom:20px;"></div>

              `;
            }else{
              tableContent+=`<textarea type="text"  rows="2" name="question" class="form-control summernote5" >${val.question}</textarea><h4> ${val.question} </h4></div>`;
            }
            var ansId=0;
            val.answers.forEach(function(val2,ind2){  
              ansId++;
              tableContent+=`<div class="col-md-12 answer" style="padding-bottom:10px;">
              <div class="col-md-2" style="padding: 25px,25px,25px,25px">
              <label >IsCorrect</label>&nbsp;&nbsp;&nbsp;<input type="radio" class="" ${val2.is_correct==1?'checked':''}  name="${val.id}"  value="${val2.is_correct?'checked':''}">
              </div>
              <div class="col-md-8">`;

              tableContent+=`<textarea type="text"  rows="2" name="answer[]" class="form-control summernote${ansId}" >${val2.ans}</textarea></div>
              </div>`;

            });

            tableContent+=` 
            </tr> `;
          });
        }else{
          var tableContent=`<tr> <td> Record not found</td></tr>`;
        }

        $('#questionTable').html(tableContent);
        textAreaLoad();
      }

    });
  });
  function selectTopic(obj){
    $("[name='topic_id']").html(` <option selected="selected" disabled='disabled'> Select Topic  </option>`);
    var branch_id  = $("[name='sub_id']").val();

    console.log('branch',$("[name='sub_id']").val());
    $('.branch').val(branch_id);
    $.ajax({
      method:"POST",
      url:"{{route('SubjectHasTopic')}}",
      data : {id:branch_id},
      dataType:"json",
      success:function(data){
        data.forEach(function(val,ind){
          var id = val.id;
          var name = val.name;
          var option = `<option value="${id}">${name}</option>`;
          $("[name='topic_id']").append(option);
        });
      }
    });
  }

  $('.type').on('change',function(){
    var type=$(this).val();
    if(type==1){
      $('.inputTextarea').show();
      $('.questionFile').css('display',"none");
    }else{
      $('.inputTextarea').css('display',"none");
      $('.questionFile').show();
    }
  });
</script>
<script type="text/javascript">

  function textAreaLoad() {
    console.log('summernote');
    $('.summernote1').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });

    $('.summernote2').summernote({

      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
    $('.summernote3').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
    $('.summernote4').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
    $('.summernote5').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });

  }

  $('.typeD').on('change',function(){
    var answerType=$(this).val();
    if(answerType==1){
      $('.answerTextD').show();
      $('.answerFileD').css('display',"none");
    }else{
      $('.answerTextD').css('display',"none");
      $('.answerFileD').show();
    }
  });
  $('.typeC').on('change',function(){
    var answerType=$(this).val();
    if(answerType==1){
      $('.answerTextC').show();
      $('.answerFileC').css('display',"none");
    }else{
      $('.answerTextC').css('display',"none");
      $('.answerFileC').show();
    }
  });
  $('.typeB').on('change',function(){
    var answerType=$(this).val();
    if(answerType==1){
      $('.answerTextB').show();
      $('.answerFileB').css('display',"none");
    }else{
      $('.answerTextB').css('display',"none");
      $('.answerFileB').show();
    }
  });
  $('.typeA').on('change',function(){
    var answerType=$(this).val();
    if(answerType==1){
      $('.answerTextA').show();
      $('.answerFileA').css('display',"none");
    }else{
      $('.answerTextA').css('display',"none");
      $('.answerFileA').show();
    }
  });

  function approveQuestionAns(id){
    console.log($('#answerApproved'+id).serialize());
    console.log('approved request',$('#answerApproved'+id).serialize(),'#answerApproved'+id)
    $.ajax({
      method:"POST",
      url:"{{route('approveQuestionAns')}}",
      data : {id:id},
      dataType:"json",
      success:function(data){
       if(data.status){
        $('.'+id).closest('tr').remove();
        swal("Question Approved Successfully", "Well done", "success");
      }
    }
  });
  }
  function upApproveQuestionAns(id){
    console.log($('#answerApproved'+id).serialize());
    console.log('approved request',$('#answerApproved'+id).serialize(),'#answerApproved'+id)
    $.ajax({
      method:"POST",
      url:"{{route('upApproveQuestionAns')}}",
      data : {id:id},
      dataType:"json",
      success:function(data){
       if(data.status){
        $('.'+id).closest('tr').remove();
        swal("Oops! Question upApproved", "Thanks", "warning");
      }
    }
  });
  }
</script>
@endpush