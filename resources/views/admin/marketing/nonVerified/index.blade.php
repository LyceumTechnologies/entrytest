<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Non Verified User ')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
   <h2>Manage Non Verified User </h2>
   <div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
     <li>
      <a href="index.html">
       <i class="fa fa-home"></i>
     </a>
   </li>
   <li><span>Index</span></li>
 </ol>
 <a class="sidebar-right-toggle" ></a>
</div>
</header>
<section class="panel">
 <header class="panel-heading">
  <h2 class="panel-title">Manage Non Verified User </h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="panel panel-primary basic_info_panel">
    <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
    text-transform: uppercase;">Non Verified User</b></h4></div>
    @component('_components.alerts-default')
    @endcomponent
    <form method="POST" action="{{route('non-verified.store')}}" enctype = "multipart/form-data">
     @csrf
     <div class="panel-body">
      <div class="row">


        <div class="col-md-12" style="margin-bottom: 20px;">
          <div class="row" style="margin: 10px 0px;">

            <div class="col-md-6">
             <label class="control-label">Email</label>
             <input name="name"  checked="" value="email" type="radio" class="form-control">
            
          </div>

         <!--  <div class="col-md-6">
             <label class="control-label">sms</label>
             <input name="name"  value="sms" type="radio"  class="form-control">
            
          </div> -->

        </div>
        


      </div>

    </div>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-offset-3 col-md-9">

         <button class="btn btn-success" type="Submit" name="">Save</button>
       </form>

     </div>
   </div>
 </div>
</div>
</div>
</div>
</section>
</section>
@endsection
@push('post-styles')

@endpush
@push('post-scripts')
    <link href="{{asset('assets/multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('assets/multi-select/jquery.multi-select.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 

</script>
<script type="text/javascript">

$('#pre-selected-options').multiSelect();
   </script>
@endpush