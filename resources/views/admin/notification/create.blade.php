<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Notification')
@section('content')
  <section role="main" class="content-body">
    <header class="page-header">
      <h2>Manage Notification</h2>
      <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
          <li>
            <a href="index.html">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li><span>Index</span></li>
        </ol>
        <a class="sidebar-right-toggle" ></a>
      </div>
    </header>
    <section class="panel">
      <header class="panel-heading">
        <h2 class="panel-title">Manage Notification</h2>
      </header>
      <div class="panel-body">
        <div class="row">
          <div class="panel panel-primary basic_info_panel">
             
              @component('_components.alerts-default')
        @endcomponent
              <form method="POST" action="{{route('notification.store')}}" enctype = "multipart/form-data">
                @csrf
              <div class="panel-body">
                <div class="row">
                  
                  <input type="hidden" name="{{$notifcation->id}}">
                  <div class="col-md-9" style="margin-bottom: 20px;">
                  
                  <div class="row" style="margin: 10px 0px;">
                      <div class="col-md-12">
                        <label class="control-label">Notification </label>
                          <textarea name="notifcation" type="text" placeholder="Notification " class="form-control" required>{{$notifcation->notifcation}}</textarea>
                         
                      </div>
                     
                  </div>
                
                </div>
              </div>
            </div>
        </div>
              
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('grade.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </section>
@endsection
@push('post-styles')
      
@endpush
@push('post-scripts')
 

@endpush