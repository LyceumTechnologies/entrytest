<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Question')
@section('content')
  <section role="main" class="content-body">
    <header class="page-header">
      <h2>Manage Question</h2>
      <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
          <li>
            <a href="index.html">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li><span>Index</span></li>
        </ol>
        <a class="sidebar-right-toggle" ></a>
      </div>
    </header>
    <section class="panel">
      <header class="panel-heading">
        <h2 class="panel-title">Manage Question</h2>
      </header>
      <div class="panel-body">
        <div class="row">

          <div class="panel panel-primary basic_info_panel">
              <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
              text-transform: uppercase;">Question Info</b></h4></div>
              @component('_components.alerts-default')
        @endcomponent
        
       
              <form method="POST" action="{{route('question.store')}}" enctype = "multipart/form-data">
                @csrf
              <div class="panel-body">
                <div class="row">
                  
                  <div class="col-md-9" style="margin-bottom: 20px;">
                    <div class="row" style="margin: 10px 0px;">

                      <div class="col-md-6">
                       <label class="control-label">Subject Name</label>
                       <select name="sub_id"  type="text"  class="form-control" onchange="selectTopic(this)">
                        <option disabled="disabled" selected="selected">Choose the subject</option>
                        @foreach($subject as $sub)
                          <option value="{{$sub->id}}" @if($sub->id==Session()->get('sub_id')) selected @endif>{{$sub->sb_name}}</option>
                        @endforeach
                       </select>
                       @if ($errors->has('name'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('name')}}
                      </div>
                      @endif
                    </div>
                    <div class="col-md-6">
                       <label class="control-label"> Topic Name</label>
                       <select name="topic_id" value="{{ old('topic_id') }}" type="text" placeholder="First Name" class="form-control">
                        @if(Session()->get('topic_id'))
                          @foreach($topic as $top)
                           <option value="{{$top->id}}" @if($top->id==Session()->get('topic_id')) selected @endif>{{$top->name}}</option>
                           @endforeach
                        @endif
                        </select>
                       @if ($errors->has('topic_id'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('topic_id')}}
                      </div>
                      @endif
                    </div>

                </div>

                <div class="row" style="margin: 10px 0px;">
                  <div class="col-md-3"></div>
                  <div class="col-md-3">
                      
                        <label><input type="radio" class="form-control type" name="type" value="1" checked>Text</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control type"  name="type" value="2">File</label>
                      

                     
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">Question</label>
             
                    <textarea id="" rows="2" class="form-control inputTextarea" id="answer1" name="question"></textarea> 
                    <input type="file" name="questionFile" class="form-control questionFile" style="display: none;">
                    @if ($errors->has('question'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('question')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->

                
              </div> 
              <div class="row" style="margin: 10px 0px;">
                   <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;text-transform: uppercase;">Answer Info</b></h4></div>
                  <div class="col-md-12 answer">
                     <div class="col-md-6">
                       <label><input type="radio" class="form-control typeA" name="typeA" value="1" checked>Text</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control typeA"  name="typeA" value="2">File</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control "  name="correct" value="1">IsCorrect</label>
                    </div>
                    <div class="col-md-6">
                      <textarea id="" rows="2" class="form-control answerTextA" name="answerA"></textarea> 
                      <input type="file" name="answerFileA" class="form-control answerFileA" style="display: none;">
                   </div>
                  </div>
                  <div class="col-md-12 answer">
                     <div class="col-md-6">
                       <label><input type="radio" class="form-control typeB" name="typeB" value="1" checked>Text</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control typeB"  name="typeB" value="2">File</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control "  name="correct" value="2">IsCorrect</label>
                    </div>
                    <div class="col-md-6">
                      <textarea id="" rows="2" class="form-control answerTextB" name="answerB"></textarea> 
                      <input type="file" name="answerFileB" class="form-control answerFileB" style="display: none;">
                   </div>
                  </div>
                  <div class="col-md-12 answer">
                    <div class="col-md-6">
                       <label><input type="radio" class="form-control typeC" name="typeC" value="1" checked>Text</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control typeC"  name="typeC" value="2">File</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control "  name="correct" value="3">IsCorrect</label>
                    </div>
                    <div class="col-md-6">
                      <textarea id="" rows="2" class="form-control answerTextC" name="answerC"></textarea> 
                      <input type="file" name="answerFileC" class="form-control answerFileC" style="display: none;">
                   </div>
                  </div>
                  <div class="col-md-12 answer">
                    <div class="col-md-6">
                       <label><input type="radio" class="form-control typeD" name="typeD" value="1" checked>Text</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control typeD"  name="typeD" value="2">File</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
                        <label><input type="radio" class="form-control "  name="correct" value="4">IsCorrect</label>
                    </div>
                    <div class="col-md-6">
                      <textarea id="" rows="2" class="form-control answerTextD" name="answerD"></textarea> 
                      <input type="file" name="answerFileD" class="form-control answerFileD" style="display: none;">
                   </div>
                  </div>
                   
                  
              </div> 


              

              </div> 
            </div>
        </div>

      </div>
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('teacher.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </section>
@endsection
@push('post-styles')
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
      <link href="{{asset('assets/mathLib/mathquill.css')}}" rel="stylesheet">
      <link href="{{asset('assets/mathLib/matheditor.css')}}" rel="stylesheet">
  
@endpush
@push('post-scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<script src="{{asset('assets/mathLib/mathquill.min.js')}}"></script>
<script src="{{asset('assets/mathLib/matheditor.js')}}"></script>



    <script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 
 
</script>
<script type="text/javascript">
 
  function selectTopic(obj){
    console.log(obj);
    $("[name='topic_id']").html(` <option selected="selected" disabled='disabled'> Select Topic  </option>`);
    var branch_id  = $("[name='sub_id']").val();
 
    console.log('branch',$("[name='sub_id']").val());
    $('.branch').val(branch_id);
    $.ajax({
      method:"POST",
      url:"{{route('SubjectHasTopic')}}",
      data : {id:branch_id},
      dataType:"json",
      success:function(data){
        console.log(data);
            data.forEach(function(val,ind){
                var id = val.id;
                var name = val.name;
                var option = `<option value="${id}">${name}</option>`;
                $("[name='topic_id']").append(option);
            });
            }
        });
  }

  $('.type').on('change',function(){
      console.log('changed',$(this).val());
      var type=$(this).val();
      if(type==1){
          $('.inputTextarea').show();
          $('.questionFile').css('display',"none");
      }else{
          $('.inputTextarea').css('display',"none");
          $('.questionFile').show();
      }
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#summernote').summernote();
    $('#summernote2').summernote();
    $('#summernote3').summernote();
    $('#summernote4').summernote();
    $('#summernote5').summernote();
  });

  $('.typeD').on('change',function(){
    var answerType=$(this).val();
    console.log('answerD',answerType);
    if(answerType==1){
          $('.answerTextD').show();
          $('.answerFileD').css('display',"none");
      }else{
          $('.answerTextD').css('display',"none");
          $('.answerFileD').show();
      }
  });
  $('.typeC').on('change',function(){
    var answerType=$(this).val();
    console.log('answerc',answerType);
    if(answerType==1){
          $('.answerTextC').show();
          $('.answerFileC').css('display',"none");
      }else{
          $('.answerTextC').css('display',"none");
          $('.answerFileC').show();
      }
  });
   $('.typeB').on('change',function(){
    var answerType=$(this).val();
    console.log('answerB',answerType);
    if(answerType==1){
          $('.answerTextB').show();
          $('.answerFileB').css('display',"none");
      }else{
          $('.answerTextB').css('display',"none");
          $('.answerFileB').show();
      }
  });
    $('.typeA').on('change',function(){
    var answerType=$(this).val();
    console.log('answerA',answerType);
    if(answerType==1){
          $('.answerTextA').show();
          $('.answerFileA').css('display',"none");
      }else{
          $('.answerTextA').css('display',"none");
          $('.answerFileA').show();
      }
  });
</script>
@endpush