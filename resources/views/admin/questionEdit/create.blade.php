<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Question')
@section('content')
  <section role="main" class="content-body">
    <header class="page-header">
      <h2>Manage Question</h2>
      <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
          <li>
            <a href="index.html">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li><span>Index</span></li>
        </ol>
        <a class="sidebar-right-toggle" ></a>
      </div>
    </header>
    <section class="panel">
      <header class="panel-heading">
        <h2 class="panel-title">Manage Question</h2>
      </header>
      <div class="panel-body">
        <div class="row">

          <div class="panel panel-primary basic_info_panel">
              <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
              text-transform: uppercase;">Question Info</b></h4></div>
              @component('_components.alerts-default')
        @endcomponent
        
       
              <form method="POST" action="{{route('question.store')}}" enctype = "multipart/form-data">
                @csrf
              <div class="panel-body">
                <div class="row">
                  
                  <div class="col-md-9" style="margin-bottom: 20px;">
                    <div class="row" style="margin: 10px 0px;">

                      <div class="col-md-4">
                       <label class="control-label">Subject Name</label>
                       <select name="sub_id"  type="text"  class="form-control" onchange="selectTopic(this)" required>
                        <option disabled="disabled" selected="selected">Choose the subject</option>
                        @foreach($subject as $sub)
                          <option value="{{$sub->id}}" @if($sub->id==Session()->get('sub_id')) selected @endif>{{$sub->sb_name}}</option>
                        @endforeach
                       </select>
                      </div>
                      <div class="col-md-4">
                     <label class="control-label"> Grade Name</label>
                     <select name="grade_id" value="{{ old('grade_id') }}" type="text" placeholder="Grade Name" class="form-control grade_id" required>
                       <option  selected="selected" value="0">All Grade</option>
                      @foreach($grade as $to)
                      <option value="{{$to->id}}" >{{$to->grade_name}}</option>
                      @endforeach
                    </select>

                  </div>
                   <div class="col-md-4">
                     <label class="control-label"> Difficulty Level</label>
                     <select name="dif_id" value="{{ old('dif_id') }}" type="text" placeholder="First Name" class="form-control dif_id" required>
                      <option  selected="selected" value="0">All Difficulty</option>
                      @foreach($difficulty as $tops)
                      <option value="{{$tops->id}}" >{{$tops->difficulty}}</option>
                      @endforeach
                    </select>

                  </div>
                    

                </div>
                <div class="row" style="margin: 10px 0px;">
                    <div class="col-md-6">
                       <label class="control-label"> Topic Name</label>
                       <select name="topic_id" value="{{ old('topic_id') }}" type="text" placeholder="First Name" class="form-control" onchange="selectSubTopic(this)" required>
                        @if(Session()->get('topic_id'))
                          @foreach($topic as $top)
                           <option value="{{$top->id}}" @if($top->id==Session()->get('topic_id')) selected @endif>{{$top->name}}</option>
                           @endforeach
                        @endif
                        </select>
                     
                    </div>
                    <div class="col-md-6">
                        <label class="control-label"> Sub Topic Name</label required>
                        <select name="sub_topic_id" value="{{ old('sub_topic_id') }}" type="text" placeholder="First Name" class="form-control sub_topic_id">
                        </select>
                    </div>
                </div>

                <div class="row" style="margin: 10px 0px;">
                 
                  <div class="col-md-12">
                    <label class="control-label">Question</label>
             
                    <textarea id="summernote" rows="2" class="form-control inputTextarea" id="answer1" name="question"></textarea> 
                    @if ($errors->has('question'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('question')}}
                    </div>
                    @endif
                  </div><!--form-group ends-->

                
              </div> 
              <div class="row" style="margin: 10px 0px;">
                   <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;text-transform: uppercase;">Answer Info</b></h4></div>
                  <div class="col-md-12 answer">
                     <div class="col-md-6">
                       <label>Answer A</label>
                      <textarea id="summernote2" rows="2" class="form-control inputTextarea" id="answer1" name="answerA"></textarea> 
                    </div>
                    
                     <div class="col-md-6">
                       <label>Answer B</label>
                      <textarea id="summernote3" rows="2" class="form-control inputTextarea" id="answer1" name="answerB"></textarea> 
                    </div>
                   
                  </div>
                  <div class="col-md-12 answer">
                     <div class="col-md-6">
                       <label>Answer C</label>
                      <textarea id="summernote4" rows="2" class="form-control inputTextarea" id="answer1" name="answerC"></textarea> 
                    </div>
                    
                     <div class="col-md-6">
                       <label>Answer D</label>
                      <textarea id="summernote5" rows="2" class="form-control inputTextarea" id="answer1" name="answerD"></textarea> 
                    </div>
                   
                  </div>
                   
                  
              </div> 
              <div class="row" style="margin: 10px 0px;">
                <div class="col-md-2">
                  <label><input type="radio" class="form-control "  name="correct" value="1">A Correct</label>
                </div>
                <div class="col-md-2">
                  <label><input type="radio" class="form-control "  name="correct" value="2">B Correct</label>
                </div>
                <div class="col-md-2">
                  <label><input type="radio" class="form-control "  name="correct" value="3">C Correct</label>
                </div>
                <div class="col-md-2">
                  <label><input type="radio" class="form-control "  name="correct" value="4">D Correct</label>
                </div>
                
              </div>


              

              </div> 
            </div>
        </div>

      </div>
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('teacher.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </section>
@endsection
@push('post-styles')
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
     
  
@endpush
@push('post-scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>


       
<link href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.js"></script>
<script type="text/javascript" src="{{asset('assets/summernote/summernote-math.js')}}"></script>

    <script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 
 
</script>
<script type="text/javascript">
  function selectTopic(obj){
    console.log(obj);
    $("[name='topic_id']").html(` <option selected="selected" disabled='disabled'> Select Topic  </option>`);
    var branch_id  = $("[name='sub_id']").val();
 
    console.log('branch',$("[name='sub_id']").val());
    $('.branch').val(branch_id);
    $.ajax({
      method:"POST",
      url:"{{route('SubjectHasTopic')}}",
      data : {id:branch_id},
      dataType:"json",
      success:function(data){
        console.log(data);
            data.forEach(function(val,ind){
                var id = val.id;
                var name = val.name;
                var option = `<option value="${id}">${name}</option>`;
                $("[name='topic_id']").append(option);
            });
            }
        });
  }
  function selectSubTopic(obj){
    console.log('topic_id autopick',obj);
    $(".sub_topic_id").first().html(` <option value="0" selected='selected'> All  </option>`);
    var branch_id  = $(obj).val();
    if(branch_id==undefined && branch_id==null){
      branch_id=obj;
    }

    console.log('topic change value',$(obj).val());
    $.ajax({
      method:"POST",
      url:"{{route('TopicHasSubtopic')}}",
      data : {id:branch_id},
      dataType:"json",
      success:function(data){
        console.log('sub topic response',data);
        data.forEach(function(val,ind){
          var id = val.id;
          var name = val.sub_topic_name;
          var option = `<option value="${id}" ${id==obj?'selected':''}>${name}</option>`;
          $(".sub_topic_id").first().append(option);
        });
      }
    });
  }

  $('.type').on('change',function(){
      console.log('changed',$(this).val());
      var type=$(this).val();
      if(type==1){
          $('.inputTextarea').show();
          $('.questionFile').css('display',"none");
      }else{
          $('.inputTextarea').css('display',"none");
          $('.questionFile').show();
      }
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#summernote').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

  ]
    });
  
    $('#summernote2').summernote({

      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

  ]
    });
    $('#summernote3').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

  ]
    });
    $('#summernote4').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
       ['Insert', ['picture','math']],

  ]
    });
    $('#summernote5').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
       ['Insert', ['picture','math']],

  ]
    });
  });

  $('.typeD').on('change',function(){
    var answerType=$(this).val();
    console.log('answerD',answerType);
    if(answerType==1){
          $('.answerTextD').show();
          $('.answerFileD').css('display',"none");
      }else{
          $('.answerTextD').css('display',"none");
          $('.answerFileD').show();
      }
  });
  $('.typeC').on('change',function(){
    var answerType=$(this).val();
    console.log('answerc',answerType);
    if(answerType==1){
          $('.answerTextC').show();
          $('.answerFileC').css('display',"none");
      }else{
          $('.answerTextC').css('display',"none");
          $('.answerFileC').show();
      }
  });
   $('.typeB').on('change',function(){
    var answerType=$(this).val();
    console.log('answerB',answerType);
    if(answerType==1){
          $('.answerTextB').show();
          $('.answerFileB').css('display',"none");
      }else{
          $('.answerTextB').css('display',"none");
          $('.answerFileB').show();
      }
  });
    $('.typeA').on('change',function(){
    var answerType=$(this).val();
    console.log('answerA',answerType);
    if(answerType==1){
          $('.answerTextA').show();
          $('.answerFileA').css('display',"none");
      }else{
          $('.answerTextA').css('display',"none");
          $('.answerFileA').show();
      }
  });
</script>
@endpush