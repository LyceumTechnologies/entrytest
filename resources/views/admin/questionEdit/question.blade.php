<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Question')
@section('content')
  <section role="main" class="content-body">
    <header class="page-header">
      <h2>Manage Question</h2>
      <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
          <li>
            <a href="index.html">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li><span>Index</span></li>
        </ol>
        <a class="sidebar-right-toggle" ></a>
      </div>
    </header>
    <section class="panel">
      <header class="panel-heading">
        <h2 class="panel-title">Manage Question</h2>
      </header>
      <div class="panel-body">
        <div class="row">
          <div class="panel panel-primary basic_info_panel">
          <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
          text-transform: uppercase;">Question Info</b></h4></div>
          @component('_components.alerts-default')
          @endcomponent


          <div class="panel-body">
            
        {{-- ///////////////////////////////// Table Show here in row////////////////// --}}
        <div class="row" >
         <div class="" style="max-width: 1024px;">
           <table id="questionTable" class="table table-bordered table-striped mb-none">
            @php($index=1)
            @foreach($question as $q)
            <tr class="{{$q->id}}"  style="margin-bottom: 15px">


              <form method="POST" action="javascript:;" id="answerApproved{{$q->id}}">
                @csrf
              <input type="hidden" name="id" value="{{$q->id}}">
              <input type="hidden" name="question_id" value="{{$q->id}}">
              <input type="hidden" name="dd" class="selectedTopicId" value="{{$q->topic_id}}">
              <td >
                <div class="row answerApproved{{$q->id}}">
                  <div class="" style="margin-bottom: 20px;max-width: 1024px;">
                    <div class="row" style="margin: 10px 0px;">
                      <div class="col-md-4">
                        <label class="control-label">Subject Name</label> 
                    <input name="sub_id"  type="text"  class="form-control sub_id" readonly="" value="@isset($q->subject->sb_name){{$q->subject->sb_name}} @endisset" >
                      
                    </div>
                     <div class="col-md-4">
                     <label class="control-label"> Grade Name</label>
                   <input name="grade_id"  type="text"  class="form-control grade_id" readonly="" value="@isset($q->course->grade_name){{$q->course->grade_name}} @endisset" >

                  </div>
                    <div class="col-md-4">
                     <label class="control-label"> Chapter Name</label>
                      <input name="chapter_id"  type="text"  class="form-control chapter_id" readonly="" value="@isset($q->chapter->chaper_name){{$q->chapter->chaper_name}} @endisset" >
                  </div>


                  <div class="col-md-4">
                     <label class="control-label"> Topic Name</label>
                     <input name="topic_id"  type="text"  class="form-control topic_id" readonly="" value="@isset($q->topic->name){{$q->topic->name}} @endisset" >


                  </div>

                  <div class="col-md-4">
                     <label class="control-label"> Sub Topic Name</label>
                      <input name="sub_topic_id"  type="text"  class="form-control sub_topic_id" readonly="" value="@isset($q->SubTopic->sub_topic_name){{$q->SubTopic->sub_topic_name}} @endisset" >
                  </div>
                  
                   <div class="col-md-4">
                     <label class="control-label"> Difficulty Level</label>

                    <input name="diff_id"  type="text"  class="form-control diff_id" readonly="" value="@isset($q->difficulty->difficulty){{$q->difficulty->difficulty}} @endisset" >
                    

                  </div>

                </div>
              </div> 
            </div> 

            <div>
              <strong>Question {{$index}}:</strong> {!! $q->question !!} </h4>
            </div>
            @isset($q->answers)
            @php($ansIndex=1)
            @foreach($q->answers as $answer)
            <div class="answer row" style="padding-bottom:10px;max-width: 1024px;">
              <div class="col-md-2" style="padding: 25px,25px,25px,25px">
                <label>IsCorrect</label>&nbsp;&nbsp;&nbsp;
                <input type="radio" @if($answer->is_correct==1){{'checked'}}@endif  name="{{$q->id}}" value="{{$ansIndex}}">
              </div>
              <div class="col-md-4">
                <h4>{!! $answer->ans !!}</h4><h4></h4>
              </div>
            </div>
            @php($ansIndex++)
            @endforeach
            @endisset
            <div class="answer" style="max-width:1024px;">
         
              <div class="col-md-4">
                <a href="javascript:;" onclick="editItem({{$q->id}})" class="btn btn-info" >Edit </a>
                <button class="btn btn-danger" onclick=CancelQuestion({{$q->id}})>Skip</button>
              </div>
            </div>



          </td>
        </tr>
        </form>
        @php($index++)
        @endforeach



      </table>
      {{ $question->links() }}

    </div>
  </div>
  {{-- /////////////////////////////////////// End Table here ///////////////////////////// --}}
</div>
</div>

</div>

</div>
</div>
</div>
</section>
</section>
{{-- ////////////////// Model Target /////////////////// --}}
<div id="show_edit_modal"></div>
@endsection
@push('post-styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">

@endpush
@push('post-scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<script type="text/javascript">
  $(function(){
    var sub_id=$('.sub_id').val();
    selectTopic(sub_id);
    var sub_id=$('.sub_id').val();
    var topic_id=$('.sub_id').attr('data-topic_id');
    var chapter_id=$('.sub_id').attr('data-chapter_id');

    selectSubTopic(topic_id);
    selectChapterToTopic(chapter_id);
  });

</script>
<script type="text/javascript">


    function editItem(id) {

      console.log('question edit ${id}',id);
    $.ajax({
      url: 'admin/questionEditModel/'+id,
      type: 'get',
      data: {
        'id': id
      },
      success: function (response) {

        $("#show_edit_modal").html(response);
        jQuery("#updateCourse").modal('show');
      },
      error: function (e) {
        console.log('error', e);
      }
    });
  }



  function selectTopic(obj){
    console.log(obj);
    $("[name='topic_id']").html(` <option value="0" selected='selected'> All  </option>`);
    var branch_id  = $("[name='sub_id']").val();
      var topic_id=$('.sub_id').attr('data-topic_id');
    console.log('branch',$("[name='sub_id']").val());
    console.log('subjectId',branch_id);
    $.ajax({
      method:"POST",
      url:"{{route('SubjectHasTopic')}}",
      data : {id:branch_id},
      dataType:"json",
      success:function(data){
        console.log(data);
        data.forEach(function(val,ind){
          var id = val.id;
          var name = val.name;
          console.log('anwer checked',id==topic_id?'selected':'','id',id,'topic_id',topic_id);
          var option = `<option value="${id}" ${id==topic_id?'selected':''}>${name}</option>`;
          $("[name='topic_id']").append(option);
        });
      }
    });
  }


  function selectChapterToTopic(obj){
    console.log(obj);
    $("[name='chapter_id']").html(` <option value="0" selected='selected'> All  </option>`);
    var branch_id  = $("[name='sub_id']").val();
      var chapter_id=$('.sub_id').attr('data-chapter_id');
    console.log('branch',$("[name='sub_id']").val());
    console.log('subjectId',branch_id);
    $.ajax({
      method:"POST",
      url:"{{route('SubjectHasTopic')}}",
      data : {id:branch_id},
      dataType:"json",
      success:function(data){
        console.log(data);
        data.forEach(function(val,ind){
          var id = val.id;
          var name = val.name;
          console.log('anwer checked',id==chapter_id?'selected':'','id',id,'chapter_id',chapter_id);
          var option = `<option value="${id}" ${id==chapter_id?'selected':''}>${name}</option>`;
          $("[name='chapter_id']").append(option);
        });
      }
    });
  }
  function selectSubTopic(obj){
    console.log('topic_id autopick',obj);
    $(".sub_topic_id").first().html(` <option value="0" selected='selected'> All  </option>`);
    var branch_id  = $(obj).val();
    if(branch_id==undefined && branch_id==null){
      branch_id=obj;
    }

    console.log('topic change value',$(obj).val());
    $.ajax({
      method:"POST",
      url:"{{route('TopicHasSubtopic')}}",
      data : {id:branch_id},
      dataType:"json",
      success:function(data){
        console.log('sub topic response',data);
        data.forEach(function(val,ind){
          var id = val.id;
          var name = val.sub_topic_name;
          var option = `<option value="${id}" ${id==obj?'selected':''}>${name}</option>`;
          $(".sub_topic_id").first().append(option);
        });
      }
    });
  }

  $('.type').on('change',function(){
    console.log('changed',$(this).val());
    var type=$(this).val();
    if(type==1){
      $('.inputTextarea').show();
      $('.questionFile').css('display',"none");
    }else{
      $('.inputTextarea').css('display',"none");
      $('.questionFile').show();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#summernote').summernote();
    $('#summernote2').summernote();
    $('#summernote3').summernote();
    $('#summernote4').summernote();
    $('#summernote5').summernote();
  });

  $('.typeD').on('change',function(){
    var answerType=$(this).val();
    console.log('answerD',answerType);
    if(answerType==1){
      $('.answerTextD').show();
      $('.answerFileD').css('display',"none");
    }else{
      $('.answerTextD').css('display',"none");
      $('.answerFileD').show();
    }
  });
  $('.typeC').on('change',function(){
    var answerType=$(this).val();
    console.log('answerc',answerType);
    if(answerType==1){
      $('.answerTextC').show();
      $('.answerFileC').css('display',"none");
    }else{
      $('.answerTextC').css('display',"none");
      $('.answerFileC').show();
    }
  });
  $('.typeB').on('change',function(){
    var answerType=$(this).val();
    console.log('answerB',answerType);
    if(answerType==1){
      $('.answerTextB').show();
      $('.answerFileB').css('display',"none");
    }else{
      $('.answerTextB').css('display',"none");
      $('.answerFileB').show();
    }
  });
  $('.typeA').on('change',function(){
    var answerType=$(this).val();
    console.log('answerA',answerType);
    if(answerType==1){
      $('.answerTextA').show();
      $('.answerFileA').css('display',"none");
    }else{
      $('.answerTextA').css('display',"none");
      $('.answerFileA').show();
    }
  });
function CancelQuestion(id){
   $('.'+id).closest('tr').remove();

         swal("Question Approved Successfully", "Oops", "warning");
   
}
  function approveQuestionAns(id){
      var form = $('#answerApproved'+id)[0]; // You need to use standard javascript object here
      var formData = new FormData(form);
      console.log('formData', formData);
      console.log('form', form);
      var topic_id=$('.answerApproved'+id).find('.topic_id').val();
      console.log('requestion topic',topic_id);
      var sub_id=$('.answerApproved'+id).find('.sub_id').val();
      var grade_id=$('.answerApproved'+id).find('.grade_id').val();
      var sub_id=$('.answerApproved'+id).find('.sub_id').val();
      var sub_topic_id=$('.answerApproved'+id).find('.sub_topic_id').val();
      var dif_id=$('.answerApproved'+id).find('.dif_id').val();

   $.ajax({
    method:"POST",
    url:"{{route('approveQuestionAns')}}",
    data : {id:id,topic_id:topic_id,sub_id:sub_id,grade_id:grade_id,sub_topic_id:sub_topic_id,dif_id:dif_id},
    dataType:"json",
    success:function(data){
      console.log('approved Question Request',data);
      // console.log($('.'+id).html());
      if(data.status){
       $('.'+id).closest('tr').remove();

       swal("Question Approved Successfully", "Well done", "success");
     }
   }
 });

 }
</script>
@endpush