<div class="modal fade text-left" id="updateCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
aria-hidden="true">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header bg-info">
			<h3 class="modal-title" id="myModalLabel35"> Update section</h3>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		     <form method="POST" action="{{route('question.update',$question->id)}}" enctype = "multipart/form-data" id="updateDataForm">
            @csrf
			<div class="modal-body">
				<div class="modal-body">

                
              <div class="row">
                
                <div class="col-md-12" style="margin-bottom: 20px;">
                  <div class="row" style="margin: 10px 0px;">

                    <div class="col-md-4">
                     <label class="control-label">Subject Name</label>
                     <select name="sub_id" id="sub_id"   type="text" data-topic_id="{{$question->topic_id}}"  class="form-control sub_id" >
                      <option disabled="disabled" selected="selected">Choose the subject</option>
                      @foreach($subject as $sub)
                      <option value="{{$sub->id}}" @if($sub->id==$question->sub_id) selected @endif >{{$sub->sb_name}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('name'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('name')}}
                    </div>
                    @endif
                  </div>
                  <div class="col-md-4">
                   <label class="control-label"> Grade Name</label>
                   <select name="grade_id" id="grade_id" value="{{ old('grade_id') }}" type="text" placeholder="Grade Name" class="form-control grade_id" required>
                     <option  selected="selected" value="0">All Grade</option>
                     @foreach($grade as $to)
                     <option value="{{$to->id}}" @if($to->id==$question->grade_id) selected @endif>{{$to->grade_name}}</option>
                     @endforeach
                   </select>

                 </div>
                 <div class="col-md-4">
                   <label class="control-label"> Chapter</label>
                   <select name="chapter_id" id="chapter_id" value="{{ old('chapter_id') }}" type="text" class="form-control chapter_id" required>
                    

                     @foreach($chapters as $to)
                     <option value="{{$to->id}}" @if($to->id==$question->chapter_id) selected @endif>{{$to->chaper_name}}</option>
                     @endforeach


                  </select>

                </div>

                

              </div>
              <div class="row" style="margin: 10px 0px;">
                <div class="col-md-4">
                 <label class="control-label"> Topic Name</label>
                 <select name="topic_id" id="topic_id" value="{{ old('topic_id') }}" onchange="selectSubTopic(this)" type="text" placeholder="First Name" class="form-control">

                  @if($question->topic_id)
                  @foreach($question->subject->chapters as $top)
                  <option value="{{$top->id}}" @if($top->id==$question->topic_id) selected @endif>{{$top->name}}</option>
                  @endforeach
                  @endif
                </select>
                
              </div>
              <div class="col-md-4">
                <label class="control-label"> Sub Topic Name</label required>
                  <select name="sub_topic_id" id="sub_topic_id" value="{{ old('sub_topic_id') }}" type="text" placeholder="First Name" class="form-control sub_topic_id">
                  </select>
                </div>
                <div class="col-md-4">
                 <label class="control-label"> Difficulty Level</label>
                 <select name="dif_id" value="{{ old('dif_id') }}" type="text" placeholder="First Name" class="form-control dif_id" required>
                  <option  selected="selected" value="0">All Difficulty</option>
                  @foreach($difficulty as $tops)
                  <option value="{{$tops->id}}" @if($tops->id==$question->dif_id) selected @endif>{{$tops->difficulty}}</option>
                  @endforeach
                </select>

              </div>
            </div>

            <div class="row" style="margin: 10px 0px;">
             
              <div class="col-md-12">
                <label class="control-label">Question</label>
                
                <textarea id="summernote" rows="2" class="form-control inputTextarea" id="answer1" name="question">{{$question->question}}</textarea> 
                @if ($errors->has('question'))
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                  </button>
                  <strong>Warning!</strong> {{$errors->first('question')}}
                </div>
                @endif
              </div><!--form-group ends-->

              
            </div> 
            <div class="row" style="margin: 10px 0px;">
             <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;text-transform: uppercase;">Answer Info</b></h4></div>
             <div class="col-md-12 answer">
               <div class="col-md-6">
                 <label>Answer A</label>
                 <textarea id="summernote2" rows="2" class="form-control inputTextarea" id="answer1" name="answerA">@isset($question->answers[0]->ans){{$question->answers[0]->ans}}@endisset</textarea> 
               </div>
               
               <div class="col-md-6">
                 <label>Answer B</label>
                 <textarea id="summernote3" rows="2" class="form-control inputTextarea" id="answer1" name="answerB">@isset($question->answers[1]->ans){{$question->answers[1]->ans}}@endisset</textarea> 
               </div>
               
             </div>
             <div class="col-md-12 answer">
               <div class="col-md-6">
                 <label>Answer C</label>
                 <textarea id="summernote4" rows="2" class="form-control inputTextarea" id="answer1" name="answerC">@isset($question->answers[2]->ans){{$question->answers[2]->ans}}@endisset</textarea> 
               </div>
               
               <div class="col-md-6">
                 <label>Answer D</label>
                 <textarea id="summernote5" rows="2" class="form-control inputTextarea" id="answer1" name="answerD">@isset($question->answers[3]->ans){{$question->answers[3]->ans}}@endisset</textarea> 
               </div>
               
             </div>
             
             
           </div> 
           <div class="row" style="margin: 10px 0px;">
            <div class="col-md-2">
              <label><input type="radio" class="form-control "  name="correct" value="1" .
                @isset($question->answers[0]->is_correct) ) 
                @if($question->answers[0]->is_correct){{'checked'}} @endif
                @endisset>A Correct</label>
              </div>
              <div class="col-md-2">
                <label><input type="radio" class="form-control "  name="correct" value="2" 
                  @isset($question->answers[1]->is_correct) ) 
                  @if($question->answers[1]->is_correct){{'checked'}} @endif
                  @endisset>B Correct
                </label>
              </div>
              <div class="col-md-2">
                <label><input type="radio" class="form-control "  name="correct" value="3"  @isset($question->answers[2]->is_correct) ) 
                  @if($question->answers[2]->is_correct){{'checked'}} @endif
                  @endisset>C Correct</label>
                </div>
                <div class="col-md-2">
                  <label><input type="radio" class="form-control "  name="correct" value="4" @isset($question->answers[3]->is_correct) ) 
                    @if($question->answers[3]->is_correct){{'checked'}} @endif
                    @endisset>D Correct</label>
                  </div>
                  
                </div>


                

              </div> 
           
					

					
				<br>
				</div> <!-- modal body end here -->


				<input type="hidden" name="id" value="{{$question->id}}">

			</div>
			<div class="modal-footer">
				<img class="loader-img" src="{{asset('images/ajax-loader.gif')}}" width="50"
				height="50"/>
				&nbsp;&nbsp;&nbsp;
				<input type="reset" class="btn-danger btn-outline-secondary btn-sm" data-dismiss="modal"
				value="Dismiss">
				<input type="submit" class="btn-success btn-outline-info btn-sm"  id="updateDataBtn" value="Update">
			</div>
		</form>
	</div>
</div>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0/katex.min.js"></script>
<script type="text/javascript" src="{{asset('assets/summernote/summernote-math.js')}}"></script>



<script type="text/javascript">

    $("[name='sub_id']").add("[name='grade_id']").on('on change',function() {
        SelectChapter();
    });

    $("[name='sub_id']").add("[name='grade_id']").add("[name='chapter_id']").on('on change',function() {
        SelectTopic();
    });

    function SelectChapter(obj){
        console.log(obj);
        $("[name='chapter_id']").html(` <option selected="selected" disabled='disabled' value="0"> Select Chapter  </option>`);
        var sub_id  = $("#sub_id").val();
        var grade_id  = $("#grade_id").val();
        
        $.ajax({
          method:"POST",
          url:"{{route('SubjectChapter')}}",
          data : {sub_id:sub_id,grade_id:grade_id},
          dataType:"json",
          success:function(res){
            if(res.status){
                 res.data.forEach(function(val,ind){
                  var id = val.id;
                  var name = val.chaper_name;
                  var option = `<option value="${id}">${name}</option>`;
                  $("[name='chapter_id']").append(option);
                });
            }
           
        }
    });
  }

  function SelectTopic(){
        $("[name='topic_id']").html(` <option selected="selected" disabled='disabled' value="0"> Select Topic  </option>`);
        var sub_id  = $("#sub_id").val();
        var grade_id  = $("#grade_id").val();
        var chapter_id  = $("#chapter_id").val();
        
        $.ajax({
          method:"POST",
          url:"{{route('ChapterHasTopic')}}",
          data : {sub_id:sub_id,grade_id:grade_id,chapter_id:chapter_id},
          dataType:"json",
          success:function(res){
            console.log(res);
            if(res.status){
                res.data.forEach(function(val,ind){
                  var id = val.id;
                  var name = val.name;
                  var option = `<option value="${id}">${name}</option>`;
                  $("[name='topic_id']").append(option);
                });
            }
            
          }
        });
  }
 function selectSubTopic(obj){
    $("#sub_topic_id").first().html(` <option value="0" selected='selected' value="0"> Select sub topic  </option>`);
    var topic_id  = $('#topic_id').val();
    
    console.log('topic change value',topic_id);
    $.ajax({
      method:"POST",
      url:"{{route('TopicHasSubtopic')}}",
      data : {id:topic_id},
      dataType:"json",
      success:function(data){
        console.log('sub topic response',data);
        data.forEach(function(val,ind){
          var id = val.id;
          var name = val.sub_topic_name;
          var option = `<option value="${id}">${name}</option>`;
          $("#sub_topic_id").append(option);
        });
      }
    });
    
  }



 $(function(){
  var sub_id=$('.sub_id').val();
  selectTopic(sub_id);
  var sub_id=$('.sub_id').val();
  var topic_id=$('.sub_id').attr('data-topic_id');


  SelectChapter();
SelectTopic();
  selectSubTopic(topic_id);
})


</script>
<script>


	$('.loader-img').hide();
	$("#updateDataBtn").click(function (e) {
        var id=<?php echo $question->id; ?>

        var form = $('#updateDataForm')[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        console.log('formData', formData);
        console.log('form', form);
        $.ajax({
        	url: "admin/questionUpdateAjax",
        	type: "POST",
        	enctype: 'multipart/form-data',
            processData: false,  // Important!
            contentType: false,
            cache: false,
            data: formData,
            beforeSend: function () {
            	$('.loader-img').show();
            	$('#preloader').show();
            },
            complete: function () {
            	$('#preloader').fadeOut('slow', function () {
            		$(this).remove();
            	});
            	$('.loader-img').hide();
            },
            success: function (response) {
            	console.log('response', response.status);
            	if (response.status) {

            		$('#updateCourse').modal('hide');

            		$("#updateDataForm")[0].reset();

            		swal(
            			'Success!',
            			'Question Updated Successfully',
            			'success'
            			);

                    $('.'+id).closest('tr').remove();
            	} else {
            		console.log('error blank', response.message);
            		swal(
            			'Warning!',
            			'Something went wrong please again',
            			'warning'
            			);
            		;
            	}
            }, error: function (e) {
            	console.log('error', e);
            	swal(
            		'Oops...',
            		'Something went wrong!',
            		'error'
            		)
            }
        });
        e.preventDefault();
    });

 $(document).ready(function() {
    $('#summernote').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
    
    $('#summernote2').summernote({

      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
    $('#summernote3').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
    $('#summernote4').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
    $('#summernote5').summernote({
      toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['Insert', ['picture','math']],

    ]
  });
  });

  $('.typeD').on('change',function(){
    var answerType=$(this).val();
    console.log('answerD',answerType);
    if(answerType==1){
      $('.answerTextD').show();
      $('.answerFileD').css('display',"none");
    }else{
      $('.answerTextD').css('display',"none");
      $('.answerFileD').show();
    }
  });

  $('.type').on('change', function(){
    
    var type=$(this).val();
    if(type==1){
      $('.inputTextarea').show();
      $('.questionFile').css('display',"none");
    }else{
      $('.inputTextarea').css('display',"none");
      $('.questionFile').show();
    }
  });
  $('.typeC').on('change',function(){
    var answerType=$(this).val();
    console.log('answerc',answerType);
    if(answerType==1){
      $('.answerTextC').show();
      $('.answerFileC').css('display',"none");
    }else{
      $('.answerTextC').css('display',"none");
      $('.answerFileC').show();
    }
  });
  $('.typeB').on('change',function(){
    var answerType=$(this).val();
    console.log('answerB',answerType);
    if(answerType==1){
      $('.answerTextB').show();
      $('.answerFileB').css('display',"none");
    }else{
      $('.answerTextB').css('display',"none");
      $('.answerFileB').show();
    }
  });
  $('.typeA').on('change',function(){
    var answerType=$(this).val();
    if(answerType==1){
      $('.answerTextA').show();
      $('.answerFileA').css('display',"none");
    }else{
      $('.answerTextA').css('display',"none");
      $('.answerFileA').show();
    }
  });
</script>

