<?php
/**
 * Project: Lahore Motor Way City.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Subject')
@section('content')
	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage Subject</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="index.html">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage Subject</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="panel panel-primary basic_info_panel">
              <div class="panel-heading" style="padding: 20px 0px;background-color:#0088cc;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-user" style="color: #fff;"> </i> <b style="color: #fff!important;
              text-transform: uppercase;">Basic Info</b></h4></div>
              @component('_components.alerts-default')
        @endcomponent
              <form method="POST" action="{{route('subject.store')}}" enctype = "multipart/form-data">
              	@csrf
              <div class="panel-body">
                <div class="row">
                  

                  <div class="col-md-9" style="margin-bottom: 20px;">
                    <div class="row" style="margin: 10px 0px;">

                      <div class="col-md-12">
                       <label class="control-label"> Name</label>
                       <input name="sb_name" value="{{ old('sb_name') }}" type="text" placeholder="First Name" class="form-control">
                       @if ($errors->has('sb_name'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('sb_name')}}
                      </div>
                      @endif
                    </div>

                </div>
                {{-- <div class="row" style="margin: 10px 0px;">
                      <div class="col-md-10">
                        <label class="control-label">Sub Subject Name</label>
                          <input name="sub_subject[]" type="text" placeholder="Sub Subject Name" class="form-control">
                          
                      </div>
                      <div class="col-md-2" style="padding-top: 25px"> 
                        <a class="addMore btn btn-success firstBtn" href="javascript:void(0)">+</a>
                      </div>
                      <div class=" addMoreInput">
                        
                      </div>
                  </div> --}}
                <div class="row">


                <div class="col-md-6" style="padding-top: 8px">
                  <label class="control-label" style="padding-top: 0px !important">Status: <span class="req">*</span></label>
                  <div style="padding-top: 20px">
                    <label style="display:inline; padding-right: 35px">
                      <input name="status" id="facility_used_for_active"  class="icheck facility_status" type="radio" value="1"  checked> Active </label>
                      <label style="display:inline;">
                        <input name="status" id="facility_used_for_inactive" class="icheck facility_status" type="radio" value="0"  >  Inactive </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <label class="control-label">Description: <span class="req"></span></label>
                      <textarea  class="form-control" name="remark" id="document_description" maxlength="150"></textarea>

                    </div><!--form-group ends-->
                  </div>
                </div>
              </div>
            </div>
</div>
              
      <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">

                 <button class="btn btn-success" type="Submit" name="">Save</button>
                     </form>
              <a href="{{route('subject.index')}}" class="btn  btn-primary" type="reset">Cancel</a>
                  
                  </div>
                </div>
              </div>
            </div>
				</div>
			</div>
		</section>
	</section>
@endsection
@push('post-styles')
			
@endpush

		@push('post-scripts')
    <script type="text/javascript">
 
        function SomeDeleteRowFunction(o) {
          console.log(o);
            $(o).parents('.addChapterName').remove();
            var numItems = $('.addChapterName').length;
            console.log('count',numItems);
            if(numItems==0){
              console.log(numItems,'add More');
              $('.addMore').show();
            }

        }

       
        $(".addMore").click(function() {
            $("#subjectId").attr('readonly', 'true');
            option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Sub Subject Name</label>
                          
                              <input name="sub_subject[]" type="text" placeholder="Sub Subject Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            

            $('.addMoreInput').html(option);
            $('.addMore').css('display', 'none');
        });
       function  SomeAddRowFunction(obj){
          console.log('add more',obj);
          option = `<div class="addChapterName">
                          <div class="col-md-10">
                            <label class="control-label">Sub Subject Name</label>
                          
                              <input name="sub_subject[]" type="text" placeholder="Sub Subject Name" class="form-control">
                          </div>
                          <div class="col-md-2" style="padding-top: 25px"> 
                            <a class="btn btn-success" onclick="SomeAddRowFunction(this)" href="javascript:void(0)">+</a>
                            <a class=" btn btn-danger" onclick="SomeDeleteRowFunction(this)" href="javascript:void(0)">-</a>
                          </div>
                      </div>`;
            
            console.log('add more data',option);
            $('.addMoreInput').append(option);
       }
</script>



@endpush