<?php
/**
 * Project: Entrytest4u.
 * User: shafqat bhatti
 * Date: 12/03/2019
 * Time: 1:44 PM
 */
?>

@extends('_layouts.admin.default')
@section('title', 'Chapter')
@section('content')

	<section role="main" class="content-body">
		<header class="page-header">
			<h2>Manage Chapter</h2>
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="{{url('admin')}}">
							<i class="fa fa-home"></i>
						</a>
					</li>
					<li><span>Index</span></li>
				</ol>
				<a class="sidebar-right-toggle" ></a>
			</div>
		</header>
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Manage Chapter</h2>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="mb-md" style="float:left;">
							<a class="btn btn-primary" href="{{route('chapters.create')}}">Add Chapter &nbsp;<i class="fa fa-plus"></i></a>
						</div>
					</div>
				</div>
			
					@component('_components.alerts-default')
        		@endcomponent
					<table id="example" class="table border table-bordered">
						<thead>
							<tr style=" color: white; background: linear-gradient(#0088cc, #fff) !important; font-weight: unset;">
								<th>#id</th>
								<th>Class Name</th>
								<th>Subject Name</th>
								<th>Chapters</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
									@isset($subject)
									@foreach($subject as $pro)
									<tr>
										<td>@isset($pro->id){{$pro->id}}@endisset</td>
										<td>@isset($pro->grade->grade_name){{$pro->grade->grade_name}}@endisset</td>

										<td>@isset($pro->subject->sb_name){{$pro->subject->sb_name}}@endisset</td>
										<td>@isset($pro->chaper_name){{($pro->chaper_name)}}   <a href="{{route('bulk-question-edit.questionGetByChapter',$pro->id)}}"  class="btn-sm btn-info">{{$pro->questionCount()}}</a>
										@endisset  </td>

										<td>
										@if($pro->status)
										<a href="javascript:;" onclick="Deactive({{$pro->id}})"  class="btn btn-success btn-sm">publish</a> 
										@else 
										<a href="javascript:;" onclick="activeAccount({{$pro->id}})" class="btn btn-warning btn-sm">unpublish</a> 
										
										@endif</td>

										<td><a href="{{route('chapters.edit',$pro->id)}}" class="btn btn-info btn-sm">Edit</a></td>
									</tr>
									@endforeach
									@endisset
						</tbody>
					</table>
				</div>
			</div>
		</section>


@endsection

@push('post-styles')
	<link href="{{asset('assets/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
@endpush

@push('post-scripts')
<script src="{{asset('assets/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>
 <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
	$(document).ready(function() {
     //Default data table
     $('#default-datatable').DataTable();


     var table = $('#example').DataTable( {
     	"order": [[ 0, "desc" ]],
     	lengthChange: false,
     	buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
     } );

     table.buttons().container()
     .appendTo( '#example_wrapper .col-md-6:eq(0)' );

 } );

	function Deactive(id){
    swal({
      title: "Are you sure?",
      text: "You will be unpublish chapter!",
      icon: "warning",
      buttons: [
      'No, cancel it!',
      'Yes, I am sure!'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          method:"POST",
          url: "{{route('deleteChapter')}}",
          data : {'id': id},
          dataType:"json",
          success: function (response) {
            console.log('id', response);

            if (response.status) {
              
              swal(
                'Success!',
                'Account Deactive Successfully',
                'success'
                );

              location.reload(true);

            } else {
             swal(
              'Oops...',
              'Something went wrong!',
              'error'
              )
           }
         },
         error: function () {
          swal(
            'Oops...',
            'Something went wrong!',
            'error'
            )
        }
      });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    })

  }

  function activeAccount(id){
    swal({
      title: "Are you sure?",
      text: "You will be publish topic!",
      icon: "warning",
      buttons: [
      'No, cancel it!',
      'Yes, I am sure!'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          method:"POST",
          url: "{{route('deleteChapter')}}",
          data : {'id': id},
          dataType:"json",
          success: function (response) {
            console.log('id', response);

            if (response.status) {
              
              swal(
                'Success!',
                'Account Deactive Successfully',
                'success'
                );

              location.reload(true);

            } else {
             swal(
              'Oops...',
              'Something went wrong!',
              'error'
              )
           }
         },
         error: function () {
          swal(
            'Oops...',
            'Something went wrong!',
            'error'
            )
        }
      });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    })

  }
</script>
@endpush


