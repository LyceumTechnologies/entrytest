<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_videos', function (Blueprint $table) {
            $table->bigIncrements('id');

             $table->integer('sub_id');
            $table->integer('grade_id');
            $table->integer('chapter_id')->nullable();
            $table->integer('topic_id')->nullable();
            $table->integer('sub_topic_id')->nullable()
            $table->string('video_url')->nullable();
            $table->string('video_file')->nullable();

          
            $table->string('video_title')->nullable();
            $table->text('video_description')->nullable();

            $table->boolean('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer("updated_by")->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_videos');
    }
}
