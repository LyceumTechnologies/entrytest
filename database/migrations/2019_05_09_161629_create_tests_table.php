<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('test_name')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('course_id')->nullable();
            $table->integer('sub_id')->nullable();
            $table->integer('level')->nullable();
            $table->double('score')->default(0)->nullable();
            $table->integer('total_question')->default(0)->nullable();
            $table->integer('wrong_ans')->default(0)->nullable();
            $table->integer('correct_ans')->default(0)->nullable();
            $table->boolean('status')->default(1)->nullable();
            $table->string('description')->default(' ')->nullable();
            $table->date('test_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
