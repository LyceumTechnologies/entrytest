<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursePaperQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_paper_question_answers', function (Blueprint $table) {
            $table->bigIncrements('id');

             $table->bigIncrements('id');
            $table->integer('past_paper_question_id');
            $table->integer('type')->default(1)->nullable();
            $table->text('ans')->nullable();
            $table->integer('is_correct')->default(0)->nullable();
            
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_paper_question_answers');
    }
}
