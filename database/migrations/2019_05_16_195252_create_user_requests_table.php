<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('package_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('gateway')->nullable();
            $table->string('Bank_Mnemonic')->nullable();
            $table->date('Due_Date')->nullable();
            $table->date('Amount_Within_Due')->nullable();
            $table->date('Amount_After_DueDate')->nullable();
            $table->string('Billing_Month')->nullable();
            $table->date('Date_Paid')->nullable();
            $table->double('Amount_Paid')->default(0)->nullable();
            $table->string('Tran_Auth_Id')->nullable();
            $table->string('Bill_Status')->default('U')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_requests');
    }
}
