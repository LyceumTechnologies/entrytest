<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTodaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_todays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('question_id')->nullable();

            $table->integer('topic_id')->nullable();
            $table->integer('sub_id')->nullable();
            $table->text('question');

            $table->text('ans_A')->nullable();
            $table->text('ans_B')->nullable();
            $table->text('ans_C')->nullable();
            $table->text('ans_D')->nullable();
            $table->string('is_correct')->nullable();
            $table->date('questionDate')->nullable();


            $table->boolean('status')->default(0)->nullable();
            $table->string('description')->default(' ')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_todays');
    }
}
