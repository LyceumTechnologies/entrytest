<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursePaperQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_paper_questions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('course_past_paper')->nullable();

            $table->integer('grade_id')->nullable();
            $table->integer('sub_id')->nullable();
            $table->integer('chapter_id')->nullable();
            $table->integer('topic_id')->nullable();

            $table->text('question')->nullable();

            $table->integer('type')->default(1)->nullable();
            $table->integer('hasAns')->default(1)->nullable();
           
           $table->text('paper_image')->nullable();

            $table->boolean('status')->default(1)->nullable();
            $table->string('description')->default(' ')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_paper_questions');
    }
}
