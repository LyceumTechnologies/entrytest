<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('s_countryCode')->nullable(); 
            $table->string('s_lyceonianNo')->nullable(); 
            $table->string('s_initialReg')->nullable(); 
            $table->string('s_name')->nullable(); 
            $table->string('s_branchId')->nullable(); 
            $table->string('s_classPresent')->nullable(); 
            $table->string('s_section')->nullable(); 
            $table->string('s_sex')->nullable(); 
            $table->string('s_DOB')->nullable(); 
            $table->string('s_fatherName')->nullable(); 
            $table->string('s_phoneNo')->nullable(); 
            $table->string('std_mail')->nullable(); 
            $table->string('admission_date')->nullable(); 
            $table->string('session')->nullable(); 
            $table->string('is_active')->nullable(); 
            $table->string('is_freeze')->nullable(); 
            $table->string('freeze_month')->nullable();
            $table->string('freeze_year')->nullable(); 
            $table->string('freeze_toMonth')->nullable(); 
            $table->string('freeze_toYear')->nullable(); 
            $table->string('leaving_reason')->nullable(); 
            $table->string('leaving_date')->nullable(); 
            $table->string('FamilyNo_X')->nullable(); 
            $table->string('POB_X')->nullable(); 
            $table->string('Area_X')->nullable(); 
            $table->string('FCNIC_X')->nullable(); 
            $table->string('FOccupation_X')->nullable(); 
            $table->string('FQualification_X')->nullable(); 
            $table->string('Mname')->nullable(); 
            $table->string('MQualification_X')->nullable(); 
            $table->string('Nationality_X')->nullable(); 
            $table->string('Religion_X')->nullable(); 
            $table->string('MedicalHistory_X')->nullable(); 
            $table->string('MedicalCaution_X')->nullable(); 
            $table->string('Hobbies_X')->nullable(); 
            $table->string('DOReg')->nullable(); 
            $table->string('Remarks_X')->nullable(); 
            $table->string('Classadmitted_X')->nullable(); 
            $table->string('previousschool_X')->nullable(); 
            $table->string('MonthlyFee_X')->nullable(); 
            $table->string('Admissionfee')->nullable(); 
            $table->string('RegistrationFee_X')->nullable(); 
            $table->string('Securityfee_X')->nullable(); 
            $table->string('firstpayment_X')->nullable(); 
            $table->string('remainingamount_X')->nullable(); 
            $table->string('Scholarship_X')->nullable(); 
            $table->string('HOmetel_X')->nullable(); 
            $table->string('Officetel_X')->nullable(); 
            $table->string('HAddress_X')->nullable(); 
            $table->string('HAddress2_X')->nullable(); 
            $table->string('MobileM_X')->nullable(); 
            $table->string('MobileF_X')->nullable(); 
            $table->string('EmergencyMobile_X')->nullable(); 
            $table->string('officeAddress_X')->nullable(); 
            $table->string('paywhenunfreeze_X')->nullable(); 
            $table->string('misc1_X')->nullable(); 
            $table->string('misc1des_X')->nullable(); 
            $table->string('insurance_X')->nullable(); 
            $table->string('leftmarkedby_X')->nullable(); 
            $table->string('buffer1_X')->nullable(); 
            $table->string('reputationGrade_X')->nullable(); 
            $table->string('parentIsAmbassador_X')->nullable(); 
            $table->string('ambassadorRemarks_X')->nullable(); 
            $table->string('profileUpdateTimeStamp_X')->nullable(); 
            $table->string('profileUpdatedBy_X')->nullable(); 
            $table->string('isScholarshipAvailed_X')->nullable(); 
            $table->string('scholarshipFormNo_X')->nullable(); 
            $table->string('postby')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
