<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('images')->default('pic.png')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('cnic')->nullable();
            $table->string('api_token')->nullable();
            $table->string('role')->nullable();
          
            $table->string('password');
            $table->string('pwd');
            $table->string('available_id')->nullable();
            $table->string('confirmed_by')->nullable();
            $table->string('featured_by')->nullable();
            $table->boolean('status')->default(1);
            $table->string('remember_token')->nullable();

            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
