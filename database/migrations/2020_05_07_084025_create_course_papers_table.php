<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursePapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_papers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('past_paper_name');
            $table->string('slug');
            $table->integer('course_id')->nullable();
            $table->integer('year')->nullable();

            
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_papers');
    }
}
