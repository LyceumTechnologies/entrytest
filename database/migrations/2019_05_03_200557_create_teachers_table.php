<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
          $table->increments('id')->default(' ')->nullable();
            $table->string('name');
            $table->string('username')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('age')->nullable();
            $table->string('images')->default('pic.png')->nullable();
           
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('gender')->nullable();
            $table->string('country_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('cnic')->nullable();
            $table->string('api_token')->nullable();
            $table->string('device_token')->nullable();
            $table->string('user_type')->nullable();
            $table->string('confirmed_by')->nullable();
            $table->string('featured_by')->nullable();
            $table->string('timeout')->nullable();
            $table->boolean('status')->default(1);
            $table->string('description')->default(' ')->nullable();
            $table->boolean('activity')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
             $table->string('remark')->nullable();

            $table->integer('view_profile')->nullable();
            $table->integer('search_profile')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
