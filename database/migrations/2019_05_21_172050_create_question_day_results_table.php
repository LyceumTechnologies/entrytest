<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionDayResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_day_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('q_today_id');
            $table->integer('user_id');
            $table->integer('is_correct')->default(0)->nullable();
            $table->string('ans')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_day_results');
    }
}
