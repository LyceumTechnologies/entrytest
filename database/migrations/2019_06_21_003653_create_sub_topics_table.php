<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_topics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('topic_id')->nullable();
            $table->integer('sr_no')->default(1)->nullable();
            $table->string('sub_topic_name');

            $table->integer('hasQuestion')->default(1)->nullable();
            $table->string('description')->default(' ')->nullable();
            
            $table->boolean('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_topics');
    }
}
