<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('admins')->delete();
        DB::table('admins')->insert(array(
                'name' => 'Tahir Nadeem',
                'email' => 'info@americanlyceum.com',
                'phone' => '03076142561',
                'api_token' => str_random(60),
                'password' => Hash::make('loader123'),
                'pwd' =>('loader123'),
                'created_at' => now(),
            )
        );
        DB::table('admins')->insert(array(
                'name' => 'zaheer',
                'email' => 'zaheer@shahzad.com',
                'phone' => '03456140566',
                'api_token' => str_random(60),
                'password' => Hash::make('pakistani'),
                'pwd' => Hash::make('pakistani'),
                'created_at' => now(),
            )
        );
        DB::table('admins')->insert(array(
                'name' => 'yousaf',
                'email' => 'yousaf@hotamil.com',
                'phone' => '031561405587',
                'api_token' => str_random(60),
                'password' => Hash::make('punj@b'),
                'pwd' => ('punj@b'),
                'created_at' => now(),
            )
        );
         DB::table('admins')->insert(array(
                'name' => 'naeem',
                'email' => 'naeem@gmail.com',
                'phone' => '030861405888',
                'api_token' => str_random(60),
                'password' => Hash::make('h@llo789'),
                'pwd' => ('h@llo789'),
                'created_at' => now(),
            )
        );
         DB::table('admins')->insert(array(
                'name' => 'asif',
                'email' => 'asifmahmood@gmail.com',
                'phone' => '030761405644',
                'api_token' => str_random(60),
                'password' => Hash::make('123asd'),
                'pwd' => ('123asd'),
                'created_at' => now(),
            )
        );
         DB::table('admins')->insert(array(
            'name' => 'Yousuf Khalid',
            'email' => 'khalidyousuf696@gmail.com',
            'phone' => '03043868223',
            'api_token' => str_random(60),
            'password' => bcrypt('128232'),
            'pwd' => ('128232'),
            'created_at' => now(),
         ));
    }
}
