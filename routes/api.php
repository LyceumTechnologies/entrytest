<?php

use Illuminate\Http\Request;
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('api/book/data','Api\QueryLogController@bookData');
Route::post('question/inserted','Api\QueryLogController@QuestionStore');
Route::get('app/version','Api\ApiSettingController@AppVersion');
Route::get('check/question/ans','Api\QueryLogController@checkQuestionAns');
Route::get('question/today/enter','Api\QueryLogController@questionToday');
Route::POST('app/version/update','Api\V2\ApiUserVisitController@updateVersion');

Route::group(['prefix' => 'v1'],function() {
	
	Route::POST('bill/inquiry','Api\JazzCashController@billInquirey');
	Route::POST('bill/payment','Api\JazzCashController@PaymentTransaction');

	Route::group(['prefix' => 'user'],function() {
		//////////Authentication///////////////
		Route::post('/register','Api\ApiUserController@register');
		Route::post('/login','Api\ApiUserController@login');
  		Route::post('/forgot/password','Api\ApiUserController@forgotton');
  		//////////
		Route::get('question','Api\ApiQuestionController@question');
		Route::post('question/subject-wise','Api\ApiQuestionController@questionSubjectWise');
		Route::get('course','Api\ApiCourseController@getCourse');
		Route::post('course/subjects','Api\ApiCourseController@CourseHasSubject');
		Route::post('subject/chapter','Api\ApiCourseController@SubjectHasChapter');

		Route::post('course/question','Api\ApiCourseController@courseQuestion');
		Route::group(['middleware' => ['auth:api']],function(){
			Route::post('password/update','Api\ApiUserController@set_user_password');
			Route::post('/update/profile','Api\ApiUserController@profile');

			Route::post('test/start','Api\ApiTestController@testCreate');
			Route::post('test/question','Api\ApiTestController@testQuestion');
			Route::get('question/today','Api\ApiTestController@questionToday');
			Route::get('dignostic/question','Api\ApiTestController@dignoseTest');
			Route::post('dignostic/test/question','Api\ApiTestController@DignoseTestQuestion');
			Route::get('package','Api\ApiQuestionController@GetPackage');
			Route::get('test/history','Api\ApiTestController@TestHistory');
			Route::post('test/result','Api\ApiTestController@TestHistoryById');

			Route::post('result/today/question','Api\ApiTestController@AnswerToday');
			
			
		});
	});
});

Route::group(['prefix' => 'v2'],function() {
	
	Route::POST('bill/inquiry','Api\V2\JazzCashController@billInquirey');
	Route::POST('bill/payment','Api\V2\JazzCashController@PaymentTransaction');
	Route::get('notification','Api\V2\ApiUserVisitController@notifcation');

	

	Route::group(['prefix' => 'user'],function() {
		//////////Authentication///////////////
		Route::post('/register','Api\V2\ApiUserController@register');
		Route::post('/login','Api\V2\ApiUserController@login');
  		Route::post('/forgot/password','Api\V2\ApiUserController@forgotton');
  		//////////
		Route::get('question','Api\V2\ApiQuestionController@question');
		Route::post('question/subject-wise','Api\V2\ApiQuestionController@questionSubjectWise');
		Route::get('course','Api\V2\ApiCourseController@getCourse');
		Route::post('course/subjects','Api\V2\ApiCourseController@CourseHasSubject');
		Route::post('subject/chapter','Api\V2\ApiCourseController@SubjectHasChapter');


		Route::post('course/question','Api\V2\ApiCourseController@courseQuestion');
		Route::group(['middleware' => ['auth:api']],function(){
			Route::POST('notification/create','Api\V2\ApiUserVisitController@notifcationCreate');
			
			
			Route::post('password/update','Api\V2\ApiUserController@set_user_password');
			Route::post('/update/profile','Api\V2\ApiUserController@profile');

			Route::post('test/start','Api\V2\ApiTestController@testCreate');
			Route::post('test/question','Api\V2\ApiTestController@testQuestion');
			Route::get('question/today','Api\V2\ApiTestController@questionToday');
			Route::get('dignostic/question','Api\V2\ApiTestController@dignoseTest');
			Route::post('dignostic/test/question','Api\V2\ApiTestController@DignoseTestQuestion');
			Route::get('package','Api\V2\ApiQuestionController@GetPackage');
			Route::get('test/history','Api\V2\ApiTestController@TestHistory');
			Route::post('test/result','Api\V2\ApiTestController@TestHistoryById');

			Route::post('result/today/question','Api\V2\ApiTestController@AnswerToday');
			Route::post('visited','Api\V2\ApiUserVisitController@register');

			
			
		});
	});
});
