<?php
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('/', function () {
        return view('web.public-index');
    });
    Route::get('/package', function () {
        return view('web.public-payment');
    });
    Route::get('/privacy/policy',function(){
        return view('web.public-policy');
    });
    Route::get('/user/login',function(){
        return view('web.olive.public-login');
    })->name('user_web.login');
     Route::get('/user/register',function(){
        return view('web.olive.public-register');
    })->name('user_web.register');
    Route::post('contact/form','web\MainController@ContactFom')->name('ContactFom');
    Route::post('user/regiser','web\MainController@UserRegister')->name('user.register');
    Route::get('service','web\MainController@service')->name('service');
    Route::get('about','web\MainController@about')->name('about');
    Route::get('login','web\MainController@login')->name('login');
    Route::resource('easy-paisa','web\EasyPaisaController');
    Route::any('api', 'SoapController@server');
    Route::any('client/api','SoapController@clientWebService');
Route::prefix('admin')->group(function () {
	Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
		// Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
		// Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
		Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
		Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');


    Route::group(['middleware' => ['auth:admin']], function () {
    	Route::get('/', 'admins\DashboardController@index')->name('dashboard');
    	Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
        Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
        Route::resource('teacher','admins\TeacherController');
        Route::resource('user','admins\UserController');
        Route::resource('admin','admins\AdminController');
        Route::resource('course','admins\CourseController');
        Route::resource('subject','admins\SubjectController');
        Route::resource('level','admins\LevelController');
        Route::resource('question','admins\QuestionController');
        Route::resource('non-verified','admins\Marketing\NonVerifiedUserController');
        Route::resource('bulk-question-edit','admins\BulkQuestionEditController');
        Route::get('topic/bulk-question-edit/{id}','admins\BulkQuestionEditController@questionGetByTOpic')->name('bulk-question-edit.questionGetByTOpic');

        Route::get('chapter/bulk-question-edit/{id}','admins\BulkQuestionEditController@questionGetByChapter')->name('bulk-question-edit.questionGetByChapter');



        Route::post('questionGetAnswer','admins\BulkQuestionEditController@questionGetAnswer')->name('question.questionGetAnswer');
        Route::post('editQuestionTopic','admins\BulkQuestionEditController@editQuestionTopic')->name('question.editQuestionTopic');

        

        Route::resource('approved-question','admins\ApprovedQuestionController');
        Route::resource('user-question','admins\UserQuestionController');
        Route::resource('chapter','admins\ChapterController');
        Route::post('delete/Topic','admins\ChapterController@deleteTopic')->name('deleteTopic');

        Route::resource('chapters','admins\SubjectChapterController');
        Route::post('chapters/status/change','admins\SubjectChapterController@deleteChapter')->name('deleteChapter');



        Route::resource('topic','admins\SectionTopicController');
        Route::resource('own-question','admins\OwnQuestionController');
        Route::resource('report','admins\ReportController');
        Route::resource('user-package','admins\UserPackageController');
        Route::resource('grade','admins\GradeController');
        Route::resource('difficulty','admins\DifficultyController');
        Route::resource('notification','admins\NotificationController');
        Route::resource('question-remove','admins\QuestionRemoveController');
        Route::resource('question-export','admins\SubjectQuestionExportController');
        Route::resource('question-edit','admins\QuestionEditController');
        Route::resource('notes','admins\NotesController');
        Route::resource('video','admins\ChapterVideoController');
        Route::resource('challenge','admins\Challenge\ChallengeController');
        Route::resource('challenge-question','admins\Challenge\ChallengeQuestionController');

        

        Route::resource('past-paper','admins\Paper\CoursePaperController');
         Route::resource('question-past-paper','admins\Paper\QuestionInPastPaperController');
         Route::get('question-pastpaper/{id}','admins\Paper\QuestionInPastPaperController@PastPaperQuestionstore')->name('pastpaper.PastPaperQuestionstore');
         
         Route::get('question-past-paper-create/{id}','admins\Paper\QuestionInPastPaperController@addPastPaperQuestion')->name('addPastPaperQuestion');

         Route::get('question-challenge-create/{id}','admins\Challenge\ChallengeQuestionController@addChallengeQuestion')->name('addChallengeQuestion');
         Route::post('storeQuestionChallenge','admins\Challenge\ChallengeQuestionController@storeQuestionChallenge')->name('storeQuestionChallenge');



         
        Route::post('storeQuestionPastPaper','admins\Paper\QuestionInPastPaperController@storeQuestionPastPaper')->name('storeQuestionPastPaper');




         


        Route::get('question/approved/report','admins\AdminController@approvedReport')->name('approvedReport');


        Route::get('course/subject/detail','admins\SectionTopicController@courseAddTopic')->name('courseAddTopic');
        Route::get('subject/topic-section/{id}/{topic_id}','admins\SectionTopicController@subjectTopics')->name('subjectTopics');
        Route::post('subject/add/topic/','admins\SectionTopicController@subjectAddTopic')->name('subjectAddTopic');


        /////////////////// ajax call/////////////////////
        Route::post('/updateChapter/{id}','admins\ChapterController@updateChapter')->name('updateChapter');
        Route::post('subject/topics','admins\QuestionController@SubjectHasTopic')->name('SubjectHasTopic');
        Route::post('topic/has/subTopic','admins\QuestionController@TopicHasSubtopic')->name('TopicHasSubtopic');
        Route::post('subject/question','admins\QuestionController@getQuestionBySubject')->name('getQuestionBySubject');

        Route::post('subject/question-edit','admins\QuestionController@getQuestionEditBySubject')->name('getQuestionEditBySubject');


        Route::post('approve/question/ans','admins\QuestionController@approveQuestionAns')->name('approveQuestionAns');
        Route::post('dis-approve/questoin','admins\QuestionController@disApproveQuestion')->name('disApproveQuestion');
        Route::post('approved/question','admins\QuestionController@approvedQuestion')->name('approvedQuestion');
        Route::post('unapprove/question/ans','admins\QuestionController@upApproveQuestionAns')->name('upApproveQuestionAns');
        Route::get('own/question','admins\OwnQuestionController@getOwnQuestion')->name('getOwnQuestion');
        Route::post('user/information','admins\UserPackageController@userInformation')->name('userInformation');
        Route::post('/updateSubject/{id}','admins\SubjectController@updateSubject')->name('updateSubject');
        Route::get('question/get/subject/{id}','admins\ApprovedQuestionController@questionget')->name('questionget');

        Route::get('question-edit/get/subject/{id}','admins\QuestionEditController@questionEditget')->name('questionEditget');
        Route::POST('question/delete','admins\QuestionRemoveController@deleteQuestion')->name('deleteQuestion');


        // AJAX Question
        Route::get('questionEditModel/{id}','Ajax\CommonFirstAjaxController@questionEditModel')->name('questionEditModel');
        Route::post('ChapterHasTopic','Ajax\CommonFirstAjaxController@ChapterHasTopic')->name('ChapterHasTopic');
        Route::post('SubjectChapter','Ajax\CommonFirstAjaxController@SubjectChapter')->name('SubjectChapter');
        Route::post('questionUpdateAjax','Ajax\CommonFirstAjaxController@questionUpdateAjax')->name('questionUpdateAjax');


         Route::get('PastPaperQuestionEditModel/{id}','Ajax\AjaxPastPaperController@questionEditModel')->name('PastPaperQuestionEditModel');


        

        




    });

});
    Route::prefix('user')->group(function () {

        Route::post('submit/login','Auth\UserLoginController@login')->name('user.submitLogin');
        Route::post('submit/register','Auth\UserLoginController@register')->name('user.submitRegister');
        Route::get('logout','Auth\UserLoginController@logout')->name('user.logout');
        Route::get('verify/email/{id}','Auth\UserLoginController@confirmEmail')->name('user.confirmEmail');
        

        Route::post('password/request/email', 'Auth\UserFortgotPasswordController@token_create')->name('user.create_token');
        Route::get('/password/reset/{token}', 'Auth\UserFortgotPasswordController@showResetForm')->name('user.password.reset');
        Route::post('/password/reset', 'Auth\UserFortgotPasswordController@reset')->name('user.reset');

         Route::group(['middleware' => ['auth:web']], function () {

                Route::get('/','Users\DashboardController@index')->name('user.dashboard');
                Route::get('/test','Users\DashboardController@testPage')->name('user.test');
                Route::get('success/storey','Users\DashboardController@success_story')->name('success_story');
                Route::get('subject/question/{id}','Users\DashboardController@question')->name('user.question');
                Route::get('/practice','Users\DashboardController@practice')->name('user.practice');
                Route::get('subject/{id}/{course_id}','Users\SubjectTopicController@topicSection')->name('subject.topic');
                Route::post('subject/topic/question','Users\SubjectTopicController@getQuestion')->name('getQuestion');

                Route::get('previous/test','Users\DashboardController@previousTest')->name('previous.test');
                Route::get('fulllength/test','Users\DashboardController@fulllength')->name('fulllength.test');
                Route::get('viewnotes/test','Users\DashboardController@viewnotes')->name('viewnotes.test');
                Route::get('writenotes/test','Users\DashboardController@writenotes')->name('writenotes.test');
                /////////////////// Ajax sub Topic /////////////////
                Route::post('subSection','Users\SubjectTopicController@subSection')->name('subSection');
                Route::post('testQuestion/','Users\UserTestController@testQuestion')->name('testQuestion');
                Route::post('question/book/marked','Users\UserTestController@BookMark')->name('BookMark');
                Route::post('user/test-result/','Users\SubjectTopicController@testResult')->name('testResult');
         });
    });

    Route::post('subject/topicsApi','Users\DashboardController@subjectTopic')->name('subjectTopic');
    Route::post('SubjectHasChapter','HomeController@SubjectHasChapter')->name('SubjectHasChapter');



    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
