<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
class NonVerifiedUserEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
      public $data;
    public function __construct($request)
    {
        $this->data=$request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->data as $user){
            if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
               $emails=$user->email;

              

                Mail::send('mail.accountActivateMail', ['data'=>$user], function($message) use ($emails) {    
                    $message->to($emails)->subject('Verify Account');    
                });
            }


   
               

            
                
        }
             
    }
}
