<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
     protected $guarded=[];
    protected $hidden=['hasQuestion'];

    public function subject(){
    	return $this->belongsTo(Subject::class,'sub_id');
    }

    //////////////// self
    public function subSection(){
    	return $this->hasMany(self::class,'parent_id','id');
    }
    public function parentSection(){
    	return $this->belongsTo(self::class,'parent_id','id');
    }
}
