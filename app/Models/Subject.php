<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded=[];
    protected $hidden=['parent_id'];

    public function subject(){
        return $this->belongsTo(Subject::class,'sub_id');
    }
    public function courseHasSubject(){
    	return $this->hasMany(CourseSubject::class,'course_id');
    }
    public function chapters(){
    	return $this->hasMany(Topic::class,'sub_id');
    }


    public function questions(){

        return $this->hasMany(Question::class,'sub_id');
    }


    public function subjectChapter(){
        return $this->hasMany(Chapter::class,'sub_id');
    }
    public function filterChapters(){
    	return $this->hasMany(Topic::class,'sub_id')->where('hasQuestion','>',10)->where('status',1);
    }

    ///////////////// self join///////////
    public function subSubject(){
        return $this->hasMany(self::class,'parent_id','id');
    }
    public function parentSubject(){
        return $this->belongsTo(self::class,'parent_id','id');
    }

}
