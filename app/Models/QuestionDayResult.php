<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionDayResult extends Model
{
    protected $guarded=[];
    public function QuestionToday(){
    	return $this->belongsTo(QuestionToday::class,'q_today_id');
    }

    public function user(){
    	return $this->belongsTo(User::class,'user_id');
    }
}
