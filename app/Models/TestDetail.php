<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestDetail extends Model
{
     protected $guarded=[];
     public function test(){
     	return $this->belongsTo(Test::class,'test_id');
     }
     public function question(){
     	return $this->belongsTo(Question::class,'question_id');
     }
}
