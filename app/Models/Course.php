<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded=[];

    public function couseSubject(){
    	return $this->hasMany(CourseSubject::class,'course_id');
    }

    function hasSubject($module){
    	// dd($this->subject);
        if(isset($this->couseSubject) && !empty($this->couseSubject)) {
            foreach ($this->couseSubject as $roleModule) {
                if ($roleModule->sub_id == $module->id)
                    return true;
            }
        }
     return false;
    }
}
