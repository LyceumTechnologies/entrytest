<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable =['sub_id','grade_id','chapter_id','hasQuestion','name','status','description','created_by','updated_by','created_at','updated_at'];
    protected $hidden=['hasQuestion'];

    public function subject(){
    	return $this->belongsTo(Subject::class,'sub_id');
    }
     public function grade(){
        return $this->belongsTo(Grade::class,'grade_id');
    }

     public function chapter(){
        return $this->belongsTo(Chapter::class,'chapter_id');
    }

    public function TopicQuestion(){
    	return $this->hasMany(Question::class,'topic_id');
    }
    public function CountQuestion(){
    	return $this->hasMany(Question::class,'topic_id')->count();
    }

    public function subTopic(){
        return $this->hasMany(SubTopic::class,'topic_id');
    }
}
