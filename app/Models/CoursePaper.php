<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class CoursePaper extends Model
{
    protected $guarded=[''];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'past_paper_name'
            ]
        ];
    }

    public function course(){
    	return $this->belongsTo(Course::class,'course_id');
    }
}
