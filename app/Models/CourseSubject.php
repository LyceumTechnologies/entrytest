<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseSubject extends Model
{
    protected $guarded=[];

    public function subject(){
    	return $this->belongsTo(Subject::class,'sub_id');
    }
}
