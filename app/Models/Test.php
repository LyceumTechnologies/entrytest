<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $guarded=[];

    public function user(){
    	return $this->belongsTo(User::class,'user_id');
    }
    public function course(){
    	return $this->belongsTo(Course::class,'course_id');
    }
    public function testDetail(){
    	return $this->hasMany(TestDetail::class,'test_id');
    }

    public function subject(){
        return $this->belongsTo(Subject::class,'sub_id');
    }
    
    public function totalQuestionCount(){
        return $this->hasMany(TestDetail::class,'test_id')->count();
    }
    public function wrongQuestionCount(){
       return $this->hasMany(TestDetail::class,'test_id')->where('is_correct',0)->count();
    }
    public function correctQuestionCount(){
       return $this->hasMany(TestDetail::class,'test_id')->where('is_correct',1)->count();
    }
    public function SkipQuestionCount(){
       return $this->hasMany(TestDetail::class,'test_id')->where('is_correct',null)->count();
    }
}
