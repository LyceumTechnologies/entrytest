<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guarded=[];

     public function UserHasQuestion(){
        return $this->hasMany(Question::class,'created_by')->count();
    }
    public function todayCountQuestion(){
    	 return $this->hasMany(Question::class,'created_by')->whereDate('created_at', date('Y-m-d'))->count();
    }

     public function UserHasPastQuestion(){
        return $this->hasMany(CoursePaperQuestion::class,'created_by')->count();
    }
    public function todayCountPastQuestion(){
         return $this->hasMany(CoursePaperQuestion::class,'created_by')->whereDate('created_at', date('Y-m-d'))->count();
    }



    
    public function question(){
    	return $this->hasMany(Question::class,'created_by');
    }
    public function userGroupQuestion(){
    	return $this->hasMany(Question::class,'created_by')->groupBy('sub_id');
    }
    public function UserApprovedQuestion(){
        return $this->hasMany(Question::class,'created_by')->where('status',1)->count();
    }

    public function approvedCount(){
        return $this->hasMany(Question::class,'approved_by')->where('status',1)->count();
    }
    public function disApprovedCount(){
        return $this->hasMany(Question::class,'updated_by')->where('status',3)->count();
    }
    public function ownDisApprovedCount(){
        return $this->hasMany(Question::class,'created_by')->where('status',3)->count();
    }
    public function dailyApprovedCount(){
        return $this->hasMany(Question::class,'approved_by')->where('status',1)->whereDate('created_at', date('Y-m-d'))->count();
    }
    
}
