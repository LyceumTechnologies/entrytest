<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $guarded=[];

    protected $hidden = [
        'created_by', 'updated_by','created_at', 'updated_at',
    ];
}
