<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChallengeQuestion extends Model
{
    protected $guarded=[''];

    public function challengeAnswers(){
    	return $this->hasMany(ChallengeQuestionAnswer::class,'challenge_paper_question_id');
    }


    public function answers(){
    	return $this->hasMany(ChallengeQuestionAnswer::class,'challenge_paper_question_id');
    }



   
    public function course(){
        return $this->belongsTo(Grade::class,'grade_id');
    }

    public function chapter(){
        return $this->belongsTo(Chapter::class,'chapter_id');
    }
    public function subject(){
    	return $this->belongsTo(Subject::class,'sub_id');
    }
    public function topic(){
    	return $this->belongsTo(Topic::class,'topic_id');
    }


     public function difficulty(){
        return $this->belongsTo(Difficulty::class,'dif_id');
    }

    public function SubTopic(){
        return $this->belongsTo(Topic::class,'sub_topic_id');
    }

    public function author(){
        return $this->belongsTo(Admin::class,'created_by');
    }
    public function approvedBy(){
        return $this->belongsTo(Admin::class,'approved_by');
    }
}
