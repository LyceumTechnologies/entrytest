<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $guarded=[];


    public function subject(){
    	return $this->belongsTo(Subject::class,'sub_id');
    }


    public function grade(){
    	return $this->belongsTo(Grade::class,'course_id');
    }

    public function question(){
    	return $this->hasMany(Question::class,'chapter_id');
    }
    public function questionCount(){
    	return $this->hasMany(Question::class,'chapter_id')->count();
    }

}
