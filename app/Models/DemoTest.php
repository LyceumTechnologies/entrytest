<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemoTest extends Model
{
    protected $guarded=[];
    public function testQuestion(){
    	return $this->hasMany(DemoTestQuestion::class,'test_id');
    }
}
