<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemoTestQuestion extends Model
{
    protected $guarded=[];
    public function demoQuestion(){
    	return $this->belongsTo(Question::class,'question_id');
    }

    public function demoTest(){
    	return $this->belongsTo(DemoTest::class,'test_id');
    }
}
