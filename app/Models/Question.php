<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable=['sub_topic_id','grade_id','dif_id','approved_by','type','topic_id','sub_id','question','correctionReason','status','created_by','updated_by','description','created_at','updated_at','chapter_id'];
    protected $hidden=['hasAns','sub_topic_id','grade_id','dif_id','approved_by'];

    public function answers(){
    	return $this->hasMany(Answer::class,'question_id');
    }
    public function course(){
        return $this->belongsTo(Grade::class,'grade_id');
    }

    public function chapter(){
        return $this->belongsTo(Chapter::class,'chapter_id');
    }
    public function subject(){
    	return $this->belongsTo(Subject::class,'sub_id');
    }
    public function topic(){
    	return $this->belongsTo(Topic::class,'topic_id');
    }


     public function difficulty(){
        return $this->belongsTo(Difficulty::class,'dif_id');
    }

    public function SubTopic(){
        return $this->belongsTo(Topic::class,'sub_topic_id');
    }

    public function author(){
        return $this->belongsTo(Admin::class,'created_by');
    }
    public function approvedBy(){
        return $this->belongsTo(Admin::class,'approved_by');
    }


    
}
