<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $guarded=[];

    public function questions(){
    	return $this->hasMany(Question::class,'grade_id');
    }
}
