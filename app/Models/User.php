<?php

namespace App\Models;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded=[];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','course_id','province_id','address'
    ];

    public function BillInquiry(){
        return $this->hasOne(UserRequest::class,'user_id')->orderBy('id','DESC');
    }
    public function userPackage(){
        return $this->hasOne(UserHasPacakge::class,'user_id')->orderBy('id','DESC');
    }

   

    

}
