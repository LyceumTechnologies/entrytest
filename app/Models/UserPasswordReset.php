<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPasswordReset extends Model
{
    protected $guarded=[];
	protected $primaryKey = 'email';    
	public $timestamps = false;
}
