<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChallengeQuestionAnswer extends Model
{
    protected $guarded=[''];

    public function ChallengeQuestion(){
    	return $this->belongsTo(ChallengeQuestion::class,'challenge_paper_question_id');
    }

    
}
