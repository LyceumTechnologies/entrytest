<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursePaperQuestionAnswer extends Model
{
    protected $guarded=[''];


    public function CoursePaperQuestion(){
    	return $this->belongsTo(CoursePaperQuestion::class,'past_paper_question_id');
    }
}
