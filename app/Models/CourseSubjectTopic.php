<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseSubjectTopic extends Model
{
    protected $guarded=[];

    public function courseSubject(){
    	return $this->belongsTo(CourseSubject::class,'course_sub_id');
    }
}
