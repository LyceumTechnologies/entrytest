<?php

namespace App\Http\Middleware;

use Closure;

class RedirectInvalidIPs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->ip() != "192.168.0.155") {
       
            return redirect('home');
        }

        return $next($request);
    }
}
