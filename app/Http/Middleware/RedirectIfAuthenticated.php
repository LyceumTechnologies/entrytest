<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->ajax() || $request->wantsJson()) {
            return response(['status'=>false,'message'=>'Unauthorized.'], 401);
        }
        
        switch ($guard) 
        {
            
            case 'admin':
            if(Auth::guard($guard)->check()){
                return redirect()->route('admin.dashboard');
            }
            break;

            case 'web':
            if(Auth::guard($guard)->check()){
                return redirect()->route('user.dashboard');
            }
            break;

            case 'api':
            if(Auth::guard($guard)->check()){
                return response()->json(['status'=>true,'message' => 'User has been successfully login.'], 200);
            }
            break;

            default:
            if (Auth::guard($guard)->check()) {
                return redirect()->url('/');   
            }
            break;
        }
        return $next($request);
    }
}
