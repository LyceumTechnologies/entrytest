<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChallengeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'challenge_name'=>'required',
            'course_id'=>'required',
            'challenge_date'=>'required',
            'challenge_price'=>'required',
            'reward_challenge'=>'required'
        ];
    }
}
