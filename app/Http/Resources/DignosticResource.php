<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DignosticResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name!=null?$this->name:'',
            'status' => $this->status,
            'description' => $this->description!=null?$this->description:'',
        ];
    }
}
