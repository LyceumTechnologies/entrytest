<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TopicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $request;

        // return [

        //     'id' => $this->id,
        //     'name' => $this->name!=null?$this->name:'',
        //     'hasQuestion' => $this->hasQuestion!=null?$this->hasQuestion:0,
        // ];
    }
}
