<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=> $this->id,
            "name"=> $this->name!=null?$this->name:'',
            "email"=> $this->email!=null?$this->email:'',
            "phone"=> $this->phone!=null?$this->phone:'',
            "images"=> $this->images!=null?$this->images:'',
            "course_id"=> $this->course_id!=null?$this->course_id:'',
            "view_profile"=> $this->view_profile!=null?$this->view_profile:0,
            "address"=> $this->address!=null?$this->address:'',
            "province_id"=> $this->province_id!=null?$this->province_id:'',
            "gender"=> $this->gender!=null?$this->gender:'',
            "country_id"=> $this->country_id!=null?$this->country_id:'',
            "city_id"=> $this->city_id!=null?$this->city_id:'',
            "api_token"=> $this->api_token!=null?$this->api_token:'',
            "status"=> $this->status!=null?$this->status:0,
            "dig_score"=> $this->dig_score!=null?$this->dig_score:0,
            "dig_total"=> $this->dig_total!=null?$this->dig_total:0,
            "score"=> $this->score!=null?$this->score:'',
            "total_attempt"=> $this->total_attempt!=null?$this->total_attempt:0,
           
          
        ];
    }
}
