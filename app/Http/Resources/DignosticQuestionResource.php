<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DignosticQuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
                return [

                    'id'=>$this->id,
                    'question'=>$this->question!=null?$this->question:'',
                    'correctionReason'=>$this->correctionReason!=null?$this->correctionReason:'',
                    'description'=>$this->description!=null?$this->description:'',
                    'answers'=>AnswerResource::collection($this->answers),
                ];
            

       
    }
}
