<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AnswerResource extends JsonResource
{
    
    public function toArray($request)
    {
                    
                   
                         return [
                            'id'=>$this->id,
                            'ans'=>$this->ans!=null?$this->ans:'',
                            'is_correct'=>$this->is_correct!=null?$this->is_correct:0,
                        ];
                    
       
    }
}
