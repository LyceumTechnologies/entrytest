<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use App\Models\Question;
use Auth;
use App\Models\Answer;
use App\Models\Topic;
use App\Models\Subject;
use App\Models\Section;
use App\Models\Grade;
use App\Models\Difficulty;
use Session;

class ApprovedQuestionController extends Controller
{
    public function create()
    {
    	$topic=Topic::all();
    	$subject=Subject::all();
        return view('admin.approved.create',compact('subject','topic'));
    }

    public function index(){
    	$subject=Subject::where('status',1)->get();
    	return view('admin.approved.index');
    }
    public function questionget($obj){

        $question=Question::where('topic_id',$obj)->with('answers')->where('status',0)->limit(50)->get();
        $topic=Topic::all();
        $subject=Subject::all();
        $difficulty=Difficulty::all();
        $grade=Grade::all();
        if(count($question)){
            $subject=Subject::where('status',1)->get();
            return view('admin.approved.question',compact('question','subject','topic','subject','grade','difficulty'));
          
        }
          session()->flash('error_message', __('Record not found'));
            return redirect()->back();
    }
}
