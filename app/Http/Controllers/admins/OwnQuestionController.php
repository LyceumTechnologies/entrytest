<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Question;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\Answer;
use App\Models\Topic;
use App\Models\Subject;
use Session;
use Image;
class OwnQuestionController extends Controller
{
    public function index(){
    	// dd(date('Y-m-d'));
    	$question=Question::where('created_by',Auth::user()->id)->with('topic','answers')->get();
    	$topic=Topic::all();
        $subject=Subject::all();
    	return view('admin.question.own-question',compact('question','topic','subject'));
    }

    public function getOwnQuestion(){

    }
}
