<?php

namespace App\Http\Controllers\admins\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Auth;
use Session;
use App\Jobs\NonVerifiedUserEmailJob;
use App\Jobs\NonVerifiedUserSmsJob;


class NonVerifiedUserController extends Controller
{
    public function index(){
    	return view('admin.marketing.nonVerified.index');
    }


    public function store(Request $request){

    	
    	$users=User::where('email_verified_at',null)->get();

    	if($request->name=='email'){
    		
    		NonVerifiedUserEmailJob::dispatch($users);
    		session()->flash('success_message', __('Email Send Successfully'));
            return redirect()->back();
    	}else{
    		dd('user');
    		NonVerifiedUserSmsJob::dispatch($users);
    		session()->flash('success_message', __('Email Send Successfully'));
            return redirect()->back();
    	}
    	
    	session()->flash('success_message', __('Message Send Successfully'));
            return redirect()->back();
    	 
    }
}
