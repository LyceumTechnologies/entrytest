<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Question;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\Answer;
use App\Models\Topic;
use App\Models\Subject;
use App\Models\Admin;
use Session;
use Image;
use DB;
class ReportController extends Controller
{
    public function index(){
    	$user= Admin::orderBy('id','DESC')->get();
    	return view('admin.report.entryCount.index',compact('user'));
    }
    public function show($id){
    	$user= Admin::with('question')->find($id);
    	$question=Question::where('created_by',$id)->with('subject')->select(DB::raw('count(*) as num'))->groupBy('sub_id')->get();
    	dd($question);
    	return view('admin.report.entryCount.index',compact('user'));
    }
}
