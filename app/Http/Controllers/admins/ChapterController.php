<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Subject;
use App\Models\Topic;
use App\Models\Grade;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiMessageController;

use Auth;

class ChapterController extends Controller
{
     public function index()
    {
        $subject=Topic::orderBy('grade_id','ASC')->orderBy('sub_id','ASC')->orderBy('chapter_id','ASC')->get();

        return view('admin.chapter.index',compact('subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$subject=Subject::orderBy('id','DESC')->get();
         $grade=Grade::get();
        return view('admin.chapter.create',compact('subject','grade'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach($request->name as $val){
        	if(!empty($val)){
        		$record=Topic::create([
        			'sub_id'=>$request->sb_name,
                    'chapter_id'=>$request->chapter_id,
                    'grade_id'=>$request->grade_id,
        			'name'=>$val,
                    'created_by'=>Auth::user()->id,
                    'updated_by'=>Auth::user()->id
        		]);
        	}
        }
        if(isset($record) && !empty($record))
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$subjects=Subject::orderBy('id','DESC')->get();
         $subject = Subject::with('chapters')->find($id);
// dd($subject->id);
        if(!$subject){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.chapter.edit',compact('subject','subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateChapter(Request $request)
    {
        $subject = Subject::with('chapters')->find($request->id);
        if(!$subject){
        	return response()->json(['status'=>0,'message'=>'Record not found']);
        }
        for($i=0; $i < count($request->name); $i++){
            $topics=null;
            if(isset($request->topic_id[$i])){
                $topics=Topic::where('id',$request->topic_id[$i])->first();
            }
            
            if($topics && !empty($topics)){
                $record['name']=$request->name[$i];
                $dataUpdate=Topic::where('id',$request->topic_id[$i])->update($record);
            }else{
                $dataUpdate=Topic::create([
                    'sub_id'=>$request->id,
                    'name'=>$request->name[$i],
                ]);
            }
        	
        	
        }
        if(isset($dataUpdate) && !empty($dataUpdate))
            return response()->json(['status'=>1,'message'=>'Record found Successfully']);
        else
            return response()->json(['status'=>0,'message'=>'Record not found']);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


     public function deleteTopic(Request $request)
    {
        try {
            $allInputs = $request->all();
            $id = $request->input('id');

            $validation = Validator::make($allInputs, [
                'id' => 'required'
            ]);
            if ($validation->fails()) {
                $response = (new ApiMessageController())->validatemessage($validation->errors()->first());
            } else {

                $deleteItem =Topic::find($id);

                if(isset($deleteItem)){
                    $deleteItem->status=$deleteItem->status==1?0:1;
                    $statusChange=$deleteItem->save();

                }


                if (isset($statusChange) && !empty($statusChange)) {
                    $response = (new ApiMessageController())->saveresponse("Data Update Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to Update Data");
                }
            }

        } catch (\Illuminate\Database\QueryException $ex) {
          $response = (new ApiMessageController())->queryexception($ex);
      }

      return $response;
  }
}
