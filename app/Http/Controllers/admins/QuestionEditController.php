<?php

namespace App\Http\Controllers\admins;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use App\Models\Question;
use Auth;
use App\Models\Answer;
use App\Models\Chapter;

use App\Models\Topic;
use App\Models\Subject;
use App\Models\Section;
use App\Models\Grade;
use App\Models\Difficulty;
use Session;
class QuestionEditController extends Controller
{
    public function index()
    {
        $topic=Topic::all();
        $subject=Subject::all();
    
        return view('admin.questionEdit.index',compact('subject','topic'));
    }

    public function questionEditget ($obj){

        $question=Question::where('topic_id',$obj)->with('answers')->paginate(20);
        $topic=Topic::all();
    
        $subject=Subject::all();
        $difficulty=Difficulty::all();
        $grade=Grade::all();
        if(count($question)){
            $subject=Subject::where('status',1)->get();
            return view('admin.questionEdit.question',compact('question','subject','topic','subject','grade','difficulty'));
          
        }
          session()->flash('error_message', __('Record not found'));
            return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$topic=Topic::all();
    	$subject=Subject::all();
        $grade=Grade::all();
        $difficulty=Difficulty::all();
        return view('admin.question.create',compact('subject','topic','difficulty','grade'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        // dd($request->all());
        $sub_id=$request->sub_id;
        $topic_id=$request->topic_id;

        if($request->question){
            $question = new \DomDocument();
            libxml_use_internal_errors(true);

            $question->loadHtml($request->question, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $question->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'question'.'_'.date('YmdHis');
                    $filepath = "/images/question/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $question1= $question->saveHTML();
        }
        if($request->answerA){
            $answerA = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerA->loadHtml($request->answerA, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerA->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerA'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
             $answer1=$answerA->saveHTML();
        }
        if($request->answerB){
            $answerB = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerB->loadHtml($request->answerB, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerB->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerB'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $answer2= $answerB->saveHTML();
        }
        if($request->answerC){
            $answerC = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerC->loadHtml($request->answerC, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerC->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerC'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
             $answer3=$answerC->saveHTML();
        }
        if($request->answerD){
            $answerD = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerD->loadHtml($request->answerD, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerD->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerD'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $answer4= $answerD->saveHTML();
        }
       
       $newUser=Question::create([
            'topic_id'=>$request->topic_id,
            'sub_id'=>$request->sub_id,
            'sub_topic_id'=>$request->sub_topic_id,
            'grade_id'=>$request->grade_id,
            'dif_id'=>$request->dif_id,
            'question'=>isset($question1)?$question1:'',
            'type'=>3,
            'created_at'=>now(),
            'created_by'=>Auth::user()->id,
            'updated_by'=>Auth::user()->id
       ]);
      
            Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answer1)?$answer1:'',
                'is_correct'=>$request->correct==1?1:0,
                'type'=>$request->typeA,
                
            ]);
            Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answer2)?$answer2:'',
                'is_correct'=>$request->correct==2?1:0,
                'type'=>$request->typeA,
                
            ]);
           Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answer3)?$answer3:'',
                'is_correct'=>$request->correct==3?1:0,
                'type'=>$request->typeA,
                
            ]);
           Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answer4)?$answer4:'',
                'is_correct'=>$request->correct==4?1:0,
                'type'=>$request->typeA,
                
            ]);
        
       
        if($newUser){
            $q= Question::with('answers')->find($newUser->id);
            if(isset($q) && !empty($q)){
                foreach($q->answers as $answer){
                    $ansStatus=0;
                    if($answer->is_correct==1){
                      $top=null;
                      $re['hasAns']=1;
                      Question::where('id',$newUser->id)->update($re);
                      $ansStatus=1;
                      if($ansStatus){
                        $top=Topic::where('id',$newUser->topic_id)->first();
                        if(isset($top) && !empty($top)){
                          $top->hasQuestion++;
                          $top->save();
                        }
                        
                      }
                    }
                }
            }
            
            session()->flash('success_message', __('Record Inserted Successfully'));
        }
           
        else{
            session()->flash('error_message', __('Failed! To Insert Record'));
        }

        return redirect()->back()->with( ['sub_id' => $sub_id,'topic_id'=>$topic_id] );
    }

    public function oldStore(Request $request){
        $sub_id=$request->sub_id;
        $topic_id=$request->topic_id;
        if($request->hasfile('answerFileA')){
            $Extension_profile = $request->file('answerFileA')->getClientOriginalExtension();
            $answerA = 'answerA'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('answerFileA')->move('images/answer/', $answerA);
        }
        if($request->hasfile('answerFileB')){
            $Extension_profile = $request->file('answerFileB')->getClientOriginalExtension();
            $answerFileB = 'answerFileB'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('answerFileB')->move('images/answer/', $answerFileB);
        }
        if($request->hasfile('answerFileC')){
            $Extension_profile = $request->file('answerFileC')->getClientOriginalExtension();
            $answerFileC = 'answerFileC'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('answerFileC')->move('images/answer/', $answerFileC);
        }
        if($request->hasfile('answerFileD')){
            $Extension_profile = $request->file('answerFileD')->getClientOriginalExtension();
            $answerFileD = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('answerFileD')->move('images/answer/', $answerFileD);
        }
        if($request->hasfile('questionFile')){
            $Extension_profile = $request->file('questionFile')->getClientOriginalExtension();
            $questionFile = 'question'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('questionFile')->move('images/question/', $questionFile);
        }

       $newUser=Question::create([
            'topic_id'=>$request->topic_id,
            'sub_id'=>$request->sub_id,
            'question'=>isset($questionFile)?$questionFile:$request->question,
            'type'=>$request->type,
            'created_by'=>Auth::user()->id,
            'updated_by'=>Auth::user()->id
       ]);
      
            Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answerA)?$answerA:$request->answerA,
                'is_correct'=>$request->correct==1?1:null,
                'type'=>$request->typeA,
                
            ]);
            Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answerFileB)?$answerFileB:$request->answerB,
                'is_correct'=>$request->correct==2?1:null,
                'type'=>$request->typeA,
                
            ]);
            Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answerFileC)?$answerFileC:$request->answerFileC,
                'is_correct'=>$request->correct==3?1:null,
                'type'=>$request->typeA,
                
            ]);
            Answer::insert([
                'question_id'=>$newUser->id,
                'ans'=>isset($answerFileD)?$answerFileD:$request->answerFileD,
                'is_correct'=>$request->correct==4?1:null,
                'type'=>$request->typeA,
                
            ]);
        
       
        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back()->with( ['sub_id' => $sub_id,'topic_id'=>$topic_id] );
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $question=Question::where('id',$id)->with('answers','subject.chapters')->first();
         // dd($question);
         // $topic=Topic::all();
         $topic=Topic::all();
        $grade=Grade::all();
        $difficulty=Difficulty::all();

        $subject=Subject::with('chapters')->get();
        if(!$question){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.question.edit',compact('question','subject','topic','grade','difficulty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // dd($request->all());
        $sub_id=$request->sub_id;
        $topic_id=$request->topic_id;

        if($request->question){
            $question = new \DomDocument();
            libxml_use_internal_errors(true);
            $question->loadHtml($request->question, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $question->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'question'.'_'.date('YmdHis');
                    $filepath = "/images/question/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $question1= $question->saveHTML();
        }
        if($request->answerA){
            $answerA = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerA->loadHtml($request->answerA, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerA->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerA'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
             $answer1=$answerA->saveHTML();
        }
        if($request->answerB){
            $answerB = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerB->loadHtml($request->answerB, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerB->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerB'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $answer2= $answerB->saveHTML();
        }
        if($request->answerC){
            $answerC = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerC->loadHtml($request->answerC, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerC->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerC'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
             $answer3=$answerC->saveHTML();
        }
        if($request->answerD){
            $answerD = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerD->loadHtml($request->answerD, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerD->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerD'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $answer4= $answerD->saveHTML();
        }
        $ques=Question::find($id);
        if(!$ques){
            session()->flash('error_message', __('Failed! To Insert Record'));
            return redirect()->back();
        }
            $effectedData=array(
                        'sub_id'=>$request->sub_id?$request->sub_id:$ques->sub_id,
                        'question'=>isset($question1)?$question1:$ques->question,
                        'type'=>0,
                        'topic_id'=>$request->topic_id?$request->topic_id:$ques->topic_id,
                        'sub_id'=>$request->sub_id?$request->sub_id:$ques->sub_id,
                        'sub_topic_id'=>$request->sub_topic_id?$request->sub_topic_id:$ques->sub_topic_id,
                        'grade_id'=>$request->grade_id?$request->grade_id:$ques->grade_id,
                        'dif_id'=>$request->dif_id?$request->dif_id:$ques->dif_id,
                        'updated_by'=>Auth::user()->id
                    );
       $newUser=Question::where('id',$id)->update($effectedData);
        Answer::where('question_id',$id)->delete();
      
            Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer1)?$answer1:'',
                'is_correct'=>$request->correct==1?1:0,
                'type'=>$request->typeA,
                
            ]);
            Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer2)?$answer2:'',
                'is_correct'=>$request->correct==2?1:0,
                'type'=>$request->typeA,
                
            ]);
           Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer3)?$answer3:'',
                'is_correct'=>$request->correct==3?1:0,
                'type'=>$request->typeA,
                
            ]);
           Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer4)?$answer4:'',
                'is_correct'=>$request->correct==4?1:0,
                'type'=>$request->typeA,
                
            ]);
         $newUser=Question::where('id',$id)->with('answers')->first();
         // dd($newUser);
       
        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back()->with( ['sub_id' => $sub_id,'topic_id'=>$topic_id] );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function SubjectHasTopic(Request $request){
    	// return $request->all();
        
    	$student=Topic::select('id','name')->where('sub_id',$request->id)->get();
        if($student){
            return response()->json($student, 200);
        }else{
            $message='Record not found';
             return response()->json($message, 200);
        }
    }
    public function TopicHasSubtopic(Request $request){
        // return $request->all();
        $student=SubTopic::select('id','sub_topic_name')->where('topic_id',$request->id)->get();
        if(count($student)){
            return response()->json($student, 200);
        }else{
            $message='Record not found';
             return response()->json($message, 200);
        }
    }

    public function getQuestionEditBySubject(Request $request){

        $question=Question::where('sub_id',$request->sub_id)->with('topic','answers');
        if(isset($request->topic_id) && !empty($request->topic_id))
            $question->where('topic_id',$request->topic_id)->where('topic_id','<>',null);
        $qAnswer=$question->get();
        // return $request->all();
        if(count($qAnswer)){
            return response()->json(['status'=>1,'message'=>'Record found Successfully','data'=>$qAnswer,'input'=>$request->all()]);
        }else{
            return response()->json(['status'=>0,'message'=>'Record not found']);
        }

    }




    public function approvedQuestion(Request $request){
        $question=Question::where('sub_id',$request->sub_id)->with('topic','answers')->where('status',1);
        if(isset($request->topic_id) && !empty($request->topic_id))
            $question->where('topic_id',$request->topic_id);
        $qAnswer=$question->get();
        // return $request->all();

        if(count($qAnswer)){
            return response()->json(['status'=>1,'message'=>'Record found Successfully','data'=>$qAnswer]);
        }else{
            return response()->json(['status'=>0,'message'=>'Record not found']);
        }

    }

    public function disApproveQuestion(Request $request){
         $question=Question::where('id',$request->id)->first();
        //////////////////// disapprove status 3///////////
        $qAnswer['status']=3;
        $qAnswer['updated_by']=Auth::user()->id;
        $qAnswer['sub_topic_id']=$request->sub_topic_id;
        $qAnswer['sub_id']=$request->sub_id;
        $qAnswer['topic_id']=$request->topic_id;
        $qAnswer['grade_id']=$request->grade_id;
        $qAnswer['dif_id']=$request->dif_id;
        $record=Question::where('id',$request->id)->update($qAnswer);

        if($record){
            return response()->json(['status'=>1,'message'=>'Record found Successfully']);
        }else{
            return response()->json(['status'=>0,'message'=>'Record not found']);
        }
    }

    public function approveQuestionAns(Request $request){
        // return $request->all();
        $question=Question::where('id',$request->id)->with('answers')->first();
        
        $qAnswer['status']=1;
        $qAnswer['updated_by']=Auth::user()->id;
        $qAnswer['approved_by']=Auth::user()->id;
        $qAnswer['sub_topic_id']=$request->sub_topic_id;
        $qAnswer['sub_id']=$request->sub_id;
        $qAnswer['topic_id']=$request->topic_id;
        $qAnswer['grade_id']=$request->grade_id;
        $qAnswer['dif_id']=$request->dif_id;
        $record=Question::where('id',$request->id)->update($qAnswer);

        if($record){
            if(isset($question) && !empty($question)){
                foreach($question->answers as $answer){
                    $ansStatus=0;
                    if($answer->is_correct==1){
                      $top=null;
                      $re['hasAns']=1;
                      Question::where('id',$question->id)->update($re);
                      $ansStatus=1;
                      if($ansStatus){
                        $top=Topic::where('id',$question->topic_id)->first();
                        if(isset($top) && !empty($top)){
                          $top->hasQuestion++;
                          $top->save();
                        }
                        
                      }
                    }
                }
            }
            return response()->json(['status'=>1,'message'=>'Record found Successfully']);
        }else{
            return response()->json(['status'=>0,'message'=>'Record not found']);
        }
    }

    public function upApproveQuestionAns(Request $request){
        $question=Question::where('id',$request->id)->first();
        
        $qAnswer['status']=2;
        $qAnswer['updated_by']=1;
        $record=Question::where('id',$request->id)->update($qAnswer);

        if($record){
            return response()->json(['status'=>1,'message'=>'Record found Successfully']);
        }else{
            return response()->json(['status'=>0,'message'=>'Record not found']);
        }
    }
}
