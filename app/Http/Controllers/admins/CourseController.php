<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Subject;
use App\Models\CourseSubject;
use Session;
use Auth;
use DB;
class CourseController extends Controller
{
    public function index()
    {
        $course=Course::orderBy('id','DESC')->with('couseSubject.subject')->get();
        return view('admin.course.index',compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subject=Subject::get();
        return view('admin.course.create',compact('subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin=Course::create([
            'name'=>$request->name,
            'description'=>$request->description,
           
        ]);
        if(!$admin){
            DB::rollBack();
            Session::flash('error_message', 'Fail To insert Record, please try again.');
            return redirect()->back();
        }else{
             $data_dest1 = array();
            if(isset($request->subject) && !empty($request->subject)){
                for($i=0; $i<count($request->subject);$i++) {
                   $tmp_dest = array();
                   $tmp_dest['course_id']=$admin->id;
                   $tmp_dest['sub_id']=$request->subject[$i];
                   $tmp_dest['created_at']=now();
                   $tmp_dest['updated_at']=now();
                   $data_dest1[] = $tmp_dest;
                }
            }
              $levls = CourseSubject::insert($data_dest1);
            if($levls){
                DB::commit();
                Session::flash('success_message', 'Record added successfully.');
            }else{
                DB::rollBack();
                Session::flash('error_message', 'Fail Record added, please try again.');
            }

            return redirect()->route('course.index');
           
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $course = Course::with('couseSubject.subject')->find($id);
        $subject=Subject::get();
        if(!$course){
            Session::flash('error_message',  __('Failed! Record not found'));
            return redirect()->back();
        }
        return View('admin.course.edit',compact('course','subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $level = Course::find($id);
        if(!$level){
            Session::flash('error_message', 'No Record found.');
            return redirect(route('course.index'));
        }
        $sub = $request->except(['subject']);
        $affected = $level->update($sub);
        $level->couseSubject()->delete();
        if(isset($request->subject) && !empty($request->subject)){
            foreach ($request->subject as $sub_id){
                $level_subject = new CourseSubject();
                $level_subject->course_id = $level->id;
                $level_subject->sub_id = $sub_id;
                $level_subject->save();
            }
        }
        if($affected){
            session()->flash('success_message', 'Role has been updated successfully.');
            return redirect()->route('course.index');
        }
        else{
            session()->flash('error_message', 'Fail to update the record.');
             return redirect()->route('course.edit',$id);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
