<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Question;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\Answer;
use App\Models\Topic;
use App\Models\Subject;
use Session;
use Image;

class UserQuestionController extends Controller
{
    public function index(){
    	return view('admin.question.user-question');
    }
}
