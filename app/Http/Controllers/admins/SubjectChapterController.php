<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectChapterRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiMessageController;

use App\Models\Subject;
use App\Models\Chapter;
use App\Models\Grade;
use Session;
use App\Models\Topic;
use Auth;
class SubjectChapterController extends Controller
{
    public function index()
    {
        $subject=Chapter::orderBy('course_id','ASC')->orderBy('sub_id','ASC')->with('subject')->get();
        return view('admin.subjectChapter.index',compact('subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$subject=Subject::orderBy('id','DESC')->get();
    	$course=Grade::get();
        return view('admin.subjectChapter.create',compact('subject','course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectChapterRequest $request)
    {
        foreach($request->name as $val){
        	if(!empty($val)){
        		$record=Chapter::create([
        			'sub_id'=>$request->sub_id,
        			'course_id'=>$request->course_id,
        			'chaper_name'=>$val,
        			'created_by'=>Auth::user()->id,
        			'updated_by'=>Auth::user()->id,

        		]);
        	}
        }
        if(isset($record) && !empty($record))
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->route('chapters.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$subjects=Subject::orderBy('id','DESC')->get();
         $chapter = Chapter::find($id);
         $course=Grade::get();
// dd($subject->id);
        if(!$chapter){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.subjectChapter.edit',compact('chapter','subjects','course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {
        $chapter = Chapter::find($id);

        if(!$chapter){
        	Session::flash('error_message',  __('Record not found'));
            return redirect()->back();
        }
			$chapter->course_id=$request->course_id;
			$chapter->sub_id=$request->sb_name;
			$chapter->chaper_name=$request->chaper_name;


       
        if($chapter->save())
             session()->flash('success_message', __('Record Update Successfully'));
        else
           Session::flash('error_message',  __('Record failed to Update'));

        return redirect()->route('chapters.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function deleteChapter(Request $request)
    {
        try {
            $allInputs = $request->all();
            $id = $request->input('id');

            $validation = Validator::make($allInputs, [
                'id' => 'required'
            ]);
            if ($validation->fails()) {
                $response = (new ApiMessageController())->validatemessage($validation->errors()->first());
            } else {

                $deleteItem =Chapter::find($id);

                if(isset($deleteItem)){
                    $deleteItem->status=$deleteItem->status==1?0:1;
                    $statusChange=$deleteItem->save();

                }


                if (isset($statusChange) && !empty($statusChange)) {
                    $response = (new ApiMessageController())->saveresponse("Data Update Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to Update Data");
                }
            }

        } catch (\Illuminate\Database\QueryException $ex) {
          $response = (new ApiMessageController())->queryexception($ex);
      }

      return $response;
  }
}
