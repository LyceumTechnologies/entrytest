<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BulkQuestionEditRequest;
use App\Models\Question;
use Auth;
use App\Models\Answer;
use App\Models\Chapter;

use App\Models\Topic;
use App\Models\Subject;
use App\Models\Section;
use App\Models\Grade;
use App\Models\Difficulty;
use Session;
class BulkQuestionEditController extends Controller
{
    public function index()
    {
        $topic=Topic::all();
        $subject=Subject::all();
    
        return view('admin.BulkquestionEdit.index',compact('subject','topic'));
    }


    public function store(Request $request){
    	// dd($request->all());

    	$topics=Topic::with('TopicQuestion')->where('sub_id',$request->sub_id)->get();

    	return view('admin.BulkquestionEdit.topic',compact('topics'));
    }

    public function questionGetByTOpic($id){
    	$questions=Question::where('topic_id',$id)->limit(50)->get();
    	$topic=Topic::all();
    	$subject=Subject::all();
        $grade=Grade::all();
        $difficulty=Difficulty::all();
    	return view('admin.BulkquestionEdit.questions',compact('questions','topic','subject','grade','difficulty'));

    }



    public function questionGetByChapter($id){
        $questions=Question::where('chapter_id',$id)->limit(50)->get();
        $topic=Topic::all();
        $subject=Subject::all();
        $grade=Grade::all();
        $difficulty=Difficulty::all();
        return view('admin.BulkquestionEdit.questions',compact('questions','topic','subject','grade','difficulty'));
    }

    public function questionGetAnswer(Request $request){

    	$question=Question::where('id',$request->question_id)->with('answers')->first();
    	// return $question;
    	if(count($question->answers)){
    		return response()->json(['status'=>1,'data'=>$question->answers]);
    	}else{
    		return response()->json(['status'=>0]);
    	}
    }

    public function editQuestionTopic(BulkQuestionEditRequest $request){
    	// dd($request->all());

    	foreach ($request->question_ids as $id) {

    		$question=Question::find($id);
    		if($question){
    			$question->topic_id=$request->topic_id;
    			$question->sub_id=$request->sub_id;
    			$question->grade_id=$request->grade_id;
    			$question->chapter_id=$request->chapter_id;
    			$question->sub_topic_id=$request->sub_topic_id;
    			$question->dif_id=$request->dif_id;
    			$question->save();
    		}
    	}

    	session()->flash('success_message', __('Record update successfully'));
    	return redirect()->back();
    }
}
