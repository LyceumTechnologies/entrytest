<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Subject;
use App\Models\Grade;
use Session;
use Auth;
use DB;

class GradeController extends Controller
{
    public function index()
    {
        $grade=Grade::orderBy('id','DESC')->where('status',1)->get();
        return view('admin.grade.index',compact('grade'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.grade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $newUser=Grade::create($request->except('_token'));

        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $grade = Grade::find($id);

        if(!$grade){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.grade.edit',compact('grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grade = Grade::find($id);
        if(!$grade){
            Session::flash('error_message',  __('messages.not_found', ['name' => 'user']));
            return redirect()->back();
        }
        
       $newUser=Grade::where('id',$id)->update($request->except('_token','_method'));

     

        if($newUser)
            session()->flash('success_message', __('messages.success_curd', ['name' => 'user','action'=>'updated']));
        else
            session()->flash('error_message', __('messages.fail_curd', ['name' => 'user','action'=>'updated']));
        return redirect()->route('grade.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
