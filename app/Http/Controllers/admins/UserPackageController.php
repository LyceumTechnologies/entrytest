<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPackageRequest;
use App\Models\User;
use App\Models\Package;
use App\Models\UserHasPacakge;
use Auth;
class UserPackageController extends Controller
{
    public function index(){
    	$package=User::where('status',1)->with('userPackage.package')->get();
    	return view('admin.user-package.index',compact('package'));
    }
    public function create(){
    	$package=Package::get();
    	return view('admin.user-package.create',compact('package'));
    }
    public function store(UserPackageRequest $request){
    	$user=User::where('id',$request->user_id)->first();
    	if(!$user){
    		Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
    	}
    	$package=UserHasPacakge::create(['user_id'=>$request->user_id,'package_id'=>$request->packageId,'transaction_id'=>$request->transactionId,'amount'=>$request->amount,'created_by'=>Auth::user()->id]);
    	if($package){
    		$user->status=1;
    		$user->save();
    		session()->flash('success_message', __('User Status update Successfully'));
    		return redirect()->back();

    	}else{
    		Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
    	}

    }
    public function userInformation(Request $request){
    	$client=User::where('email',$request->email)->first();
        if($client){
            $status=1;
            $message='Record found Successfuly';
            return response()->json(['status'=>$status,'message'=>$message,'record'=>$client]);
        }else{
            $status=0;
            $message='Record not found';
            return response()->json(['status'=>$status,'message'=>$message]);
        }
    }
}
