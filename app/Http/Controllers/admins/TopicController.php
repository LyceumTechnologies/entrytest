<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Auth;
class TopicController extends Controller
{
    public function index()
    {
        $user=User::orderBy('id','DESC')->where('user_type','user')->get();
        return view('admin.topic.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.topic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // dd($request->all());
        if($request->hasfile('images')){
            $Extension_profile = $request->file('images')->getClientOriginalExtension(); 
            $request->file('images')->move('images/user/', $profile);
        }
       $newUser=User::create([
        'name' => $request->name,
        'email' => $request->email,
        'phone' => $request->phone,
        'pwd'=>$request->password,
        'images'=>isset($profile)?$profile:'no-image.png',
        'api_token'=> str_random(60),
        'user_type'=>'user',
        'password' => Hash::make($request->input('password')),
      ]);

        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $teacher = User::find($id);

        if(!$teacher){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.topic.edit',compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $user = User::find($id);
        if(!$user){
            Session::flash('error_message',  __('messages.not_found', ['name' => 'user']));
            return redirect()->back();
        }
        if($request->password){
        	$input['pwd']=$request->password;
        	$input['password'] = Hash::make($request->input('password'));
        }
        $input=$request->except('_token','_method','password','password_confirmation','subject');
        if($request->hasfile('images')){
            $Extension_profile = $request->file('images')->getClientOriginalExtension();
            $profile = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('images')->move('images/user/', $profile);
            $input['images']=isset($profile)?$profile:'no-image.png';
        }
       $newUser=User::where('id',$id)->update($input);

     

        if($newUser)
            session()->flash('success_message', __('messages.success_curd', ['name' => 'user','action'=>'updated']));
        else
            session()->flash('error_message', __('messages.fail_curd', ['name' => 'user','action'=>'updated']));
        return redirect()->route('user.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function deleteTopic(Request $request)
    {
        try {
            $allInputs = $request->all();
            $id = $request->input('id');

            $validation = Validator::make($allInputs, [
                'id' => 'required'
            ]);
            if ($validation->fails()) {
                $response = (new ApiMessageController())->validatemessage($validation->errors()->first());
            } else {

                $deleteItem =Topic::find($id);

                if(isset($deleteItem)){
                    $deleteItem->status=$deleteItem->status==1?0:1;
                    $statusChange=$deleteItem->save();

                }


                if (isset($statusChange) && !empty($statusChange)) {
                    $response = (new ApiMessageController())->saveresponse("Data Update Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to Update Data");
                }
            }

        } catch (\Illuminate\Database\QueryException $ex) {
          $response = (new ApiMessageController())->queryexception($ex);
      }

      return $response;
  }
}
