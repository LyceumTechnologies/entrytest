<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\UserVisit;
use DB;
use Auth;
use Carbon\Carbon;
use App\Http\Resources\NotificationResource;
use App\Models\Setting;
class NotificationController extends Controller
{
    public function index(){
    	$notifcation=Setting::first();
    	// dd($notifcation->updated_at);
    	if(Carbon::parse($notifcation->updated_at)->addMinutes(60)->isPast()) {
                $notifcation=Setting::first()->update(['notifcation'=>'']);
        }
    	$notifcation=Setting::first();
    	return view('admin.notification.create',compact('notifcation'));
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(),[
			'notifcation' => 'required',
		]);

		if ($validator->fails())
		{
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}else{
			$newUser=Setting::first()->update(['notifcation'=>$request->notifcation]);
			if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
	        else
	            session()->flash('error_message', __('Failed! To Insert Record'));

	        return redirect()->back();


		}
    }
}
