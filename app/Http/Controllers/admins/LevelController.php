<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Level;
use Illuminate\Support\Facades\Mail;
use Auth;

class LevelController extends Controller
{
    public function index()
    {
        $level=Level::orderBy('id','DESC')->get();
        return view('admin.level.index',compact('level'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        
      
       $newUser=Level::create($request->all());

        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $level = Level::find($id);

        if(!$level){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.level.edit',compact('level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $user = Level::find($id);
        if(!$user){
            Session::flash('error_message',  __('Record not found'));
            return redirect()->back();
        }
     
        $input=$request->except('_token','_method');
      
       $newUser=Level::where('id',$id)->update($input);

     

        if($newUser)
            session()->flash('success_message', __('Record update Successfully'));
        else
            session()->flash('error_message', __('Failed! Record not update'));
        return redirect()->route('level.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
