<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Subject;
use Auth;

class SubjectController extends Controller
{
    public function index()
    {
        $subject=Subject::orderBy('id','DESC')->get();
        // dd($subject);
        return view('admin.subject.index',compact('subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $request)
    {
        
       $newUser=Subject::create($request->except('_token','sub_subject'));
       $data_dest = array();
            // if(isset($request->sub_subject) && !empty($request->sub_subject)){
            //     for($i=0; $i<count($request->sub_subject);$i++) {
            //        $tmp_dest = array();
            //        $tmp_dest['sb_name']=$request->sub_subject[$i];
            //        $tmp_dest['parent_id']=$newUser->id;
            //        $tmp_dest['created_at']=now();
            //        $tmp_dest['updated_at']=now();
            //        $data_dest[] = $tmp_dest;
            //     }
            // }
            $levls = Subject::insert($data_dest);
        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back();
    }

    public function updateSubject(Request $request)
    {
        // return $request->all();
        $subject = Subject::find($request->id);
        if(!$subject){
            return response()->json(['status'=>0,'message'=>'Record not found']);
        }
        $dataUpdate=$subject->update(['sb_name'=>$request->subjectName,'status'=>$request->status]);
        // for($i=0; $i < count($request->sub_subject); $i++){
        //     $topics=null;
        //     if(isset($request->sub_subject_id[$i])){
        //         $topics=Subject::where('id',$request->sub_subject_id[$i])->first();
        //     }
            
        //     if(isset($topics) && !empty($topics)){
        //         $record['sb_name']=$request->sub_subject[$i];
        //         $record['parent_id']=$request->id;
        //         $dataUpdate=Subject::where('id',$request->sub_subject_id[$i])->update($record);
        //     }else{
        //         $dataUpdate=Subject::create(['sb_name'=>$request->sub_subject[$i],'parent_id'=>$request->id]);
        //     }
            
            
        // }
        if(isset($dataUpdate) && !empty($dataUpdate))
            return response()->json(['status'=>1,'message'=>'Record found Successfully']);
        else
            return response()->json(['status'=>0,'message'=>'Record not found']);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $subject = Subject::with('subSubject')->find($id);

        if(!$subject){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.subject.edit',compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $user = Subject::find($id);
        if(!$user){
            Session::flash('error_message',  __('Record not found'));
            return redirect()->back();
        }
     
        $input=$request->except('_token','_method');
      
       $newUser=Subject::where('id',$id)->update($input);

     

        if($newUser)
            session()->flash('success_message', __('Record update Successfully'));
        else
            session()->flash('error_message', __('Failed! Record not update'));
        return redirect()->route('subject.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
