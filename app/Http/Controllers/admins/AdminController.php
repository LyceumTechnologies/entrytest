<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use Illuminate\Support\Facades\Mail;
use Auth;
use Session;
class AdminController extends Controller
{
   public function index()
    {
        $user=Admin::orderBy('id','DESC')->get();
        return view('admin.admin.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        if($request->hasfile('images')){
            $Extension_profile = $request->file('images')->getClientOriginalExtension();
            $profile = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('images')->move('images/admin/', $profile);
        }
	    $newUser=Admin::create([
	        'name' => $request->name,
	        'email' => $request->email,
	        'phone' => $request->phone,
	        'pwd'=>$request->password,
	        'images'=>isset($profile)?$profile:'no-image.png',
	        'api_token'=> str_random(60),
	        'role'=>$request->role,
	        'password' => Hash::make($request->input('password')),
	    ]);

        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $teacher = Admin::find($id);

        if(!$teacher){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }
        return View('admin.admin.edit',compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Admin::find($id);
        if(!$user){
            Session::flash('error_message',  __('messages.not_found', ['name' => 'user']));
            return redirect()->back();
        }
        if($request->password){
        	$input['pwd']=$request->password;
        	$input['password'] = Hash::make($request->input('password'));
        }
        $input=$request->except('_token','_method','password','password_confirmation','subject');
        if($request->hasfile('images')){
            $Extension_profile = $request->file('images')->getClientOriginalExtension();
            $profile = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('images')->move('images/admin/', $profile);
            $input['images']=isset($profile)?$profile:'no-image.png';
        }
       $newUser=Admin::where('id',$id)->update($input);

     

        if($newUser)
            session()->flash('success_message', __(''));
        else
            session()->flash('error_message', __('messages.fail_curd', ['name' => 'user','action'=>'updated']));
        return redirect()->route('admin.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approvedReport(){
        $admin=Admin::get();
        return view('admin.report.approvedReport',compact('admin'));
    }
}
