<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Question;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\Answer;
use App\Models\Topic;
use App\Models\Subject;
use App\Models\SubTopic;
use App\Models\Grade;
use App\Models\Difficulty;
use Session;

class QuestionRemoveController extends Controller
{
    function index(){
    	$subject=Subject::where('status',1)->get();
    	return view('admin.removeQuestion.subject',compact('subject'));
    }

    public function show($id){
    	$questions=Question::with('answers')->where('topic_id',$id)->get();
    	return view('admin.removeQuestion.index',compact('questions'));
    }

    public function deleteQuestion(Request $request){
    	$questions=Question::with('answers')->find($request->id);
    	$questions->answers()->delete();
    	$data=$questions->delete();
    	if($data){
    		return response()->json(['status'=>1,'message'=>'record update successfully']);
    	}else{
    		return response()->json(['status'=>0,'message'=>'failed! record not update']);
    	}
    }
}
