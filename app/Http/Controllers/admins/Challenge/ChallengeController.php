<?php

namespace App\Http\Controllers\admins\Challenge;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChallengeRequest;

use App\Models\Course;
use App\Models\Subject; 
use App\Models\Grade;

use App\Models\Challenge;
use App\Models\CoursePaperQuestion;

use App\Models\CoursePaperQuestionAnswer;

use Illuminate\Support\Str;


use Session;
use Auth;
use DB;

class ChallengeController extends Controller
{
    public function index()
	{
		$data=Challenge::orderBy('id','DESC')->get();
		return view('admin.challenge.challenge.index',compact('data'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$courses=Course::get();
    	return view('admin.challenge.challenge.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChallengeRequest $request)
    {
    	$newUser=Challenge::create([
    		'course_id' => $request->course_id,
    		'challenge_name' => $request->challenge_name,
            'challenge_price' => $request->challenge_price,
            'reward_challenge' => $request->reward_challenge,
            'challenge_description' => $request->challenge_description,
            
    		'slug'=>Str::slug($request->challenge_name),
    		'challenge_date' => $request->challenge_date,
    		'created_by'=>Auth::user()->id,
    		'updated_by'=>Auth::user()->id,
    	]);

    	if($newUser)
    		session()->flash('success_message', __('Record Inserted Successfully'));
    	else
    		session()->flash('error_message', __('Failed! To Insert Record'));

    	return redirect()->route('challenge.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    	$data = Challenge::find($id);
    	$courses=Course::get();

    	if(!$data){
    		Session::flash('error_message',  __('Failed! Record not found'));
    		return redirect()->back();
    	}
    	return View('admin.challenge.challenge.edit',compact('data','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ChallengeRequest $request, $id)
    {
        // dd($request->all());
    	$data = Challenge::find($id);

    	if(!$data){
    		Session::flash('error_message', 'Record not found');
    		return redirect()->back();
    	}
    	
    
    	$newUser=Challenge::where('id',$id)->update([
    		'course_id' => $request->course_id,
            'challenge_name' => $request->challenge_name,
            'challenge_price' => $request->challenge_price,
            'reward_challenge' => $request->reward_challenge,
            'challenge_description' => $request->challenge_description,
            'slug'=>Str::slug($request->challenge_name),
            'challenge_date' => $request->challenge_date,
            'updated_by'=>Auth::user()->id,
    	]);



    	if($newUser)
    		session()->flash('success_message', 'Record update Successfully');
    	else
    		session()->flash('error_message','Failed to update record');
    	return redirect()->route('challenge.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
