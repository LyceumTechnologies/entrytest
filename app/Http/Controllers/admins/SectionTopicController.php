<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subject;
use App\Models\Section;
use App\Models\Topic;
use App\Models\TopicSection;
use App\Models\SubTopic;
use Session;
use Auth;
use DB;
class SectionTopicController extends Controller
{
	public function index()
	{
        $section=Topic::with('subject','subTopic')->get();
        return view('admin.section.index',compact('section'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$subject=Topic::with('subject')->get();
    	return view('admin.section.create',compact('subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$data_dest1 = array();
    	if(isset($request->sub_topic) && !empty($request->sub_topic)){
    		for($i=0; $i<count($request->sub_topic);$i++) {
    			$tmp_dest = array();
    			$tmp_dest['topic_id']=$request->topic_id;
    			$tmp_dest['sub_topic_name']=$request->sub_topic[$i];
    			$tmp_dest['created_by']=Auth::user()->id;
    			$tmp_dest['updated_by']=Auth::user()->id;
    			$tmp_dest['created_at']=now();
    			$tmp_dest['updated_at']=now();
    			$data_dest1[] = $tmp_dest;
    		}
    	}
    	$sectionTopic=SubTopic::insert($data_dest1);
    	if(!$sectionTopic){
    		Session::flash('error_message', 'Fail To insert Record, please try again.');
    	}
    	else{
    		Session::flash('success_message', 'Record added successfully.');
    	}

      return redirect()->route('topic.index');

    }

    public function subjectAddTopic(Request $request)
    {
        $courseSubject=\App\Models\CourseSubject::where('course_id',$request->topic_id)->where('sub_id',$request->sub_id)->first();
        $data_dest1 = array();
        if(isset($request->course_id) && !empty($request->course_id)){

            for($i=0; $i<count($request->course_id);$i++) {
                $tmp_dest = array();
                $tmp_dest['course_sub_id']=$courseSubject->id;
                $tmp_dest['chap_id']=$request->course_id[$i];
                $data_dest1[] = $tmp_dest;
            }
        }
        $sectionTopic=\App\Models\CourseSubjectTopic::insert($data_dest1);
        if(!$sectionTopic){
            Session::flash('error_message', 'Fail To insert Record, please try again.');
        }
        else{
            Session::flash('success_message', 'Record added successfully.');
        }

      return redirect()->route('courseAddTopic');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    	$subject=Subject::orderBy('id','DESC')->where('status',1)->get();
    	// $section=Section::with('parentSection','subject')->find($id);
        $section=SubTopic::with('topic.subject')->find($id);

        if(!$section){
          Session::flash('error_message',  __('Failed! Record failed to update'));
          return redirect()->back();
      }
      return View('admin.section.edit',compact('subject','section'));
  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
    	$user = User::find($id);
    	if(!$user){
    		Session::flash('error_message',  __('messages.not_found', ['name' => 'user']));
    		return redirect()->back();
    	}
    	if($request->password){
    		$input['pwd']=$request->password;
    		$input['password'] = Hash::make($request->input('password'));
    	}
    	$input=$request->except('_token','_method','password','password_confirmation','subject');
    	if($request->hasfile('images')){
    		$Extension_profile = $request->file('images')->getClientOriginalExtension();
    		$profile = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
    		$request->file('images')->move('images/user/', $profile);
    		$input['images']=isset($profile)?$profile:'no-image.png';
    	}
    	$newUser=User::where('id',$id)->update($input);



    	if($newUser)
    		session()->flash('success_message', __('messages.success_curd', ['name' => 'user','action'=>'updated']));
    	else
    		session()->flash('error_message', __('messages.fail_curd', ['name' => 'user','action'=>'updated']));
    	return redirect()->route('user.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function courseAddTopic(){

        $section=\App\Models\Course::with('couseSubject.subject')->get();
        // dd($section);
        return view('admin.course.courseSubject',compact('section'));
    }

    public function subjectTopics($course_id,$topic_id){
       $course=\App\Models\Course::find($course_id);
       $top=\App\Models\Subject::find($topic_id);
       $subject=Topic::with('subject')->get();
       return view('admin.course.subjectTopics',compact('subject','course','top'));
   }
}
