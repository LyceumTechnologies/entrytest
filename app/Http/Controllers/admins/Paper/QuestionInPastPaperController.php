<?php

namespace App\Http\Controllers\admins\Paper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CoursePaperRequest;


use App\Models\Course;
use App\Models\Subject;
use App\Models\Grade;
use App\Models\Topic;
use App\Models\Difficulty;
use App\Models\CoursePaper;
use App\Models\CoursePaperQuestion;

use App\Models\CoursePaperQuestionAnswer;

use Illuminate\Support\Str;

use Image;

use Session;
use Auth;
use DB;
class QuestionInPastPaperController extends Controller
{

	public function index(){
		$question=CoursePaperQuestion::paginate(20);
		$topic=Topic::all();

		$subject=Subject::all();
		$difficulty=Difficulty::all();
		$grade=Grade::all();
		$subject=Subject::where('status',1)->get();


		return view('admin.coursePaper.PastQuestion.index',compact('question','past_paper_id','topic','subject','difficulty','grade','subject'));
	}
	public function addPastPaperQuestion($id){
		$course_past_paper=$id;

		$topic=Topic::all();
		$subject=Subject::all();
		$difficulty=Difficulty::all();
		$grade=Grade::all();
		$subject=Subject::where('status',1)->get();

		return view('admin.coursePaper.PastQuestion.create',compact('topic','subject','difficulty','grade','subject','course_past_paper'));
	}



	public function PastPaperQuestionstore($id){
		$past_paper_id=$id;
		$question=CoursePaperQuestion::where('course_past_paper',$id)->paginate(20);

		$topic=Topic::all();

		$subject=Subject::all();
		$difficulty=Difficulty::all();
		$grade=Grade::all();
		$subject=Subject::where('status',1)->get();

		return view('admin.coursePaper.PastQuestion.index',compact('question','past_paper_id','topic','subject','difficulty','grade','subject'));
	}


	public function storeQuestionPastPaper(Request $request){
		$sub_id=$request->sub_id;
		$topic_id=$request->topic_id;

		if($request->question){
			$question = new \DomDocument();
			libxml_use_internal_errors(true);

			$question->loadHtml($request->question, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
			$images = $question->getElementsByTagName('img');
			foreach($images as $img){
				$src = $img->getAttribute('src');
				if(preg_match('/data:image/', $src)){                
					preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
					$mimetype = $groups['mime'];                
					$filename = 'question'.'_'.date('YmdHis');
					$filepath = "/images/question/$filename.$mimetype";    
					$image = Image::make($src)
					->resize(300, 300) 
					->encode($mimetype, 100)
					->save(public_path($filepath));                
					$new_src = asset($filepath);
					$img->removeAttribute('src');
					$img->setAttribute('src', $new_src);
				} 
			} 
			$question1= $question->saveHTML();
		}
		if($request->answerA){
			$answerA = new \DomDocument();
			libxml_use_internal_errors(true);
			$answerA->loadHtml($request->answerA, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
			$images = $answerA->getElementsByTagName('img');
			foreach($images as $img){
				$src = $img->getAttribute('src');
				if(preg_match('/data:image/', $src)){                
					preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
					$mimetype = $groups['mime'];                
					$filename = 'answerA'.'_'.date('YmdHis');
					$filepath = "/images/answer/$filename.$mimetype";    
					$image = Image::make($src)
					->resize(300, 300) 
					->encode($mimetype, 100)
					->save(public_path($filepath));                
					$new_src = asset($filepath);
					$img->removeAttribute('src');
					$img->setAttribute('src', $new_src);
				} 
			} 
			$answer1=$answerA->saveHTML();
		}
		if($request->answerB){
			$answerB = new \DomDocument();
			libxml_use_internal_errors(true);
			$answerB->loadHtml($request->answerB, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
			$images = $answerB->getElementsByTagName('img');
			foreach($images as $img){
				$src = $img->getAttribute('src');
				if(preg_match('/data:image/', $src)){                
					preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
					$mimetype = $groups['mime'];                
					$filename = 'answerB'.'_'.date('YmdHis');
					$filepath = "/images/answer/$filename.$mimetype";    
					$image = Image::make($src)
					->resize(300, 300) 
					->encode($mimetype, 100)
					->save(public_path($filepath));                
					$new_src = asset($filepath);
					$img->removeAttribute('src');
					$img->setAttribute('src', $new_src);
				} 
			} 
			$answer2= $answerB->saveHTML();
		}
		if($request->answerC){
			$answerC = new \DomDocument();
			libxml_use_internal_errors(true);
			$answerC->loadHtml($request->answerC, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
			$images = $answerC->getElementsByTagName('img');
			foreach($images as $img){
				$src = $img->getAttribute('src');
				if(preg_match('/data:image/', $src)){                
					preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
					$mimetype = $groups['mime'];                
					$filename = 'answerC'.'_'.date('YmdHis');
					$filepath = "/images/answer/$filename.$mimetype";    
					$image = Image::make($src)
					->resize(300, 300) 
					->encode($mimetype, 100)
					->save(public_path($filepath));                
					$new_src = asset($filepath);
					$img->removeAttribute('src');
					$img->setAttribute('src', $new_src);
				} 
			} 
			$answer3=$answerC->saveHTML();
		}
		if($request->answerD){
			$answerD = new \DomDocument();
			libxml_use_internal_errors(true);
			$answerD->loadHtml($request->answerD, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
			$images = $answerD->getElementsByTagName('img');
			foreach($images as $img){
				$src = $img->getAttribute('src');
				if(preg_match('/data:image/', $src)){                
					preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
					$mimetype = $groups['mime'];                
					$filename = 'answerD'.'_'.date('YmdHis');
					$filepath = "/images/answer/$filename.$mimetype";    
					$image = Image::make($src)
					->resize(300, 300) 
					->encode($mimetype, 100)
					->save(public_path($filepath));                
					$new_src = asset($filepath);
					$img->removeAttribute('src');
					$img->setAttribute('src', $new_src);
				} 
			} 
			$answer4= $answerD->saveHTML();
		}

		$newUser=CoursePaperQuestion::create([
			'topic_id'=>$request->topic_id,
			'sub_id'=>$request->sub_id,
			'chapter_id'=>$request->chapter_id,

			'course_past_paper'=>$request->course_past_paper,

			'sub_topic_id'=>$request->sub_topic_id,
			'grade_id'=>$request->grade_id,
			'dif_id'=>$request->dif_id,
			'question'=>isset($question1)?$question1:'',
			'type'=>3,
			'created_at'=>now(),
			'created_by'=>Auth::user()->id,
			'updated_by'=>Auth::user()->id
		]);

		CoursePaperQuestionAnswer::insert([
			'past_paper_question_id'=>$newUser->id,
			'ans'=>isset($answer1)?$answer1:'',
			'is_correct'=>$request->correct==1?1:0,
			'type'=>$request->typeA,

		]);
		CoursePaperQuestionAnswer::insert([
			'past_paper_question_id'=>$newUser->id,
			'ans'=>isset($answer2)?$answer2:'',
			'is_correct'=>$request->correct==2?1:0,
			'type'=>$request->typeA,

		]);
		CoursePaperQuestionAnswer::insert([
			'past_paper_question_id'=>$newUser->id,
			'ans'=>isset($answer3)?$answer3:'',
			'is_correct'=>$request->correct==3?1:0,
			'type'=>$request->typeA,

		]);
		CoursePaperQuestionAnswer::insert([
			'past_paper_question_id'=>$newUser->id,
			'ans'=>isset($answer4)?$answer4:'',
			'is_correct'=>$request->correct==4?1:0,
			'type'=>$request->typeA,

		]);


		if($newUser){
			$q= CoursePaperQuestion::with('PastPaperAnswers')->find($newUser->id);
			if(isset($q) && !empty($q)){
				foreach($q->PastPaperAnswers as $answer){
					$ansStatus=0;
					if($answer->is_correct==1){
						$top=null;
						$re['hasAns']=1;
						CoursePaperQuestion::where('id',$newUser->id)->update($re);

					}
				}

				session()->flash('success_message', __('Record Inserted Successfully'));
			}

			else{
				session()->flash('error_message', __('Failed! To Insert Record'));
			}

			return redirect()->back()->with( ['sub_id' => $sub_id,'topic_id'=>$topic_id] );
		}
	}
}
