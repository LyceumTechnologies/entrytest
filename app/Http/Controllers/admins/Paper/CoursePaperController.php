<?php

namespace App\Http\Controllers\admins\Paper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CoursePaperRequest;


use App\Models\Course;
use App\Models\Subject;
use App\Models\Grade;

use App\Models\CoursePaper;
use App\Models\CoursePaperQuestion;

use App\Models\CoursePaperQuestionAnswer;

use Illuminate\Support\Str;


use Session;
use Auth;
use DB;
class CoursePaperController extends Controller
{
	public function index()
	{
		$data=CoursePaper::orderBy('id','DESC')->get();
		return view('admin.coursePaper.pastPaper.index',compact('data'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$courses=Course::get();
    	return view('admin.coursePaper.pastPaper.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CoursePaperRequest $request)
    {
    	
    	$newUser=CoursePaper::create([
    		'course_id' => $request->course_id,
    		'past_paper_name' => $request->past_paper_name,
    		'slug'=>Str::slug($request->past_paper_name),
    		'year' => $request->year,
    		'created_by'=>Auth::user()->id,
    		'updated_by'=>Auth::user()->id,
    	]);

    	if($newUser)
    		session()->flash('success_message', __('Record Inserted Successfully'));
    	else
    		session()->flash('error_message', __('Failed! To Insert Record'));

    	return redirect()->route('past-paper.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    	$data = CoursePaper::find($id);
    	$courses=Course::get();

    	if(!$data){
    		Session::flash('error_message',  __('Failed! Record not found'));
    		return redirect()->back();
    	}
    	return View('admin.coursePaper.pastPaper.edit',compact('data','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CoursePaperRequest $request, $id)
    {
        // dd($request->all());
    	$data = CoursePaper::find($id);
    	if(!$data){
    		Session::flash('error_message', 'Record not found');
    		return redirect()->back();
    	}
    	
    
    	$newUser=CoursePaper::where('id',$id)->update([
    		'course_id' => $request->course_id,
    		'past_paper_name' => $request->past_paper_name,
    		'slug'=>Str::slug($request->past_paper_name),
    		'year' => $request->year,
    		'updated_by'=>Auth::user()->id,
    	]);



    	if($newUser)
    		session()->flash('success_message', 'Record update Successfully');
    	else
    		session()->flash('error_message','Failed to update record');
    	return redirect()->route('past-paper.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
