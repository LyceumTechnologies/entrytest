<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Subject;
use App\Models\Question;

use App\Models\Topic;
use Auth;

class SubjectQuestionExportController extends Controller
{
    public function index(){
    	$subject=Subject::all();
    	return view('admin.export.index',compact('subject'));
    }

    public function store(Request $request){
    	// dd($request->all());
    	$questons=Question::where('sub_id',$request->sb_name)->orderBy('id','DESC')->paginate(100);
    	// dd($questons);

    	return view('admin.export.store',compact('questons'));
    }
}
