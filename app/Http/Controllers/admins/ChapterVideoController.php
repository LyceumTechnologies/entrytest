<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Subject;
use App\Models\CourseVideo;
use App\Models\Grade;
use App\Models\Chapter;
use App\Models\Topic;
use App\Models\SubTopic;

use Auth;

class ChapterVideoController extends Controller
{
     public function index()
    {
        $notes=CourseVideo::orderBy('id','DESC')->get();
        // dd($subject);
        return view('admin.video.index',compact('notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$grade=Grade::get();
    	$subject=Subject::get();

        return view('admin.video.create',compact('grade','subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $request)
    {
        $notes_file=null;
         if($request->hasfile('notes_file')){
            $Extension_profile = $request->file('notes_file')->getClientOriginalExtension();
            $notes_file = 'note'.'_'."$request->grade_id".'_'."$request->sub_id".'_'.date('YmdHis').'.'.$Extension_profile;
            $request->file('notes_file')->move('images/notes/', $notes_file);
        }



       $newUser=CourseVideo::create([
	       	'sub_id'=>$request->sub_id,
	       	'grade_id'=>$request->grade_id,
	       	'chapter_id'=>$request->chapter_id,
	       	'topic_id'=>$request->topic_id,
	       	'sub_topic_id'=>$request->sub_topic_id,
	       	'video_url'=>$request->video_url,

	       	
	       	'video_title'=>$request->video_title,
	       	'video_description'=>$request->video_description,
	       	'created_by'=>Auth::user()->id,
	       	'updated_by'=>Auth::user()->id
       ]);
      
        if($newUser)
            session()->flash('success_message', __('Record Inserted Successfully'));
        else
            session()->flash('error_message', __('Failed! To Insert Record'));

        return redirect()->back();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $note = CourseVideo::find($id);

         



        if(!$note){
            Session::flash('error_message',  __('Failed! Record failed to update'));
            return redirect()->back();
        }

        $grade=Grade::get();
    	$subject=Subject::get();

    	$chapters=Chapter::where('id',$note->chapter_id)->get();
    	$topic=Topic::where('id',$note->topic_id)->get();
    	$sub_topic=SubTopic::where('id',$note->sub_topic_id)->get();

    	// dd($sub_topic);
        return View('admin.video.edit',compact('note','grade','subject','chapters','topic','sub_topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VideoRequest $request, $id)
    {
        $note = CourseVideo::find($id);
        if(!$note){
            Session::flash('error_message',  __('Record not found'));
            return redirect()->back();
        }

       



       $newUser=CourseVideo::where('id',$id)->update([
	       	'sub_id'=>$request->sub_id?$request->sub_id:$note->sub_id,
	       	'grade_id'=>$request->grade_id?$request->grade_id:$note->grade_id,
	       	'chapter_id'=>$request->chapter_id?$request->chapter_id:$note->chapter_id,
	       	'topic_id'=>$request->topic_id?$request->topic_id:$note->topic_id,
	       	'sub_topic_id'=>$request->sub_topic_id?$request->sub_topic_id:$note->sub_topic_id,
	       	'notes_file'=>isset($notes_file)?$notes_file:$note->notes_file,
	       	'notes_title'=>$request->notes_title?$request->notes_title:$note->notes_title,
	       	'notes_description'=>$request->notes_description?$request->notes_description:$note->notes_description,
	       	'updated_by'=>Auth::user()->id
       ]);

        if($newUser)
            session()->flash('success_message', __('Record update Successfully'));
        else
            session()->flash('error_message', __('Failed! Record not update'));
        return redirect()->route('video.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
