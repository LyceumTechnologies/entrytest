<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Subject;
use App\Models\Chapter;
use App\Models\Grade;

use App\Models\Question;
use App\Models\Topic;
use Illuminate\Support\Facades\Validator;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function SubjectHasChapter(Request $request){
       $chapters=Chapter::where('sub_id',$request->sub_id)->where('course_id',$request->grade_id)->get();

       return response()->json(['data'=>$chapters]);
    }
}
