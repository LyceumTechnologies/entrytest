<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Question;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\Answer;
use App\Models\Topic;
use App\Models\Subject;
use App\Models\SubTopic;
use App\Models\Grade;
use App\Models\Chapter;
use App\Models\Difficulty;
use Session;
use Image;


class CommonFirstAjaxController extends Controller
{
    public function questionEditModel($id){

    	try {

           $question=Question::where('id',$id)->with('answers','subject.chapters')->first();
         
	        $topic=Topic::where('sub_id',$question->sub_id)->where('chapter_id',$question->chapter_id)->get();
	        $grade=Grade::all();
	        $difficulty=Difficulty::all();
	        $subject=Subject::with('chapters')->get();
	        $chapters=Chapter::where('sub_id',$question->sub_id)->where('course_id',$question->grade_id)->get();

            return view('admin.ajax.questionEdit',compact('question','topic','grade','difficulty','subject','chapters'));
        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }


    public function ChapterHasTopic(Request $request){
// return $request->all();
    	$topics=Topic::where('sub_id',$request->sub_id)->where('chapter_id',$request->chapter_id)->get();

    	if(count($topics)){
    		return response()->json(['data'=>$topics,'status'=>1]);
    	}else{
			return response()->json(['data'=>$topics,'status'=>0]);
    	}
    }


    public function SubjectChapter(Request $request){
    	$data=Chapter::where('sub_id',$request->sub_id)->where('course_id',$request->grade_id)->get();
    	if(count($data)){
    		return response()->json(['data'=>$data,'status'=>1]);
    	}else{
			return response()->json(['data'=>$data,'status'=>0]);
    	}
    }


    public function questionUpdateAjax(Request $request){



    	$sub_id=$request->sub_id;
        $topic_id=$request->topic_id;
        $id=$request->id;

        if($request->question){
            $question = new \DomDocument();
            libxml_use_internal_errors(true);
            $question->loadHtml($request->question, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $question->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'question'.'_'.date('YmdHis');
                    $filepath = "/images/question/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $question1= $question->saveHTML();
        }
        if($request->answerA){
            $answerA = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerA->loadHtml($request->answerA, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerA->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerA'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
             $answer1=$answerA->saveHTML();
        }
        if($request->answerB){
            $answerB = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerB->loadHtml($request->answerB, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerB->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerB'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $answer2= $answerB->saveHTML();
        }
        if($request->answerC){
            $answerC = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerC->loadHtml($request->answerC, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerC->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerC'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
             $answer3=$answerC->saveHTML();
        }
        if($request->answerD){
            $answerD = new \DomDocument();
            libxml_use_internal_errors(true);
            $answerD->loadHtml($request->answerD, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $answerD->getElementsByTagName('img');
            foreach($images as $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){                
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];                
                    $filename = 'answerD'.'_'.date('YmdHis');
                    $filepath = "/images/answer/$filename.$mimetype";    
                    $image = Image::make($src)
                      ->resize(300, 300) 
                      ->encode($mimetype, 100)
                      ->save(public_path($filepath));                
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } 
            } 
            $answer4= $answerD->saveHTML();
        }
        $ques=Question::find($id);
        if(!$ques){
            return response()->json(['status'=>0]);
        }
            $effectedData=array(
                        'sub_id'=>$request->sub_id?$request->sub_id:$ques->sub_id,
                        'question'=>isset($question1)?$question1:$ques->question,
                        'type'=>0,
                        'topic_id'=>$request->topic_id?$request->topic_id:$ques->topic_id,
                        'sub_id'=>$request->sub_id?$request->sub_id:$ques->sub_id,
                        'sub_topic_id'=>$request->sub_topic_id?$request->sub_topic_id:$ques->sub_topic_id,
                        'grade_id'=>$request->grade_id?$request->grade_id:$ques->grade_id,
                        'dif_id'=>$request->dif_id?$request->dif_id:$ques->dif_id,
                        'chapter_id'=>$request->chapter_id?$request->chapter_id:$ques->chapter_id,
                        'updated_by'=>Auth::user()->id
                    );
       $newUser=Question::where('id',$id)->update($effectedData);
        Answer::where('question_id',$id)->delete();
      
            Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer1)?$answer1:'',
                'is_correct'=>$request->correct==1?1:0,
                'type'=>$request->typeA,
                
            ]);
            Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer2)?$answer2:'',
                'is_correct'=>$request->correct==2?1:0,
                'type'=>$request->typeA,
                
            ]);
           Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer3)?$answer3:'',
                'is_correct'=>$request->correct==3?1:0,
                'type'=>$request->typeA,
                
            ]);
           Answer::insert([
                'question_id'=>$id,
                'ans'=>isset($answer4)?$answer4:'',
                'is_correct'=>$request->correct==4?1:0,
                'type'=>$request->typeA,
                
            ]);
         $newUser=Question::where('id',$id)->with('answers')->first();
         // dd($newUser);
       
        if($newUser)
            return response()->json(['status'=>true]);
        else
           return response()->json(['status'=>false]);
    }
}
