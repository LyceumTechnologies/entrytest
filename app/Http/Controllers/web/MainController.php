<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;
use Auth;
use Session;
class MainController extends Controller
{
    public function service(){
    	return view('web.service');
    }
    public function about(){
    	return view('web.About');
    }
    public function login(){
    	return view('web.login');
    }
    public function UserRegister(Request $request){
        $validator = Validator::make($request->all(),[
          'email' => 'required|unique:users,email',
          'password' => 'required|',

        ]);

    if ($validator->fails())
    {
    	Session::flash('error_message',  __('Ops! You are not register'));
      return redirect()->back()->withInput(Input::all());
    }else{
         $newUser=User::create([

            'name' => ($request->name)!=null?$request->name:'',
            'email' => $request->email,
            'phone' => $request->phone,
            'api_token'=> str_random(60),
            'user_type'=>'user',
            'pwd'=>$request->password,
            'password' => Hash::make($request->password),
          ]);
         if($newUser->save()){
				Session::flash('success_message', 'Record added successfully.');
              return redirect()->back();
         }
         else{
                Session::flash('error_message',  __('Ops! You are not register'));
              return redirect()->back()->withInput(Input::all());
          }


         }
    }

    public function ContactFom(Request $request){
      // Mail::send(new ContactMail($request));
      $sendMail=\Mail::send('mail.contactmail',
       array(
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'subject' => $request->get('subject'),
          'user_message' => $request->get('message')
       ), function($message) use ($request){
          $message->from($request->email);
          $message->to('info@entrytest4u.com', 'Admin')->subject($request->get('subject'));
       });
        if($sendMail){
            // Session::flash('success_message', 'Record added successfully.');
              return response()->json(['status'=>0]);
        }else{
           return response()->json(['status'=>1]);
        }
    }
}
