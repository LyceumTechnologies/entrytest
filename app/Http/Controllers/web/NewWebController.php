<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Auth;
use Session;
class NewWebController extends Controller
{
    public function index(){
    	return view('web.new.index');
    }
    public function howPrepare(){
    	return view('web.new.how-to-prepare');
    }

    public function register(){
    	return view('web.new.account.register');
    }

    public function login(){
    	return view('web.new.account.login');
    }

    public function about(){
    	return view('web.new.about');
    }
    public function contact(){
    	return view('web.new.contact');
    }
    public function policy(){
    	return view('web.new.policy');
    }
    public function terms(){
    	return view('web.new.terms-condition');
    }
    public function testimonial(){
    	return view('web.new.testimonial');
    }
    public function demo(){
    	return view('web.new.demo');
    }

    
}
