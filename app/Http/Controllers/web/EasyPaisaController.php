<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EasyPaisaController extends Controller
{
    public function index(){
    	$hashRequest = '';
		$hashKey = 'GJ0WXA292VPTS7ZS'; // generated from easypay account
		$storeId="1111";
		$amount="30.0" ;
		$postBackURL=route('easy-paisa.create');
		$orderRefNum="7637";
		$expiryDate="20190721 112300";
		$autoRedirect=0 ;
		$paymentMethod='CC_PAYMENT_METHOD';
		$emailAddr='khizer.987@gmail.com';
		$mobileNum="03458509233";

		///starting encryption///
		
		$paramMap = array();
		$paramMap['amount']  = $amount;
		$paramMap['autoRedirect']  = $autoRedirect;
		$paramMap['emailAddr']  = $emailAddr;
		$paramMap['expiryDate'] = $expiryDate;
		$paramMap['mobileNum'] =$mobileNum;
		$paramMap['orderRefNum']  = $orderRefNum;
		$paramMap['paymentMethod']  = $paymentMethod;
		$paramMap['postBackURL'] = $postBackURL;
		$paramMap['storeId']  = $storeId;
		// exit;
		//Creating string to be encoded
		// $mapString = '';
		// foreach ($paramMap as $key => $val) {
		//       $mapString .=  $key.'='.$val.'&';
		// }
		// $mapString  = substr($mapString , 0, -1);

		// Encrypting mapString
		// function pkcs5_pad($text, $blocksize) {
		//       $pad = $blocksize - (strlen($text) % $blocksize);
		//       return $text . str_repeat(chr($pad), $pad);
		// }

		// $alg = MCRYPT_RIJNDAEL_128; // AES
		// $mode = MCRYPT_MODE_ECB; // ECB

		// $iv_size = mcrypt_get_iv_size($alg, $mode);
		// $block_size = mcrypt_get_block_size($alg, $mode);
		// $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);

		// $mapString = pkcs5_pad($mapString, $block_size);
		// $crypttext = mcrypt_encrypt($alg, $hashKey, $mapString, $mode, $iv);
		// $hashRequest = base64_encode($crypttext);
			$cipher="AES-128-ECB";
		    $ivlen = openssl_cipher_iv_length($cipher);
            $iv = openssl_random_pseudo_bytes($ivlen);
            $crypttext = openssl_encrypt($stringTobeEncoded, $cipher, $hashKey,OPENSSL_RAW_DATA, $iv);
            $hashRequest = base64_encode( $crypttext );
            dd($hashRequest);
    	return view('web.easypaisa.easy-paisa');
    }

    public function store(Request $request){
    	dd($request->all());
    }

    public function create(){
    	return view('web.easypaisa.create');
    }

}
