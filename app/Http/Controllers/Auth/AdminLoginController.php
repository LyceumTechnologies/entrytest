<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Models\Admin;
class AdminLoginController extends Controller
{
    public function __construct()
	{
		$this->middleware("guest:admin", ['except' => ['logout']]);
	}

     public function showLoginForm()
    {
      
        return view('web.login');
    }

    public function login(Request $request){
    	// dd($request->all());
    	$add=Admin::get();
    	
       $validate = $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
            ]);

      if($validate){
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password'=>$request->password], $request->remember)){
          session()->flash('success_message', 'You login successfully');
          return redirect(route('dashboard'));
        }
        else{
          session()->flash('error_message', 'Email or Password not match');
        }
      }
      else{
         session()->flash('error_message', 'validation failed');
      }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    

   public function logout()
   {
   	Auth::guard('admin')->logout();
   	return redirect('/admin');
   }
}
