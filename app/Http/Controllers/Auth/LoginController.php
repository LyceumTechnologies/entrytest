<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

     public function login(Request $request){
       $validate = $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
            ]);

      if($validate){
        if(Auth::guard('web')->attempt(['email' => $request->email, 'password'=>$request->password], $request->remember)){
          session()->flash('success', 'You login successfully');
          return redirect(route('user.dashboard'));
        }
        else{
          session()->flash('danger', 'Email or Password not match');
        }
      }
      else{
         session()->flash('warning', 'validation failed');
      }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


}
