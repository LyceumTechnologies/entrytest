<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Auth;
use Session;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserLoginController extends Controller
{
   public function __construct()
	{
		$this->middleware("guest:admin", ['except' => ['logout']]);
	}

     public function showLoginForm()
    {
      
        return view('web.login');
    }

    public function register(UserRequest $request){
    	// dd($request->all());
      $password=str_random(6);
      	$newUser=User::create([
  	        'name' => $request->name?$request->name:' ',
  	        'email' => $request->email,
  	        'phone' => $request->phone,
  	        'pwd'=>$password,
  	        'api_token'=> str_random(60),
  	        'user_type'=>'user',
  	        'password' => Hash::make($password),
            'course_id'=>$request->course_id
  	    ]);

        if($newUser){
          $emails = $request->email;
              Mail::send('mail.accountActivateMail', ['data'=>$newUser], function($message) use ($emails) {    
                  $message->to($emails)->subject('Verify Account');    
              });
        	session()->flash('success_message', __('Mail send successfully! Please Verify your Account'));
        }
        else{
            session()->flash('error_message', __('Failed! To Insert Record'));
        }

        return redirect()->back();
    }

    public function login(Request $request){
    	
       $validate = $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
            ]);

      if($validate){
        $Verify=User::where('email',$request->email)->first();
        if(isset($Verify) && empty($Verify->email_verified_at)){
          session()->flash('error_message', 'Ops! Please Verify Your Account');
          return redirect()->back()->withInput($request->only('email', 'remember'));
        }
        if((Auth::guard('web')->attempt(['email' => $request->email, 'password'=>$request->password], $request->remember)) || (Auth::guard('web')->attempt(['phone' => $request->email, 'password'=>$request->password], $request->remember))){
          session()->flash('success_message', 'You login successfully');
          return redirect(route('user.dashboard'));
        }
        else{
          session()->flash('error_message', 'Email or Password not match');
        }
      }
      else{
         session()->flash('error_message', 'validation failed');
      }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    

   public function logout()
   {
   	Auth::guard('web')->logout();
   	return redirect('/');
   }

   public function confirmEmail($id){
      $user=User::find($id);
      if(!$user){
         session()->flash('error_message', 'Ops! You Have not Account');
          return redirect()->route('user_web.login');
      }
      $user->email_verified_at=date('Y-m-d');
      $record=$user->save();
      if($record){
        session()->flash('success_message', 'Your Account verified successfully');
      }
      else{
        session()->flash('error_message', 'Ops! You Have not Account');
      }

      return redirect()->route('user_web.login');
   }
}
