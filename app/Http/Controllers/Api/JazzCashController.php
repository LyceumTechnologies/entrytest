<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Package;
use App\Models\UserRequest;
use App\Models\UserHasPacakge;
use DB;
use Auth;
class JazzCashController extends Controller
{


		
	
	public function billInquirey(Request $request){

		// if ($request->ip() != "192.168.0.155") {
		// 	return response()->json('Ops! You can not Authorize');
		// }
		$validator = Validator::make($request->all(),[
			'Username' => 'required',
			'Password' => 'required',
			'Consumer_Number'=>'required',
			'Bank_Mnemonic'=>'required',
		]);

		if ($validator->fails()){
			$status = 422;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}else{
			$newUser=User::where('email',$request->Username)->orWhere('phone',$request->Consumer_Number)->where('pwd',$request->Password)->first();
			if(!$newUser){
				$status=0;
				$code='01';
				return $this->ResponseBillEnquiry($status,$newUser,$code);
			}else{
				$package=Package::first();
				if($package){
					$userRequest=UserRequest::create([
						'package_id'=>$package->id,
						'user_id'=>$newUser->id,
						'Bank_Mnemonic'=>$request->Bank_Mnemonic,
						'Due_Date'=>$package->Due_Date,
						'Amount_Within_Due'=>$package->amount,
						'Amount_After_DueDate'=>$package->amount,
						'Billing_Month'=>'',
						'Tran_Auth_Id'=>$package->id
					]);
					$code='00';
					$status=1;
					return $this->ResponseBillEnquiry($status,$newUser,$code);
				}
				

			}     	

		}
	}

	public function PaymentTransaction(Request $request){
		// if ($request->ip() != "192.168.0.155") {
		// 	return response()->json('Ops! You can not Authorize');
		// }
		$validator = Validator::make($request->all(),[
			'Username' => 'required',
			'Password' => 'required',
			'Consumer_Number'=>'required',
			'Bank_Mnemonic'=>'required',
			'Tran_Auth_Id'=>'required',
			'Transaction_Amount'=>'required',
			'Tran_Date'=>'required',
			'Tran_Time'=>'required',

		]);

		if ($validator->fails()){
			$status = 422;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}else{
			$newUser=User::where('email',$request->Username)->orWhere('phone',$request->Consumer_Number)->where('pwd',$request->Password)->first();
			if(!$newUser){

				return response()->json(['Response_Code'=>'01']);
			}else{
				
					$userRequest=UserRequest::where('id',$request->Tran_Auth_Id)->first();
					if(!$userRequest){
						return response()->json(['Response_Code'=>'04']);
					}
				
					if($request->Transaction_Amount==$userRequest->Amount_Within_Due){
						$data['Amount_Paid']=$request->Transaction_Amount;
						$data['Bill_Status']='P';
						UserRequest::where('id',$request->Tran_Auth_Id)->update($data);
						
						$userPackage=UserHasPacakge::create(['user_id'=>$newUser->id,'package_id'=>$userRequest->package_id]);
						if($userPackage){
							return response()->json(['Response_Code'=>'00']);
						}else{
							return response()->json(['Response_Code'=>'02']);
						}

					}
					
				
				return response()->json(['Response_Code'=>'05']);

			}    
		}
	}

	public function ResponseBillEnquiry($status,$newUser,$code){
			$Response_Code=$code;
			$Consumer_Detail=isset($newUser)?$newUser->name:'';
			$Bill_Status='U';
			$Due_Date='0000-00-00';
			$Amount_Within_DueDate=isset($newUser->BillInquiry)?$newUser->BillInquiry->Amount_Within_Due:0;
			$Amount_After_DueDate=isset($newUser->BillInquiry)?$newUser->BillInquiry->Amount_After_DueDate:0;
			$Billing_Month=date('M-d');
			$Date_Paid='';
			$Amount_Paid='';
			$Tran_Auth_Id=isset($newUser->BillInquiry)?$newUser->BillInquiry->id:'';
			$Reserved=isset($newUser->BillInquiry)?$newUser->BillInquiry->package_id:'';
			return response()->json(['Response_Code'=>$Response_Code,'Consumer_Detail'=>$Consumer_Detail,'Bill_Status'=>$Bill_Status,'Due_Date'=>$Due_Date,'Amount_Within_DueDate'=>$Amount_Within_DueDate,'Amount_After_DueDate'=>$Amount_After_DueDate,'Billing_Month'=>$Billing_Month,'Date_Paid'=>$Date_Paid,'Amount_Paid'=>$Amount_Paid,'Tran_Auth_Id'=>$Tran_Auth_Id,'Reserved'=>$Reserved

			]);
	}
}
