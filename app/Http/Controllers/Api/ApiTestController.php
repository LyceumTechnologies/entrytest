<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Question;
use App\Models\Course;
use App\Models\Answer;
use App\Models\Test;
use App\Models\TestDetail;
use App\Models\QuestionToday;
use App\Models\DemoTest;
use App\Models\DemoTestQuestion;
use App\Models\QuestionDayResult;
use Auth;
use DB;
use App\Http\Resources\DignosticResourceCollection;
use App\Http\Resources\DignosticQuestionResourceCollection;
use Illuminate\Support\Facades\Log;

class ApiTestController extends Controller
{
    public function testCreate(Request $request){
    	Log::info('Topic wise question: '.json_encode($request->all()));

    	$validator = Validator::make($request->all(), [
	        'courseid' => 'required_without:sub_id' ,
	        'sub_id' => 'required_without:courseid',
        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	     }else{
	    	$test=Test::create([
	    	'test_name'=>($request->test_name)?$request->test_name:'',
	    	'user_id'=>Auth::guard('api')->id(),
	    	'course_id'=>($request->courseid)?$request->courseid:'',
	    	'sub_id'=>($request->sub_id)?$request->sub_id:'',
	    	'level'=>($request->level)?$request->level:0,
	    	'test_date'=>date('Y-m-d'),
	    	'description'=>($request->description)?$request->description:''
	    	]);
	    	if($test){
	    		$sub_id=array();
	    		if(!empty($test->course_id)){
	    			$course=Course::with('couseSubject.subject')->where('id',$test->course_id)->first();
		    		foreach($course->couseSubject as $subject){
		    			array_push($sub_id, $subject->sub_id);
		    		}
	    		}if(!empty($test->sub_id)){
	    			array_push($sub_id, $test->sub_id);
	    		}
	    		
	    		$question=Question::whereIn('sub_id',$sub_id)->with('answers')->where('hasAns',1);
	    		if(isset($request->topic_id) && !empty($request->topic_id)){
	    			$question->where('topic_id',$request->topic_id);
	    		}

	    		$quetionRecord=$question->get();
	    
	    		 Log::info('Questionz count '.count($quetionRecord));
	    		if(count($quetionRecord)){
	    			return response()->json(['status'=>true,'message'=>'Test Start now','test_id'=>$test->id,'data'=>$quetionRecord]);
	    		}else{
	    			return response()->json(['status'=>false,'message'=>'Oops! Something is wrong']);
	    		}
	    		
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Something is wrong']);
	    	}
	    }

    }

    public function testQuestion(Request $request){
    	$validator = Validator::make($request->all(), [
        'question_id' => 'required',
        'test_id'=>'required',
        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	     }else{
	    	$test=TestDetail::create([
	    	'test_id'=>$request->test_id,
	    	'question_id'=>$request->question_id,
	    	'user_ans'=>($request->user_ans)?$request->user_ans:0,
	    	'is_correct'=>($request->is_correct),
	    	]);
	    	if($test){
	    		return response()->json(['status'=>true,'message'=>'Record inserted Successfully']);
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Record not found']);
	    	}
	    }
    }

    public function questionToday(){
    	$question=QuestionToday::where('questionDate',date('Y-m-d'))->first();
    	if($question){
	    		return response()->json(['status'=>true,'message'=>'Record Found Successfully','data'=>$question]);
	    }else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Record not found']);
	    }
    }

    public function dignoseTest(){

    	$test=DemoTest::where('status',1)->get();
    	if(count($test)){
    			$response=new DignosticResourceCollection($test);
	    		return response()->json(['status'=>true,'message'=>'Record Found Successfully','data'=>$response]);
	    }else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Record not found']);
	    }
    }

    public function DignoseTestQuestion(Request $request){
    	$validator = Validator::make($request->all(), [
        	'dig_test_id' => 'required',
        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	     }else{
	    	$test=DemoTestQuestion::select('question_id')->where('test_id',$request->dig_test_id)->get();
	    	$question=Question::whereIn('id',$test)->with('answers')->where('hasAns',1)->get();
	    	if(count($question)){
	    		$response=new DignosticQuestionResourceCollection($question);
	    		return response()->json(['status'=>true,'message'=>'Record inserted Successfully','data'=>$response]);
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Record not found']);
	    	}
	    }
    }

    public function TestHistory(){
    	$test=Test::where('user_id',Auth::guard('api')->id())->with('course','subject','testDetail')->select("tests.*",
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id 
                        ) as total_question"),
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id AND test_details.is_correct = 1
                        ) as total_correct"),
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id AND test_details.is_correct = 0
                        ) as total_wrong"),
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id AND test_details.is_correct = 2 
                        ) as total_skip")
              )->orderBy('id','DESC')->get();
    	if(count($test)){
    		return response()->json(['status'=>true,'message'=>'Record Found Successfully','data'=>$test]);
    	}else{
    		return response()->json(['status'=>false,'message'=>'Oops! Record not found']);
    	}
    }

    public function TestHistoryById(Request $request){
    	$validator = Validator::make($request->all(), [
        'test_id'=>'required',
        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	    }else{
	    	$test=Test::where('id',$request->test_id)->with('course')->with('testDetail')->select("tests.*",
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id 
                        ) as total_question"),
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id AND test_details.is_correct = 1
                        ) as total_correct"),
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id AND test_details.is_correct = 0
                        ) as total_wrong"),
                  	\DB::raw("(SELECT count(*) FROM test_details
                          WHERE test_details.test_id = tests.id AND test_details.is_correct = 2
                        ) as total_skip")
              )->first();
	    	if(($test)){
	    		return response()->json(['status'=>true,'message'=>'Record Found Successfully','data'=>$test]);
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Record not found']);
	    	}
	    }
    	
    }
    public function AnswerToday(Request $request){
    	$validator = Validator::make($request->all(), [
        	'q_today_id'=>'required',
        	'is_correct'=>'required'

        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	    }else{
	    	$today=QuestionDayResult::where('q_today_id',$request->q_today_id)->where('user_id',Auth::guard('api')->id())->first();
	    	if($today){
	    		return response()->json(['status'=>false,'message'=>'You have already attempt this question']);
	    	}
	    	$test=QuestionDayResult::create([
	    		'q_today_id'=>$request->q_today_id,
	    		'user_id'=>Auth::guard('api')->id(),
	    		'is_correct'=>$request->is_correct,
	    	]);
	    	if(($test)){
	    		return response()->json(['status'=>true,'message'=>'Record Inserted Successfully','data'=>$test]);
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Record not insert']);
	    	}
	    }
    }
}
