<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\SettingResource;
use App\Models\Setting;
class ApiSettingController extends Controller
{
    public function AppVersion(){
    	$setting=Setting::orderBy('id','DESC')->first();
    	if($setting){
    		$response=new SettingResource($setting);
    		return response()->json(['status'=>true,'message'=>'Record found successfully','data'=>$response]);
    	}else{
    		return response()->json(['status'=>false,'message'=>'Record not found']);
    	}
    }
}
