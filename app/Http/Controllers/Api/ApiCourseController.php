<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Subject;
use App\Models\Course;
use App\Models\Topic;
use Auth;
use DB;
use App\Http\Resources\DignosticResourceCollection;
use App\Http\Resources\DignosticQuestionResourceCollection;
use Illuminate\Support\Facades\Log;

class ApiCourseController extends Controller
{
    public function getCourse(){
    	$course=Course::get();
    	if(count($course)){
            $response=new DignosticResourceCollection($course);
    		return response()->json(['status'=>true,'message'=>'Record found successfully','data'=>$response]);
    	}else{
    		return response()->json(['status'=>false,'message'=>'Record not found']);
    	}
    }
    public function courseQuestion(Request $request){
    	$validator = Validator::make($request->all(),[
				'courseid' => 'required', 
		]);
		if($validator->fails()){
			$status = false;
			$error = $validator->errors()->first();
			return response()->json(['status'=>$status, 'message'=>$error], 200);
		}else{
			$sub_id=array();
    		$course=Course::with('couseSubject.subject')->where('id',$request->courseid)->first();
    		foreach($course->couseSubject as $subject){
    			array_push($sub_id, $subject->sub_id);
    		}
    		$question=Question::whereIn('sub_id',$sub_id)->with('answers')->get();
    		if(count($question)){
                $response=new DignosticQuestionResourceCollection($question);
    			return response()->json(['status'=>true,'message'=>'Record found successfully','data'=>$question]);
    		}else{
    			return response()->json(['status'=>false,'message'=>'Record not found']);
    		}

    	}
    }

    public function CourseHasSubject(Request $request){
        $validator = Validator::make($request->all(),[
                'courseid' => 'required', 
        ]);
        if($validator->fails()){
            $status = false;
            $error = $validator->errors()->first();
            return response()->json(['status'=>$status, 'message'=>$error], 200);
        }else{
            $sub_id=array();
            $course=Course::with('couseSubject')->where('id',$request->courseid)->first();
            foreach($course->couseSubject as $subject){
                array_push($sub_id, $subject->sub_id);
            }
            $question=Subject::whereIn('id',$sub_id)->get();
            if(count($question)){
                return response()->json(['status'=>true,'message'=>'Record found successfully','data'=>$question]);
            }else{
                return response()->json(['status'=>false,'message'=>'Record not found']);
            }

        }

    }

    public function SubjectHasChapter(Request $request){
        Log::info('chapters: '.json_encode($request->all()));
        $validator = Validator::make($request->all(),[
                'subid' => 'required', 
        ]);
        if($validator->fails()){
            $status = false;
            $error = $validator->errors()->first();
            return response()->json(['status'=>$status, 'message'=>$error], 200);
        }else{
            $tempArray=array();
            $question=Topic::where('sub_id',$request->subid)->where('hasQuestion','>',10)->where('status',1)->get();
           
            if(count($question) ){
                return response()->json(['status'=>true,'message'=>'Record found successfully','data'=>$question]);
            }else{
                return response()->json(['status'=>false,'message'=>'Record not found']);
            }

        }
    }
}
