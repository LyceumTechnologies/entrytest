<?php

namespace App\Http\Controllers\Api\V2;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Package;
use Auth;
use DB;
class ApiQuestionController extends Controller
{
    public function question(){
    	$question=Question::with('answers','subject')->where('hasAns',1)->get();
    	if(count($question)){
    		$status=true;
    		$message='Record found Successfully';
    		return response()->json(['status'=>$status,'message'=>$message,'data'=>$question],200);
    	}else{
    		$status=false;
    		$message='Oops! Record not Found';
    		return response()->json(['status'=>$status,'message'=>$message],200);
    	}
    }
    public function questionSubjectWise(Request $request){
        $validator = Validator::make($request->all(), [
        'sub_id' => 'required'
        ]);    
        if ($validator->fails()){
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status'=>$status,'message'=>$message], 200);
        }else{
            $question=Question::where('sub_id',$request->sub_id)->with('answers')->where('hasAns',1)->get();
            $tempArray=array();
            foreach($question as $ans){
                    $ansStatus=0;
                    foreach($ans->answers as $answer){
                        if($answer->is_correct)
                            $ansStatus=1;
                    }
                    if($ansStatus){
                        array_push($tempArray, $ans);
                    }
            }

            if(count($tempArray)){
                $status=true;
                $message='Record found Successfully';
                return response()->json(['status'=>$status,'message'=>$message,'data'=>$tempArray],200);
            }else{
                $status=false;
                $message='Oops! Record not Found';
                return response()->json(['status'=>$status,'message'=>$message],200);
            }
        }
    }

    public function GetPackage(){
        $question=Package::get();
        if(count($question)){
            $status=true;
            $message='Record found Successfully';
            return response()->json(['status'=>$status,'message'=>$message,'data'=>$question],200);
        }else{
            $status=false;
            $message='Oops! Record not Found';
            return response()->json(['status'=>$status,'message'=>$message],200);
        }
    }
}
