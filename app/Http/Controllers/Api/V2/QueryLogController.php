<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Question;
use App\Models\Answer;
use App\Models\Topic;
use Auth;
use File;
class QueryLogController extends Controller
{
    public function bookData(){
    	 $fineStd=DB::select( DB::raw("SELECT *  FROM english_19april " ) );
       // return count($fineStd);
    	 foreach($fineStd as $qu){
    	 	$topic=Topic::where('name',$qu->ChapNumber)->where('sub_id',5)->first();
            if(!$topic){
	      
	                $topic=Topic::create(['sub_id'=>5,'name'=>$qu->ChapNumber]);
	            
                    			
            }
    	 	$newUser=Question::create([
	       		'topic_id'=>isset($topic->id)?$topic->id:0,
	       		'sub_id'=>5,
	       		'question'=>($qu->Question),
       		]);
       		Answer::insert([
	       		'question_id'=>$newUser->id,
	       		'ans'=>$qu->A,
	       		'is_correct'=>($qu->A==$qu->Answer)?1:0,
	       	]);
	       	Answer::insert([
	       		'question_id'=>$newUser->id,
	       		'ans'=>$qu->B,
	       		'is_correct'=>($qu->B==$qu->Answer)?1:0,
	       	]);
	       	Answer::insert([
	       		'question_id'=>$newUser->id,
	       		'ans'=>$qu->C,
	       		'is_correct'=>($qu->C==$qu->Answer)?1:0,
	       	]);
	       	Answer::insert([
	       		'question_id'=>$newUser->id,
	       		'ans'=>$qu->D,
	       		'is_correct'=>($qu->D==$qu->Answer)?1:0,
	       	]);
    	 }
    	 return $fineStd;
    }
    public function toNum($data) {
      $numarr=array(
         0=>'A',
         1=>'B',
         2=>'C',
         3=>'D' ,
         4=>'E',
         5=>'F',
         6=>'G',
         7=>'H',
         8=>'I',
         9=>'J');
      $narr = array_flip($numarr);
      $arr = str_split($data);

      $str = '';
      if(isset($data) && !empty($data)){
        foreach($arr as $s)
        $str .= $narr[$s];
      }else{
        $str=null;
      }
      
      return $str;
  }

    public function QuestionStore(Request $request){
        $question=(isset($request->question)?$this->toNum($request->question):null);
        $a=(isset($request->a)?$this->toNum($request->a):null);
        $b=(isset($request->b)?$this->toNum($request->b):null);
        $c=(isset($request->c)?$this->toNum($request->c):null);
        $d=(isset($request->d)?$this->toNum($request->d):null);
        $correct=(isset($request->correct)?$this->toNum($request->correct):null);
        $topic_id=$request->topic_id;
        
        $excelData=array();
        $repeatedArray=[];
        $missedFeeIdArray=[];
        // return ($request->all());
        if($request->hasFile('mis')){

            $extension = File::extension($request->mis->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                $path = $request->mis->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                $headerRow = $data->first()->keys()->toArray();
                $ddCount=(count($data));
                $entry=$ddCount;
                if(!empty($data) && $data->count()){
                    $excelData=$data;
                    $index=0;
                    foreach($data as $record){
                    	// return $record;exit;
                    	if(empty($record->question)){
                    	
                    	}else{

                    		$topic=Topic::where('name',$record->ch)->where('sub_id',1)->first();
                    		// return $record;
                    		if(!$topic){
                    			if(isset($record->ch) && !empty($record->ch)){
                    				$topic=Topic::create(['sub_id'=>1,'name'=>$record->ch]);
                    			}
                    			
                    		}
                    		// return $topic;
                    		$newUser=Question::create([
				       		'topic_id'=>isset($topic->id)?$topic->id:'0',
				       		'sub_id'=>1,
				       		'question'=>($record->question),
				       		]);
				       		Answer::insert([
					       		'question_id'=>$newUser->id,
					       		'ans'=>$record->a,
					       		'is_correct'=>($record->a==$record->answer)?1:0,
					       	]);
					       	Answer::insert([
					       		'question_id'=>$newUser->id,
					       		'ans'=>$record->b,
					       		'is_correct'=>($record->b==$record->answer)?1:0,
					       	]);
					       	Answer::insert([
					       		'question_id'=>$newUser->id,
					       		'ans'=>$record->c,
					       		'is_correct'=>($record->c==$record->answer)?1:0,
					       	]);
					       	Answer::insert([
					       		'question_id'=>$newUser->id,
					       		'ans'=>$record->d,
					       		'is_correct'=>($record->d==$record->answer)?1:0,
					       	]);
                    	}
                    	
                    }

                }
            }
        }
       

       return response()->json(['status'=>true,'message'=>'Record inserted Successfully']);
    }

    public function checkQuestionAns(){
        $q= Question::with('answers')->get();
          foreach($q as $ans){
            foreach($ans->answers as $answer){
              $ansStatus=0;
                if($answer->is_correct==1){
                  
                  $top=null;
                  $re['hasAns']=1;
                  Question::where('id',$ans->id)->update($re);
                  $ansStatus=1;
                  if($ansStatus){
                    $top=Topic::where('id',$ans->topic_id)->first();
                    if(isset($top) && !empty($top)){
                      $top->hasQuestion++;
                      $top->save();
                    }
                    
                  }
                }
            }

              
              
            
          }
          return 'true';
    }

   
}
