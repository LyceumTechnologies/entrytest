<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DB;
use Auth;
use App\Http\Resources\UserResource;

class ApiUserController extends Controller
{
    
  public function register(Request $request)
  {
    $validator = Validator::make($request->all(),[
      'email' => 'required|unique:users,phone',
      'password' => 'required|',

    ]);

    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);
    }else{
     $newUser=User::create([

        'name' => ($request->input('name'))!=null?$request->input('name'):'',
        'email' => $request->input('email'),
        'phone' => $request->input('email'),
        'api_token'=> ($request->api_token)!=null?$request->api_token:str_random(60),
        'user_type'=>'user',
        'pwd'=>$request->input('password'),
        'password' => Hash::make($request->input('password')),
      ]);
     if($newUser->save()){
         
          $status = true;
          $message = "Sign up successful, check your email ";
          $resour=new UserResource($newUser);
          return response()->json(['status'=>$status,'message'=>$message,'user'=>$resour], 200);
     }
     else{
          $status = false;
          $message = "Sign up Incorrect email , try correct email format ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }


     }
  }

  
   public function login(Request $request){
      $validator = Validator::make($request->all(), [
        'email'=>'required',
        'password' => 'required',

        ]);    
      if ($validator->fails())
      {
        $status = false;
        $message = $validator->errors()->first();
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }else{
      if(isset($request->email) && !empty($request->email)){
        $user = User::where(['email' => $request->email])->orWhere('phone',$request->email)->first();
      if(isset($user) && !empty($user)){
        if(Hash::check($request->password, $user->password)){
          
          if(!empty($request->device_token)){
            $fat['device_token']=$request->device_token;
          }

          $fat['status']=true;
          $user12=User::where('email',$request->email)->update($fat);
          $status = true;
          $message = 'user has been successfully login.';
          $resour=new UserResource($user);
            return response()->json(['status'=>$status,'message'=>$message,'user'=>$resour], 200);
        }else{
        $status = false;
        $message = 'Sign in incorrect email or password , write correct';
          return response()->json(['status'=>$status,'message'=>$message], 200);
          }
      }else{
        $status = false;
        $message = 'Sign in incorrect email or password , write correct';
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }
  }
}

    public function logout()
   {
      $user = User::where('id', Auth::guard('api')->id())->update(array('timeout'=> now()));

     if($user){
          $status = true;
          $message = "'user has been successfully logout.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
     }
     else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }


    public function forgotton(Request $request){
      $validator = Validator::make($request->all(), [
        'email' => 'required_without:phone|min:6' ,
        'phone' => 'required_without:email|min:6',
        ]);    
      if ($validator->fails())
      {
        $status = true;
        $message = $validator->errors()->first();
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }
      else
      {
        if(isset($request->email) && !empty($request->email))
        {
          $user = User::select('api_token')->where(['email' => $request->email])->orWhere('phone',$request->phone)->first();
        }
        else if(isset($request->phone) && !empty($request->phone))
        {
          $user=User::select('api_token')->where(['phone' => $request->phone])->first();
        }
        else{
          $status = false;
          $message = "Email/Phone is not matched.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }
        if(isset($user) && !empty($user))
        {
          $status = true;
          $message = 'Enter new password.';
          $resour=new UserResource($user);
          return response()->json(['status'=>$status,'message'=>$message, 'user'=>$resour], 200);
        }
      
      }
    }

    public function set_user_password(Request $request)
    {
      $validator = Validator::make($request->all(),[
        'email' => 'required_without:phone|min:6', 
        'phone' => 'required_without:email|min:6',
        'password'=>'required'
      ]);
    if($validator->fails())
      {
        $status = false;
        $error = $validator->errors()->first();
        return response()->json(['status'=>$status, 'message'=>$error], 200);
      } else
      {
      $pass = $request->password;
      $hash = Hash::make($pass);
      $update = User::where('phone', $request->phone)->update(['password' => $hash]);
      if($update)
      {
        $status = true;
        $message = 'Password Update Success';
        return response()->json(['message'=>$message, 'status'=>$status], 200);
      } else 
      {
        $status = false;
        $message = 'Error on updation try again';
        return response()->json(['message'=>$message, 'status'=>$status], 200);
      }
    }
  }


  function profile(Request $request){
    $validator = Validator::make($request->all(),[
      'email'=>'required'
    ]);
    if($validator->fails()){
      $status = false;
      $error = $validator->errors()->first();
      return response()->json(['status'=>$status, 'message'=>$error], 200);
    } else {
      $data = User::findOrFail(Auth::guard('api')->id());
      $record=User::where('id','<>',Auth::guard('api')->id())->where('email',$request->email)->orWhere('phone',$request->email)->first();
    
      if($record){
          $status = false;
          $message = " Email or phone already exist ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
      if($data){
        $input = $request->all();
        if($images = $request->file('images')){
          $captain_image = $request->file('images')->getClientOriginalName();
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $captain_image = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/user/', $captain_image);
          $input['images'] = $captain_image;
        }
        if(isset($input['password']) && !empty($input['password'])) { 
           $input['password'] = Hash::make($request->input('password'));
           $input['pwd']=$request->input('password');
        }

        $update_image = $data->update($input);
        if(!empty($update_image)){
          $user = User::where('id',Auth::guard('api')->id())->first();
           
          $status = true;
          $message = "user Record update successfully";
          $resour=new UserResource($user);
          return response()->json(['status'=>$status,'message'=>$message, 'data'=>$resour], 200);
        }else{
          $status = false;
          $message = " user Record not update. ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
        }
      }else{
        $status = false;
        $message = " user not found. ";
        return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }
  }
}
