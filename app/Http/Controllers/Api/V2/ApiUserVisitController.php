<?php

namespace App\Http\Controllers\APi\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\UserVisit;
use DB;
use Auth;
use Carbon\Carbon;
use App\Http\Resources\NotificationResource;
use App\Models\Setting;
class ApiUserVisitController extends Controller
{
	public function register(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'visitUrl' => 'required',
		]);

		if ($validator->fails())
		{
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}else{
			$newUser=UserVisit::create([

				'user_id' => Auth::guard('api')->id(),
				'visitUrl' => $request->visitUrl,
				'visit_timing' => $request->visit_timing,
			]);
			if($newUser->save()){

				$status = true;
				$message = "Record Inserted Successfully";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}
			else{
				$status = false;
				$message = "Failed! Record not Inserted ";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}


		}
	}
	public function notifcation(){
		$notifcation=Setting::first();
		// if(Carbon::parse($notifcation->updated_at)->addMinutes(600)->isPast()) {
  //               $notifcation=Setting::first()->update(['notifcation'=>'']);
  //       }
		if(isset($notifcation->notifcation) && !empty($notifcation->notifcation)){
			// return $notifcation;
				$data=new NotificationResource($notifcation);
				$status = true;
				$message = "Record Found Successfully";
				return response()->json(['status'=>$status,'message'=>$notifcation->notifcation], 200);
			}
			else{
				$status = false;
				$message = "Failed! Record not found ";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}
	}
	public function updateVersion(Request $request){
		$validator = Validator::make($request->all(),[
			'app_version' => 'required',
		]);

		if ($validator->fails())
		{
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}else{
			$newUser=Setting::first()->update(['app_version'=>$request->app_version]);
			if($newUser){
				$status = true;
				$message = "Record Update Successfully";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}
			else{
				$status = false;
				$message = "Failed! Record not Update ";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}


		}
	}
	public function notifcationCreate(Request $request){
		$validator = Validator::make($request->all(),[
			'notifcation' => 'required',
		]);

		if ($validator->fails())
		{
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}else{
			$newUser=Setting::first()->update(['notifcation'=>$request->notifcation]);
			if($newUser){
				$status = true;
				$message = "Record Inserted Successfully";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}
			else{
				$status = false;
				$message = "Failed! Record not Inserted ";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}


		}
	}
}
