<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use nusoap_client;
use nusoap_server;
class SoapController extends Controller
{
    public function server() {
        // require_once ('nusoap.php');
        $server = new nusoap_server;
 
    $server->configureWSDL('opencart', 'urn:opencart');
      
    $server->wsdl->schemaTargetNamespace = 'urn:opencart';
          
    // register a function with name "whatsNew"
    $server->register('whatsNew',
                array('json' => 'xsd:string'),  //parameter
                array('return' => 'xsd:string'),  //output
                'urn:server',   //namespace
                'urn:server#opencart',  //soapaction
                'rpc', // style
                'encoded', // use
                'Fetch latest tech');  //description
      
    //function implementation
    function whatsNew($json) {
 
        $data = json_decode($json);
 
        $username = $data->username;
 
        $latest = array('Google Glass','Form 1','Oculus Rift','Leap Motion','Eye Tribe','SmartThings','Firefox OS','Project Fiona','Parallella','Google Driverless Car');
        $string = '';
 
        foreach ($latest as $key) {
            $string .= $key.', ';
        }
        $latest = rtrim($string,', ');
        return 'Hello, '.$username.', latest technologies of 2016 are '.$latest.'.';
    }
          
    $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
      
    $server->service($HTTP_RAW_POST_DATA);
    }

    public function clientWebService(){
    	$wsdl = "/any";
      
    	//create client object
	    $client = new nusoap_client($wsdl, 'wsdl');
	 
	    $data = array(
	            'username'=>'John Doe'
	        );
	 
	    $json = json_encode($data);
	    function whatsNew($json) {
 
        $data = json_decode($json);
 
        $username = $data->username;
 
        $latest = array('Google Glass','Form 1','Oculus Rift','Leap Motion','Eye Tribe','SmartThings','Firefox OS','Project Fiona','Parallella','Google Driverless Car');
        $string = '';
 
        foreach ($latest as $key) {
            $string .= $key.', ';
        }
        $latest = rtrim($string,', ');
        return 'Hello, '.$username.', latest technologies of 2016 are '.$latest.'.';
    }
	         
	        // passing function name "whatsNew" and json encoded data
	    $result = $client->call('whatsNew', array('json'=>$json));
	 
	    if ($result) {
	        print_r($result);
	    } else {
	        print_r('Error!!!');
	    }
    }
}
