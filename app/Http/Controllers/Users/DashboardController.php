<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Subject;
use App\Models\Question;
use App\Models\Topic;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiMessageController;
class DashboardController extends Controller
{
    public function index(){
    	return view('user.dashboard');
    }
    public function testPage(){
    	$course=Course::where('status',1)->with('couseSubject.subject')->get();
    	return view('user.test',compact('course'));
    }
    public function success_story(){
    	return view('user.success_story');
    }
    public function question($id){
    	$question=Question::where('topic_id',$id)->with('answers')->where('hasAns',1)->paginate(12);
        $topic=Topic::find($id);
    	return view('user.questions',compact('question','id','topic'));
    }
    public function practice(){
    	$data=Subject::where('status',1)->with('filterChapters')->get();
    	return view('user.practiceBook',compact('data'));
    }
    public function previousTest(){
        return view("user.previousTest");
    }
  public function fulllength(){
        return view("user.fulllength");
    }
      public function viewnotes(){
        return view("user.viewnotes");
    }
       public function writenotes(){
        return view("user.writenotes");
    }
    public function subjectTopic(Request $request)
    {
        try {
            $allInputs = $request->all();
            $id = $request->input('id');


            $validation = Validator::make($allInputs, [
                'id' => 'required'
            ]);

            if ($validation->fails()) {
                $response = (new ApiMessageController())->validatemessage($validation->errors()->first());
            } else {
            	 $data=Topic::where('sub_id',$id)->where('hasQuestion','>',10)->where('status',1)->get();
                $subject = Subject::find($id);

                return view('user.subjectTopic',compact('data','subject'));
            }
        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;
    }
}
