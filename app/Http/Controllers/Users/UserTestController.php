<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Subject;
use App\Models\Question;
use App\Models\Topic;
use App\Models\CourseSubject;
use App\Models\CourseSubjectTopic;
use App\Models\Answer;
use App\Models\QuestionToday;
use App\Models\DemoTest;
use App\Models\DemoTestQuestion;
use App\Models\QuestionDayResult;
use App\Models\SubTopic;
use App\Models\Test;
use App\Models\TestDetail;
use App\Models\BookMark;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiMessageController;
use Auth;
use Session;
class UserTestController extends Controller
{
    public function testCreate(Request $request){

    	$validator = Validator::make($request->all(), [
	        'courseid' => 'required_without:sub_id' ,
	        'sub_id' => 'required_without:courseid',
        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	     }else{
	    	$test=Test::create([
	    	'test_name'=>($request->test_name)?$request->test_name:'',
	    	'user_id'=>Auth::user()->id(),
	    	'course_id'=>($request->courseid)?$request->courseid:'',
	    	'sub_id'=>($request->sub_id)?$request->sub_id:'',
	    	'level'=>($request->level)?$request->level:0,
	    	'test_date'=>date('Y-m-d'),
	    	'description'=>($request->description)?$request->description:''
	    	]);
	    	if($test){
	    		$sub_id=array();
	    		if(!empty($test->course_id)){
	    			$course=Course::with('couseSubject.subject')->where('id',$test->course_id)->first();
		    		foreach($course->couseSubject as $subject){
		    			array_push($sub_id, $subject->sub_id);
		    		}
	    		}if(!empty($test->sub_id)){
	    			array_push($sub_id, $test->sub_id);
	    		}
	    		
	    		$question=Question::whereIn('sub_id',$sub_id)->with('answers')->where('hasAns',1);
	    		if(isset($request->topic_id) && !empty($request->topic_id)){
	    			$question->where('topic_id',$request->topic_id);
	    		}

	    		$quetionRecord=$question->get();
	    
	    		 Log::info('Questionz count '.count($quetionRecord));
	    		if(count($quetionRecord)){

	    			return response()->json(['status'=>true,'message'=>'Test Start now','test_id'=>$test->id,'data'=>$quetionRecord]);
	    		}else{
	    			return response()->json(['status'=>false,'message'=>'Oops! Something is wrong']);
	    		}
	    		
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Something is wrong']);
	    	}
	    }

    }

    public function testQuestion(Request $request){
    	$validator = Validator::make($request->all(), [
        'question_id' => 'required',
        'test_id'=>'required',
        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	     }else{
	     	

	     	$testEdit=Test::find($request->test_id);
	     	$testEdit->total_question++;
	     	if($request->is_correct==1){
	     		$testEdit->correct_ans++;
	     	}if($request->is_correct==0){
	     		$testEdit->wrong_ans++;
	     	}
	     	$testEdit->save();
	     	$testCheck=TestDetail::where('test_id',$request->test_id)->where('question_id',$request->question_id)->first();
	     	if($testCheck){
	     		return response()->json(['status'=>false,'message'=>'Oops! All ready test inserted']);
	     	}
	     	
	    	$test=TestDetail::create([
	    	'test_id'=>$request->test_id,
	    	'question_id'=>$request->question_id,
	    	'user_ans'=>($request->user_ans)?$request->user_ans:0,
	    	'is_correct'=>($request->is_correct),
	    	]);
	    	if($test){

	    		return response()->json(['status'=>true,'message'=>'Record inserted Successfully']);
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Record not found']);
	    	}
	    }
    }

    public function BookMark(Request $request){
    	$validator = Validator::make($request->all(), [
        'question_id' => 'required',
        ]);    
	    if($validator->fails()){
	        $status = false;
	        $message = $validator->errors()->first();
	        return response()->json(['status'=>$status,'message'=>$message], 200);
	     }else{
	     	$testCheck=BookMark::where('user_id',Auth::user()->id)->where('question_id',$request->question_id)->first();
	     	if($testCheck){
	     		return response()->json(['status'=>false,'message'=>'Question already book marked']);
	     	}
	    	$test=BookMark::create([
	    	'test_id'=>$request->test_id,
	    	'question_id'=>$request->question_id,
	    	'user_id'=>Auth::user()->id,
	    	]);
	    	if($test){

	    		return response()->json(['status'=>true,'message'=>'Question Bookmarked Successfully']);
	    	}else{
	    		return response()->json(['status'=>false,'message'=>'Oops! Something is wrong..']);
	    	}
	    }
    }
}
