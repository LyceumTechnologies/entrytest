<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Subject;
use App\Models\Question;
use App\Models\Topic;
use App\Models\CourseSubject;
use App\Models\CourseSubjectTopic;
use App\Models\SubTopic;
use App\Models\Test;
use App\Models\TestDetail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiMessageController;
use Auth;
class SubjectTopicController extends Controller
{
    public function topicSection($id,$course_id=null){
    	$data=Subject::with('chapters','chapters.subTopic')->find($id);

    	// dd($data);
    	return view('user.sectionSubsection',compact('data','course_id','id'));
    }

    public function subSection(Request $request){
        $student=SubTopic::select('id','sub_topic_name')->where('topic_id',$request->id)->get();
        if(count($student)){

            return response()->json(['data'=>$student,'status'=>1],200);
        }else{
            $message='Record not found';
             return response()->json(['data'=>$message,'status'=>0], 200);
        }
    }
    public function getQuestion(Request $request){
        sessionValueFlush();
        $course= Course::find($request->selected_course_id);
        $subject= Subject::find($request->sub_id);
        $testName=(isset($subject->sb_name)?$subject->sb_name:'').' of '.(isset($course->name)?$course->name:'');

        $test=Test::create([
            'test_name'=>($request->testName)?$request->testName:'',
            'user_id'=>Auth::user()->id,
            'course_id'=>($request->selected_course_id)?$request->selected_course_id:'',
            'sub_id'=>($request->sub_id)?$request->sub_id:'',
            'level'=>($request->level)?$request->level:0,
            'test_date'=>date('Y-m-d'),
            'description'=>($request->description)?$request->description:''
            ]);

        $question=Question::where('sub_id',$request->sub_id)->with('answers');
        if(isset($request->section_id) && count($request->section_id))
            $question->whereIn('topic_id',$request->section_id);

        if(isset($request->sub_section_id) && count($request->sub_section_id))
            $question->whereIn('sub_topic_id',$request->sub_section_id);

         if(isset($request->no_of_question) && !empty($request->no_of_question))
             $question->limit($request->no_of_question);

         $ques=$question->get();
         $testId=$test->id;
    	return view('user.getQuestion',compact('ques','testId'));
    }

    public function testResult(Request $request){
            $result=Test::with('testDetail')->find($request->test_id);
            // return $result->correctQuestionCount();
            $content=view('user.resultAjax',compact('result'))->render();
            return response()->json(['data'=>$content,'status'=>1], 200);

    }
}
