<?php
if (!function_exists('find_row_in_array_by_key')) {
  function find_row_in_array_by_key($data,$key,$val)
  {

    foreach ($data as $row) {

      if($row->{$key}==$val)

        return $row;     
    }

    return false; 

  }
}
if(!function_exists('getMonthName')){
  function getMonthName($month){
    $dateObj= DateTime::createFromFormat('!m', $month);
    $monthName = $dateObj->format('F');
    return $monthName;
  }
}

if(!function_exists('AnserCorrect')){
  function AnserCorrect($answer){
      $index=0;
      foreach($answer as $ans){
          if($ans->is_correct){
            return toNumAlpha($index);
          }
        $index++;
      }
    return null;
  }
}

if (!function_exists('get_month_name_by_no')) {
  function get_month_name_by_no($month)
  {
    $month_name = "";
    switch ($month) {
      case 1:
      $month_name = "Jan";
      break;
      case 2:
      $month_name = "Feb";
      break;   
      case 3:
      $month_name = "Mar";
      break;  
      case 4:
      $month_name = "Apr";
      break; 
      case 5:
      $month_name = "May";
      break; 
      case 6:
      $month_name = "Jun";
      break;
      case 7:
      $month_name = "Jul";
      break; 
      case 8:
      $month_name = "Aug";
      break; 
      case 9:
      $month_name = "Sep";
      break; 
      case 10:
      $month_name = "Oct";
      break; 
      case 11:
      $month_name = "Nov";
      break; 
      case 12:
      $month_name = "Dec";
      break;

      default:
      $month_name = "Jan";
      break;
    }


    return $month_name;

  }

}

if (!function_exists('isActiveRoute')) {

  function isActiveRoute($route, $output = "active")
  {
    if (Route::currentRouteName() == $route) return $output;
  }

}

if (!function_exists('areActiveRoutes')) {

  function areActiveRoutes(Array $routes, $output = "active")
  {
    foreach ($routes as $route)
    {
      if (Route::currentRouteName() == $route) return $output;
    }

  }
}

if(!function_exists('roleCheck')){
  function roleCheck($tempArray,$role){
    foreach($tempArray as $secondArray){
      if($secondArray->role_name==$role){
        return true;
      }
    }
    return false;
  }
}
if(!function_exists('randomPassword')) {
  function randomPassword($length=8)
  {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
      }
    }


    if(!function_exists('get_response')){
      function get_response($code=200,$error=false,$message='',$data=array()){
        $response = array();
        $response['status']= $code;
        $response['error']= $error;
        $response['message']= $message;
        if(!empty($data))
          $response['data']= $data;
        return $response;
      }
    }


    if(!function_exists('getTotalRows')){
      function getTotalRows(){
       $total_rows= \DB::select( \DB::raw("SELECT FOUND_ROWS() AS Totalcount;"));
       return isset($total_rows[0]->Totalcount)?$total_rows[0]->Totalcount:0;
     }
   }

   if(!function_exists('account_update')){
    function account_update($id,$amount){
      $acc= \App\Models\Account::find($id);
      $amnt['c_balance']=$acc->c_balance + $amount;
    // dd($id);
      $master_acc=$acc->update($amnt);
      return $master_acc;
    }
  }
  if(!function_exists('account_minus')){
    function account_minus($id,$amount){
      $acc= \App\Models\Account::find($id);
      $amnt['c_balance']=$acc->c_balance - $amount;
    // dd($id);
      $master_acc=$acc->update($amnt);
      return $master_acc;
    }
  }

  if(!function_exists('toNum')){
    function toNum($data) {
      $numarr=array(
       0=>'A',
       1=>'B',
       2=>'C',
       3=>'D' ,
       4=>'E',
       5=>'F',
       6=>'G',
       7=>'H',
       8=>'I',
       9=>'J');
      $narr = array_flip($numarr);
      $arr = str_split($data);

      $str = '';
      if(isset($data) && !empty($data)){
        foreach($arr as $s)
          $str .= $narr[$s];
      }else{
        $str=null;
      }
      return $str;
    }
  }
  if(!function_exists('toNumAlpha')){
    function toNumAlpha($data) {
      $numarr=array(
       0=>'A',
       1=>'B',
       2=>'C',
       3=>'D' ,
       4=>'E',
       5=>'F',
       6=>'G',
       7=>'H',
       8=>'I',
       9=>'J');

      return $numarr[$data];
    }
  }
  if(! function_exists('bankAccount')){
    function bankAccount($amount, $bankId){
      $bank=\App\Models\Bank::where('BankId',$bankId)->first();
      $bank->balance+=$amount;
      if($bank->save())
        return true;
      else
        return false;

    }
  }

  if(! function_exists('chapterString')){
    function chapterString($topic){
      $tempArray=array();
      foreach($topic as $chp){
        array_push($tempArray, $chp->name);
      }
      return implode(', ', $tempArray);
    }
  }
  if(! function_exists('topicString')){
    function topicString($topic){
      $tempArray=array();
      foreach($topic as $chp){
        array_push($tempArray, $chp->sub_topic_name);
      }
      return implode(', ', $tempArray);
    }
  }
  if(! function_exists('subjectString')){
    function subjectString($topic){
      $tempArray=array();
      foreach($topic as $chp){
      // dd($chp);
        array_push($tempArray, $chp->sb_name);
      }
      return implode(', ', $tempArray);
    }
  }

  if(! function_exists('sessionValueFlush')){
    function sessionValueFlush(){
      Session::put('userCorrectionAns',0);
      Session::put('userWrongAns',0);
      Session::put('userUnanswer',0);

    }
  }

  if(! function_exists('sessionValueUpdate')){
    function sessionValueUpdate($data){
      if($data==1){
        if (Session::has('userCorrectionAns')){
          $correct=Session::get('userCorrectionAns');
          Session::put('userCorrectionAns',$correct+1);
        }else{
          Session::put('userCorrectionAns',1);
        }
      }elseif ($data==0) {

        if (Session::has('userWrongAns')){
          $userWrongAns=Session::get('userWrongAns');
          Session::put('userWrongAns',$userWrongAns+1);
        }else{
          Session::put('userWrongAns',1);
        }
        
      }else{

        if (Session::has('userUnanswer')){
          $userUnanswer=Session::get('userUnanswer');
          Session::put('userUnanswer',$userUnanswer+1);
        }else{
          Session::put('userUnanswer',1);
        }
        
      }

    }
  }

function ReferenceDecrypt($string) {
   return Crypt::decryptString($string);
}

function ReferenceEncrypt($string) {
  return  Crypt::encryptString($string);
  
}
  ?>