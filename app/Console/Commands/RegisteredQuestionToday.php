<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use Illuminate\Support\Facades\Log;

class RegisteredQuestionToday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registered:question_todays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Question Of day registered for month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model=\App\Models\Question::with('answers')->orderBy(DB::raw('RAND()'))->limit(30)->where('status',1)->get();
        $today=\App\Models\QuestionToday::orderBy('id','DESC')->first();
        $temp=array();
      for($i=1; $i<count($model); $i++) {
          $effectiveDate = strtotime("+$i day", strtotime($today->questionDate));
          $correct=(isset($model[$i]->answers[0]->is_correct) && $model[$i]->answers[0]->is_correct==1)?'ans_A':(isset($model[$i]->answers[1]->is_correct) && $model[$i]->answers[1]->is_correct==1)?'ans_B':(isset($model[$i]->answers[2]->is_correct) && $model[$i]->answers[2]->is_correct==1)?'ans_C':(isset($model[$i]->answers[3]->is_correct) && $model[$i]->answers[3]->is_correct==1)?'ans_D':'';
          
          $time = date("Y-m-d", $effectiveDate);
          $tmp_dest = array();

          $tmp_dest['question_id']=$model[$i]->id;
          $tmp_dest['question']=$model[$i]->question;


          $tmp_dest['ans_A']=isset($model[$i]->answers[0]->ans)?($model[$i]->answers[0]->ans):'';
          $tmp_dest['ans_B']=isset($model[$i]->answers[1]->ans)?($model[$i]->answers[1]->ans):'';
          $tmp_dest['ans_C']=isset($model[$i]->answers[2]->ans)?($model[$i]->answers[2]->ans):'';
          $tmp_dest['ans_D']=isset($model[$i]->answers[3]->ans)?($model[$i]->answers[3]->ans):'';
          $tmp_dest['is_correct']=$correct;
          $tmp_dest['questionDate']=$time;
          $temp[] = $tmp_dest;
        }
        Log::info('Cron Job working: '.json_encode($temp));
        \App\Models\QuestionToday::insert($temp);
    }
}
